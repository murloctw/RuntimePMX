﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimInternal.Bullet.WorldProperty
// Assembly: MMD4Mecanim, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: ADBCBBA1-CEE8-40F4-A775-E57AB2D2693F
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Scripts\MMD4Mecanim.dll

using System;
using UnityEngine;

namespace MMD4MecanimInternal.Bullet
{
    [Serializable]
    public class WorldProperty
    {
        public bool accurateStep = true;
        public int framePerSecond;
        public int resetFrameRate;
        public int limitDeltaFrames;
        public float axisSweepDistance;
        public float gravityScale = 10f;
        public float gravityNoise;
        public Vector3 gravityDirection = new Vector3(0.0f, -1f, 0.0f);
        public float vertexScale = 8f;
        public float importScale = 0.01f;
        public int worldSolverInfoNumIterations;
        public bool worldSolverInfoSplitImpulse = true;
        public bool worldAddFloorPlane;
        public bool optimizeSettings = true;
        public bool multiThreading = true;
        public bool multiThreadingSynchronize = true;
        public bool parallelDispatcher;
        public bool parallelSolver;

        public float worldScale => this.vertexScale * this.importScale;
    }
}