﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimModelImpl
// Assembly: MMD4Mecanim, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: ADBCBBA1-CEE8-40F4-A775-E57AB2D2693F
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Scripts\MMD4Mecanim.dll

using MMD4MecanimInternal.Bullet;
using System;
using System.Text;
using UnityEngine;
using UnityEngine.Rendering;

[ExecuteInEditMode]
public class MMD4MecanimModelImpl : MonoBehaviour, MMD4MecanimAnim.IAnimModel
{
  private Animator _animator;
  private MMD4MecanimModelImpl.Anim _playingAnim;
  private float _prevDeltaTime;
  private float[] _animMorphCategoryWeights;
  private int _delayedAwakeFrame;
  private bool _isDelayedAwake;
  private MMD4MecanimBoneImpl _leftShoulderBone;
  private MMD4MecanimBoneImpl _rightShoulderBone;
  private MMD4MecanimBoneImpl _leftArmBone;
  private MMD4MecanimBoneImpl _rightArmBone;
  [NonSerialized]
  public MMD4MecanimIKTargetImpl[] fullBodyIKTargetList;
  [NonSerialized]
  public Transform fullBodyIKTargets;
  [NonSerialized]
  public Transform[] fullBodyIKGroupList;
  public const float NEXTEdgeScale = 0.05f;
  public Material nextEdgeMaterial_Pass4;
  public Material nextEdgeMaterial_Pass8;
  public MMD4MecanimModelImpl.NEXTEdgePass nextEdgePass;
  public float nextEdgeSize = 1f;
  public Color nextEdgeColor = new Color(0.4f, 1f, 1f, 1f);
  private MeshRenderer[] _nextEdgeMeshRenderers;
  private SkinnedMeshRenderer[] _nextEdgeSkinnedMeshRenderers;
  private bool _nextEdgeVisibleCached;
  private float _nextEdgeSizeCached;
  private Color _nextEdgeColorCached;
  public bool supportNEXTEdge;
  private bool _notFoundShaderNEXTPass4;
  private bool _notFoundShaderNEXTPass8;
  public const int MaterialEdgeRenderQueue = 2001;
  public const int MaterialBaseRenderQueue = 2002;
  public const int MaterialEdgeRenderQueue_AfterSkybox = 2501;
  public const int MaterialBaseRenderQueue_AfterSkybox = 2502;
  public float importScale = 0.01f;
  public bool initializeOnAwake;
  public bool postfixRenderQueue = true;
  public bool renderQueueAfterSkybox;
  public bool updateWhenOffscreen = true;
  public bool animEnabled = true;
  public bool animSyncToAudio = true;
  public TextAsset modelFile;
  public TextAsset indexFile;
  public TextAsset vertexFile;
  public AudioSource audioSource;
  public int animDelayedAwakeFrame = 1;
  public bool boneInherenceEnabled;
  public bool boneMorphEnabled;
  public bool pphEnabled = true;
  public bool pphEnabledNoAnimation = true;
  public bool pphShoulderEnabled = true;
  public float pphShoulderFixRate = 0.7f;
  public bool ikEnabled;
  public bool fullBodyIKEnabled;
  public bool vertexMorphEnabled = true;
  public bool materialMorphEnabled = true;
  public bool blendShapesEnabled = true;
  public bool xdefEnabled;
  public bool xdefNormalEnabled;
  public bool xdefMobileEnabled;
  private bool _blendShapesEnabledCache;
  private float _pphShoulderFixRateImmediately;
  public float generatedColliderMargin = 0.01f;
  [NonSerialized]
  public MMD4MecanimBoneImpl[] boneList;
  [NonSerialized]
  public MMD4MecanimModelImpl.IK[] ikList;
  [NonSerialized]
  public MMD4MecanimModelImpl.Morph[] morphList;
  [NonSerialized]
  public MMD4MecanimModelImpl.MorphAutoLuminous morphAutoLuminous;
  public bool tessEnabled = true;
  public float tessEdgeLengthRate = 1f;
  public float tessPhongStrengthRate = 1f;
  public float tessExtrusionAmountRate = 1f;
  public MMD4MecanimModelImpl.PhysicsEngine physicsEngine;
  public MMD4MecanimModelImpl.BulletPhysics bulletPhysics;
  public MMD4MecanimModelImpl.RigidBody[] rigidBodyList;
  public MMD4MecanimModelImpl.Anim[] animList;
  private bool _initialized;
  private MMD4MecanimBoneImpl _rootBone;
  private MMD4MecanimBoneImpl[] _sortedBoneList;
  private MeshRenderer[] _meshRenderers;
  private SkinnedMeshRenderer[] _skinnedMeshRenderers;
  private MMD4MecanimModelImpl.CloneMesh[] _cloneMeshes;
  private MMD4MecanimModelImpl.MorphBlendShape[] _morphBlendShapes;
  private MMD4MecanimModelImpl.CloneMaterial[] _cloneMaterials;
  public MMD4MecanimData.ModelData _modelData;
  private bool _cached_tessEnabled = true;
  private float _cached_tessEdgeLengthRate = 1f;
  private float _cached_tessPhongStrengthRate = 1f;
  private float _cached_tessExtrusionAmountRate = 1f;
  [HideInInspector]
  public MMD4MecanimModelImpl.EditorViewPage editorViewPage;
  [HideInInspector]
  public byte editorViewMorphBits = 15;
  [HideInInspector]
  public bool editorViewRigidBodies;
  [HideInInspector]
  public MMD4MecanimModelImpl.EditorViewMorphNameType editorViewMorphNameType;
  [NonSerialized]
  public Mesh defaultMesh;
  private MMD4MecanimBulletPhysicsImpl.MMDModel _bulletPhysicsMMDModel;
  public Action onUpdating;
  public Action onUpdated;
  public Action onLateUpdating;
  public Action onLateUpdated;
  internal bool _invokeed;
  private static bool _invokeEliminate;

  MMD4MecanimAnim.IMorph MMD4MecanimAnim.IAnimModel.GetMorph(
    string name)
  {
    return (MMD4MecanimAnim.IMorph) this.GetMorph(name);
  }

  MMD4MecanimAnim.IMorph MMD4MecanimAnim.IAnimModel.GetMorph(
    string name,
    bool startsWith)
  {
    return (MMD4MecanimAnim.IMorph) this.GetMorph(name, startsWith);
  }

  int MMD4MecanimAnim.IAnimModel.morphCount => this.morphList != null ? this.morphList.Length : 0;

  MMD4MecanimAnim.IMorph MMD4MecanimAnim.IAnimModel.GetMorphAt(
    int index)
  {
    return this.morphList != null && index >= 0 && index < this.morphList.Length ? (MMD4MecanimAnim.IMorph) this.morphList[index] : (MMD4MecanimAnim.IMorph) null;
  }

  int MMD4MecanimAnim.IAnimModel.animCount => this.animList != null ? this.animList.Length : 0;

  MMD4MecanimAnim.IAnim MMD4MecanimAnim.IAnimModel.GetAnimAt(int index) => this.animList != null && index >= 0 && index < this.animList.Length ? (MMD4MecanimAnim.IAnim) this.animList[index] : (MMD4MecanimAnim.IAnim) null;

  Animator MMD4MecanimAnim.IAnimModel.animator => this._animator;

  AudioSource MMD4MecanimAnim.IAnimModel.audioSource
  {
    get
    {
      if ((UnityEngine.Object) this.audioSource == (UnityEngine.Object) null)
        this.audioSource = MMD4MecanimCommon.WeakAddComponent<AudioSource>(this.gameObject);
      return this.audioSource;
    }
  }

  bool MMD4MecanimAnim.IAnimModel.animSyncToAudio => this.animSyncToAudio;

  float MMD4MecanimAnim.IAnimModel.prevDeltaTime
  {
    get => this._prevDeltaTime;
    set => this._prevDeltaTime = value;
  }

  MMD4MecanimAnim.IAnim MMD4MecanimAnim.IAnimModel.playingAnim
  {
    get => (MMD4MecanimAnim.IAnim) this._playingAnim;
    set => this._playingAnim = (MMD4MecanimModelImpl.Anim) value;
  }

  void MMD4MecanimAnim.IAnimModel._SetAnimMorphWeight(
    MMD4MecanimAnim.IMorph morph,
    float weight)
  {
    ((MMD4MecanimModelImpl.Morph) morph)._animWeight = weight;
  }

  private void _InitializeAnimatoion()
  {
    this._animator = this.GetComponent<Animator>();
    if (Application.isPlaying && this.animEnabled && this.animDelayedAwakeFrame > 0 && (UnityEngine.Object) this._animator != (UnityEngine.Object) null && this._animator.enabled)
    {
      this._animator.speed = 0.0f;
      this._isDelayedAwake = true;
    }
    this._animMorphCategoryWeights = new float[5];
    if (!Application.isPlaying)
      return;
    MMD4MecanimAnim.InitializeAnimModel((MMD4MecanimAnim.IAnimModel) this);
  }

  private void _UpdateAnim()
  {
    if (!Application.isPlaying || this._bulletPhysicsMMDModel == null)
      return;
    if (this._isDelayedAwake)
    {
      if (this.animEnabled && this.animDelayedAwakeFrame > 0 && ++this._delayedAwakeFrame <= this.animDelayedAwakeFrame)
        return;
      this._isDelayedAwake = false;
      this._animator.speed = 1f;
    }
    MMD4MecanimAnim.PreUpdateAnimModel((MMD4MecanimAnim.IAnimModel) this);
    if (!this.animEnabled)
      MMD4MecanimAnim.StopAnimModel((MMD4MecanimAnim.IAnimModel) this);
    else
      MMD4MecanimAnim.UpdateAnimModel((MMD4MecanimAnim.IAnimModel) this);
    MMD4MecanimAnim.PostUpdateAnimModel((MMD4MecanimAnim.IAnimModel) this);
  }

  private void _BindBone()
  {
    foreach (Transform trn in this.gameObject.transform)
      this._BindBone(trn);
  }

  private void _BindBone(Transform trn)
  {
    if (!string.IsNullOrEmpty(trn.gameObject.name))
    {
      int index = 0;
      if (this._modelData.boneDataDictionary.TryGetValue(trn.gameObject.name, out index))
      {
        MMD4MecanimBoneImpl d4MecanimBoneImpl = trn.gameObject.GetComponent<MMD4MecanimBoneImpl>();
        if ((UnityEngine.Object) d4MecanimBoneImpl == (UnityEngine.Object) null)
          d4MecanimBoneImpl = this.AddComponentBoneImpl(trn.gameObject);
        d4MecanimBoneImpl.model = this;
        d4MecanimBoneImpl.boneID = index;
        d4MecanimBoneImpl.Setup();
        this.boneList[index] = d4MecanimBoneImpl;
        if (this.boneList[index].boneData != null && this.boneList[index].boneData.isRootBone)
          this._rootBone = this.boneList[index];
      }
    }
    foreach (Transform trn1 in trn)
      this._BindBone(trn1);
  }

  private void _UpdateBone()
  {
  }

  private void _LateUpdateBone() => this._UpdatePPHBones();

  private void _InitializePPHBones()
  {
    if ((UnityEngine.Object) this._animator == (UnityEngine.Object) null || (UnityEngine.Object) this._animator.avatar == (UnityEngine.Object) null || !this._animator.avatar.isValid || !this._animator.avatar.isHuman)
      return;
    Transform boneTransform1 = this._animator.GetBoneTransform(HumanBodyBones.LeftShoulder);
    Transform boneTransform2 = this._animator.GetBoneTransform(HumanBodyBones.LeftUpperArm);
    if ((UnityEngine.Object) boneTransform1 != (UnityEngine.Object) null && (UnityEngine.Object) boneTransform2 != (UnityEngine.Object) null)
    {
      this._leftShoulderBone = boneTransform1.gameObject.GetComponent<MMD4MecanimBoneImpl>();
      this._leftArmBone = boneTransform2.gameObject.GetComponent<MMD4MecanimBoneImpl>();
      if ((UnityEngine.Object) this._leftShoulderBone != (UnityEngine.Object) null)
        this._leftShoulderBone.humanBodyBones = 11;
      if ((UnityEngine.Object) this._leftArmBone != (UnityEngine.Object) null)
        this._leftArmBone.humanBodyBones = 13;
    }
    Transform boneTransform3 = this._animator.GetBoneTransform(HumanBodyBones.RightShoulder);
    Transform boneTransform4 = this._animator.GetBoneTransform(HumanBodyBones.RightUpperArm);
    if (!((UnityEngine.Object) boneTransform3 != (UnityEngine.Object) null) || !((UnityEngine.Object) boneTransform4 != (UnityEngine.Object) null))
      return;
    this._rightShoulderBone = boneTransform3.gameObject.GetComponent<MMD4MecanimBoneImpl>();
    this._rightArmBone = boneTransform4.gameObject.GetComponent<MMD4MecanimBoneImpl>();
    if ((UnityEngine.Object) this._rightShoulderBone != (UnityEngine.Object) null)
      this._rightShoulderBone.humanBodyBones = 12;
    if (!((UnityEngine.Object) this._rightArmBone != (UnityEngine.Object) null))
      return;
    this._rightArmBone.humanBodyBones = 14;
  }

  public void ForceUpdatePPHBones() => this._UpdatePPHBones();

  private static bool _IsPlayingAnimation(Animator animator, out float pphRate)
  {
    AnimatorClipInfo[] animatorClipInfo1 = animator.GetCurrentAnimatorClipInfo(0);
    if (animatorClipInfo1 != null && animatorClipInfo1.Length != 0)
    {
      pphRate = 0.0f;
      for (int index = 0; index != animatorClipInfo1.Length; ++index)
      {
        AnimatorClipInfo animatorClipInfo2 = animatorClipInfo1[index];
        if ((UnityEngine.Object) animatorClipInfo2.clip != (UnityEngine.Object) null)
        {
          string name = animatorClipInfo2.clip.name;
          if (name != null && name.EndsWith(".vmd") && !name.EndsWith("_vmd"))
            pphRate += animatorClipInfo2.weight;
        }
      }
      return true;
    }
    pphRate = 1f;
    return false;
  }

  private void _UpdatePPHBones()
  {
    this._pphShoulderFixRateImmediately = 0.0f;
    if (!this.pphEnabled || !(!MMD4MecanimCommon.animatorInitializedEnabled ? (UnityEngine.Object) this._animator != (UnityEngine.Object) null && (UnityEngine.Object) this._animator.runtimeAnimatorController != (UnityEngine.Object) null && this._animator.layerCount > 0 : (UnityEngine.Object) this._animator != (UnityEngine.Object) null && MMD4MecanimCommon._IsAnimatorInitialized(this._animator) && (UnityEngine.Object) this._animator.runtimeAnimatorController != (UnityEngine.Object) null && this._animator.layerCount > 0))
      return;
    float pphRate;
    bool flag = !MMD4MecanimModelImpl._IsPlayingAnimation(this._animator, out pphRate);
    if (flag && !this.pphEnabledNoAnimation)
      return;
    if (flag)
      pphRate = 1f;
    else if ((double) pphRate <= (double) Mathf.Epsilon)
      return;
    this._pphShoulderFixRateImmediately = this.pphShoulderFixRate * pphRate;
  }

  private void _InitializeDeferredMaterial()
  {
  }

  private void _DestroyDeferredMaterial()
  {
  }

  private void _InitializeDeferredMesh()
  {
  }

  private static GameObject _CreateDeferrecClearGameObject(GameObject parentGameObject) => (GameObject) null;

  private Material[] _CloneDeferredClearMaterials(Material[] materials) => (Material[]) null;

  private void _UpdatedDeffered()
  {
  }

  private void _SetDeferredShaderSettings(Light directionalLight)
  {
  }

  private void _InitializeIK() => this._CheckTransformIK();

  private void _CheckTransformIK()
  {
    if (!Application.isPlaying || !this.fullBodyIKEnabled)
      return;
    if (this.fullBodyIKTargetList != null)
    {
      if (this.fullBodyIKTargetList.Length != 36)
      {
        this.fullBodyIKTargetList = (MMD4MecanimIKTargetImpl[]) null;
      }
      else
      {
        for (int index = 0; index != this.fullBodyIKTargetList.Length; ++index)
        {
          if ((UnityEngine.Object) this.fullBodyIKTargetList[index] == (UnityEngine.Object) null)
          {
            this.fullBodyIKTargetList = (MMD4MecanimIKTargetImpl[]) null;
            break;
          }
        }
      }
    }
    if (this.fullBodyIKTargetList != null || this._bulletPhysicsMMDModel == null)
      return;
    Vector3[] bodyIkPositionList = this._bulletPhysicsMMDModel.fullBodyIKPositionList;
    Quaternion[] bodyIkRotationList = this._bulletPhysicsMMDModel.fullBodyIKRotationList;
    if (bodyIkPositionList == null || bodyIkRotationList == null || bodyIkPositionList.Length != 36 || bodyIkRotationList.Length != 36)
      return;
    if ((UnityEngine.Object) this.fullBodyIKTargets == (UnityEngine.Object) null)
    {
      this.fullBodyIKTargets = new GameObject("FullBodyIKTargets").transform;
      this.fullBodyIKTargets.parent = this.transform;
      this.fullBodyIKTargets.localPosition = Vector3.zero;
      this.fullBodyIKTargets.localRotation = Quaternion.identity;
      this.fullBodyIKTargets.localScale = Vector3.one;
    }
    if (this.fullBodyIKGroupList == null || this.fullBodyIKGroupList.Length != 8)
    {
      this.fullBodyIKGroupList = new Transform[8];
      for (int index = 0; index != 8; ++index)
      {
        GameObject gameObject = new GameObject(((MMD4MecanimData.FullBodyIKGroup) index).ToString());
        this.fullBodyIKGroupList[index] = gameObject.transform;
        this.fullBodyIKGroupList[index].parent = this.fullBodyIKTargets;
        this.fullBodyIKGroupList[index].localPosition = Vector3.zero;
        this.fullBodyIKGroupList[index].localRotation = Quaternion.identity;
        this.fullBodyIKGroupList[index].localScale = Vector3.one;
      }
    }
    MMD4MecanimBoneImpl rootBoneImpl = this.GetRootBoneImpl();
    Transform transform1 = (Transform) null;
    if ((UnityEngine.Object) rootBoneImpl != (UnityEngine.Object) null)
      transform1 = rootBoneImpl.transform;
    this.fullBodyIKTargetList = new MMD4MecanimIKTargetImpl[36];
    for (int fullBodyIKTargetIndex = 0; fullBodyIKTargetIndex != 36; ++fullBodyIKTargetIndex)
    {
      MMD4MecanimData.FullBodyIKGroup fullBodyIKGroup;
      string bodyIkTargetName = MMD4MecanimData.GetFullBodyIKTargetName(out fullBodyIKGroup, fullBodyIKTargetIndex);
      if (fullBodyIKGroup != MMD4MecanimData.FullBodyIKGroup.Unknown)
      {
        GameObject gameObject = new GameObject(bodyIkTargetName);
        Transform transform2 = gameObject.transform;
        Vector3 position = bodyIkPositionList[fullBodyIKTargetIndex];
        Quaternion quaternion = bodyIkRotationList[fullBodyIKTargetIndex];
        if ((UnityEngine.Object) transform1 != (UnityEngine.Object) null)
        {
          position = transform1.TransformPoint(position);
          quaternion = transform1.rotation * quaternion;
        }
        transform2.parent = this.fullBodyIKGroupList[(int) fullBodyIKGroup];
        transform2.position = position;
        transform2.rotation = quaternion;
        transform2.localScale = Vector3.one;
        this.fullBodyIKTargetList[fullBodyIKTargetIndex] = this.AddComponentIKTargetImpl(gameObject);
        this.fullBodyIKTargetList[fullBodyIKTargetIndex].model = this;
      }
    }
  }

  private void _UpdateIK() => this._CheckTransformIK();

  private void _LateUpdateIK()
  {
    if (!Application.isPlaying || this._bulletPhysicsMMDModel == null)
      return;
    int[] fullBodyIkFlagsList = this._bulletPhysicsMMDModel.updateFullBodyIKFlagsList;
    float[] bodyIkTransformList = this._bulletPhysicsMMDModel.updateFullBodyIKTransformList;
    float[] bodyIkWeightList = this._bulletPhysicsMMDModel.updateFullBodyIKWeightList;
    if (fullBodyIkFlagsList == null || bodyIkTransformList == null || bodyIkWeightList == null || fullBodyIkFlagsList.Length != 36 || bodyIkTransformList.Length != 144 || bodyIkWeightList.Length != 36 || this.fullBodyIKTargetList == null || this.fullBodyIKTargetList.Length != 36)
      return;
    int index1 = 0;
    int index2 = 0;
    while (index1 != 36)
    {
      MMD4MecanimIKTargetImpl fullBodyIkTarget = this.fullBodyIKTargetList[index1];
      if ((UnityEngine.Object) fullBodyIkTarget != (UnityEngine.Object) null)
      {
        Vector3 position = fullBodyIkTarget.transform.position;
        bodyIkTransformList[index2] = position.x;
        bodyIkTransformList[index2 + 1] = position.y;
        bodyIkTransformList[index2 + 2] = position.z;
        bodyIkTransformList[index2 + 3] = 1f;
        bodyIkWeightList[index1] = fullBodyIkTarget.ikWeight;
      }
      ++index1;
      index2 += 4;
    }
  }

  private void _UpdateAmbientPreview()
  {
  }

  private void _UpdateAmbientPreviewInternal(Material[] materials)
  {
  }

  private void _InitializeGlobalAmbient()
  {
  }

  private void _UpdateGlobalAmbient()
  {
  }

  private void _CleanupGlobalAmbient()
  {
  }

  private void _UpdateAutoLuminous()
  {
    if (this.morphAutoLuminous == null || !this.morphAutoLuminous.updated || this._cloneMaterials == null)
      return;
    for (int index1 = 0; index1 != this._cloneMaterials.Length; ++index1)
    {
      MMD4MecanimModelImpl.CloneMaterial cloneMaterial = this._cloneMaterials[index1];
      if (cloneMaterial.updateMaterialData != null && cloneMaterial.materialData != null && cloneMaterial.materials != null)
      {
        for (int index2 = 0; index2 < cloneMaterial.updateMaterialData.Length; ++index2)
        {
          if (!cloneMaterial.updateMaterialData[index2] && (double) cloneMaterial.materialData[index2].shininess > 100.0)
            cloneMaterial.updateMaterialData[index2] = true;
        }
      }
    }
  }

  private void _UpdateTessellation()
  {
    if (this._cached_tessEnabled != this.tessEnabled)
    {
      int length1 = this._cloneMaterials.Length;
      for (int index1 = 0; index1 != length1; ++index1)
      {
        MMD4MecanimModelImpl.CloneMaterial cloneMaterial = this._cloneMaterials[index1];
        if (cloneMaterial.materials != null)
        {
          int length2 = cloneMaterial.materials.Length;
          for (int index2 = 0; index2 < length2; ++index2)
            MMD4MecanimCommon.FeedbackMaterial_Tessellation_Enabled(cloneMaterial.materials[index2], this.tessEnabled);
        }
      }
      this._cached_tessEnabled = this.tessEnabled;
    }
    bool flag1 = (double) this._cached_tessEdgeLengthRate != (double) this.tessEdgeLengthRate;
    bool flag2 = (double) this._cached_tessPhongStrengthRate != (double) this.tessPhongStrengthRate;
    bool flag3 = (double) this._cached_tessExtrusionAmountRate != (double) this.tessExtrusionAmountRate;
    if (!(flag1 | flag2 | flag3) || this._cloneMaterials == null)
      return;
    float t1 = Mathf.Clamp01(this.tessEdgeLengthRate);
    float t2 = Mathf.Clamp01(this.tessPhongStrengthRate);
    float t3 = Mathf.Clamp01(this.tessExtrusionAmountRate);
    int length3 = this._cloneMaterials.Length;
    for (int index3 = 0; index3 != length3; ++index3)
    {
      MMD4MecanimModelImpl.CloneMaterial cloneMaterial = this._cloneMaterials[index3];
      if (cloneMaterial.backupMaterialData != null && cloneMaterial.materialData != null && cloneMaterial.materials != null)
      {
        int length4 = cloneMaterial.materialData.Length;
        for (int index4 = 0; index4 < length4; ++index4)
        {
          if (flag1)
            cloneMaterial.materialData[index4].tessEdgeLength = Mathf.Lerp(50f, cloneMaterial.backupMaterialData[index4].tessEdgeLength, t1);
          if (flag2)
            cloneMaterial.materialData[index4].tessPhongStrength = Mathf.Lerp(0.0f, cloneMaterial.backupMaterialData[index4].tessPhongStrength, t2);
          if (flag3)
            cloneMaterial.materialData[index4].tessExtrusionAmount = Mathf.Lerp(0.0f, cloneMaterial.backupMaterialData[index4].tessExtrusionAmount, t3);
          MMD4MecanimCommon.FeedbackMaterial_Tessellation(ref cloneMaterial.materialData[index4], cloneMaterial.materials[index4]);
        }
      }
    }
    this._cached_tessEdgeLengthRate = this.tessEdgeLengthRate;
    this._cached_tessPhongStrengthRate = this.tessPhongStrengthRate;
    this._cached_tessExtrusionAmountRate = this.tessExtrusionAmountRate;
  }

  private void _CleanupAutoLuminous()
  {
    if (this.morphAutoLuminous == null)
      return;
    this.morphAutoLuminous.updated = false;
  }

  public int _skinnedMeshCount => this._skinnedMeshRenderers != null ? this._skinnedMeshRenderers.Length : 0;

  public int _cloneMeshCount => this._cloneMeshes != null ? this._cloneMeshes.Length : 0;

  public void _UploadMeshVertex(int meshID, Vector3[] vertices, Vector3[] normals)
  {
    if (this._cloneMeshes == null || meshID < 0 || meshID >= this._cloneMeshes.Length)
      return;
    MMD4MecanimModelImpl.CloneMesh cloneMesh = this._cloneMeshes[meshID];
    if (cloneMesh == null)
      return;
    if (vertices != null)
      cloneMesh.mesh.vertices = vertices;
    if (normals == null)
      return;
    cloneMesh.mesh.normals = normals;
  }

  private void _UploadMeshMaterial()
  {
    if (!Application.isPlaying)
      return;
    this._UpdateGlobalAmbient();
    this._UpdateAutoLuminous();
    this._UpdateTessellation();
    if (this._cloneMaterials != null)
    {
      for (int index1 = 0; index1 != this._cloneMaterials.Length; ++index1)
      {
        MMD4MecanimModelImpl.CloneMaterial cloneMaterial = this._cloneMaterials[index1];
        if (cloneMaterial.updateMaterialData != null && cloneMaterial.materialData != null && cloneMaterial.materials != null)
        {
          for (int index2 = 0; index2 < cloneMaterial.updateMaterialData.Length; ++index2)
          {
            if (cloneMaterial.updateMaterialData[index2])
            {
              cloneMaterial.updateMaterialData[index2] = false;
              MMD4MecanimCommon.FeedbackMaterial(ref cloneMaterial.materialData[index2], cloneMaterial.materials[index2], this.morphAutoLuminous);
            }
          }
        }
      }
    }
    this._CleanupGlobalAmbient();
    this._CleanupAutoLuminous();
  }

  public int[] _PrepareMeshFlags()
  {
    if (this._skinnedMeshRenderers != null)
    {
      int length = this._skinnedMeshRenderers.Length;
      if (length > 0)
        return new int[length];
    }
    return (int[]) null;
  }

  public int[] _PrepareMeshFlags(out bool blendShapesAnything)
  {
    blendShapesAnything = false;
    if (this._skinnedMeshRenderers != null)
    {
      int length = this._skinnedMeshRenderers.Length;
      if (length > 0)
      {
        int[] numArray = new int[length];
        if (this.vertexMorphEnabled && this.blendShapesEnabled)
        {
          for (int index = 0; index != length; ++index)
          {
            SkinnedMeshRenderer skinnedMeshRenderer = this._skinnedMeshRenderers[index];
            if ((UnityEngine.Object) skinnedMeshRenderer != (UnityEngine.Object) null && (UnityEngine.Object) skinnedMeshRenderer.sharedMesh != (UnityEngine.Object) null && skinnedMeshRenderer.sharedMesh.blendShapeCount != 0)
            {
              numArray[index] |= 1;
              blendShapesAnything = true;
            }
          }
        }
        return numArray;
      }
    }
    return (int[]) null;
  }

  public void _InitializeCloneMesh(int[] meshFlags)
  {
    int index1 = 0;
    int length = 0;
    if (this._meshRenderers != null)
      length += this._meshRenderers.Length;
    if (this._skinnedMeshRenderers != null)
      length += this._skinnedMeshRenderers.Length;
    if (length > 0)
      this._cloneMaterials = new MMD4MecanimModelImpl.CloneMaterial[length];
    if (this._meshRenderers != null)
    {
      for (int index2 = 0; index2 != this._meshRenderers.Length; ++index2)
      {
        MeshRenderer meshRenderer = this._meshRenderers[index2];
        Material[] materials = MMD4MecanimModelImpl._GetMaterials((Renderer) meshRenderer);
        MMD4MecanimModelImpl._PostfixRenderQueue(materials, this.postfixRenderQueue, this.renderQueueAfterSkybox);
        this._cloneMaterials[index1] = new MMD4MecanimModelImpl.CloneMaterial();
        MMD4MecanimModelImpl._SetupCloneMaterial(this._cloneMaterials[index1], materials);
        MMD4MecanimModelImpl._PrefixShadowCasting((Renderer) meshRenderer, materials);
        ++index1;
      }
    }
    bool flag = false;
    if (meshFlags != null && this._skinnedMeshRenderers != null && meshFlags.Length == this._skinnedMeshRenderers.Length && Application.isPlaying)
    {
      for (int index3 = 0; index3 != meshFlags.Length && !flag; ++index3)
        flag = (uint) (meshFlags[index3] & 6) > 0U;
    }
    if (this._skinnedMeshRenderers != null)
    {
      if (flag)
        this._cloneMeshes = new MMD4MecanimModelImpl.CloneMesh[this._skinnedMeshRenderers.Length];
      for (int index4 = 0; index4 != this._skinnedMeshRenderers.Length; ++index4)
      {
        SkinnedMeshRenderer skinnedMeshRenderer = this._skinnedMeshRenderers[index4];
        if ((meshFlags[index4] & 6) != 0)
        {
          this._cloneMeshes[index4] = new MMD4MecanimModelImpl.CloneMesh();
          MMD4MecanimCommon.CloneMeshWork cloneMeshWork = MMD4MecanimCommon.CloneMesh(skinnedMeshRenderer.sharedMesh);
          if (cloneMeshWork != null && (UnityEngine.Object) cloneMeshWork.mesh != (UnityEngine.Object) null)
          {
            this._cloneMeshes[index4].skinnedMeshRenderer = skinnedMeshRenderer;
            this._cloneMeshes[index4].mesh = cloneMeshWork.mesh;
            if ((UnityEngine.Object) this._cloneMeshes[index4].mesh != (UnityEngine.Object) null)
              this._cloneMeshes[index4].mesh.MarkDynamic();
            this._cloneMeshes[index4].vertices = cloneMeshWork.vertices;
            if ((meshFlags[index4] & 4) != 0)
            {
              if (this.xdefNormalEnabled)
                this._cloneMeshes[index4].normals = cloneMeshWork.normals;
              this._cloneMeshes[index4].bindposes = cloneMeshWork.bindposes;
              this._cloneMeshes[index4].boneWeights = cloneMeshWork.boneWeights;
            }
            skinnedMeshRenderer.sharedMesh = this._cloneMeshes[index4].mesh;
          }
          else
            Debug.LogError((object) ("CloneMesh() Failed. : " + this.gameObject.name));
        }
        Material[] materials = MMD4MecanimModelImpl._GetMaterials((Renderer) skinnedMeshRenderer);
        MMD4MecanimModelImpl._PostfixRenderQueue(materials, this.postfixRenderQueue, this.renderQueueAfterSkybox);
        this._cloneMaterials[index1] = new MMD4MecanimModelImpl.CloneMaterial();
        MMD4MecanimModelImpl._SetupCloneMaterial(this._cloneMaterials[index1], materials);
        MMD4MecanimModelImpl._PrefixShadowCasting((Renderer) skinnedMeshRenderer, materials);
        ++index1;
      }
    }
    MMD4MecanimModelImpl.CloneMaterial[] cloneMaterials = this._cloneMaterials;
  }

  public MMD4MecanimModelImpl.CloneMesh _GetCloneMesh(int meshIndex) => this._cloneMeshes != null && meshIndex >= 0 && meshIndex < this._cloneMeshes.Length ? this._cloneMeshes[meshIndex] : (MMD4MecanimModelImpl.CloneMesh) null;

  public void _CleanupCloneMesh()
  {
    if (this._cloneMeshes == null)
      return;
    for (int index = 0; index != this._cloneMeshes.Length; ++index)
    {
      if (this._cloneMeshes[index] != null)
      {
        if (!this.xdefNormalEnabled)
          this._cloneMeshes[index].normals = (Vector3[]) null;
        this._cloneMeshes[index].bindposes = (Matrix4x4[]) null;
        this._cloneMeshes[index].boneWeights = (BoneWeight[]) null;
      }
    }
  }

  public MMD4MecanimModelImpl.Morph GetMorph(string morphName) => this.GetMorph(morphName, false);

  public MMD4MecanimModelImpl.Morph GetMorph(
    string morphName,
    bool isStartsWith)
  {
    if (this.modelData != null)
    {
      int morphDataIndexJp = this.modelData.GetMorphDataIndexJp(morphName, isStartsWith);
      if (morphDataIndexJp != -1)
        return this.morphList[morphDataIndexJp];
      if (!isStartsWith)
      {
        int morphDataIndexEn = this.modelData.GetMorphDataIndexEn(morphName, false);
        if (morphDataIndexEn != -1)
          return this.morphList[morphDataIndexEn];
        int translatedMorphDataIndex = this.modelData.GetTranslatedMorphDataIndex(morphName, false);
        if (translatedMorphDataIndex != -1)
          return this.morphList[translatedMorphDataIndex];
      }
    }
    return (MMD4MecanimModelImpl.Morph) null;
  }

  public void ForceUpdateMorph() => this._UpdateMorph();

  private void _UpdateMorph()
  {
    if (this.morphList == null || this._animMorphCategoryWeights == null)
      return;
    for (int index = 0; index != this._animMorphCategoryWeights.Length; ++index)
      this._animMorphCategoryWeights[index] = 1f;
    for (int index = 0; index != this.morphList.Length; ++index)
    {
      MMD4MecanimModelImpl.Morph morph = this.morphList[index];
      switch (morph.morphCategory)
      {
        case MMD4MecanimData.MorphCategory.EyeBrow:
        case MMD4MecanimData.MorphCategory.Eye:
        case MMD4MecanimData.MorphCategory.Lip:
          if ((double) morph.weight2 != 0.0)
          {
            this._animMorphCategoryWeights[(int) morph.morphCategory] = (double) morph.weight2 != 1.0 ? Mathf.Min(this._animMorphCategoryWeights[(int) morph.morphCategory], 1f - morph.weight2) : 0.0f;
            break;
          }
          break;
      }
    }
    bool flag1 = false;
    for (int index = 0; index != this.morphList.Length; ++index)
    {
      this.morphList[index]._updateWeight = MMD4MecanimModelImpl._GetMorphUpdateWeight(this.morphList[index], this._animMorphCategoryWeights);
      flag1 |= (double) this.morphList[index]._updateWeight != (double) this.morphList[index]._updatedWeight;
    }
    if (!flag1)
      return;
    for (int index = 0; index != this.morphList.Length; ++index)
      this.morphList[index]._appendWeight = 0.0f;
    if (this._modelData == null || this._modelData.morphDataList == null)
      return;
    bool flag2 = false;
    for (int morphIndex = 0; morphIndex != this._modelData.morphDataList.Length; ++morphIndex)
    {
      if (this._modelData.morphDataList[morphIndex].morphType == MMD4MecanimData.MorphType.Group)
      {
        flag2 = true;
        this._ApplyMorph(morphIndex);
      }
    }
    for (int morphIndex = 0; morphIndex != this.morphList.Length; ++morphIndex)
    {
      if (this._modelData.morphDataList[morphIndex].morphType != MMD4MecanimData.MorphType.Group)
      {
        if (flag2)
          this.morphList[morphIndex]._updateWeight = MMD4MecanimModelImpl._GetMorphUpdateWeight(this.morphList[morphIndex], this._animMorphCategoryWeights);
        this._ApplyMorph(morphIndex);
      }
    }
  }

  private void _ApplyMorph(int morphIndex)
  {
    if (this._morphBlendShapes == null && this._cloneMeshes == null || this.morphList == null || (uint) morphIndex >= (uint) this.morphList.Length)
      return;
    MMD4MecanimModelImpl.Morph morph = this.morphList[morphIndex];
    if (morph == null)
      return;
    float updateWeight = morph._updateWeight;
    morph._updatedWeight = updateWeight;
    if (this._modelData == null || this._modelData.morphDataList == null || (uint) morphIndex >= (uint) this._modelData.morphDataList.Length)
      return;
    MMD4MecanimData.MorphData morphData = this._modelData.morphDataList[morphIndex];
    if (this.morphAutoLuminous != null && morph.morphAutoLuminousType != MMD4MecanimData.MorphAutoLumninousType.None)
    {
      switch (morph.morphAutoLuminousType)
      {
        case MMD4MecanimData.MorphAutoLumninousType.LightUp:
          if ((double) this.morphAutoLuminous.lightUp != (double) updateWeight)
          {
            this.morphAutoLuminous.lightUp = updateWeight;
            this.morphAutoLuminous.updated = true;
            break;
          }
          break;
        case MMD4MecanimData.MorphAutoLumninousType.LightOff:
          if ((double) this.morphAutoLuminous.lightOff != (double) updateWeight)
          {
            this.morphAutoLuminous.lightOff = updateWeight;
            this.morphAutoLuminous.updated = true;
            break;
          }
          break;
        case MMD4MecanimData.MorphAutoLumninousType.LightBlink:
          if ((double) this.morphAutoLuminous.lightBlink != (double) updateWeight)
          {
            this.morphAutoLuminous.lightBlink = updateWeight;
            this.morphAutoLuminous.updated = true;
            break;
          }
          break;
        case MMD4MecanimData.MorphAutoLumninousType.LightBS:
          if ((double) this.morphAutoLuminous.lightBS != (double) updateWeight)
          {
            this.morphAutoLuminous.lightBS = updateWeight;
            this.morphAutoLuminous.updated = true;
            break;
          }
          break;
      }
    }
    if (morphData.morphType == MMD4MecanimData.MorphType.Group)
    {
      if (morphData.indices == null)
        return;
      for (int index = 0; index != morphData.indices.Length; ++index)
        this.morphList[morphData.indices[index]]._appendWeight += updateWeight;
    }
    else if (morphData.morphType == MMD4MecanimData.MorphType.Vertex)
    {
      if (!this._blendShapesEnabledCache || this._morphBlendShapes == null)
        return;
      float num = updateWeight * 100f;
      if (this._skinnedMeshRenderers == null || this._skinnedMeshRenderers.Length != this._morphBlendShapes.Length)
        return;
      for (int index = 0; index != this._morphBlendShapes.Length; ++index)
      {
        if (this._morphBlendShapes[index].blendShapeIndices != null && morphIndex < this._morphBlendShapes[index].blendShapeIndices.Length)
        {
          int blendShapeIndex = this._morphBlendShapes[index].blendShapeIndices[morphIndex];
          if (blendShapeIndex != -1 && (UnityEngine.Object) this._skinnedMeshRenderers[index] != (UnityEngine.Object) null)
            this._skinnedMeshRenderers[index].SetBlendShapeWeight(blendShapeIndex, num);
        }
      }
    }
    else
    {
      if (morphData.morphType != MMD4MecanimData.MorphType.Material || morphData.materialData == null || !this.materialMorphEnabled)
        return;
      for (int index = 0; index != morphData.materialData.Length; ++index)
        this._ApplyMaterialData(ref morphData.materialData[index], updateWeight);
    }
  }

  private void _ApplyMaterialData(
    ref MMD4MecanimData.MorphMaterialData morphMaterialData,
    float weight)
  {
    if (this._cloneMaterials == null)
      return;
    for (int index1 = 0; index1 != this._cloneMaterials.Length; ++index1)
    {
      MMD4MecanimModelImpl.CloneMaterial cloneMaterial = this._cloneMaterials[index1];
      if (cloneMaterial.backupMaterialData != null && cloneMaterial.updateMaterialData != null && cloneMaterial.materialData != null && cloneMaterial.materials != null)
      {
        for (int index2 = 0; index2 < cloneMaterial.updateMaterialData.Length; ++index2)
        {
          if (cloneMaterial.backupMaterialData[index2].materialID == morphMaterialData.materialID)
          {
            if (!cloneMaterial.updateMaterialData[index2])
            {
              cloneMaterial.updateMaterialData[index2] = true;
              cloneMaterial.materialData[index2] = cloneMaterial.backupMaterialData[index2];
            }
            MMD4MecanimCommon.OperationMaterial(ref cloneMaterial.materialData[index2], ref morphMaterialData, weight);
          }
        }
      }
    }
  }

  private static float _FastScl(float weight, float weight2)
  {
    if ((double) weight2 == 0.0)
      return 0.0f;
    return (double) weight2 == 1.0 ? weight : weight * weight2;
  }

  private static float _GetMorphUpdateWeight(
    MMD4MecanimModelImpl.Morph morph,
    float[] animMorphCategoryWeights)
  {
    float morphCategoryWeight = animMorphCategoryWeights[(int) morph.morphCategory];
    float weight2 = Mathf.Min(1f - morph.weight2, morphCategoryWeight);
    return Mathf.Min(1f, Mathf.Max(morph.weight, MMD4MecanimModelImpl._FastScl(morph._animWeight + morph._appendWeight, weight2)));
  }

  private void _InitializeNEXTMaterial()
  {
    if (((UnityEngine.Object) this.nextEdgeMaterial_Pass4 == (UnityEngine.Object) null || (UnityEngine.Object) this.nextEdgeMaterial_Pass4.shader == (UnityEngine.Object) null) && !this._notFoundShaderNEXTPass4)
    {
      Shader shader = Shader.Find("MMD4Mecanim/MMDLit-NEXTEdge-Pass4");
      if ((UnityEngine.Object) shader != (UnityEngine.Object) null)
        this.nextEdgeMaterial_Pass4 = new Material(shader);
      else
        this._notFoundShaderNEXTPass4 = true;
    }
    if (((UnityEngine.Object) this.nextEdgeMaterial_Pass8 == (UnityEngine.Object) null || (UnityEngine.Object) this.nextEdgeMaterial_Pass8.shader == (UnityEngine.Object) null) && !this._notFoundShaderNEXTPass8)
    {
      Shader shader = Shader.Find("MMD4Mecanim/MMDLit-NEXTEdge-Pass8");
      if ((UnityEngine.Object) shader != (UnityEngine.Object) null)
        this.nextEdgeMaterial_Pass8 = new Material(shader);
      else
        this._notFoundShaderNEXTPass8 = true;
    }
    this._PostfixNEXTMaterial();
  }

  private void _DestroyNEXTMaterial()
  {
    if ((UnityEngine.Object) this.nextEdgeMaterial_Pass4 != (UnityEngine.Object) null)
    {
      if (Application.isPlaying)
        UnityEngine.Object.Destroy((UnityEngine.Object) this.nextEdgeMaterial_Pass4);
      else
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.nextEdgeMaterial_Pass4);
      this.nextEdgeMaterial_Pass4 = (Material) null;
    }
    if (!((UnityEngine.Object) this.nextEdgeMaterial_Pass8 != (UnityEngine.Object) null))
      return;
    if (Application.isPlaying)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.nextEdgeMaterial_Pass8);
    else
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.nextEdgeMaterial_Pass8);
    this.nextEdgeMaterial_Pass8 = (Material) null;
  }

  private void _PostfixNEXTMaterial()
  {
    if (this.postfixRenderQueue)
    {
      int num = this.renderQueueAfterSkybox ? 2501 : 2001;
      if ((UnityEngine.Object) this.nextEdgeMaterial_Pass4 != (UnityEngine.Object) null && (double) this.nextEdgeMaterial_Pass4.GetFloat("_PostfixRenderQueue") <= 0.5)
      {
        this.nextEdgeMaterial_Pass4.SetFloat("_PostfixRenderQueue", 1f);
        this.nextEdgeMaterial_Pass4.renderQueue = num;
      }
      if (!((UnityEngine.Object) this.nextEdgeMaterial_Pass8 != (UnityEngine.Object) null) || (double) this.nextEdgeMaterial_Pass8.GetFloat("_PostfixRenderQueue") > 0.5)
        return;
      this.nextEdgeMaterial_Pass8.SetFloat("_PostfixRenderQueue", 1f);
      this.nextEdgeMaterial_Pass8.renderQueue = num;
    }
    else
    {
      if ((UnityEngine.Object) this.nextEdgeMaterial_Pass4 != (UnityEngine.Object) null && (double) this.nextEdgeMaterial_Pass4.GetFloat("_PostfixRenderQueue") > 0.5)
      {
        this.nextEdgeMaterial_Pass4.SetFloat("_PostfixRenderQueue", 0.0f);
        this.nextEdgeMaterial_Pass4.renderQueue = -1;
      }
      if (!((UnityEngine.Object) this.nextEdgeMaterial_Pass8 != (UnityEngine.Object) null) || (double) this.nextEdgeMaterial_Pass8.GetFloat("_PostfixRenderQueue") <= 0.5)
        return;
      this.nextEdgeMaterial_Pass8.SetFloat("_PostfixRenderQueue", 0.0f);
      this.nextEdgeMaterial_Pass8.renderQueue = -1;
    }
  }

  private void _InitializeNEXTEdgeMesh()
  {
    if (!this.supportNEXTEdge)
      return;
    this._InitializeNEXTMaterial();
    if ((UnityEngine.Object) this.nextEdgeMaterial_Pass4 == (UnityEngine.Object) null)
      Debug.LogWarning((object) "nextEdgeMaterial_Pass4 is null. Skipped _InitializenextEdgeMesh().");
    else if ((UnityEngine.Object) this.nextEdgeMaterial_Pass8 == (UnityEngine.Object) null)
    {
      Debug.LogWarning((object) "nextEdgeMaterial_Pass8 is null. Skipped _InitializenextEdgeMesh().");
    }
    else
    {
      bool flag = (double) this.nextEdgeSize > 0.0;
      this._nextEdgeVisibleCached = flag;
      if (this._meshRenderers != null)
      {
        this._nextEdgeMeshRenderers = new MeshRenderer[this._meshRenderers.Length];
        for (int index = 0; index < this._meshRenderers.Length; ++index)
        {
          MeshRenderer meshRenderer1 = this._meshRenderers[index];
          if ((UnityEngine.Object) meshRenderer1 != (UnityEngine.Object) null)
          {
            Material[] materials = meshRenderer1.sharedMaterials;
            if (materials == null || materials.Length == 0)
              materials = meshRenderer1.materials;
            if (materials != null)
              materials = this._CloneNEXTEdgeMaterials(materials);
            if (materials != null)
            {
              GameObject nextEdgeGameObject = MMD4MecanimModelImpl._CreateNEXTEdgeGameObject(meshRenderer1.gameObject);
              MeshRenderer meshRenderer2 = nextEdgeGameObject.AddComponent<MeshRenderer>();
              meshRenderer2.enabled = flag;
              meshRenderer2.shadowCastingMode = ShadowCastingMode.Off;
              meshRenderer2.receiveShadows = false;
              meshRenderer2.materials = materials;
              MeshFilter component = this._meshRenderers[index].gameObject.GetComponent<MeshFilter>();
              if ((UnityEngine.Object) component != (UnityEngine.Object) null)
                nextEdgeGameObject.AddComponent<MeshFilter>().sharedMesh = component.sharedMesh;
              this._nextEdgeMeshRenderers[index] = meshRenderer2;
            }
          }
        }
      }
      if (this._skinnedMeshRenderers != null)
      {
        this._nextEdgeSkinnedMeshRenderers = new SkinnedMeshRenderer[this._skinnedMeshRenderers.Length];
        for (int index = 0; index < this._skinnedMeshRenderers.Length; ++index)
        {
          SkinnedMeshRenderer skinnedMeshRenderer1 = this._skinnedMeshRenderers[index];
          if ((UnityEngine.Object) skinnedMeshRenderer1 != (UnityEngine.Object) null)
          {
            Material[] materials = skinnedMeshRenderer1.sharedMaterials;
            if (materials == null || materials.Length == 0)
              materials = skinnedMeshRenderer1.materials;
            if (materials != null)
              materials = this._CloneNEXTEdgeMaterials(materials);
            if (materials != null)
            {
              SkinnedMeshRenderer skinnedMeshRenderer2 = MMD4MecanimModelImpl._CreateNEXTEdgeGameObject(skinnedMeshRenderer1.gameObject).AddComponent<SkinnedMeshRenderer>();
              skinnedMeshRenderer2.sharedMesh = skinnedMeshRenderer1.sharedMesh;
              skinnedMeshRenderer2.bones = skinnedMeshRenderer1.bones;
              skinnedMeshRenderer2.rootBone = skinnedMeshRenderer1.rootBone;
              skinnedMeshRenderer2.shadowCastingMode = ShadowCastingMode.Off;
              skinnedMeshRenderer2.receiveShadows = false;
              skinnedMeshRenderer2.materials = materials;
              skinnedMeshRenderer2.enabled = flag;
              this._nextEdgeSkinnedMeshRenderers[index] = skinnedMeshRenderer2;
            }
          }
        }
      }
      this._UpdatedNEXTEdge();
    }
  }

  private static GameObject _CreateNEXTEdgeGameObject(GameObject parentGameObject)
  {
    if (!((UnityEngine.Object) parentGameObject != (UnityEngine.Object) null))
      return (GameObject) null;
    return new GameObject(parentGameObject.name + "(NEXTEdge)")
    {
      transform = {
        parent = parentGameObject.transform,
        localPosition = Vector3.zero,
        localRotation = Quaternion.identity,
        localScale = Vector3.one
      }
    };
  }

  private Material[] _CloneNEXTEdgeMaterials(Material[] materials)
  {
    if (materials == null)
      return (Material[]) null;
    Material[] materialArray = new Material[materials.Length];
    for (int index = 0; index < materials.Length; ++index)
    {
      if ((UnityEngine.Object) materials[index] != (UnityEngine.Object) null && (UnityEngine.Object) materials[index].shader != (UnityEngine.Object) null)
        materialArray[index] = this.nextEdgePass != MMD4MecanimModelImpl.NEXTEdgePass.Pass4 ? this.nextEdgeMaterial_Pass8 : this.nextEdgeMaterial_Pass4;
    }
    return materialArray;
  }

  private void _UpdatedNEXTEdge()
  {
    if (!this.supportNEXTEdge)
      return;
    bool flag = (double) this.nextEdgeSize > 0.0;
    if (this._nextEdgeVisibleCached != flag)
    {
      this._nextEdgeVisibleCached = flag;
      if (this._nextEdgeMeshRenderers != null)
      {
        for (int index = 0; index != this._nextEdgeMeshRenderers.Length; ++index)
          this._nextEdgeMeshRenderers[index].enabled = flag;
      }
      if (this._nextEdgeSkinnedMeshRenderers != null)
      {
        for (int index = 0; index != this._nextEdgeSkinnedMeshRenderers.Length; ++index)
          this._nextEdgeSkinnedMeshRenderers[index].enabled = flag;
      }
    }
    if ((double) this._nextEdgeSizeCached == (double) this.nextEdgeSize && !(this._nextEdgeColorCached != this.nextEdgeColor))
      return;
    this._nextEdgeSizeCached = this.nextEdgeSize;
    this._nextEdgeColorCached = this.nextEdgeColor;
    if ((UnityEngine.Object) this.nextEdgeMaterial_Pass4 != (UnityEngine.Object) null)
    {
      this.nextEdgeMaterial_Pass4.SetFloat("_EdgeSize", this._nextEdgeSizeCached * 0.05f);
      this.nextEdgeMaterial_Pass4.SetColor("_EdgeColor", this._nextEdgeColorCached);
    }
    if (!((UnityEngine.Object) this.nextEdgeMaterial_Pass8 != (UnityEngine.Object) null))
      return;
    this.nextEdgeMaterial_Pass8.SetFloat("_EdgeSize", this._nextEdgeSizeCached * 0.05f);
    this.nextEdgeMaterial_Pass8.SetColor("_EdgeColor", this._nextEdgeColorCached);
  }

  private void _InitializeRigidBody()
  {
    if (this._modelData == null)
      this.rigidBodyList = (MMD4MecanimModelImpl.RigidBody[]) null;
    else if (this._modelData.rigidBodyDataList == null)
    {
      this.rigidBodyList = (MMD4MecanimModelImpl.RigidBody[]) null;
    }
    else
    {
      if (this.rigidBodyList != null)
      {
        for (int index = 0; index != this.rigidBodyList.Length; ++index)
        {
          if (this.rigidBodyList[index] == null)
          {
            this.rigidBodyList = (MMD4MecanimModelImpl.RigidBody[]) null;
            break;
          }
        }
      }
      if (this.rigidBodyList == null || this.rigidBodyList.Length != this._modelData.rigidBodyDataList.Length)
      {
        this.rigidBodyList = new MMD4MecanimModelImpl.RigidBody[this._modelData.rigidBodyDataList.Length];
        for (int index = 0; index != this.rigidBodyList.Length; ++index)
        {
          this.rigidBodyList[index] = new MMD4MecanimModelImpl.RigidBody();
          this.rigidBodyList[index].rigidBodyData = this._modelData.rigidBodyDataList[index];
          this.rigidBodyList[index].freezed = this.rigidBodyList[index].rigidBodyData.isFreezed;
        }
      }
      else
      {
        for (int index = 0; index != this.rigidBodyList.Length; ++index)
          this.rigidBodyList[index].rigidBodyData = this._modelData.rigidBodyDataList[index];
      }
    }
  }

  private void _InitializePhysicsEngine()
  {
    if ((UnityEngine.Object) this.modelFile == (UnityEngine.Object) null)
    {
      Debug.LogWarning((object) (this.gameObject.name + ":modelFile is nothing."));
    }
    else
    {
      if (this.physicsEngine != MMD4MecanimModelImpl.PhysicsEngine.None && this.physicsEngine != MMD4MecanimModelImpl.PhysicsEngine.BulletPhysics)
        return;
      MMD4MecanimBulletPhysicsImpl instanceImpl = MMD4MecanimBulletPhysicsImpl.instanceImpl;
      if (!((UnityEngine.Object) instanceImpl != (UnityEngine.Object) null))
        return;
      this._bulletPhysicsMMDModel = instanceImpl.CreateMMDModel(this);
    }
  }

  public void SetGravity(float gravityScale, float gravityNoise, Vector3 gravityDirection)
  {
    if (this.bulletPhysics == null || this.bulletPhysics.worldProperty == null)
      return;
    this.bulletPhysics.worldProperty.gravityScale = gravityScale;
    this.bulletPhysics.worldProperty.gravityNoise = gravityNoise;
    this.bulletPhysics.worldProperty.gravityDirection = gravityDirection;
  }

  public float pphShoulderFixRateImmediately => this._pphShoulderFixRateImmediately;

  public MMD4MecanimData.ModelData modelData => this._modelData;

  public byte[] modelFileBytes => !((UnityEngine.Object) this.modelFile != (UnityEngine.Object) null) ? (byte[]) null : this.modelFile.bytes;

  public byte[] indexFileBytes => !((UnityEngine.Object) this.indexFile != (UnityEngine.Object) null) ? (byte[]) null : this.indexFile.bytes;

  public byte[] vertexFileBytes => !((UnityEngine.Object) this.vertexFile != (UnityEngine.Object) null) ? (byte[]) null : this.vertexFile.bytes;

  public bool skinningEnabled => this._skinnedMeshRenderers != null && (uint) this._skinnedMeshRenderers.Length > 0U;

  private bool _Invoke()
  {
    if (!this._invokeed)
    {
      RuntimePlatform platform = Application.platform;
      switch (platform)
      {
        case RuntimePlatform.OSXEditor:
        case RuntimePlatform.OSXPlayer:
        case RuntimePlatform.WindowsPlayer:
        case RuntimePlatform.WindowsEditor:
        case RuntimePlatform.IPhonePlayer:
        case RuntimePlatform.MetroPlayerX86:
        case RuntimePlatform.MetroPlayerX64:
        case RuntimePlatform.MetroPlayerARM:
          this._invokeed = true;
          return true;
        default:
          if (platform.ToString().StartsWith("PS"))
          {
            this._invokeed = true;
            return true;
          }
          MMD4MecanimModelImpl._InvokeHelper invokeHelper = new MMD4MecanimModelImpl._InvokeHelper(this._meshRenderers, this._skinnedMeshRenderers);
          if ((UnityEngine.Object) this._animator != (UnityEngine.Object) null)
          {
            float speed = this._animator.speed;
            this._animator.speed = 0.0f;
            if (!this._InvokeInternal())
              return false;
            this._animator.speed = speed;
          }
          else
          {
            Animation component = this.GetComponent<Animation>();
            if ((UnityEngine.Object) component != (UnityEngine.Object) null)
            {
              component.enabled = false;
              if (!this._InvokeInternal())
                return false;
              component.enabled = true;
            }
          }
          invokeHelper._Finalize();
          this._invokeed = true;
          break;
      }
    }
    return true;
  }

  private bool _InvokeInternal()
  {
    if (this._invokeed)
      return true;
    string str1 = "cvjmeubh";
    StringBuilder stringBuilder1 = new StringBuilder();
    for (int index = 0; index < 8; ++index)
      stringBuilder1.Append((char) ((uint) str1[index] - 1U));
    TextAsset textAsset1 = Resources.Load(stringBuilder1.ToString(), typeof (TextAsset)) as TextAsset;
    if ((UnityEngine.Object) textAsset1 != (UnityEngine.Object) null)
      return MMD4MecanimBulletPhysicsImpl.Invoke(Encoding.ASCII.GetBytes(textAsset1.text));
    string str2 = "efwjdft";
    StringBuilder stringBuilder2 = new StringBuilder();
    for (int index = 0; index < 7; ++index)
      stringBuilder2.Append((char) ((uint) str2[index] - 1U));
    TextAsset textAsset2 = Resources.Load(stringBuilder2.ToString(), typeof (TextAsset)) as TextAsset;
    if ((UnityEngine.Object) textAsset2 != (UnityEngine.Object) null)
    {
      byte[] bytes = Encoding.ASCII.GetBytes(textAsset2.text);
      if (bytes != null)
      {
        byte[] numArray = new byte[bytes.Length + 1];
        Array.Copy((Array) bytes, 0, (Array) numArray, 1, bytes.Length);
        return MMD4MecanimBulletPhysicsImpl.Invoke(numArray);
      }
    }
    return MMD4MecanimBulletPhysicsImpl.Invoke();
  }

  private static bool _isEliminatable
  {
    get
    {
      if (Application.isEditor)
        return true;
      RuntimePlatform platform = Application.platform;
      switch (platform)
      {
        case RuntimePlatform.OSXPlayer:
        case RuntimePlatform.WindowsPlayer:
        case RuntimePlatform.Android:
        case RuntimePlatform.MetroPlayerX86:
        case RuntimePlatform.MetroPlayerX64:
        case RuntimePlatform.MetroPlayerARM:
          return true;
        default:
          return platform == RuntimePlatform.IPhonePlayer;
      }
    }
  }

  private void _InvokeEliminate()
  {
    if (MMD4MecanimModelImpl._invokeEliminate)
      return;
    MMD4MecanimModelImpl._invokeEliminate = true;
    if (!MMD4MecanimModelImpl._isEliminatable)
      return;
    Shader.SetGlobalFloat("___Eliminate", 1f);
  }

  public MMD4MecanimBoneImpl GetRootBoneImpl() => this._rootBone;

  public MMD4MecanimBoneImpl GetBoneImpl(int boneID) => this.boneList != null && boneID >= 0 && boneID < this.boneList.Length ? this.boneList[boneID] : (MMD4MecanimBoneImpl) null;

  private void Awake()
  {
    if (!MMD4MecanimCommon.versionChecked)
      MMD4MecanimCommon.VersionCheck();
    if (!this.initializeOnAwake)
      return;
    this.Initialize();
  }

  private void Start() => this.Initialize();

  private void Update()
  {
    if (!Application.isPlaying)
      return;
    this._Salvage();
    if (!this._InvokeInternal())
      return;
    this._InvokeEliminate();
    if (this.onUpdating != null)
      this.onUpdating();
    this._UpdateAnim();
    this._UpdateMorph();
    this._UpdateBone();
    this._UpdateIK();
    if (this.onUpdated == null)
      return;
    this.onUpdated();
  }

  private void _Salvage()
  {
    if (this._meshRenderers == null || this._skinnedMeshRenderers == null)
      this._InitializeMesh();
    if (!((UnityEngine.Object) this.modelFile != (UnityEngine.Object) null))
      return;
    if (this._modelData == null)
      this._InitializeModelData();
    if (this._modelData == null)
      return;
    if ((this.boneList != null ? this.boneList.Length : 0) != (this._modelData.boneDataList != null ? this._modelData.boneDataList.Length : 0))
      this._InitializeModel();
    if ((this.rigidBodyList != null ? this.rigidBodyList.Length : 0) != (this._modelData.rigidBodyDataList != null ? this._modelData.rigidBodyDataList.Length : 0))
      this._InitializeRigidBody();
    if (this._morphBlendShapes == null && this._modelData.morphDataList != null && this._modelData.morphDataList.Length != 0 && this.blendShapesEnabled && this._IsBlendShapesAnything())
    {
      this._blendShapesEnabledCache = this.blendShapesEnabled;
      this._PrepareBlendShapes();
      this._InitializeBlendShapes();
    }
    if ((UnityEngine.Object) this._animator == (UnityEngine.Object) null || this._animMorphCategoryWeights == null)
      this._InitializeAnimatoion();
    if (this._bulletPhysicsMMDModel == null && (this.physicsEngine == MMD4MecanimModelImpl.PhysicsEngine.None || this.physicsEngine == MMD4MecanimModelImpl.PhysicsEngine.BulletPhysics))
      this._InitializePhysicsEngine();
    if ((UnityEngine.Object) this._animator != (UnityEngine.Object) null && (UnityEngine.Object) this._animator.avatar != (UnityEngine.Object) null && this._animator.avatar.isValid && this._animator.avatar.isHuman && ((UnityEngine.Object) this._leftArmBone == (UnityEngine.Object) null || (UnityEngine.Object) this._rightArmBone == (UnityEngine.Object) null))
      this._InitializePPHBones();
    if (!((UnityEngine.Object) this.nextEdgeMaterial_Pass4 == (UnityEngine.Object) null) && !((UnityEngine.Object) this.nextEdgeMaterial_Pass4.shader == (UnityEngine.Object) null) && !((UnityEngine.Object) this.nextEdgeMaterial_Pass8 == (UnityEngine.Object) null) && !((UnityEngine.Object) this.nextEdgeMaterial_Pass8.shader == (UnityEngine.Object) null))
      return;
    this._InitializeNEXTMaterial();
  }

  private void LateUpdate()
  {
    this._UpdatedDeffered();
    this._UpdatedNEXTEdge();
    if (!Application.isPlaying)
      this._UpdateAmbientPreview();
    if (!Application.isPlaying)
      return;
    if (this.onLateUpdating != null)
      this.onLateUpdating();
    this._LateUpdateBone();
    this._LateUpdateIK();
    this._UploadMeshMaterial();
    if (this.onLateUpdated == null)
      return;
    this.onLateUpdated();
  }

  private void OnRenderObject()
  {
  }

  private void OnDestroy()
  {
    this._DestroyDeferredMaterial();
    this._DestroyNEXTMaterial();
    if (this.ikList != null)
    {
      for (int index = 0; index < this.ikList.Length; ++index)
      {
        if (this.ikList[index] != null)
          this.ikList[index].Destroy();
      }
      this.ikList = (MMD4MecanimModelImpl.IK[]) null;
    }
    this._sortedBoneList = (MMD4MecanimBoneImpl[]) null;
    if (this.boneList != null)
    {
      for (int index = 0; index < this.boneList.Length; ++index)
      {
        if ((UnityEngine.Object) this.boneList[index] != (UnityEngine.Object) null)
          this.boneList[index].Destroy();
      }
      this.boneList = (MMD4MecanimBoneImpl[]) null;
    }
    if (this._bulletPhysicsMMDModel != null && !this._bulletPhysicsMMDModel.isExpired)
    {
      MMD4MecanimBulletPhysicsImpl instanceImpl = MMD4MecanimBulletPhysicsImpl.instanceImpl;
      if ((UnityEngine.Object) instanceImpl != (UnityEngine.Object) null)
        instanceImpl.DestroyMMDModel(this._bulletPhysicsMMDModel);
    }
    this._bulletPhysicsMMDModel = (MMD4MecanimBulletPhysicsImpl.MMDModel) null;
  }

  public void Initialize()
  {
    if (!Application.isPlaying)
    {
      this.InitializeOnEditor();
    }
    else
    {
      if ((UnityEngine.Object) this == (UnityEngine.Object) null || this._initialized)
        return;
      this._initialized = true;
      this._blendShapesEnabledCache = this.blendShapesEnabled;
      this._InitializeMesh();
      this._InitializeModel();
      this._InitializeRigidBody();
      this._PrepareBlendShapes();
      this._InitializeBlendShapes();
      this._InitializeAnimatoion();
      this._InitializePhysicsEngine();
      this._InitializePPHBones();
      this._InitializeDeferredMesh();
      this._InitializeNEXTEdgeMesh();
      this._InitializeGlobalAmbient();
      this._InitializeIK();
      if (!this._InvokeInternal())
        return;
      this._InvokeEliminate();
    }
  }

  public AudioSource GetAudioSource()
  {
    if ((UnityEngine.Object) this.audioSource == (UnityEngine.Object) null)
    {
      this.audioSource = this.gameObject.GetComponent<AudioSource>();
      if ((UnityEngine.Object) this.audioSource == (UnityEngine.Object) null)
        this.audioSource = this.gameObject.AddComponent<AudioSource>();
    }
    return this.audioSource;
  }

  public void InitializeOnEditor()
  {
    if ((UnityEngine.Object) this == (UnityEngine.Object) null)
      return;
    if (this._modelData == null)
      this._initialized = false;
    if (this._modelData == null && (UnityEngine.Object) this.modelFile == (UnityEngine.Object) null || this._modelData == null && !this._InitializeModelData())
      return;
    if (this._modelData != null && this._modelData.boneDataList != null && (this.boneList == null || this.boneList.Length != this._modelData.boneDataList.Length))
      this._initialized = false;
    if (this._initialized)
      return;
    this._initialized = true;
    this._blendShapesEnabledCache = this.blendShapesEnabled;
    this._InitializeMesh();
    this._InitializeModel();
    this._InitializeRigidBody();
    this._PrepareBlendShapes();
    this._InitializeBlendShapes();
    this._InitializeAnimatoion();
    this._InitializeDeferredMaterial();
    this._InitializeNEXTMaterial();
    this._InitializeIK();
  }

  private void _InitializeMesh()
  {
    if (this._meshRenderers != null)
    {
      for (int index = 0; index != this._meshRenderers.Length; ++index)
      {
        if ((UnityEngine.Object) this._meshRenderers[index] == (UnityEngine.Object) null)
        {
          this._meshRenderers = (MeshRenderer[]) null;
          break;
        }
      }
    }
    if (this._skinnedMeshRenderers != null)
    {
      for (int index = 0; index != this._skinnedMeshRenderers.Length; ++index)
      {
        if ((UnityEngine.Object) this._skinnedMeshRenderers[index] == (UnityEngine.Object) null)
        {
          this._skinnedMeshRenderers = (SkinnedMeshRenderer[]) null;
          break;
        }
      }
    }
    if (this._meshRenderers == null || this._meshRenderers.Length == 0)
    {
      this._meshRenderers = MMD4MecanimCommon.GetMeshRenderers(this.gameObject);
      if (this._meshRenderers == null)
        this._meshRenderers = new MeshRenderer[0];
      if (this._meshRenderers != null)
      {
        for (int index = 0; index != this._meshRenderers.Length; ++index)
          MMD4MecanimModelImpl._PrefixShadowCasting((Renderer) this._meshRenderers[index], MMD4MecanimModelImpl._GetMaterials((Renderer) this._meshRenderers[index]));
      }
    }
    if (this._skinnedMeshRenderers == null || this._skinnedMeshRenderers.Length == 0)
    {
      this._skinnedMeshRenderers = MMD4MecanimCommon.GetSkinnedMeshRenderers(this.gameObject);
      if (this._skinnedMeshRenderers == null)
        this._skinnedMeshRenderers = new SkinnedMeshRenderer[0];
      if (this._skinnedMeshRenderers != null)
      {
        for (int index = 0; index != this._skinnedMeshRenderers.Length; ++index)
        {
          if (this._skinnedMeshRenderers[index].updateWhenOffscreen != this.updateWhenOffscreen)
            this._skinnedMeshRenderers[index].updateWhenOffscreen = this.updateWhenOffscreen;
          MMD4MecanimModelImpl._PrefixShadowCasting((Renderer) this._skinnedMeshRenderers[index], MMD4MecanimModelImpl._GetMaterials((Renderer) this._skinnedMeshRenderers[index]));
        }
      }
    }
    if ((UnityEngine.Object) this.defaultMesh == (UnityEngine.Object) null && this._skinnedMeshRenderers != null && this._skinnedMeshRenderers.Length != 0)
      this.defaultMesh = this._skinnedMeshRenderers[0].sharedMesh;
    if (!((UnityEngine.Object) this.defaultMesh == (UnityEngine.Object) null) || this._meshRenderers == null || this._meshRenderers.Length == 0)
      return;
    MeshFilter component = this.gameObject.GetComponent<MeshFilter>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    this.defaultMesh = component.sharedMesh;
  }

  private static Material[] _GetMaterials(Renderer renderer)
  {
    if ((UnityEngine.Object) renderer == (UnityEngine.Object) null)
      return (Material[]) null;
    Material[] materials = (Material[]) null;
    if (Application.isPlaying)
      materials = renderer.materials;
    if (materials == null)
      materials = renderer.sharedMaterials;
    return materials;
  }

  private static void _PrefixShadowCasting(Renderer renderer, Material[] materials)
  {
    if ((UnityEngine.Object) renderer == (UnityEngine.Object) null || materials == null || !MMD4MecanimModelImpl._IsNoShadowCasting(materials))
      return;
    renderer.shadowCastingMode = ShadowCastingMode.Off;
  }

  private void _PrepareBlendShapes()
  {
    if (this._skinnedMeshRenderers == null)
      return;
    for (int index1 = 0; index1 < this._skinnedMeshRenderers.Length; ++index1)
    {
      SkinnedMeshRenderer skinnedMeshRenderer = this._skinnedMeshRenderers[index1];
      if ((UnityEngine.Object) skinnedMeshRenderer != (UnityEngine.Object) null)
      {
        Mesh sharedMesh = skinnedMeshRenderer.sharedMesh;
        if ((UnityEngine.Object) sharedMesh != (UnityEngine.Object) null)
        {
          for (int index2 = 0; index2 < sharedMesh.blendShapeCount; ++index2)
          {
            if (Application.isPlaying)
              skinnedMeshRenderer.SetBlendShapeWeight(index2, 0.0f);
            else if ((double) skinnedMeshRenderer.GetBlendShapeWeight(index2) != 0.0)
              skinnedMeshRenderer.SetBlendShapeWeight(index2, 0.0f);
          }
        }
      }
    }
  }

  private bool _IsBlendShapesAnything()
  {
    if (this._skinnedMeshRenderers != null)
    {
      for (int index = 0; index < this._skinnedMeshRenderers.Length; ++index)
      {
        SkinnedMeshRenderer skinnedMeshRenderer = this._skinnedMeshRenderers[index];
        if ((UnityEngine.Object) skinnedMeshRenderer != (UnityEngine.Object) null)
        {
          Mesh sharedMesh = skinnedMeshRenderer.sharedMesh;
          if ((UnityEngine.Object) sharedMesh != (UnityEngine.Object) null && sharedMesh.blendShapeCount > 0)
            return true;
        }
      }
    }
    return false;
  }

  private void _InitializeBlendShapes()
  {
    if (!this._blendShapesEnabledCache || this._skinnedMeshRenderers == null || this._modelData == null || this._modelData.morphDataList == null || this._modelData.morphDataList.Length == 0 || this._morphBlendShapes != null && this._morphBlendShapes.Length == this._skinnedMeshRenderers.Length)
      return;
    this._morphBlendShapes = (MMD4MecanimModelImpl.MorphBlendShape[]) null;
    if (!this._IsBlendShapesAnything())
      return;
    this._morphBlendShapes = new MMD4MecanimModelImpl.MorphBlendShape[this._skinnedMeshRenderers.Length];
    for (int index1 = 0; index1 < this._skinnedMeshRenderers.Length; ++index1)
    {
      this._morphBlendShapes[index1] = new MMD4MecanimModelImpl.MorphBlendShape();
      this._morphBlendShapes[index1].blendShapeIndices = new int[this._modelData.morphDataList.Length];
      for (int index2 = 0; index2 < this._modelData.morphDataList.Length; ++index2)
        this._morphBlendShapes[index1].blendShapeIndices[index2] = -1;
      SkinnedMeshRenderer skinnedMeshRenderer = this._skinnedMeshRenderers[index1];
      if ((UnityEngine.Object) skinnedMeshRenderer.sharedMesh != (UnityEngine.Object) null && skinnedMeshRenderer.sharedMesh.blendShapeCount > 0)
      {
        for (int shapeIndex = 0; shapeIndex < skinnedMeshRenderer.sharedMesh.blendShapeCount; ++shapeIndex)
        {
          string blendShapeName = skinnedMeshRenderer.sharedMesh.GetBlendShapeName(shapeIndex);
          int index3 = !MMD4MecanimCommon.IsID(blendShapeName) ? this._modelData.GetMorphDataIndex(blendShapeName, false) : MMD4MecanimCommon.ToInt(blendShapeName);
          if ((uint) index3 < (uint) this._modelData.morphDataList.Length)
            this._morphBlendShapes[index1].blendShapeIndices[index3] = shapeIndex;
        }
      }
    }
  }

  private static void _PostfixRenderQueue(
    Material[] materials,
    bool postfixRenderQueue,
    bool renderQueueAfterSkybox)
  {
    if (!Application.isPlaying || materials == null)
      return;
    for (int index = 0; index < materials.Length; ++index)
    {
      if ((UnityEngine.Object) materials[index].shader != (UnityEngine.Object) null && materials[index].shader.name != null && materials[index].shader.name.StartsWith("MMD4Mecanim") && postfixRenderQueue)
      {
        int num = renderQueueAfterSkybox ? 2502 : 2002;
        materials[index].renderQueue = num + MMD4MecanimCommon.ToInt(materials[index].name);
      }
    }
  }

  private static void _SetupCloneMaterial(
    MMD4MecanimModelImpl.CloneMaterial cloneMaterial,
    Material[] materials)
  {
    cloneMaterial.materials = materials;
    if (materials == null)
      return;
    int length = materials.Length;
    cloneMaterial.materialData = new MMD4MecanimData.MorphMaterialData[length];
    cloneMaterial.backupMaterialData = new MMD4MecanimData.MorphMaterialData[length];
    cloneMaterial.updateMaterialData = new bool[length];
    for (int index = 0; index < length; ++index)
    {
      if ((UnityEngine.Object) materials[index] != (UnityEngine.Object) null)
      {
        MMD4MecanimCommon.BackupMaterial(ref cloneMaterial.backupMaterialData[index], materials[index]);
        cloneMaterial.materialData[index] = cloneMaterial.backupMaterialData[index];
      }
    }
  }

  private static bool _IsNoShadowCasting(Material[] materials)
  {
    if (materials == null || materials.Length == 0)
      return false;
    int index = 0;
    for (int length = materials.Length; index < length; ++index)
    {
      Material material = materials[index];
      if (!material.HasProperty("_NoShadowCasting") || (double) material.GetFloat("_NoShadowCasting") <= (double) Mathf.Epsilon)
        return false;
    }
    return true;
  }

  public bool IsEnableMorphBlendShapes(int meshIndex)
  {
    if (this._morphBlendShapes == null || meshIndex < 0 || meshIndex >= this._morphBlendShapes.Length || this._morphBlendShapes[meshIndex].blendShapeIndices == null)
      return false;
    for (int index = 0; index != this._morphBlendShapes[meshIndex].blendShapeIndices.Length; ++index)
    {
      if (this._morphBlendShapes[meshIndex].blendShapeIndices[index] != -1)
        return true;
    }
    return false;
  }

  private bool _InitializeModelData()
  {
    if ((UnityEngine.Object) this.modelFile == (UnityEngine.Object) null)
    {
      Debug.LogWarning((object) (this.gameObject.name + ":modelFile is nothing."));
      return false;
    }
    this._modelData = MMD4MecanimData.BuildModelData(this.modelFile);
    if (this._modelData != null)
      return true;
    Debug.LogError((object) (this.gameObject.name + ":modelFile is unsupported format."));
    return false;
  }

  private void _InitializeModel()
  {
    if (this._modelData == null && !this._InitializeModelData())
      return;
    if (this._modelData.boneDataList != null && this._modelData.boneDataDictionary != null && (this.boneList == null || this.boneList.Length != this._modelData.boneDataList.Length))
    {
      this.boneList = new MMD4MecanimBoneImpl[this._modelData.boneDataList.Length];
      this._BindBone();
      for (int index = 0; index < this.boneList.Length; ++index)
      {
        if ((UnityEngine.Object) this.boneList[index] != (UnityEngine.Object) null)
          this.boneList[index].Bind();
      }
      this._sortedBoneList = new MMD4MecanimBoneImpl[this.boneList.Length];
      for (int index = 0; index < this.boneList.Length; ++index)
      {
        if ((UnityEngine.Object) this.boneList[index] != (UnityEngine.Object) null)
        {
          MMD4MecanimData.BoneData boneData = this.boneList[index].boneData;
          if (boneData != null)
          {
            int sortedBoneId = boneData.sortedBoneID;
            if (sortedBoneId >= 0 && sortedBoneId < this.boneList.Length)
              this._sortedBoneList[sortedBoneId] = this.boneList[index];
          }
        }
      }
    }
    if (this._modelData.ikDataList != null)
    {
      int length = this._modelData.ikDataList.Length;
      this.ikList = new MMD4MecanimModelImpl.IK[length];
      for (int ikID = 0; ikID < length; ++ikID)
        this.ikList[ikID] = new MMD4MecanimModelImpl.IK(this, ikID);
    }
    if (this._modelData.morphDataList != null)
    {
      this.morphList = new MMD4MecanimModelImpl.Morph[this._modelData.morphDataList.Length];
      for (int index = 0; index < this._modelData.morphDataList.Length; ++index)
      {
        this.morphList[index] = new MMD4MecanimModelImpl.Morph();
        this.morphList[index].morphData = this._modelData.morphDataList[index];
        string name = this.morphList[index].name;
        if (!string.IsNullOrEmpty(name))
        {
          if (!(name == "LightUp"))
          {
            if (!(name == "LightOff"))
            {
              if (!(name == "LightBlink"))
              {
                if (name == "LightBS")
                  this.morphList[index].morphAutoLuminousType = MMD4MecanimData.MorphAutoLumninousType.LightBS;
              }
              else
                this.morphList[index].morphAutoLuminousType = MMD4MecanimData.MorphAutoLumninousType.LightBlink;
            }
            else
              this.morphList[index].morphAutoLuminousType = MMD4MecanimData.MorphAutoLumninousType.LightOff;
          }
          else
            this.morphList[index].morphAutoLuminousType = MMD4MecanimData.MorphAutoLumninousType.LightUp;
        }
      }
    }
    this.morphAutoLuminous = new MMD4MecanimModelImpl.MorphAutoLuminous();
  }

  protected virtual MMD4MecanimBoneImpl AddComponentBoneImpl(
    GameObject gameObject)
  {
    return (UnityEngine.Object) gameObject != (UnityEngine.Object) null ? gameObject.AddComponent<MMD4MecanimBoneImpl>() : (MMD4MecanimBoneImpl) null;
  }

  protected virtual MMD4MecanimIKTargetImpl AddComponentIKTargetImpl(
    GameObject gameObject)
  {
    return (UnityEngine.Object) gameObject != (UnityEngine.Object) null ? gameObject.AddComponent<MMD4MecanimIKTargetImpl>() : (MMD4MecanimIKTargetImpl) null;
  }

  public class IK
  {
    private int _ikID;
    private MMD4MecanimData.IKData _ikData;
    private MMD4MecanimBoneImpl _destBone;
    private MMD4MecanimBoneImpl _targetBone;
    private MMD4MecanimModelImpl.IK.IKLink[] _ikLinkList;

    public int ikID => this._ikID;

    public MMD4MecanimData.IKData ikData => this._ikData;

    public MMD4MecanimBoneImpl destBone => this._destBone;

    public MMD4MecanimBoneImpl targetBone => this._targetBone;

    public MMD4MecanimModelImpl.IK.IKLink[] ikLinkList => this._ikLinkList;

    public bool ikEnabled
    {
      get => (UnityEngine.Object) this._destBone != (UnityEngine.Object) null && this._destBone.ikEnabled;
      set
      {
        if (!((UnityEngine.Object) this._destBone != (UnityEngine.Object) null))
          return;
        this._destBone.ikEnabled = value;
      }
    }

    public float ikWeight
    {
      get => (UnityEngine.Object) this._destBone != (UnityEngine.Object) null ? this._destBone.ikWeight : 0.0f;
      set
      {
        if (!((UnityEngine.Object) this._destBone != (UnityEngine.Object) null))
          return;
        this._destBone.ikWeight = value;
      }
    }

    public IK(MMD4MecanimModelImpl model, int ikID)
    {
      if ((UnityEngine.Object) model == (UnityEngine.Object) null || model.modelData == null || model.modelData.ikDataList == null || ikID >= model.modelData.ikDataList.Length)
      {
        object[] objArray = new object[10];
        objArray[0] = (object) "model:";
        bool flag = (UnityEngine.Object) model != (UnityEngine.Object) null;
        objArray[1] = (object) flag.ToString();
        objArray[2] = (object) " modelData:";
        flag = (UnityEngine.Object) model != (UnityEngine.Object) null && model.modelData != null;
        objArray[3] = (object) flag.ToString();
        objArray[4] = (object) " ikDataList:";
        flag = (UnityEngine.Object) model != (UnityEngine.Object) null && model.modelData != null && model.modelData.ikDataList != null;
        objArray[5] = (object) flag.ToString();
        objArray[6] = (object) " ikID:";
        objArray[7] = (object) ikID;
        objArray[8] = (object) " Length:";
        objArray[9] = (object) (!((UnityEngine.Object) model != (UnityEngine.Object) null) || model.modelData == null || model.modelData.ikDataList == null ? 0 : model.modelData.ikDataList.Length);
        Debug.LogError((object) string.Concat(objArray));
      }
      else
      {
        this._ikID = ikID;
        this._ikData = model.modelData.ikDataList[ikID];
        if (this._ikData == null)
          return;
        this._destBone = model.GetBoneImpl(this._ikData.destBoneID);
        this._targetBone = model.GetBoneImpl(this._ikData.targetBoneID);
        if (this._ikData.ikLinkDataList == null)
          return;
        this._ikLinkList = new MMD4MecanimModelImpl.IK.IKLink[this._ikData.ikLinkDataList.Length];
        for (int index = 0; index < this._ikData.ikLinkDataList.Length; ++index)
        {
          this._ikLinkList[index] = new MMD4MecanimModelImpl.IK.IKLink();
          this._ikLinkList[index].ikLinkData = this._ikData.ikLinkDataList[index];
        }
      }
    }

    public void Destroy()
    {
      this._ikData = (MMD4MecanimData.IKData) null;
      this._destBone = (MMD4MecanimBoneImpl) null;
      this._targetBone = (MMD4MecanimBoneImpl) null;
      this._ikLinkList = (MMD4MecanimModelImpl.IK.IKLink[]) null;
    }

    public class IKLink
    {
      public MMD4MecanimData.IKLinkData ikLinkData;
      public MMD4MecanimBoneImpl bone;
    }
  }

  public enum NEXTEdgePass
  {
    Pass4,
    Pass8,
  }

  public enum PhysicsEngine
  {
    None,
    BulletPhysics,
  }

  public class Morph : MMD4MecanimAnim.IMorph
  {
    public float weight;
    public float weight2;
    public float _animWeight;
    public float _appendWeight;
    public float _updateWeight;
    public float _updatedWeight;
    public MMD4MecanimData.MorphData morphData;
    public MMD4MecanimData.MorphAutoLumninousType morphAutoLuminousType;

    public MMD4MecanimData.MorphType morphType => this.morphData != null ? this.morphData.morphType : MMD4MecanimData.MorphType.Group;

    public MMD4MecanimData.MorphCategory morphCategory => this.morphData != null ? this.morphData.morphCategory : MMD4MecanimData.MorphCategory.Base;

    public string name => this.morphData != null ? this.morphData.nameJp : (string) null;

    public string translatedName => this.morphData != null ? this.morphData.translatedName : (string) null;

    string MMD4MecanimAnim.IMorph.name => this.name;

    float MMD4MecanimAnim.IMorph.weight
    {
      get => this.weight;
      set => this.weight = value;
    }
  }

  public class MorphAutoLuminous
  {
    public float lightUp;
    public float lightOff;
    public float lightBlink;
    public float lightBS;
    public bool updated;
  }

  [Serializable]
  public class RigidBody
  {
    [NonSerialized]
    public MMD4MecanimData.RigidBodyData rigidBodyData;
    public bool freezed = true;
    public int _freezedCached = -1;
    public float mass = -1f;
    public float linearDamping = -1f;
    public float angularDamping = -1f;
    public float restitution = -1f;
    public float friction = -1f;
  }

  [Serializable]
  public class Anim : MMD4MecanimAnim.Anim
  {
  }

  [Serializable]
  public class BulletPhysics
  {
    public bool joinLocalWorld = true;
    public bool useOriginalScale = true;
    public bool useCustomResetTime;
    public float resetMorphTime = 1.8f;
    public float resetWaitTime = 1.2f;
    public WorldProperty worldProperty;
    public MMDModelProperty mmdModelProperty;
  }

  public class CloneMesh
  {
    public SkinnedMeshRenderer skinnedMeshRenderer;
    public Mesh mesh;
    public Vector3[] vertices;
    public Vector3[] normals;
    public Matrix4x4[] bindposes;
    public BoneWeight[] boneWeights;
  }

  public class CloneMaterial
  {
    public Material[] materials;
    public MMD4MecanimData.MorphMaterialData[] materialData;
    public MMD4MecanimData.MorphMaterialData[] backupMaterialData;
    public bool[] updateMaterialData;
  }

  private struct MorphBlendShape
  {
    public int[] blendShapeIndices;
  }

  public enum PPHType
  {
    Shoulder,
  }

  public enum EditorViewPage
  {
    Model,
    Bone,
    IK,
    Morph,
    Anim,
    Physics,
  }

  public enum EditorViewMorphNameType
  {
    Japanese,
    English,
    Translated,
  }

  private struct _InvokeHelper
  {
    private MeshRenderer[] _meshRenderers;
    private SkinnedMeshRenderer[] _skinnedMeshRenderers;
    private bool[] _isOverrayMeshRenderers;
    private bool[] _isOverraySkinnedMeshRenderers;

    public _InvokeHelper(MeshRenderer[] meshRenderers, SkinnedMeshRenderer[] skinnedMeshRenderers)
    {
      this._meshRenderers = meshRenderers;
      this._skinnedMeshRenderers = skinnedMeshRenderers;
      this._isOverrayMeshRenderers = (bool[]) null;
      this._isOverraySkinnedMeshRenderers = (bool[]) null;
      if (this._meshRenderers != null)
      {
        int length = this._meshRenderers.Length;
        if (length > 0)
        {
          this._isOverrayMeshRenderers = new bool[length];
          for (int index = 0; index < length; ++index)
          {
            if ((UnityEngine.Object) this._meshRenderers[index] != (UnityEngine.Object) null && this._meshRenderers[index].enabled)
            {
              this._isOverrayMeshRenderers[index] = true;
              this._meshRenderers[index].enabled = false;
            }
          }
        }
      }
      if (this._skinnedMeshRenderers == null)
        return;
      int length1 = this._skinnedMeshRenderers.Length;
      if (length1 <= 0)
        return;
      this._isOverraySkinnedMeshRenderers = new bool[length1];
      for (int index = 0; index < length1; ++index)
      {
        if ((UnityEngine.Object) this._skinnedMeshRenderers[index] != (UnityEngine.Object) null && this._skinnedMeshRenderers[index].enabled)
        {
          this._isOverraySkinnedMeshRenderers[index] = true;
          this._skinnedMeshRenderers[index].enabled = false;
        }
      }
    }

    public void _Finalize()
    {
      if (this._isOverrayMeshRenderers != null)
      {
        int length = this._isOverrayMeshRenderers.Length;
        for (int index = 0; index < length; ++index)
        {
          if (this._isOverrayMeshRenderers[index] && (UnityEngine.Object) this._meshRenderers[index] != (UnityEngine.Object) null)
            this._meshRenderers[index].enabled = true;
        }
      }
      if (this._isOverraySkinnedMeshRenderers == null)
        return;
      int length1 = this._isOverraySkinnedMeshRenderers.Length;
      for (int index = 0; index < length1; ++index)
      {
        if (this._isOverraySkinnedMeshRenderers[index] && (UnityEngine.Object) this._skinnedMeshRenderers[index] != (UnityEngine.Object) null)
          this._skinnedMeshRenderers[index].enabled = true;
      }
    }
  }
}
