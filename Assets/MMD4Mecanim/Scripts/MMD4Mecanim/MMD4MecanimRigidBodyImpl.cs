﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimRigidBodyImpl
// Assembly: MMD4Mecanim, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: ADBCBBA1-CEE8-40F4-A775-E57AB2D2693F
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Scripts\MMD4Mecanim.dll

using MMD4MecanimInternal.Bullet;
using UnityEngine;

public class MMD4MecanimRigidBodyImpl : MonoBehaviour
{
    public RigidBodyProperty bulletPhysicsRigidBodyProperty;
    private MMD4MecanimBulletPhysicsImpl.RigidBody _bulletPhysicsRigidBody;

    private void Start()
    {
        MMD4MecanimBulletPhysicsImpl instanceImpl = MMD4MecanimBulletPhysicsImpl.instanceImpl;
        if (!((Object) instanceImpl != (Object) null))
            return;
        this._bulletPhysicsRigidBody = instanceImpl.CreateRigidBody(this);
    }

    private void OnDestroy()
    {
        if (this._bulletPhysicsRigidBody != null && !this._bulletPhysicsRigidBody.isExpired)
        {
            MMD4MecanimBulletPhysicsImpl instanceImpl = MMD4MecanimBulletPhysicsImpl.instanceImpl;
            if ((Object) instanceImpl != (Object) null)
                instanceImpl.DestroyRigidBody(this._bulletPhysicsRigidBody);
        }
        this._bulletPhysicsRigidBody = (MMD4MecanimBulletPhysicsImpl.RigidBody) null;
    }
}