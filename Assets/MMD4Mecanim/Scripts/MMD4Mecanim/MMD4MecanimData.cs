﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimData
// Assembly: MMD4Mecanim, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: ADBCBBA1-CEE8-40F4-A775-E57AB2D2693F
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Scripts\MMD4Mecanim.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public static class MMD4MecanimData
{
  public const int FullBodyIKTransformElements = 4;
  public const int FullBodyIKTargetMax = 36;

  private static bool _Decl(ref int index, int max)
  {
    if (index < max)
      return true;
    index -= max;
    return false;
  }

  public static MMD4MecanimData.FullBodyIKGroup GetFullBodyIKGroup(
    int fullBodyIKTargetIndex)
  {
    return MMD4MecanimData.GetFullBodyIKGroup(ref fullBodyIKTargetIndex);
  }

  public static MMD4MecanimData.FullBodyIKGroup GetFullBodyIKGroup(
    ref int fullBodyIKTargetIndex)
  {
    if (fullBodyIKTargetIndex >= 0 && fullBodyIKTargetIndex < 36)
    {
      if (MMD4MecanimData._Decl(ref fullBodyIKTargetIndex, 2))
        return MMD4MecanimData.FullBodyIKGroup.Body;
      if (MMD4MecanimData._Decl(ref fullBodyIKTargetIndex, 5))
        return MMD4MecanimData.FullBodyIKGroup.LeftArm;
      if (MMD4MecanimData._Decl(ref fullBodyIKTargetIndex, 5))
        return MMD4MecanimData.FullBodyIKGroup.RightArm;
      if (MMD4MecanimData._Decl(ref fullBodyIKTargetIndex, 5))
        return MMD4MecanimData.FullBodyIKGroup.LeftLeg;
      if (MMD4MecanimData._Decl(ref fullBodyIKTargetIndex, 5))
        return MMD4MecanimData.FullBodyIKGroup.RightLeg;
      if (MMD4MecanimData._Decl(ref fullBodyIKTargetIndex, 5))
        return MMD4MecanimData.FullBodyIKGroup.LeftWristFinger;
      if (MMD4MecanimData._Decl(ref fullBodyIKTargetIndex, 5))
        return MMD4MecanimData.FullBodyIKGroup.RightWristFinger;
      if (MMD4MecanimData._Decl(ref fullBodyIKTargetIndex, 4))
        return MMD4MecanimData.FullBodyIKGroup.Look;
    }
    return MMD4MecanimData.FullBodyIKGroup.Unknown;
  }

  public static string GetFullBodyIKTargetName(int fullBodyIKTargetIndex) => MMD4MecanimData.GetFullBodyIKTargetName(out MMD4MecanimData.FullBodyIKGroup _, fullBodyIKTargetIndex);

  public static string GetFullBodyIKTargetName(
    out MMD4MecanimData.FullBodyIKGroup fullBodyIKGroup,
    int fullBodyIKTargetIndex)
  {
    fullBodyIKGroup = MMD4MecanimData.FullBodyIKGroup.Unknown;
    if (fullBodyIKTargetIndex >= 0 && fullBodyIKTargetIndex < 36)
    {
      fullBodyIKGroup = MMD4MecanimData.GetFullBodyIKGroup(ref fullBodyIKTargetIndex);
      switch (fullBodyIKGroup)
      {
        case MMD4MecanimData.FullBodyIKGroup.Body:
          return ((MMD4MecanimData.FullBodyIKBodyTarget) fullBodyIKTargetIndex).ToString();
        case MMD4MecanimData.FullBodyIKGroup.LeftArm:
          return ((MMD4MecanimData.FullBodyIKArmTarget) fullBodyIKTargetIndex).ToString();
        case MMD4MecanimData.FullBodyIKGroup.RightArm:
          return ((MMD4MecanimData.FullBodyIKArmTarget) fullBodyIKTargetIndex).ToString();
        case MMD4MecanimData.FullBodyIKGroup.LeftLeg:
          return ((MMD4MecanimData.FullBodyIKLegTarget) fullBodyIKTargetIndex).ToString();
        case MMD4MecanimData.FullBodyIKGroup.RightLeg:
          return ((MMD4MecanimData.FullBodyIKLegTarget) fullBodyIKTargetIndex).ToString();
        case MMD4MecanimData.FullBodyIKGroup.LeftWristFinger:
          return ((MMD4MecanimData.FullBodyIKFingerTarget) fullBodyIKTargetIndex).ToString();
        case MMD4MecanimData.FullBodyIKGroup.RightWristFinger:
          return ((MMD4MecanimData.FullBodyIKFingerTarget) fullBodyIKTargetIndex).ToString();
        case MMD4MecanimData.FullBodyIKGroup.Look:
          return ((MMD4MecanimData.FullBodyIKLookTarget) fullBodyIKTargetIndex).ToString();
      }
    }
    return "";
  }

  public static MMD4MecanimData.ModelData BuildModelData(TextAsset modelFile)
  {
    if ((UnityEngine.Object) modelFile == (UnityEngine.Object) null)
    {
      Debug.LogError((object) "BuildModelData: modelFile is norhing.");
      return (MMD4MecanimData.ModelData) null;
    }
    byte[] bytes = modelFile.bytes;
    if (bytes == null || bytes.Length == 0)
    {
      Debug.LogError((object) "BuildModelData: Nothing modelBytes.");
      return (MMD4MecanimData.ModelData) null;
    }
    MMD4MecanimCommon.BinaryReader binaryReader = new MMD4MecanimCommon.BinaryReader(bytes);
    if (!binaryReader.Preparse())
    {
      Debug.LogError((object) "BuildModelData:modelFile is unsupported fomart.");
      return (MMD4MecanimData.ModelData) null;
    }
    MMD4MecanimData.ModelData modelData = new MMD4MecanimData.ModelData();
    binaryReader.BeginHeader();
    modelData.fileType = (MMD4MecanimData.FileType) binaryReader.ReadHeaderInt();
    double num = (double) binaryReader.ReadHeaderFloat();
    binaryReader.ReadHeaderInt();
    modelData.modelAdditionalFlags = (MMD4MecanimData.ModelAdditionalFlags) binaryReader.ReadHeaderInt();
    modelData.vertexCount = binaryReader.ReadHeaderInt();
    binaryReader.ReadHeaderInt();
    modelData.vertexScale = binaryReader.ReadHeaderFloat();
    modelData.importScale = binaryReader.ReadHeaderFloat();
    binaryReader.EndHeader();
    int structListLength = binaryReader.structListLength;
    for (int index = 0; index < structListLength; ++index)
    {
      if (!binaryReader.BeginStructList())
      {
        Debug.LogError((object) "BuildModelData: Parse error.");
        return (MMD4MecanimData.ModelData) null;
      }
      int currentStructFourCc = binaryReader.currentStructFourCC;
      if (currentStructFourCc == MMD4MecanimCommon.BinaryReader.MakeFourCC("BONE"))
      {
        if (!MMD4MecanimData._ParseBoneData(modelData, binaryReader))
        {
          Debug.LogError((object) "BuildModelData: Parse error.");
          return (MMD4MecanimData.ModelData) null;
        }
      }
      else if (currentStructFourCc == MMD4MecanimCommon.BinaryReader.MakeFourCC("IK__"))
      {
        if (!MMD4MecanimData._ParseIKData(modelData, binaryReader))
        {
          Debug.LogError((object) "BuildModelData: Parse error.");
          return (MMD4MecanimData.ModelData) null;
        }
      }
      else if (currentStructFourCc == MMD4MecanimCommon.BinaryReader.MakeFourCC("MRPH"))
      {
        if (!MMD4MecanimData._ParseMorphData(modelData, binaryReader))
        {
          Debug.LogError((object) "BuildModelData: Parse error.");
          return (MMD4MecanimData.ModelData) null;
        }
      }
      else if (currentStructFourCc == MMD4MecanimCommon.BinaryReader.MakeFourCC("RGBD"))
      {
        if (!MMD4MecanimData._ParseRigidBodyData(modelData, binaryReader))
        {
          Debug.LogError((object) "BuildModelData: Parse error.");
          return (MMD4MecanimData.ModelData) null;
        }
      }
      else if (currentStructFourCc == MMD4MecanimCommon.BinaryReader.MakeFourCC("JOIN") && !MMD4MecanimData._ParseJointData(modelData, binaryReader))
      {
        Debug.LogError((object) "BuildModelData: Parse error.");
        return (MMD4MecanimData.ModelData) null;
      }
      if (!binaryReader.EndStructList())
      {
        Debug.LogError((object) "BuildModelData: Parse error.");
        return (MMD4MecanimData.ModelData) null;
      }
    }
    return modelData;
  }

  private static bool _ParseBoneData(
    MMD4MecanimData.ModelData modelData,
    MMD4MecanimCommon.BinaryReader binaryReader)
  {
    modelData.boneDataDictionary = new Dictionary<string, int>();
    modelData.boneDataList = new MMD4MecanimData.BoneData[binaryReader.currentStructLength];
    for (int index = 0; index < binaryReader.currentStructLength; ++index)
    {
      if (!binaryReader.BeginStruct())
        return false;
      MMD4MecanimData.BoneData boneData = new MMD4MecanimData.BoneData();
      boneData.boneAdditionalFlags = (MMD4MecanimData.BoneAdditionalFlags) binaryReader.ReadStructInt();
      boneData.nameJp = binaryReader.GetName(binaryReader.ReadStructInt());
      binaryReader.ReadStructInt();
      boneData.skeletonName = binaryReader.GetName(binaryReader.ReadStructInt());
      boneData.parentBoneID = binaryReader.ReadStructInt();
      boneData.sortedBoneID = binaryReader.ReadStructInt();
      binaryReader.ReadStructInt();
      boneData.originalParentBoneID = binaryReader.ReadStructInt();
      boneData.originalSortedBoneID = binaryReader.ReadStructInt();
      boneData.baseOrigin = binaryReader.ReadStructVector3();
      boneData.baseOrigin.x = -boneData.baseOrigin.x;
      boneData.baseOrigin.z = -boneData.baseOrigin.z;
      if (modelData.fileType == MMD4MecanimData.FileType.PMD)
      {
        boneData.pmdBoneType = (MMD4MecanimData.PMDBoneType) binaryReader.ReadStructInt();
        boneData.childBoneID = binaryReader.ReadStructInt();
        boneData.targetBoneID = binaryReader.ReadStructInt();
        boneData.followCoef = binaryReader.ReadStructFloat();
      }
      else if (modelData.fileType == MMD4MecanimData.FileType.PMX)
      {
        boneData.transformLayerID = binaryReader.ReadStructInt();
        boneData.pmxBoneFlags = (MMD4MecanimData.PMXBoneFlags) binaryReader.ReadStructInt();
        boneData.inherenceParentBoneID = binaryReader.ReadStructInt();
        boneData.inherenceWeight = binaryReader.ReadStructFloat();
        boneData.externalID = binaryReader.ReadStructInt();
      }
      if (!binaryReader.EndStruct())
        return false;
      modelData.boneDataList[index] = boneData;
      if (!string.IsNullOrEmpty(boneData.skeletonName))
        modelData.boneDataDictionary[boneData.skeletonName] = index;
    }
    return true;
  }

  private static Vector3 _ToDegree(Vector3 radian) => new Vector3(radian.x * 57.29578f, radian.y * 57.29578f, radian.z * 57.29578f);

  private static bool _ParseIKData(
    MMD4MecanimData.ModelData modelData,
    MMD4MecanimCommon.BinaryReader binaryReader)
  {
    modelData.ikDataList = new MMD4MecanimData.IKData[binaryReader.currentStructLength];
    for (int index1 = 0; index1 < binaryReader.currentStructLength; ++index1)
    {
      if (!binaryReader.BeginStruct())
        return false;
      MMD4MecanimData.IKData ikData = new MMD4MecanimData.IKData();
      ikData.ikAdditionalFlags = (MMD4MecanimData.IKAdditionalFlags) binaryReader.ReadStructInt();
      ikData.destBoneID = binaryReader.ReadStructInt();
      ikData.targetBoneID = binaryReader.ReadStructInt();
      ikData.iteration = binaryReader.ReadStructInt();
      ikData.angleConstraint = binaryReader.ReadStructFloat();
      int length = binaryReader.ReadStructInt();
      ikData.ikLinkDataList = new MMD4MecanimData.IKLinkData[length];
      for (int index2 = 0; index2 < length; ++index2)
      {
        MMD4MecanimData.IKLinkData ikLinkData = new MMD4MecanimData.IKLinkData();
        ikLinkData.ikLinkBoneID = binaryReader.ReadInt();
        ikLinkData.ikLinkFlags = (MMD4MecanimData.IKLinkFlags) binaryReader.ReadInt();
        if ((ikLinkData.ikLinkFlags & MMD4MecanimData.IKLinkFlags.HasAngleJoint) != MMD4MecanimData.IKLinkFlags.None)
        {
          Vector3 vector3_1 = binaryReader.ReadVector3();
          Vector3 vector3_2 = binaryReader.ReadVector3();
          ikLinkData.lowerLimit = vector3_1;
          ikLinkData.upperLimit = vector3_2;
          ikLinkData.lowerLimit = new Vector3(-vector3_2[0], vector3_1[1], -vector3_2[2]);
          ikLinkData.upperLimit = new Vector3(-vector3_1[0], vector3_2[1], -vector3_1[2]);
          ikLinkData.lowerLimitAsDegree = MMD4MecanimData._ToDegree(ikLinkData.lowerLimit);
          ikLinkData.upperLimitAsDegree = MMD4MecanimData._ToDegree(ikLinkData.upperLimit);
        }
        ikData.ikLinkDataList[index2] = ikLinkData;
      }
      if (!binaryReader.EndStruct())
        return false;
      modelData.ikDataList[index1] = ikData;
    }
    return true;
  }

  private static bool _ParseMorphData(
    MMD4MecanimData.ModelData modelData,
    MMD4MecanimCommon.BinaryReader binaryReader)
  {
    modelData.morphDataDictionaryJp = new Dictionary<string, int>();
    modelData.morphDataDictionaryEn = new Dictionary<string, int>();
    if (modelData.isMorphTranslateName)
      modelData.translatedMorphDataDictionary = new Dictionary<string, int>();
    modelData.morphDataList = new MMD4MecanimData.MorphData[binaryReader.currentStructLength];
    for (int index1 = 0; index1 < binaryReader.currentStructLength; ++index1)
    {
      if (!binaryReader.BeginStruct())
        return false;
      MMD4MecanimData.MorphData morphData = new MMD4MecanimData.MorphData();
      int num1 = binaryReader.ReadStructInt();
      int index2 = binaryReader.ReadStructInt();
      int index3 = binaryReader.ReadStructInt();
      int num2 = binaryReader.ReadStructInt();
      int num3 = binaryReader.ReadStructInt();
      int length = binaryReader.ReadStructInt();
      int index4 = binaryReader.ReadStructInt();
      morphData.nameJp = binaryReader.GetName(index2);
      morphData.nameEn = binaryReader.GetName(index3);
      if (modelData.isMorphTranslateName)
        morphData.translatedName = binaryReader.GetName(index4);
      morphData.morphCategory = (MMD4MecanimData.MorphCategory) num2;
      morphData.morphType = (MMD4MecanimData.MorphType) num3;
      if ((num1 & 1) != 0)
        morphData.isMorphBaseVertex = true;
      switch (num3)
      {
        case 0:
          morphData.indices = new int[length];
          morphData.weights = new float[length];
          for (int index5 = 0; index5 < length; ++index5)
          {
            morphData.indices[index5] = binaryReader.ReadInt();
            morphData.weights[index5] = binaryReader.ReadFloat();
          }
          break;
        case 8:
          morphData.materialData = new MMD4MecanimData.MorphMaterialData[length];
          for (int index6 = 0; index6 < length; ++index6)
          {
            MMD4MecanimData.MorphMaterialData morphMaterialData = new MMD4MecanimData.MorphMaterialData();
            morphMaterialData.materialID = binaryReader.ReadInt();
            morphMaterialData.operation = (MMD4MecanimData.MorphMaterialOperation) binaryReader.ReadInt();
            morphMaterialData.diffuse = binaryReader.ReadColor();
            morphMaterialData.specular = binaryReader.ReadColorRGB();
            morphMaterialData.shininess = binaryReader.ReadFloat();
            morphMaterialData.ambient = binaryReader.ReadColorRGB();
            morphMaterialData.edgeColor = binaryReader.ReadColor();
            morphMaterialData.edgeSize = binaryReader.ReadFloat();
            morphMaterialData.textureColor = binaryReader.ReadColor();
            morphMaterialData.sphereColor = binaryReader.ReadColor();
            morphMaterialData.toonTextureColor = binaryReader.ReadColor();
            if (morphMaterialData.operation == MMD4MecanimData.MorphMaterialOperation.Adding)
            {
              morphMaterialData.specular.a = 0.0f;
              morphMaterialData.ambient.a = 0.0f;
            }
            morphData.materialData[index6] = morphMaterialData;
          }
          break;
      }
      if (!binaryReader.EndStruct())
        return false;
      modelData.morphDataList[index1] = morphData;
      if (!string.IsNullOrEmpty(morphData.nameJp))
        modelData.morphDataDictionaryJp[morphData.nameJp] = index1;
      if (!string.IsNullOrEmpty(morphData.nameEn))
        modelData.morphDataDictionaryEn[morphData.nameEn] = index1;
      if (modelData.isMorphTranslateName && !string.IsNullOrEmpty(morphData.translatedName))
        modelData.translatedMorphDataDictionary[morphData.translatedName] = index1;
    }
    return true;
  }

  private static bool _ParseRigidBodyData(
    MMD4MecanimData.ModelData modelData,
    MMD4MecanimCommon.BinaryReader binaryReader)
  {
    modelData.rigidBodyDataList = new MMD4MecanimData.RigidBodyData[binaryReader.currentStructLength];
    for (int index = 0; index < binaryReader.currentStructLength; ++index)
    {
      if (!binaryReader.BeginStruct())
        return false;
      MMD4MecanimData.RigidBodyData rigidBodyData = new MMD4MecanimData.RigidBodyData();
      int num = binaryReader.ReadStructInt();
      rigidBodyData.nameJp = binaryReader.GetName(binaryReader.ReadStructInt());
      rigidBodyData.nameEn = binaryReader.GetName(binaryReader.ReadStructInt());
      rigidBodyData.boneID = binaryReader.ReadStructInt();
      rigidBodyData.collisionGroupID = binaryReader.ReadStructInt();
      rigidBodyData.collisionMask = binaryReader.ReadStructInt();
      rigidBodyData.shapeType = (MMD4MecanimData.ShapeType) binaryReader.ReadStructInt();
      rigidBodyData.rigidBodyType = (MMD4MecanimData.RigidBodyType) binaryReader.ReadStructInt();
      rigidBodyData.shapeSize = binaryReader.ReadStructVector3();
      rigidBodyData.position = binaryReader.ReadStructVector3();
      rigidBodyData.rotation = binaryReader.ReadStructVector3();
      rigidBodyData.mass = binaryReader.ReadStructFloat();
      rigidBodyData.linearDamping = binaryReader.ReadStructFloat();
      rigidBodyData.angularDamping = binaryReader.ReadStructFloat();
      rigidBodyData.restitution = binaryReader.ReadStructFloat();
      rigidBodyData.friction = binaryReader.ReadStructFloat();
      rigidBodyData.rigidBodyAdditionalFlags = (MMD4MecanimData.RigidBodyAdditionalFlags) num;
      modelData.rigidBodyDataList[index] = rigidBodyData;
      if (!binaryReader.EndStruct())
        return false;
    }
    return true;
  }

  private static bool _ParseJointData(
    MMD4MecanimData.ModelData modelData,
    MMD4MecanimCommon.BinaryReader binaryReader)
  {
    modelData.jointDataList = new MMD4MecanimData.JointData[binaryReader.currentStructLength];
    for (int index = 0; index < binaryReader.currentStructLength; ++index)
    {
      if (!binaryReader.BeginStruct())
        return false;
      MMD4MecanimData.JointData jointData = new MMD4MecanimData.JointData();
      binaryReader.ReadStructInt();
      binaryReader.ReadStructInt();
      binaryReader.ReadStructInt();
      binaryReader.ReadStructInt();
      jointData.rigidBodyIDA = binaryReader.ReadStructInt();
      jointData.rigidBodyIDB = binaryReader.ReadStructInt();
      modelData.jointDataList[index] = jointData;
      if (!binaryReader.EndStruct())
        return false;
    }
    return true;
  }

  public static MMD4MecanimData.ExtraData BuildExtraData(TextAsset extraFile)
  {
    if ((UnityEngine.Object) extraFile == (UnityEngine.Object) null)
    {
      Debug.LogError((object) "BuildExtraData: extraFile is norhing.");
      return (MMD4MecanimData.ExtraData) null;
    }
    byte[] bytes = extraFile.bytes;
    if (bytes == null || bytes.Length == 0)
    {
      Debug.LogError((object) "BuildModelData: Nothing extraBytes.");
      return (MMD4MecanimData.ExtraData) null;
    }
    MMD4MecanimCommon.BinaryReader binaryReader = new MMD4MecanimCommon.BinaryReader(bytes);
    if (!binaryReader.Preparse())
    {
      Debug.LogError((object) "BuildModelData:extraFile is unsupported fomart.");
      return (MMD4MecanimData.ExtraData) null;
    }
    MMD4MecanimData.ExtraData extraData = new MMD4MecanimData.ExtraData();
    binaryReader.BeginHeader();
    binaryReader.ReadHeaderInt();
    double num = (double) binaryReader.ReadHeaderFloat();
    binaryReader.ReadHeaderInt();
    binaryReader.ReadHeaderInt();
    extraData.vertexCount = binaryReader.ReadHeaderInt();
    binaryReader.ReadHeaderInt();
    extraData.vertexScale = binaryReader.ReadHeaderFloat();
    extraData.importScale = binaryReader.ReadHeaderFloat();
    binaryReader.EndHeader();
    int structListLength = binaryReader.structListLength;
    for (int index = 0; index < structListLength; ++index)
    {
      if (!binaryReader.BeginStructList())
      {
        Debug.LogError((object) "BuildExtraData: Parse error.");
        return (MMD4MecanimData.ExtraData) null;
      }
      if (binaryReader.currentStructFourCC == MMD4MecanimCommon.BinaryReader.MakeFourCC("VTX_") && !MMD4MecanimData._ParseVertexData(extraData, binaryReader))
      {
        Debug.LogError((object) "BuildExtraData: Parse error.");
        return (MMD4MecanimData.ExtraData) null;
      }
      if (!binaryReader.EndStructList())
      {
        Debug.LogError((object) "BuildExtraData: Parse error.");
        return (MMD4MecanimData.ExtraData) null;
      }
    }
    return extraData;
  }

  private static bool _ParseVertexData(
    MMD4MecanimData.ExtraData extraData,
    MMD4MecanimCommon.BinaryReader binaryReader)
  {
    extraData.vertexDataList = new MMD4MecanimData.VertexData[binaryReader.currentStructLength];
    for (int index = 0; index < binaryReader.currentStructLength; ++index)
    {
      if (!binaryReader.BeginStruct())
        return false;
      MMD4MecanimData.VertexData vertexData = new MMD4MecanimData.VertexData();
      vertexData.flags = (MMD4MecanimData.VertexFlags) binaryReader.ReadStructInt();
      vertexData.position = binaryReader.ReadStructVector3();
      vertexData.normal = binaryReader.ReadStructVector3();
      vertexData.uv = binaryReader.ReadStructVector2();
      vertexData.edgeScale = binaryReader.ReadStructFloat();
      vertexData.boneTransform = (MMD4MecanimData.BoneTransform) binaryReader.ReadStructByte();
      vertexData.boneWeight = new BoneWeight();
      switch (vertexData.boneTransform)
      {
        case MMD4MecanimData.BoneTransform.BDEF1:
          vertexData.boneWeight.boneIndex0 = binaryReader.ReadInt();
          vertexData.boneWeight.weight0 = 1f;
          break;
        case MMD4MecanimData.BoneTransform.BDEF2:
        case MMD4MecanimData.BoneTransform.SDEF:
          vertexData.boneWeight.boneIndex0 = binaryReader.ReadInt();
          vertexData.boneWeight.boneIndex1 = binaryReader.ReadInt();
          vertexData.boneWeight.weight0 = binaryReader.ReadFloat();
          vertexData.boneWeight.weight1 = 1f - vertexData.boneWeight.weight0;
          break;
        case MMD4MecanimData.BoneTransform.BDEF4:
        case MMD4MecanimData.BoneTransform.QDEF:
          vertexData.boneWeight.boneIndex0 = binaryReader.ReadInt();
          vertexData.boneWeight.boneIndex1 = binaryReader.ReadInt();
          vertexData.boneWeight.boneIndex2 = binaryReader.ReadInt();
          vertexData.boneWeight.boneIndex3 = binaryReader.ReadInt();
          vertexData.boneWeight.weight0 = binaryReader.ReadFloat();
          vertexData.boneWeight.weight1 = binaryReader.ReadFloat();
          vertexData.boneWeight.weight2 = binaryReader.ReadFloat();
          vertexData.boneWeight.weight3 = binaryReader.ReadFloat();
          break;
      }
      if (vertexData.boneTransform == MMD4MecanimData.BoneTransform.SDEF)
      {
        vertexData.sdefC = binaryReader.ReadVector3();
        vertexData.sdefR0 = binaryReader.ReadVector3();
        vertexData.sdefR1 = binaryReader.ReadVector3();
      }
      if (!binaryReader.EndStruct())
        return false;
      extraData.vertexDataList[index] = vertexData;
    }
    return true;
  }

  public static MMD4MecanimData.AnimData BuildAnimData(TextAsset animFile)
  {
    if ((UnityEngine.Object) animFile == (UnityEngine.Object) null)
    {
      Debug.LogError((object) "BuildAnimData: animFile is norhing.");
      return (MMD4MecanimData.AnimData) null;
    }
    byte[] bytes = animFile.bytes;
    if (bytes == null || bytes.Length == 0)
    {
      Debug.LogError((object) "BuildAnimData: Nothing animBytes.");
      return (MMD4MecanimData.AnimData) null;
    }
    MMD4MecanimCommon.BinaryReader binaryReader = new MMD4MecanimCommon.BinaryReader(bytes);
    if (!binaryReader.Preparse())
    {
      Debug.LogError((object) "BuildAnimData:animFile is unsupported fomart.");
      return (MMD4MecanimData.AnimData) null;
    }
    MMD4MecanimData.AnimData animData = new MMD4MecanimData.AnimData();
    binaryReader.BeginHeader();
    binaryReader.ReadHeaderInt();
    binaryReader.ReadHeaderInt();
    animData.maxFrame = binaryReader.ReadHeaderInt();
    binaryReader.EndHeader();
    int structListLength = binaryReader.structListLength;
    for (int index = 0; index < structListLength; ++index)
    {
      if (!binaryReader.BeginStructList())
      {
        Debug.LogError((object) "BuildAnimData: Parse error.");
        return (MMD4MecanimData.AnimData) null;
      }
      if (binaryReader.currentStructFourCC == MMD4MecanimCommon.BinaryReader.MakeFourCC("MRPH") && !MMD4MecanimData._ParseMorphMotionData(animData, binaryReader))
      {
        Debug.LogError((object) "BuildAnimData: Parse error.");
        return (MMD4MecanimData.AnimData) null;
      }
      if (!binaryReader.EndStructList())
      {
        Debug.LogError((object) "BuildAnimData: Parse error.");
        return (MMD4MecanimData.AnimData) null;
      }
    }
    return animData;
  }

  private static bool _ParseMorphMotionData(
    MMD4MecanimData.AnimData animData,
    MMD4MecanimCommon.BinaryReader binaryReader)
  {
    animData.morphMotionDataList = new MMD4MecanimData.MorphMotionData[binaryReader.currentStructLength];
    for (int index1 = 0; index1 < binaryReader.currentStructLength; ++index1)
    {
      if (!binaryReader.BeginStruct())
        return false;
      MMD4MecanimData.MorphMotionData morphMotionData = new MMD4MecanimData.MorphMotionData();
      binaryReader.ReadStructInt();
      morphMotionData.name = binaryReader.GetName(binaryReader.ReadStructInt());
      int length = binaryReader.ReadStructInt();
      if (length < 0)
        return false;
      morphMotionData.frameNos = new int[length];
      morphMotionData.f_frameNos = new float[length];
      morphMotionData.weights = new float[length];
      for (int index2 = 0; index2 < length; ++index2)
      {
        morphMotionData.frameNos[index2] = binaryReader.ReadInt();
        morphMotionData.weights[index2] = binaryReader.ReadFloat();
        morphMotionData.f_frameNos[index2] = (float) morphMotionData.frameNos[index2];
      }
      animData.morphMotionDataList[index1] = morphMotionData;
      if (!binaryReader.EndStruct())
        return false;
    }
    return true;
  }

  public enum FileType
  {
    None,
    PMD,
    PMX,
  }

  public enum MorphCategory
  {
    Base,
    EyeBrow,
    Eye,
    Lip,
    Other,
    Max,
  }

  public enum ShapeType
  {
    Sphere,
    Box,
    Capsule,
  }

  public enum RigidBodyType
  {
    Kinematics,
    Simulated,
    SimulatedAligned,
  }

  public enum BoneTransform
  {
    Unknown = -1, // 0xFFFFFFFF
    BDEF1 = 0,
    BDEF2 = 1,
    BDEF4 = 2,
    SDEF = 3,
    QDEF = 4,
  }

  public enum MorphType
  {
    Group,
    Vertex,
    Bone,
    UV,
    UVA1,
    UVA2,
    UVA3,
    UVA4,
    Material,
  }

  public enum FullBodyIKGroup
  {
    Unknown = -1, // 0xFFFFFFFF
    Body = 0,
    LeftArm = 1,
    RightArm = 2,
    LeftLeg = 3,
    RightLeg = 4,
    LeftWristFinger = 5,
    RightWristFinger = 6,
    Look = 7,
    Max = 8,
  }

  public enum FullBodyIKBodyTarget
  {
    Torso,
    Neck,
    Max,
  }

  public enum FullBodyIKArmTarget
  {
    Arm,
    Elbow,
    Wrist,
    WristUpward,
    WristForward,
    Max,
  }

  public enum FullBodyIKLegTarget
  {
    Hip,
    Knee,
    Foot,
    FootUpward,
    FootForward,
    Max,
  }

  public enum FullBodyIKFingerTarget
  {
    Thumb,
    Index,
    Middle,
    Ring,
    Little,
    Max,
  }

  public enum FullBodyIKLookTarget
  {
    Head,
    HeadUpward,
    HeadForward,
    Eyes,
    Max,
  }

  public enum MorphAutoLumninousType
  {
    None,
    LightUp,
    LightOff,
    LightBlink,
    LightBS,
  }

  public enum PMDBoneType
  {
    Rotate,
    RotateAndMove,
    IKDestination,
    Unknown,
    UnderIK,
    UnderRotate,
    IKTarget,
    NoDisp,
    Twist,
    FollowRotate,
  }

  [Flags]
  public enum BoneAdditionalFlags
  {
    None = 0,
    IsKnee = 1,
    BoneTypeMask = -16777216, // 0xFF000000
    BoneTypeRoot = -2147483648, // 0x80000000
  }

  [Flags]
  public enum RigidBodyAdditionalFlags
  {
    None = 0,
    IsFreezed = 1,
  }

  [Flags]
  public enum PMXBoneFlags
  {
    None = 0,
    Destination = 1,
    Rotate = 2,
    Translate = 4,
    Visible = 8,
    Controllable = 16, // 0x00000010
    IK = 32, // 0x00000020
    IKChild = 64, // 0x00000040
    InherenceLocal = 128, // 0x00000080
    InherenceRotate = 256, // 0x00000100
    InherenceTranslate = 512, // 0x00000200
    FixedAxis = 1024, // 0x00000400
    LocalAxis = 2048, // 0x00000800
    TransformAfterPhysics = 4096, // 0x00001000
    TransformExternalParent = 8192, // 0x00002000
  }

  [Flags]
  public enum ModelAdditionalFlags
  {
    None = 0,
    MorphTranslateName = 1,
  }

  [Flags]
  public enum IKAdditionalFlags
  {
  }

  [Flags]
  public enum IKLinkFlags
  {
    None = 0,
    HasAngleJoint = 1,
  }

  [Flags]
  public enum PMXBoneFlag
  {
    None = 0,
    Destination = 1,
    Rotate = 2,
    Translate = 4,
    Visible = 8,
    Controllable = 16, // 0x00000010
    IK = 32, // 0x00000020
    IKChild = 64, // 0x00000040
    InherenceLocal = 128, // 0x00000080
    InherenceRotate = 256, // 0x00000100
    InherenceTranslate = 512, // 0x00000200
    FixedAxis = 1024, // 0x00000400
    LocalAxis = 2048, // 0x00000800
    TransformAfterPhysics = 4096, // 0x00001000
    TransformExternalParent = 8192, // 0x00002000
  }

  [Flags]
  public enum MeshFlags
  {
    None = 0,
    BlendShapes = 1,
    VertexMorph = 2,
    XDEF = 4,
  }

  [Serializable]
  public class BoneData
  {
    public MMD4MecanimData.BoneAdditionalFlags boneAdditionalFlags;
    public string nameJp;
    public string skeletonName;
    public int parentBoneID;
    public int sortedBoneID;
    public int originalParentBoneID;
    public int originalSortedBoneID;
    public Vector3 baseOrigin;
    public MMD4MecanimData.PMDBoneType pmdBoneType;
    public int childBoneID;
    public int targetBoneID;
    public float followCoef;
    public int transformLayerID;
    public MMD4MecanimData.PMXBoneFlags pmxBoneFlags;
    public int inherenceParentBoneID;
    public float inherenceWeight;
    public int externalID;

    public bool isKnee => (uint) (this.boneAdditionalFlags & MMD4MecanimData.BoneAdditionalFlags.IsKnee) > 0U;

    public bool isRootBone => (this.boneAdditionalFlags & MMD4MecanimData.BoneAdditionalFlags.BoneTypeMask) == MMD4MecanimData.BoneAdditionalFlags.BoneTypeRoot;
  }

  [Serializable]
  public class IKLinkData
  {
    public int ikLinkBoneID;
    public MMD4MecanimData.IKLinkFlags ikLinkFlags;
    public Vector3 lowerLimit;
    public Vector3 upperLimit;
    public Vector3 lowerLimitAsDegree;
    public Vector3 upperLimitAsDegree;

    public bool hasAngleJoint => (uint) (this.ikLinkFlags & MMD4MecanimData.IKLinkFlags.HasAngleJoint) > 0U;
  }

  [Serializable]
  public class IKData
  {
    public MMD4MecanimData.IKAdditionalFlags ikAdditionalFlags;
    public int destBoneID;
    public int targetBoneID;
    public int iteration;
    public float angleConstraint;
    public MMD4MecanimData.IKLinkData[] ikLinkDataList;
  }

  [Serializable]
  public class RigidBodyData
  {
    public MMD4MecanimData.RigidBodyAdditionalFlags rigidBodyAdditionalFlags;
    public string nameJp;
    public string nameEn;
    public int boneID;
    public int collisionGroupID;
    public int collisionMask;
    public MMD4MecanimData.ShapeType shapeType;
    public MMD4MecanimData.RigidBodyType rigidBodyType;
    public Vector3 shapeSize;
    public Vector3 position;
    public Vector3 rotation;
    public float mass;
    public float linearDamping;
    public float angularDamping;
    public float restitution;
    public float friction;

    public bool isFreezed => (uint) (this.rigidBodyAdditionalFlags & MMD4MecanimData.RigidBodyAdditionalFlags.IsFreezed) > 0U;
  }

  [Serializable]
  public class JointData
  {
    public int rigidBodyIDA;
    public int rigidBodyIDB;
  }

  public enum MorphMaterialOperation
  {
    Multiply,
    Adding,
  }

  public struct MorphMaterialData
  {
    public int materialID;
    public MMD4MecanimData.MorphMaterialOperation operation;
    public Color diffuse;
    public Color specular;
    public float shininess;
    public Color ambient;
    public Color edgeColor;
    public float edgeScale;
    public float edgeSize;
    public Color textureColor;
    public Color sphereColor;
    public Color toonTextureColor;
    public float tessEdgeLength;
    public float tessPhongStrength;
    public float tessExtrusionAmount;
    public float alPower;
  }

  [Serializable]
  public class MorphData
  {
    public string nameJp;
    public string nameEn;
    public string translatedName;
    public MMD4MecanimData.MorphCategory morphCategory;
    public MMD4MecanimData.MorphType morphType;
    [NonSerialized]
    public bool isMorphBaseVertex;
    [NonSerialized]
    public int[] indices;
    [NonSerialized]
    public float[] weights;
    [NonSerialized]
    public Vector3[] positions;
    [NonSerialized]
    public MMD4MecanimData.MorphMaterialData[] materialData;
  }

  public class ModelData
  {
    public MMD4MecanimData.FileType fileType;
    public MMD4MecanimData.ModelAdditionalFlags modelAdditionalFlags;
    public int vertexCount;
    public float vertexScale;
    public float importScale;
    public MMD4MecanimData.BoneData[] boneDataList;
    public Dictionary<string, int> boneDataDictionary;
    public MMD4MecanimData.IKData[] ikDataList;
    public MMD4MecanimData.MorphData[] morphDataList;
    public Dictionary<string, int> morphDataDictionaryJp;
    public Dictionary<string, int> morphDataDictionaryEn;
    public Dictionary<string, int> translatedMorphDataDictionary;
    public MMD4MecanimData.RigidBodyData[] rigidBodyDataList;
    public MMD4MecanimData.JointData[] jointDataList;

    public Dictionary<string, int> morphDataDictionary => this.morphDataDictionaryJp;

    public bool isMorphTranslateName => (uint) (this.modelAdditionalFlags & MMD4MecanimData.ModelAdditionalFlags.MorphTranslateName) > 0U;

    public int GetMorphDataIndex(string morphName, bool isStartsWith) => this.GetMorphDataIndexJp(morphName, isStartsWith);

    public int GetMorphDataIndexJp(string morphName, bool isStartsWith)
    {
      if (morphName != null && this.morphDataList != null && this.morphDataDictionaryJp != null && this.morphDataDictionaryJp.Count > 0)
      {
        int morphDataIndexJp1 = 0;
        if (this.morphDataDictionaryJp.TryGetValue(morphName, out morphDataIndexJp1))
          return morphDataIndexJp1;
        if (isStartsWith)
        {
          for (int morphDataIndexJp2 = 0; morphDataIndexJp2 < this.morphDataList.Length; ++morphDataIndexJp2)
          {
            if (this.morphDataList[morphDataIndexJp2].nameJp != null && this.morphDataList[morphDataIndexJp2].nameJp.StartsWith(morphName))
              return morphDataIndexJp2;
          }
        }
      }
      return -1;
    }

    public int GetMorphDataIndexEn(string morphName, bool isStartsWith)
    {
      if (morphName != null && this.morphDataList != null && this.morphDataDictionaryEn != null && this.morphDataDictionaryEn.Count > 0)
      {
        int morphDataIndexEn1 = 0;
        if (this.morphDataDictionaryEn.TryGetValue(morphName, out morphDataIndexEn1))
          return morphDataIndexEn1;
        if (isStartsWith)
        {
          for (int morphDataIndexEn2 = 0; morphDataIndexEn2 < this.morphDataList.Length; ++morphDataIndexEn2)
          {
            if (this.morphDataList[morphDataIndexEn2].nameEn != null && this.morphDataList[morphDataIndexEn2].nameEn.StartsWith(morphName))
              return morphDataIndexEn2;
          }
        }
      }
      return -1;
    }

    public int GetTranslatedMorphDataIndex(string morphName, bool isStartsWith)
    {
      if (morphName != null && this.morphDataList != null && this.translatedMorphDataDictionary != null && this.translatedMorphDataDictionary.Count > 0)
      {
        int translatedMorphDataIndex1 = 0;
        if (this.translatedMorphDataDictionary.TryGetValue(morphName, out translatedMorphDataIndex1))
          return translatedMorphDataIndex1;
        if (isStartsWith)
        {
          for (int translatedMorphDataIndex2 = 0; translatedMorphDataIndex2 < this.morphDataList.Length; ++translatedMorphDataIndex2)
          {
            if (this.morphDataList[translatedMorphDataIndex2].translatedName != null && this.morphDataList[translatedMorphDataIndex2].translatedName.StartsWith(morphName))
              return translatedMorphDataIndex2;
          }
        }
      }
      return -1;
    }

    public MMD4MecanimData.MorphData GetMorphData(
      string morphName,
      bool isStartsWith)
    {
      int morphDataIndex = this.GetMorphDataIndex(morphName, isStartsWith);
      return morphDataIndex != -1 ? this.morphDataList[morphDataIndex] : (MMD4MecanimData.MorphData) null;
    }

    public MMD4MecanimData.MorphData GetMorphData(string morphName) => this.GetMorphData(morphName, false);
  }

  [Flags]
  public enum VertexFlags
  {
    None = 0,
    NoEdge = 1,
  }

  public struct VertexData
  {
    public MMD4MecanimData.VertexFlags flags;
    public Vector3 position;
    public Vector3 normal;
    public Vector2 uv;
    public float edgeScale;
    public MMD4MecanimData.BoneTransform boneTransform;
    public BoneWeight boneWeight;
    public Vector3 sdefC;
    public Vector3 sdefR0;
    public Vector3 sdefR1;
  }

  public class ExtraData
  {
    public int vertexCount;
    public float vertexScale;
    public float importScale;
    public MMD4MecanimData.VertexData[] vertexDataList;
  }

  [Flags]
  public enum MorphMotionAdditionalFlags
  {
    None = 0,
    NameIsFull = 1,
  }

  public class MorphMotionData
  {
    public string name;
    public MMD4MecanimData.MorphMotionAdditionalFlags morphMotionAdditionalFlags;
    public int[] frameNos;
    public float[] f_frameNos;
    public float[] weights;

    public bool nameIsFull => (uint) (this.morphMotionAdditionalFlags & MMD4MecanimData.MorphMotionAdditionalFlags.NameIsFull) > 0U;
  }

  [Flags]
  public enum AnimAdditionalFlags
  {
    None = 0,
    SupportNameIsFull = 1,
  }

  public class AnimData
  {
    public MMD4MecanimData.AnimAdditionalFlags animAdditionalFlags;
    public int maxFrame;
    public MMD4MecanimData.MorphMotionData[] morphMotionDataList;

    public bool supportNameIsFull => (uint) (this.animAdditionalFlags & MMD4MecanimData.AnimAdditionalFlags.SupportNameIsFull) > 0U;
  }
}
