﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimInternal.Bullet.RigidBodyProperty
// Assembly: MMD4Mecanim, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: ADBCBBA1-CEE8-40F4-A775-E57AB2D2693F
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Scripts\MMD4Mecanim.dll

using System;

namespace MMD4MecanimInternal.Bullet
{
    [Serializable]
    public class RigidBodyProperty
    {
        public bool isKinematic = true;
        public bool isFreezed;
        public bool isAdditionalDamping = true;
        public float mass = 1f;
        public float linearDamping = 0.5f;
        public float angularDamping = 0.5f;
        public float restitution = 0.5f;
        public float friction = 0.5f;
    }
}