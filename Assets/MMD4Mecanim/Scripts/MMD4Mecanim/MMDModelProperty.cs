﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimInternal.Bullet.MMDModelProperty
// Assembly: MMD4Mecanim, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: ADBCBBA1-CEE8-40F4-A775-E57AB2D2693F
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Scripts\MMD4Mecanim.dll

using System;
using UnityEngine;

namespace MMD4MecanimInternal.Bullet
{
  [Serializable]
  public class MMDModelProperty
  {
    public float worldScale;
    public float importScale;
    public Vector3 lossyScale = Vector3.one;
    public int keepIKTargetBoneFlag = -1;
    public int forceIKResetBoneFlag = -1;
    public int enableIKSecondPassFlag = -1;
    public float secondPassLimitAngle = -1f;
    public int enableIKInnerLockFlag = -1;
    public int enableIKInnerLockKneeFlag = -1;
    public float innerLockKneeClamp = -1f;
    public float innerLockKneeRatioL = -1f;
    public float innerLockKneeRatioU = -1f;
    public float innerLockKneeScale = -1f;
    public int enableIKMuscleFlag = -1;
    public int enableIKMuscleHipFlag = -1;
    public int enableIKMuscleFootFlag = -1;
    public float muscleHipUpperXAngle = -1f;
    public float muscleHipLowerXAngle = -1f;
    public float muscleHipInnerYAngle = -1f;
    public float muscleHipOuterYAngle = -1f;
    public float muscleHipInnerZAngle = -1f;
    public float muscleHipOuterZAngle = -1f;
    public float muscleFootUpperXAngle = -1f;
    public float muscleFootLowerXAngle = -1f;
    public float muscleFootInnerYAngle = -1f;
    public float muscleFootOuterYAngle = -1f;
    public float muscleFootInnerZAngle = -1f;
    public float muscleFootOuterZAngle = -1f;
    public bool rigidBodyIsAdditionalDamping = true;
    public bool rigidBodyIsEnableSleeping;
    public bool rigidBodyIsUseCcd = true;
    public float rigidBodyCcdMotionThreshold = 0.1f;
    public float rigidBodyShapeScale = 1f;
    public float rigidBodyMassRate = 1f;
    public float rigidBodyLinearDampingRate = 1f;
    public float rigidBodyAngularDampingRate = 1f;
    public float rigidBodyRestitutionRate = 1f;
    public float rigidBodyFrictionRate = 1f;
    public float rigidBodyAntiJitterRate = 1f;
    public float rigidBodyAntiJitterRateOnKinematic = 1f;
    public float rigidBodyPreBoneAlignmentLimitLength = 0.01f;
    public float rigidBodyPreBoneAlignmentLossRate = 0.01f;
    public float rigidBodyPostBoneAlignmentLimitLength = 0.01f;
    public float rigidBodyPostBoneAlignmentLossRate = 0.01f;
    public float rigidBodyLinearDampingLossRate = -1f;
    public float rigidBodyLinearDampingLimit = -1f;
    public float rigidBodyAngularDampingLossRate = -1f;
    public float rigidBodyAngularDampingLimit = -1f;
    public float rigidBodyLinearVelocityLimit = -1f;
    public float rigidBodyAngularVelocityLimit = -1f;
    public bool rigidBodyIsUseForceAngularVelocityLimit = true;
    public bool rigidBodyIsUseForceAngularAccelerationLimit = true;
    public float rigidBodyForceAngularVelocityLimit = 600f;
    public bool rigidBodyIsAdditionalCollider;
    public float rigidBodyAdditionalColliderBias = 0.99f;
    public bool rigidBodyIsForceTranslate = true;
    public float jointRootAdditionalLimitAngle = -1f;
    public float jointAdditionalLimitAngle = -1f;
    public bool jointAdditionalLimitForcibly;

    public MMDModelProperty Clone() => (MMDModelProperty) this.MemberwiseClone();

    public void Postfix()
    {
      if (this.keepIKTargetBoneFlag == -1)
        this.keepIKTargetBoneFlag = 0;
      if (this.forceIKResetBoneFlag == -1)
        this.forceIKResetBoneFlag = 0;
      if (this.enableIKSecondPassFlag == -1)
        this.enableIKSecondPassFlag = 0;
      if ((double) this.secondPassLimitAngle < 0.0)
        this.secondPassLimitAngle = 20f;
      if (this.enableIKInnerLockFlag == -1)
        this.enableIKInnerLockFlag = 0;
      if (this.enableIKInnerLockKneeFlag == -1)
        this.enableIKInnerLockKneeFlag = 1;
      if ((double) this.innerLockKneeClamp < 0.0)
        this.innerLockKneeClamp = 1f / 16f;
      if ((double) this.innerLockKneeRatioL < 0.0)
        this.innerLockKneeRatioL = 0.4f;
      if ((double) this.innerLockKneeRatioU < 0.0)
        this.innerLockKneeRatioU = 0.1f;
      if ((double) this.innerLockKneeScale < 0.0)
        this.innerLockKneeScale = 8f;
      if (this.enableIKMuscleFlag == -1)
        this.enableIKMuscleFlag = 0;
      if (this.enableIKMuscleHipFlag == -1)
        this.enableIKMuscleHipFlag = 1;
      if (this.enableIKMuscleFootFlag == -1)
        this.enableIKMuscleFootFlag = 1;
      if ((double) this.muscleHipUpperXAngle < 0.0)
        this.muscleHipUpperXAngle = 176f;
      if ((double) this.muscleHipLowerXAngle < 0.0)
        this.muscleHipLowerXAngle = 86f;
      if ((double) this.muscleHipInnerYAngle < 0.0)
        this.muscleHipInnerYAngle = 45f;
      if ((double) this.muscleHipOuterYAngle < 0.0)
        this.muscleHipOuterYAngle = 90f;
      if ((double) this.muscleHipInnerZAngle < 0.0)
        this.muscleHipInnerZAngle = 30f;
      if ((double) this.muscleHipOuterZAngle < 0.0)
        this.muscleHipOuterZAngle = 90f;
      if ((double) this.muscleFootUpperXAngle < 0.0)
        this.muscleFootUpperXAngle = 70f;
      if ((double) this.muscleFootLowerXAngle < 0.0)
        this.muscleFootLowerXAngle = 90f;
      if ((double) this.muscleFootInnerYAngle < 0.0)
        this.muscleFootInnerYAngle = 25f;
      if ((double) this.muscleFootOuterYAngle < 0.0)
        this.muscleFootOuterYAngle = 25f;
      if ((double) this.muscleFootInnerZAngle < 0.0)
        this.muscleFootInnerZAngle = 12.5f;
      if ((double) this.muscleFootOuterZAngle < 0.0)
        this.muscleFootOuterZAngle = 0.0f;
      this.secondPassLimitAngle *= (float) Math.PI / 180f;
      this.muscleHipUpperXAngle *= (float) Math.PI / 180f;
      this.muscleHipLowerXAngle *= (float) Math.PI / 180f;
      this.muscleHipInnerYAngle *= (float) Math.PI / 180f;
      this.muscleHipOuterYAngle *= (float) Math.PI / 180f;
      this.muscleHipInnerZAngle *= (float) Math.PI / 180f;
      this.muscleHipOuterZAngle *= (float) Math.PI / 180f;
      this.muscleFootUpperXAngle *= (float) Math.PI / 180f;
      this.muscleFootLowerXAngle *= (float) Math.PI / 180f;
      this.muscleFootInnerYAngle *= (float) Math.PI / 180f;
      this.muscleFootOuterYAngle *= (float) Math.PI / 180f;
      this.muscleFootInnerZAngle *= (float) Math.PI / 180f;
      this.muscleFootOuterZAngle *= (float) Math.PI / 180f;
    }
  }
}
