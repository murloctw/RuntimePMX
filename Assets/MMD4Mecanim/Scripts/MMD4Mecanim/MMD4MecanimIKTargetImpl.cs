﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimIKTargetImpl
// Assembly: MMD4Mecanim, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: ADBCBBA1-CEE8-40F4-A775-E57AB2D2693F
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Scripts\MMD4Mecanim.dll

using UnityEngine;

public class MMD4MecanimIKTargetImpl : MonoBehaviour
{
    public MMD4MecanimModelImpl model;
    public bool ikEnabled = true;
    public float ikWeight = 1f;
    private Transform _transform;

    public Matrix4x4 localToWorldMatrix
    {
        get
        {
            if ((Object) this._transform == (Object) null)
                this._transform = this.transform;
            return this._transform.localToWorldMatrix;
        }
    }
}