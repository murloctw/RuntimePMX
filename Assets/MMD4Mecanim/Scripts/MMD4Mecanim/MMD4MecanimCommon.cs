﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimCommon
// Assembly: MMD4Mecanim, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: ADBCBBA1-CEE8-40F4-A775-E57AB2D2693F
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Scripts\MMD4Mecanim.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;

public static class MMD4MecanimCommon
{
  public static bool versionChecked;
  public static bool animatorInitializedEnabled;
  public static readonly Color MMDLit_centerAmbient = new Color(0.5f, 0.5f, 0.5f);
  public static readonly Color MMDLit_centerAmbientInv = new Color(2f, 2f, 2f);
  public static readonly Color MMDLit_globalLighting = new Color(0.6f, 0.6f, 0.6f);
  public static readonly float MMDLit_edgeScale = 1f / 1000f;
  private const string _MMD4Mecanim_Shader_Prefix_Standard = "MMD4Mecanim/Standard/MMDLit";
  private const string _MMD4Mecanim_Shader_Prefix_Compatible = "MMD4Mecanim/MMDLit";
  public static readonly string ExtensionAnimBytesLower = ".anim.bytes";
  public static readonly string ExtensionAnimBytesUpper = ".ANIM.BYTES";

  public static void VersionCheck()
  {
    if (MMD4MecanimCommon.versionChecked)
      return;
    MMD4MecanimCommon.versionChecked = true;
    string unityVersion = Application.unityVersion;
    MMD4MecanimCommon.animatorInitializedEnabled = (unityVersion.StartsWith("4.") || unityVersion.StartsWith("5.0") ? 1 : (unityVersion.StartsWith("5.1") ? 1 : 0)) == 0;
  }

  public static bool _IsAnimatorInitialized(Animator animator) => animator.isInitialized;

  public static Color MMDLit_GetTempAmbientL(Color ambient)
  {
    Color color = MMD4MecanimCommon.MMDLit_centerAmbient - ambient;
    color.r = Mathf.Max(color.r, 0.0f);
    color.g = Mathf.Max(color.g, 0.0f);
    color.b = Mathf.Max(color.b, 0.0f);
    color.a = 0.0f;
    return color * MMD4MecanimCommon.MMDLit_centerAmbientInv.r;
  }

  public static Color MMDLit_GetTempAmbient(Color globalAmbient, Color ambient)
  {
    Color _Color = (globalAmbient * (Color.white - MMD4MecanimCommon.MMDLit_GetTempAmbientL(ambient)));
    _Color.a = 0;
    return _Color;
  }

  public static Color MMDLit_GetTempDiffuse(Color globalAmbient, Color ambient, Color diffuse)
  {
    Color color = ambient + diffuse * MMD4MecanimCommon.MMDLit_globalLighting.r;
    color.r = Mathf.Min(color.r, 1f);
    color.g = Mathf.Min(color.g, 1f);
    color.b = Mathf.Min(color.b, 1f);
    Color tempDiffuse = color - MMD4MecanimCommon.MMDLit_GetTempAmbient(globalAmbient, ambient);
    tempDiffuse.r = Mathf.Max(tempDiffuse.r, 0.0f);
    tempDiffuse.g = Mathf.Max(tempDiffuse.g, 0.0f);
    tempDiffuse.b = Mathf.Max(tempDiffuse.b, 0.0f);
    tempDiffuse.a = 0.0f;
    return tempDiffuse;
  }

  public static void WeakSetMaterialFloat(Material m, string name, float v)
  {
    if ((double) m.GetFloat(name) == (double) v)
      return;
    m.SetFloat(name, v);
  }

  public static void WeakSetMaterialVector(Material m, string name, Vector4 v)
  {
    if (!(m.GetVector(name) != v))
      return;
    m.SetVector(name, v);
  }

  public static void WeakSetMaterialColor(Material m, string name, Color v)
  {
    if (!(m.GetColor(name) != v))
      return;
    m.SetColor(name, v);
  }

  public static Transform WeakAddChildTransform(Transform parentTransform, string name)
  {
    GameObject gameObject = MMD4MecanimCommon.WeakAddChildGameObject((UnityEngine.Object) parentTransform != (UnityEngine.Object) null ? parentTransform.gameObject : (GameObject) null, name);
    return !((UnityEngine.Object) gameObject != (UnityEngine.Object) null) ? (Transform) null : gameObject.transform;
  }

  public static GameObject WeakAddChildGameObject(
    GameObject parentGameObject,
    string name)
  {
    if ((UnityEngine.Object) parentGameObject != (UnityEngine.Object) null && !string.IsNullOrEmpty(name))
    {
      foreach (Transform transform in parentGameObject.transform)
      {
        if (transform.name == name)
          return transform.gameObject;
      }
    }
    GameObject gameObject = new GameObject(name != null ? name : "");
    if ((UnityEngine.Object) parentGameObject != (UnityEngine.Object) null)
      gameObject.transform.parent = parentGameObject.transform;
    gameObject.layer = parentGameObject.layer;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    gameObject.transform.localScale = Vector3.one;
    return gameObject;
  }

  public static MMD4MecanimCommon.Version GetUnityVersion()
  {
    MMD4MecanimCommon.Version unityVersion1 = new MMD4MecanimCommon.Version();
    string unityVersion2 = Application.unityVersion;
    int len = unityVersion2.IndexOf(".");
    int num = len >= 0 ? unityVersion2.IndexOf(".", len + 1) : -1;
    if (num >= 0)
    {
      unityVersion1.major = MMD4MecanimCommon.ToInt(unityVersion2, 0, len);
      unityVersion1.minor = MMD4MecanimCommon.ToInt(unityVersion2, len + 1, num - (len + 1));
      unityVersion1.revision = MMD4MecanimCommon.ToInt(unityVersion2, num + 1, unityVersion2.Length - (num + 1));
    }
    else if (len >= 0)
    {
      unityVersion1.major = MMD4MecanimCommon.ToInt(unityVersion2, 0, len);
      unityVersion1.minor = MMD4MecanimCommon.ToInt(unityVersion2, len + 1, unityVersion2.Length - (len + 1));
    }
    else
      unityVersion1.major = MMD4MecanimCommon.ToInt(unityVersion2);
    return unityVersion1;
  }

  public static GameObject[] GetChildrenRecursivery(GameObject gameObject)
  {
    List<GameObject> children = new List<GameObject>();
    if ((UnityEngine.Object) gameObject != (UnityEngine.Object) null)
      MMD4MecanimCommon._GetChildrenRecursivery(children, gameObject);
    return children.ToArray();
  }

  private static void _GetChildrenRecursivery(List<GameObject> children, GameObject gameObject)
  {
    foreach (Transform transform in gameObject.transform)
    {
      children.Add(transform.gameObject);
      MMD4MecanimCommon._GetChildrenRecursivery(children, transform.gameObject);
    }
  }

  public static void IgnoreCollisionRecursivery(GameObject gameObject, Collider targetCollider)
  {
    if (!((UnityEngine.Object) gameObject != (UnityEngine.Object) null) || !((UnityEngine.Object) targetCollider != (UnityEngine.Object) null))
      return;
    if ((UnityEngine.Object) gameObject != (UnityEngine.Object) targetCollider.gameObject)
    {
      Collider component = gameObject.GetComponent<Collider>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null && component.enabled && targetCollider.enabled)
        Physics.IgnoreCollision(component, targetCollider);
    }
    foreach (Component component in gameObject.transform)
      MMD4MecanimCommon.IgnoreCollisionRecursivery(component.gameObject, targetCollider);
  }

  public static void IgnoreCollisionRecursivery(Collider collider, Collider targetCollider)
  {
    if (!((UnityEngine.Object) collider != (UnityEngine.Object) null) || !((UnityEngine.Object) targetCollider != (UnityEngine.Object) null))
      return;
    MMD4MecanimCommon.IgnoreCollisionRecursivery(collider.gameObject, targetCollider);
  }

  public static bool ContainsNameInParents(GameObject gameObject, string name)
  {
    if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
      return false;
    for (; !gameObject.name.Contains(name); gameObject = gameObject.transform.parent.gameObject)
    {
      if ((UnityEngine.Object) gameObject.transform.parent == (UnityEngine.Object) null)
        return false;
    }
    return true;
  }

  public static int MurmurHash32(string name) => MMD4MecanimCommon.MurmurHash32(name, 2880293630U);

  public static int MurmurHash32(string name, uint seed)
  {
    byte[] bytes = Encoding.UTF8.GetBytes(name);
    return MMD4MecanimCommon.MurmurHash32(bytes, 0, bytes.Length, seed);
  }

  public static int MurmurHash32(byte[] bytes, int pos, int len) => MMD4MecanimCommon.MurmurHash32(bytes, pos, len, 2880293630U);

  private static uint mmh3_fmix32(uint h)
  {
    h ^= h >> 16;
    h *= 2246822507U;
    h ^= h >> 13;
    h *= 3266489909U;
    h ^= h >> 16;
    return h;
  }

  public static int MurmurHash32(byte[] bytes, int pos, int len, uint seed)
  {
    int num1 = len / 4;
    uint num2 = seed;
    uint num3 = 3432918353;
    uint num4 = 461845907;
    int num5 = 0;
    for (int index = num1 * 4; num5 < index; num5 += 4)
    {
      uint num6 = (uint) ((int) bytes[pos + num5] | (int) bytes[pos + num5 + 1] << 8 | (int) bytes[pos + num5 + 2] << 16 | (int) bytes[pos + num5 + 3] << 24) * num3;
      uint num7 = (num6 << 15 | num6 >> 17) * num4;
      uint num8 = num2 ^ num7;
      num2 = (uint) ((int) (num8 << 13 | num8 >> 19) * 5 - 430675100);
    }
    if ((len & 3) != 0)
    {
      uint num9 = 0;
      if ((len & 3) >= 3)
        num9 ^= (uint) bytes[pos + num1 * 4 + 2] << 16;
      if ((len & 3) >= 2)
        num9 ^= (uint) bytes[pos + num1 * 4 + 1] << 8;
      if ((len & 3) >= 1)
        num9 ^= (uint) bytes[pos + num1 * 4];
      uint num10 = num9 * num3;
      uint num11 = (num10 << 15 | num10 >> 17) * num4;
      num2 ^= num11;
    }
    return (int) MMD4MecanimCommon.mmh3_fmix32(num2 ^ (uint) len);
  }

  public static MMD4MecanimCommon.GCHValue<T> MakeGCHValue<T>(ref T value) => new MMD4MecanimCommon.GCHValue<T>(ref value);

  public static MMD4MecanimCommon.GCHValues<T> MakeGCHValues<T>(T[] values) => new MMD4MecanimCommon.GCHValues<T>(values);

  public static int ReadInt(byte[] bytes, int index) => bytes != null && index * 4 + 3 < bytes.Length ? (int) bytes[index * 4] | (int) bytes[index * 4 + 1] << 8 | (int) bytes[index * 4 + 2] << 16 | (int) bytes[index * 4 + 3] << 24 : 0;

  public static bool IsID(string str)
  {
    if (string.IsNullOrEmpty(str))
      return false;
    switch (str[0])
    {
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
        for (int index = 1; index != str.Length; ++index)
        {
          if (str[index] == '.')
            return true;
          switch (str[0])
          {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
              continue;
            default:
              return false;
          }
        }
        return false;
      default:
        return false;
    }
  }

  public static int ToInt(string str) => str == null ? 0 : MMD4MecanimCommon.ToInt(str, 0, str.Length);

  public static int ToInt(string str, int pos, int len)
  {
    if (str == null)
      return 0;
    len += pos;
    if (len < str.Length)
      len = str.Length;
    bool flag = false;
    if (pos < len && str[pos] == '-')
    {
      flag = true;
      ++pos;
    }
    int num1 = 0;
    if (pos < len)
    {
      uint num2 = (uint) str[pos] - 48U;
      if (num2 > 9U)
        return 0;
      num1 = (int) num2;
      ++pos;
    }
    for (; pos < len; ++pos)
    {
      uint num3 = (uint) str[pos] - 48U;
      if (num3 <= 9U)
        num1 = num1 * 10 + (int) num3;
      else
        break;
    }
    return !flag ? num1 : -num1;
  }

  public static MMD4MecanimCommon.CloneMeshWork CloneMesh(Mesh sharedMesh)
  {
    if ((UnityEngine.Object) sharedMesh == (UnityEngine.Object) null)
      return (MMD4MecanimCommon.CloneMeshWork) null;
    MMD4MecanimCommon.CloneMeshWork cloneMeshWork = new MMD4MecanimCommon.CloneMeshWork();
    Mesh mesh = new Mesh();
    cloneMeshWork.mesh = mesh;
    cloneMeshWork.name = sharedMesh.name;
    cloneMeshWork.vertices = sharedMesh.vertices;
    cloneMeshWork.normals = sharedMesh.normals;
    cloneMeshWork.tangents = sharedMesh.tangents;
    cloneMeshWork.colors32 = sharedMesh.colors32;
    cloneMeshWork.boneWeights = sharedMesh.boneWeights;
    cloneMeshWork.bounds = sharedMesh.bounds;
    cloneMeshWork.hideFlags = sharedMesh.hideFlags;
    cloneMeshWork.uv = sharedMesh.uv;
    cloneMeshWork.uv2 = sharedMesh.uv2;
    cloneMeshWork.bindposes = sharedMesh.bindposes;
    cloneMeshWork.triangles = sharedMesh.triangles;
    mesh.name = cloneMeshWork.name;
    mesh.vertices = cloneMeshWork.vertices;
    mesh.normals = cloneMeshWork.normals;
    mesh.tangents = cloneMeshWork.tangents;
    mesh.colors32 = cloneMeshWork.colors32;
    mesh.boneWeights = cloneMeshWork.boneWeights;
    mesh.bounds = cloneMeshWork.bounds;
    mesh.hideFlags = cloneMeshWork.hideFlags;
    mesh.uv = cloneMeshWork.uv;
    mesh.uv2 = cloneMeshWork.uv2;
    mesh.bindposes = cloneMeshWork.bindposes;
    mesh.triangles = cloneMeshWork.triangles;
    if (sharedMesh.subMeshCount > 0)
    {
      mesh.subMeshCount = sharedMesh.subMeshCount;
      for (int submesh = 0; submesh < sharedMesh.subMeshCount; ++submesh)
        mesh.SetTriangles(sharedMesh.GetIndices(submesh), submesh);
    }
    return cloneMeshWork;
  }

  public static Material CloneMaterial(Material sharedMaterial) => (UnityEngine.Object) sharedMaterial == (UnityEngine.Object) null ? sharedMaterial : new Material(sharedMaterial);

  public static bool Approx(ref float src, float dest, float step)
  {
    if ((double) src > (double) dest)
    {
      if ((double) (src -= step) > (double) dest)
        return false;
      src = dest;
      return true;
    }
    if ((double) src >= (double) dest)
      return true;
    if ((double) (src += step) < (double) dest)
      return false;
    src = dest;
    return true;
  }

  public static bool FindAnything<Type>(List<Type> elements, Type element) where Type : class
  {
    if (elements != null)
    {
      for (int index = 0; index < elements.Count; ++index)
      {
        if ((object) element == (object) elements[index])
          return true;
      }
    }
    return false;
  }

  public static bool IsAlphabet(char ch) => (uint) ch - 65U <= 25U || (uint) ch - 97U <= 25U || (uint) ch - 65313U <= 25U || (uint) ch - 65345U <= 25U;

  public static char ToHalfLower(char ch)
  {
    if ((uint) ch - 65U <= 25U)
      return (char) ((int) ch - 65 + 97);
    if ((uint) ch - 65313U <= 25U)
      return (char) ((int) ch - 65313 + 97);
    return (uint) ch - 65345U <= 25U ? (char) ((int) ch - 65345 + 97) : ch;
  }

  public static float Reciplocal(float x) => (double) x == 0.0 ? 0.0f : 1f / x;

  public static Vector3 Reciplocal(Vector3 scale) => new Vector3(MMD4MecanimCommon.Reciplocal(scale.x), MMD4MecanimCommon.Reciplocal(scale.y), MMD4MecanimCommon.Reciplocal(scale.z));

  public static Vector3 Reciplocal(ref Vector3 scale) => new Vector3(MMD4MecanimCommon.Reciplocal(scale.x), MMD4MecanimCommon.Reciplocal(scale.y), MMD4MecanimCommon.Reciplocal(scale.z));

  public static void ConvertMatrixBulletPhysics(ref Matrix4x4 matrix)
  {
    matrix.m03 = -matrix.m03;
    matrix.m10 = -matrix.m10;
    matrix.m20 = -matrix.m20;
    matrix.m01 = -matrix.m01;
    matrix.m02 = -matrix.m02;
  }

  public static Vector3 ComputeMatrixScale(ref Matrix4x4 matrix)
  {
    Vector3 vector3 = new Vector3(matrix.m00, matrix.m10, matrix.m20);
    double magnitude1 = (double) vector3.magnitude;
    vector3 = new Vector3(matrix.m01, matrix.m11, matrix.m21);
    double magnitude2 = (double) vector3.magnitude;
    vector3 = new Vector3(matrix.m02, matrix.m12, matrix.m22);
    double magnitude3 = (double) vector3.magnitude;
    return new Vector3((float) magnitude1, (float) magnitude2, (float) magnitude3);
  }

  public static Vector3 ComputeMatrixReciplocalScale(ref Matrix4x4 matrix)
  {
    Vector3 vector3 = new Vector3(matrix.m00, matrix.m10, matrix.m20);
    double x = (double) MMD4MecanimCommon.Reciplocal(vector3.magnitude);
    vector3 = new Vector3(matrix.m01, matrix.m11, matrix.m21);
    double y = (double) MMD4MecanimCommon.Reciplocal(vector3.magnitude);
    vector3 = new Vector3(matrix.m02, matrix.m12, matrix.m22);
    double z = (double) MMD4MecanimCommon.Reciplocal(vector3.magnitude);
    return new Vector3((float) x, (float) y, (float) z);
  }

  public static void SetMatrixBasis(
    ref Matrix4x4 matrix,
    ref Vector3 right,
    ref Vector3 up,
    ref Vector3 forward)
  {
    matrix.m00 = right.x;
    matrix.m10 = right.y;
    matrix.m20 = right.z;
    matrix.m01 = up.x;
    matrix.m11 = up.y;
    matrix.m21 = up.z;
    matrix.m02 = forward.x;
    matrix.m12 = forward.y;
    matrix.m22 = forward.z;
  }

  public static void NormalizeMatrixBasis(ref Matrix4x4 matrix)
  {
    Vector3 vector3_1 = new Vector3(matrix.m00, matrix.m10, matrix.m20);
    Vector3 vector3_2 = new Vector3(matrix.m01, matrix.m11, matrix.m21);
    Vector3 vector3_3 = new Vector3(matrix.m02, matrix.m12, matrix.m22);
    Vector3 right = vector3_1 * MMD4MecanimCommon.Reciplocal(vector3_1.magnitude);
    Vector3 up = vector3_2 * MMD4MecanimCommon.Reciplocal(vector3_2.magnitude);
    Vector3 forward = vector3_3 * MMD4MecanimCommon.Reciplocal(vector3_3.magnitude);
    MMD4MecanimCommon.SetMatrixBasis(ref matrix, ref right, ref up, ref forward);
  }

  public static void NormalizeMatrixBasis(ref Matrix4x4 matrix, ref Vector3 rScale)
  {
    Vector3 right = new Vector3(matrix.m00, matrix.m10, matrix.m20) * rScale.x;
    Vector3 up = new Vector3(matrix.m01, matrix.m11, matrix.m21) * rScale.y;
    Vector3 forward = new Vector3(matrix.m02, matrix.m12, matrix.m22) * rScale.z;
    MMD4MecanimCommon.SetMatrixBasis(ref matrix, ref right, ref up, ref forward);
  }

  public static bool IsNoEffects(ref float value, MMD4MecanimData.MorphMaterialOperation operation)
  {
    if (operation == MMD4MecanimData.MorphMaterialOperation.Multiply)
      return (double) Mathf.Abs(value - 1f) <= (double) Mathf.Epsilon;
    return operation != MMD4MecanimData.MorphMaterialOperation.Adding || (double) value <= (double) Mathf.Epsilon;
  }

  public static bool IsNoEffects(ref Color color, MMD4MecanimData.MorphMaterialOperation operation)
  {
    switch (operation)
    {
      case MMD4MecanimData.MorphMaterialOperation.Multiply:
        return (double) Mathf.Abs(color.r - 1f) <= (double) Mathf.Epsilon && (double) Mathf.Abs(color.g - 1f) <= (double) Mathf.Epsilon && (double) Mathf.Abs(color.b - 1f) <= (double) Mathf.Epsilon && (double) Mathf.Abs(color.a - 1f) <= (double) Mathf.Epsilon;
      case MMD4MecanimData.MorphMaterialOperation.Adding:
        return (double) color.r <= (double) Mathf.Epsilon && (double) color.g <= (double) Mathf.Epsilon && (double) color.b <= (double) Mathf.Epsilon && (double) color.a <= (double) Mathf.Epsilon;
      default:
        return true;
    }
  }

  public static bool IsNoEffectsRGB(
    ref Color color,
    MMD4MecanimData.MorphMaterialOperation operation)
  {
    switch (operation)
    {
      case MMD4MecanimData.MorphMaterialOperation.Multiply:
        return (double) Mathf.Abs(color.r - 1f) <= (double) Mathf.Epsilon && (double) Mathf.Abs(color.g - 1f) <= (double) Mathf.Epsilon && (double) Mathf.Abs(color.b - 1f) <= (double) Mathf.Epsilon;
      case MMD4MecanimData.MorphMaterialOperation.Adding:
        return (double) color.r <= (double) Mathf.Epsilon && (double) color.g <= (double) Mathf.Epsilon && (double) color.b <= (double) Mathf.Epsilon;
      default:
        return true;
    }
  }

  public static float NormalizeAsDegree(float v)
  {
    if ((double) v < -180.0)
    {
      do
      {
        v += 360f;
      }
      while ((double) v < -180.0);
    }
    else if ((double) v > 180.0)
    {
      do
      {
        v -= 360f;
      }
      while ((double) v > 180.0);
    }
    return v;
  }

  public static Vector3 NormalizeAsDegree(Vector3 degrees) => new Vector3(MMD4MecanimCommon.NormalizeAsDegree(degrees.x), MMD4MecanimCommon.NormalizeAsDegree(degrees.y), MMD4MecanimCommon.NormalizeAsDegree(degrees.z));

  public static bool FuzzyZero(float v) => (double) Math.Abs(v) <= (double) Mathf.Epsilon;

  public static bool FuzzyZero(Vector3 v) => (double) Mathf.Abs(v.x) <= (double) Mathf.Epsilon && (double) Mathf.Abs(v.y) <= (double) Mathf.Epsilon && (double) Mathf.Abs(v.z) <= (double) Mathf.Epsilon;

  public static Quaternion Inverse(Quaternion q) => new Quaternion(-q.x, -q.y, -q.z, q.w);

  public static bool FuzzyIdentity(Quaternion q) => (double) Mathf.Abs(q.x) <= (double) Mathf.Epsilon && (double) Mathf.Abs(q.y) <= (double) Mathf.Epsilon && (double) Mathf.Abs(q.z) <= (double) Mathf.Epsilon && (double) Mathf.Abs(q.w - 1f) <= (double) Mathf.Epsilon;

  public static Quaternion FuzzyMul(Quaternion lhs, Quaternion rhs)
  {
    if (MMD4MecanimCommon.FuzzyIdentity(lhs))
      return rhs;
    return MMD4MecanimCommon.FuzzyIdentity(rhs) ? lhs : lhs * rhs;
  }

  public static Quaternion FastMul(Quaternion lhs, Quaternion rhs)
  {
    if (lhs == Quaternion.identity)
      return rhs;
    return rhs == Quaternion.identity ? lhs : lhs * rhs;
  }

  public static void FuzzyAdd(ref float lhs, ref float rhs)
  {
    if ((double) rhs <= (double) Mathf.Epsilon)
      return;
    lhs += rhs;
  }

  public static void FuzzyAdd(ref Color lhs, ref Color rhs)
  {
    if ((double) rhs.r > (double) Mathf.Epsilon)
      lhs.r += rhs.r;
    if ((double) rhs.g > (double) Mathf.Epsilon)
      lhs.g += rhs.g;
    if ((double) rhs.b > (double) Mathf.Epsilon)
      lhs.b += rhs.b;
    if ((double) rhs.a <= (double) Mathf.Epsilon)
      return;
    lhs.a += rhs.a;
  }

  public static void FuzzyMul(ref float lhs, ref float rhs)
  {
    if ((double) Mathf.Abs(rhs - 1f) <= (double) Mathf.Epsilon)
      return;
    lhs *= rhs;
  }

  public static void FuzzyMul(ref Color lhs, ref Color rhs)
  {
    if ((double) Mathf.Abs(rhs.r - 1f) > (double) Mathf.Epsilon)
      lhs.r *= rhs.r;
    if ((double) Mathf.Abs(rhs.g - 1f) > (double) Mathf.Epsilon)
      lhs.g *= rhs.g;
    if ((double) Mathf.Abs(rhs.b - 1f) > (double) Mathf.Epsilon)
      lhs.b *= rhs.b;
    if ((double) Mathf.Abs(rhs.a - 1f) <= (double) Mathf.Epsilon)
      return;
    lhs.a *= rhs.a;
  }

  public static void FuzzyAdd(ref float lhs, ref float rhs, float weight)
  {
    if ((double) rhs <= (double) Mathf.Epsilon)
      return;
    lhs += rhs * weight;
  }

  public static void FuzzyAdd(ref Color lhs, ref Color rhs, float weight)
  {
    if ((double) rhs.r > (double) Mathf.Epsilon)
      lhs.r += rhs.r * weight;
    if ((double) rhs.g > (double) Mathf.Epsilon)
      lhs.g += rhs.g * weight;
    if ((double) rhs.b > (double) Mathf.Epsilon)
      lhs.b += rhs.b * weight;
    if ((double) rhs.a <= (double) Mathf.Epsilon)
      return;
    lhs.a += rhs.a * weight;
  }

  public static void FuzzyMul(ref float lhs, ref float rhs, float weight)
  {
    if ((double) Mathf.Abs(rhs - 1f) <= (double) Mathf.Epsilon)
      return;
    lhs *= (float) ((double) rhs * (double) weight + (1.0 - (double) weight));
  }

  public static void FuzzyMul(ref Color lhs, ref Color rhs, float weight)
  {
    if ((double) Mathf.Abs(rhs.r - 1f) > (double) Mathf.Epsilon)
      lhs.r *= (float) ((double) rhs.r * (double) weight + (1.0 - (double) weight));
    if ((double) Mathf.Abs(rhs.g - 1f) > (double) Mathf.Epsilon)
      lhs.g *= (float) ((double) rhs.g * (double) weight + (1.0 - (double) weight));
    if ((double) Mathf.Abs(rhs.b - 1f) > (double) Mathf.Epsilon)
      lhs.b *= (float) ((double) rhs.b * (double) weight + (1.0 - (double) weight));
    if ((double) Mathf.Abs(rhs.a - 1f) <= (double) Mathf.Epsilon)
      return;
    lhs.a *= (float) ((double) rhs.a * (double) weight + (1.0 - (double) weight));
  }

  public static void OperationMaterial(
    ref MMD4MecanimData.MorphMaterialData currentMaterialData,
    ref MMD4MecanimData.MorphMaterialData operationMaterialData,
    float weight)
  {
    if ((double) Mathf.Abs(weight - 1f) <= (double) Mathf.Epsilon)
    {
      switch (operationMaterialData.operation)
      {
        case MMD4MecanimData.MorphMaterialOperation.Multiply:
          MMD4MecanimCommon.FuzzyMul(ref currentMaterialData.diffuse, ref operationMaterialData.diffuse);
          MMD4MecanimCommon.FuzzyMul(ref currentMaterialData.specular, ref operationMaterialData.specular);
          MMD4MecanimCommon.FuzzyMul(ref currentMaterialData.shininess, ref operationMaterialData.shininess);
          MMD4MecanimCommon.FuzzyMul(ref currentMaterialData.ambient, ref operationMaterialData.ambient);
          MMD4MecanimCommon.FuzzyMul(ref currentMaterialData.edgeColor, ref operationMaterialData.edgeColor);
          MMD4MecanimCommon.FuzzyMul(ref currentMaterialData.edgeSize, ref operationMaterialData.edgeSize);
          break;
        case MMD4MecanimData.MorphMaterialOperation.Adding:
          MMD4MecanimCommon.FuzzyAdd(ref currentMaterialData.diffuse, ref operationMaterialData.diffuse);
          MMD4MecanimCommon.FuzzyAdd(ref currentMaterialData.specular, ref operationMaterialData.specular);
          MMD4MecanimCommon.FuzzyAdd(ref currentMaterialData.shininess, ref operationMaterialData.shininess);
          MMD4MecanimCommon.FuzzyAdd(ref currentMaterialData.ambient, ref operationMaterialData.ambient);
          MMD4MecanimCommon.FuzzyAdd(ref currentMaterialData.edgeColor, ref operationMaterialData.edgeColor);
          MMD4MecanimCommon.FuzzyAdd(ref currentMaterialData.edgeSize, ref operationMaterialData.edgeSize);
          break;
      }
    }
    else
    {
      switch (operationMaterialData.operation)
      {
        case MMD4MecanimData.MorphMaterialOperation.Multiply:
          MMD4MecanimCommon.FuzzyMul(ref currentMaterialData.diffuse, ref operationMaterialData.diffuse, weight);
          MMD4MecanimCommon.FuzzyMul(ref currentMaterialData.specular, ref operationMaterialData.specular, weight);
          MMD4MecanimCommon.FuzzyMul(ref currentMaterialData.shininess, ref operationMaterialData.shininess, weight);
          MMD4MecanimCommon.FuzzyMul(ref currentMaterialData.ambient, ref operationMaterialData.ambient, weight);
          MMD4MecanimCommon.FuzzyMul(ref currentMaterialData.edgeColor, ref operationMaterialData.edgeColor, weight);
          MMD4MecanimCommon.FuzzyMul(ref currentMaterialData.edgeSize, ref operationMaterialData.edgeSize, weight);
          break;
        case MMD4MecanimData.MorphMaterialOperation.Adding:
          MMD4MecanimCommon.FuzzyAdd(ref currentMaterialData.diffuse, ref operationMaterialData.diffuse, weight);
          MMD4MecanimCommon.FuzzyAdd(ref currentMaterialData.specular, ref operationMaterialData.specular, weight);
          MMD4MecanimCommon.FuzzyAdd(ref currentMaterialData.shininess, ref operationMaterialData.shininess, weight);
          MMD4MecanimCommon.FuzzyAdd(ref currentMaterialData.ambient, ref operationMaterialData.ambient, weight);
          MMD4MecanimCommon.FuzzyAdd(ref currentMaterialData.edgeColor, ref operationMaterialData.edgeColor, weight);
          MMD4MecanimCommon.FuzzyAdd(ref currentMaterialData.edgeSize, ref operationMaterialData.edgeSize, weight);
          break;
      }
    }
  }

  public static void BackupMaterial(
    ref MMD4MecanimData.MorphMaterialData materialData,
    Material material)
  {
    if (!((UnityEngine.Object) material != (UnityEngine.Object) null) || !((UnityEngine.Object) material.shader != (UnityEngine.Object) null) || !material.shader.name.StartsWith("MMD4Mecanim"))
      return;
    materialData.materialID = MMD4MecanimCommon.ToInt(material.name);
    materialData.diffuse = material.GetColor("_Color");
    materialData.specular = material.GetColor("_Specular");
    materialData.shininess = material.GetFloat("_Shininess");
    materialData.ambient = material.GetColor("_Ambient");
    materialData.edgeColor = material.GetColor("_EdgeColor");
    materialData.edgeScale = material.GetFloat("_EdgeScale");
    materialData.edgeSize = material.GetFloat("_EdgeSize");
    materialData.alPower = material.GetFloat("_ALPower");
    materialData.specular *= 1f / MMD4MecanimCommon.MMDLit_globalLighting.r;
    if ((double) materialData.edgeScale > 0.0)
      materialData.edgeSize *= 1f / materialData.edgeScale;
    materialData.tessEdgeLength = material.GetFloat("_TessEdgeLength");
    materialData.tessPhongStrength = material.GetFloat("_TessPhongStrength");
    materialData.tessExtrusionAmount = material.GetFloat("_TessExtrusionAmount");
  }

  public static void FeedbackMaterial_Tessellation_Enabled(Material material, bool tessEnabled)
  {
    if (!((UnityEngine.Object) material != (UnityEngine.Object) null) || !((UnityEngine.Object) material.shader != (UnityEngine.Object) null))
      return;
    string name1 = material.shader.name;
    if (!name1.StartsWith("MMD4Mecanim"))
      return;
    int length = name1.IndexOf("-Tess");
    bool flag = length >= 0;
    if (flag == tessEnabled)
      return;
    string name2;
    if (flag)
    {
      name2 = name1.Substring(0, length) + name1.Substring(length + 5, name1.Length - (length + 5));
    }
    else
    {
      int num = name1.IndexOf("MMDLit");
      if (num < 0)
        return;
      name2 = name1.Substring(0, num + 6) + "-Tess" + name1.Substring(num + 6);
    }
    Shader shader = Shader.Find(name2);
    if (!((UnityEngine.Object) shader != (UnityEngine.Object) null))
      return;
    int renderQueue = material.renderQueue;
    material.shader = shader;
    material.renderQueue = renderQueue;
  }

  public static void FeedbackMaterial_Tessellation(
    ref MMD4MecanimData.MorphMaterialData materialData,
    Material material)
  {
    if (!((UnityEngine.Object) material != (UnityEngine.Object) null) || !((UnityEngine.Object) material.shader != (UnityEngine.Object) null) || !material.shader.name.StartsWith("MMD4Mecanim"))
      return;
    material.SetFloat("_TessEdgeLength", materialData.tessEdgeLength);
    material.SetFloat("_TessPhongStrength", materialData.tessPhongStrength);
    material.SetFloat("_TessExtrusionAmount", materialData.tessExtrusionAmount);
  }

  public static void FeedbackMaterial(
    ref MMD4MecanimData.MorphMaterialData materialData,
    Material material,
    MMD4MecanimModelImpl.MorphAutoLuminous morphAutoLuminous)
  {
    if (!((UnityEngine.Object) material != (UnityEngine.Object) null) || !((UnityEngine.Object) material.shader != (UnityEngine.Object) null) || !material.shader.name.StartsWith("MMD4Mecanim") || morphAutoLuminous == null)
      return;
    material.SetColor("_Color", materialData.diffuse);
    material.SetColor("_Specular", materialData.specular * MMD4MecanimCommon.MMDLit_globalLighting.r);
    material.SetFloat("_Shininess", materialData.shininess);
    material.SetColor("_Ambient", materialData.ambient);
    material.SetColor("_EdgeColor", materialData.edgeColor);
    material.SetFloat("_EdgeSize", materialData.edgeSize * materialData.edgeScale);
    if ((double) materialData.shininess > 100.0)
    {
      Color luminousEmissiveColor = MMD4MecanimCommon.ComputeAutoLuminousEmissiveColor(materialData.diffuse, materialData.ambient, materialData.shininess, materialData.alPower, morphAutoLuminous);
      material.SetColor("_Emissive", luminousEmissiveColor);
    }
    else
      material.SetColor("_Emissive", new Color(0.0f, 0.0f, 0.0f, 0.0f));
  }

  public static Color ComputeAutoLuminousEmissiveColor(
    Color diffuse,
    Color ambient,
    float shininess,
    float alPower)
  {
    return (double) alPower > 0.0 ? (diffuse + ambient * 0.5f) * 0.5f * (shininess - 100f) * alPower * 0.1428571f : new Color(0.0f, 0.0f, 0.0f, 0.0f);
  }

  public static Color ComputeAutoLuminousEmissiveColor(
    Color diffuse,
    Color ambient,
    float shininess,
    float alPower,
    MMD4MecanimModelImpl.MorphAutoLuminous morphAutoLuminous)
  {
    Color luminousEmissiveColor = MMD4MecanimCommon.ComputeAutoLuminousEmissiveColor(diffuse, ambient, shininess, alPower);
    return morphAutoLuminous != null ? luminousEmissiveColor * (float) (1.0 + (double) morphAutoLuminous.lightUp * 3.0) * (1f - morphAutoLuminous.lightOff) : luminousEmissiveColor;
  }

  private static void _GetMeshRenderers(ArrayList meshRenderers, Transform parentTransform)
  {
    if ((bool) (UnityEngine.Object) parentTransform.GetComponent<Animator>())
      return;
    MeshRenderer component = parentTransform.GetComponent<MeshRenderer>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      meshRenderers.Add((object) component);
    foreach (Transform parentTransform1 in parentTransform)
      MMD4MecanimCommon._GetMeshRenderers(meshRenderers, parentTransform1);
  }

  public static MeshRenderer[] GetMeshRenderers(GameObject parentGameObject)
  {
    if ((UnityEngine.Object) parentGameObject != (UnityEngine.Object) null)
    {
      ArrayList meshRenderers1 = new ArrayList();
      foreach (Transform parentTransform in parentGameObject.transform)
      {
        if (parentTransform.name == "U_Char" || parentTransform.name.StartsWith("U_Char_"))
          MMD4MecanimCommon._GetMeshRenderers(meshRenderers1, parentTransform);
      }
      if (meshRenderers1.Count == 0)
      {
        MeshRenderer component = parentGameObject.GetComponent<MeshRenderer>();
        if ((UnityEngine.Object) component != (UnityEngine.Object) null)
          meshRenderers1.Add((object) component);
      }
      if (meshRenderers1.Count > 0)
      {
        MeshRenderer[] meshRenderers2 = new MeshRenderer[meshRenderers1.Count];
        for (int index = 0; index < meshRenderers1.Count; ++index)
          meshRenderers2[index] = (MeshRenderer) meshRenderers1[index];
        return meshRenderers2;
      }
    }
    return (MeshRenderer[]) null;
  }

  private static void _GetSkinnedMeshRenderers(
    ArrayList skinnedMeshRenderers,
    Transform parentTransform)
  {
    if ((bool) (UnityEngine.Object) parentTransform.GetComponent<Animator>())
      return;
    SkinnedMeshRenderer component = parentTransform.GetComponent<SkinnedMeshRenderer>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      skinnedMeshRenderers.Add((object) component);
    foreach (Transform parentTransform1 in parentTransform)
      MMD4MecanimCommon._GetSkinnedMeshRenderers(skinnedMeshRenderers, parentTransform1);
  }

  public static SkinnedMeshRenderer[] GetSkinnedMeshRenderers(
    GameObject parentGameObject)
  {
    if ((UnityEngine.Object) parentGameObject != (UnityEngine.Object) null)
    {
      ArrayList skinnedMeshRenderers1 = new ArrayList();
      foreach (Transform parentTransform in parentGameObject.transform)
      {
        if (parentTransform.name == "U_Char" || parentTransform.name.StartsWith("U_Char_"))
          MMD4MecanimCommon._GetSkinnedMeshRenderers(skinnedMeshRenderers1, parentTransform);
      }
      if (skinnedMeshRenderers1.Count > 0)
      {
        SkinnedMeshRenderer[] skinnedMeshRenderers2 = new SkinnedMeshRenderer[skinnedMeshRenderers1.Count];
        for (int index = 0; index < skinnedMeshRenderers1.Count; ++index)
          skinnedMeshRenderers2[index] = (SkinnedMeshRenderer) skinnedMeshRenderers1[index];
        return skinnedMeshRenderers2;
      }
    }
    return (SkinnedMeshRenderer[]) null;
  }

  private static MeshRenderer _GetMeshRenderer(Transform parentTransform)
  {
    if ((bool) (UnityEngine.Object) parentTransform.GetComponent<Animator>())
      return (MeshRenderer) null;
    MeshRenderer component = parentTransform.GetComponent<MeshRenderer>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      return component;
    foreach (Transform parentTransform1 in parentTransform)
    {
      MeshRenderer meshRenderer = MMD4MecanimCommon._GetMeshRenderer(parentTransform1);
      if ((UnityEngine.Object) meshRenderer != (UnityEngine.Object) null)
        return meshRenderer;
    }
    return (MeshRenderer) null;
  }

  public static MeshRenderer GetMeshRenderer(GameObject parentGameObject)
  {
    if ((UnityEngine.Object) parentGameObject != (UnityEngine.Object) null)
    {
      foreach (Transform parentTransform in parentGameObject.transform)
      {
        if (parentTransform.name == "U_Char" || parentTransform.name.StartsWith("U_Char_"))
        {
          MeshRenderer meshRenderer = MMD4MecanimCommon._GetMeshRenderer(parentTransform);
          if ((UnityEngine.Object) meshRenderer != (UnityEngine.Object) null)
            return meshRenderer;
        }
      }
      MeshRenderer component = parentGameObject.GetComponent<MeshRenderer>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
        return component;
    }
    return (MeshRenderer) null;
  }

  private static SkinnedMeshRenderer _GetSkinnedMeshRenderer(
    Transform parentTransform)
  {
    if ((bool) (UnityEngine.Object) parentTransform.GetComponent<Animator>())
      return (SkinnedMeshRenderer) null;
    SkinnedMeshRenderer component = parentTransform.GetComponent<SkinnedMeshRenderer>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      return component;
    foreach (Transform parentTransform1 in parentTransform)
    {
      SkinnedMeshRenderer skinnedMeshRenderer = MMD4MecanimCommon._GetSkinnedMeshRenderer(parentTransform1);
      if ((UnityEngine.Object) skinnedMeshRenderer != (UnityEngine.Object) null)
        return skinnedMeshRenderer;
    }
    return (SkinnedMeshRenderer) null;
  }

  public static SkinnedMeshRenderer GetSkinnedMeshRenderer(
    GameObject parentGameObject)
  {
    if ((UnityEngine.Object) parentGameObject != (UnityEngine.Object) null)
    {
      foreach (Transform parentTransform in parentGameObject.transform)
      {
        if (parentTransform.name == "U_Char" || parentTransform.name.StartsWith("U_Char_"))
        {
          SkinnedMeshRenderer skinnedMeshRenderer = MMD4MecanimCommon._GetSkinnedMeshRenderer(parentTransform);
          if ((UnityEngine.Object) skinnedMeshRenderer != (UnityEngine.Object) null)
            return skinnedMeshRenderer;
        }
      }
    }
    return (SkinnedMeshRenderer) null;
  }

  public static GameObject[] FindRootObjects() => Array.FindAll<GameObject>(UnityEngine.Object.FindObjectsOfType<GameObject>(), (Predicate<GameObject>) (item => (UnityEngine.Object) item.transform.parent == (UnityEngine.Object) null));

  public static bool IsExtensionAnimBytes(string name)
  {
    if (name != null)
    {
      int length1 = name.Length;
      int length2 = MMD4MecanimCommon.ExtensionAnimBytesLower.Length;
      if (length1 >= length2)
      {
        for (int index = 0; index < length2; ++index)
        {
          if ((int) name[length1 - length2 + index] != (int) MMD4MecanimCommon.ExtensionAnimBytesLower[index] && (int) name[length1 - length2 + index] != (int) MMD4MecanimCommon.ExtensionAnimBytesUpper[index])
            return false;
        }
        return true;
      }
    }
    return false;
  }

  public static bool IsDebugShader(Material material) => (UnityEngine.Object) material != (UnityEngine.Object) null && (UnityEngine.Object) material.shader != (UnityEngine.Object) null && material.shader.name != null && material.shader.name.StartsWith("MMD4Mecanim") && material.shader.name.Contains("Test");

  public static bool IsDeferredShader(Material material) => (UnityEngine.Object) material != (UnityEngine.Object) null && (UnityEngine.Object) material.shader != (UnityEngine.Object) null && material.shader.name != null && material.shader.name.StartsWith("MMD4Mecanim") && material.shader.name.Contains("Deferred");

  public static int SizeOf<T>() => Marshal.SizeOf(typeof (T));

  public static Type WeakAddComponent<Type>(GameObject go) where Type : Behaviour
  {
    if (!((UnityEngine.Object) go != (UnityEngine.Object) null))
      return default (Type);
    Type component = go.GetComponent<Type>();
    return (UnityEngine.Object) component != (UnityEngine.Object) null ? component : go.AddComponent<Type>();
  }

  public struct Version
  {
    public int major;
    public int minor;
    public int revision;

    public bool LaterThan(int major) => this.major >= major;

    public bool LaterThan(int major, int minor)
    {
      if (this.major < major)
        return false;
      return this.major > major || this.minor >= minor;
    }

    public bool LaterThan(int major, int minor, int revision)
    {
      if (this.major < major)
        return false;
      if (this.major > major)
        return true;
      if (this.minor < minor)
        return false;
      return this.minor > minor || this.revision >= revision;
    }
  }

  public struct GCHValue<T>
  {
    private GCHandle _gch_value;
    private IntPtr _valuePtr;

    public GCHValue(ref T value)
    {
      this._gch_value = GCHandle.Alloc((object) value, GCHandleType.Pinned);
      this._valuePtr = this._gch_value.AddrOfPinnedObject();
    }

    public static implicit operator IntPtr(MMD4MecanimCommon.GCHValue<T> v) => v._valuePtr;

    public void Free()
    {
      if (!(this._valuePtr != IntPtr.Zero))
        return;
      this._valuePtr = IntPtr.Zero;
      this._gch_value.Free();
    }
  }

  public struct GCHValues<T>
  {
    private GCHandle _gch_values;
    private IntPtr _valuesPtr;
    public int length;

    public GCHValues(T[] values)
    {
      if (values != null)
      {
        this._gch_values = GCHandle.Alloc((object) values, GCHandleType.Pinned);
        this._valuesPtr = this._gch_values.AddrOfPinnedObject();
        this.length = values.Length;
      }
      else
      {
        this._gch_values = new GCHandle();
        this._valuesPtr = IntPtr.Zero;
        this.length = 0;
      }
    }

    public static implicit operator IntPtr(MMD4MecanimCommon.GCHValues<T> v) => v._valuesPtr;

    public void Free()
    {
      if (!(this._valuesPtr != IntPtr.Zero))
        return;
      this._valuesPtr = IntPtr.Zero;
      this._gch_values.Free();
    }
  }

  public class PropertyWriter
  {
    private List<int> _iValues = new List<int>();
    private List<float> _fValues = new List<float>();
    private int[] _lock_iValues;
    private float[] _lock_fValues;
    private GCHandle _gch_iValues;
    private GCHandle _gch_fValues;
    public IntPtr iValuesPtr;
    public IntPtr fValuesPtr;

    public void Clear()
    {
      this._iValues.Clear();
      this._fValues.Clear();
    }

    public void Write(string propertyName, bool value)
    {
      this._iValues.Add(MMD4MecanimCommon.MurmurHash32(propertyName));
      this._iValues.Add(value ? 1 : 0);
    }

    public void Write(string propertyName, int value)
    {
      this._iValues.Add(MMD4MecanimCommon.MurmurHash32(propertyName));
      this._iValues.Add(value);
    }

    public void Write(string propertyName, float value)
    {
      this._iValues.Add(MMD4MecanimCommon.MurmurHash32(propertyName));
      this._fValues.Add(value);
    }

    public void Write(string propertyName, Vector3 value)
    {
      this._iValues.Add(MMD4MecanimCommon.MurmurHash32(propertyName));
      this._fValues.Add(value.x);
      this._fValues.Add(value.y);
      this._fValues.Add(value.z);
    }

    public void Write(string propertyName, Quaternion value)
    {
      this._iValues.Add(MMD4MecanimCommon.MurmurHash32(propertyName));
      this._fValues.Add(value.x);
      this._fValues.Add(value.y);
      this._fValues.Add(value.z);
      this._fValues.Add(value.w);
    }

    public int iValueLength => this._lock_iValues == null ? 0 : this._lock_iValues.Length;

    public int fValueLength => this._lock_fValues == null ? 0 : this._lock_fValues.Length;

    public void Lock()
    {
      this._lock_iValues = this._iValues.ToArray();
      this._lock_fValues = this._fValues.ToArray();
      this._gch_iValues = GCHandle.Alloc((object) this._lock_iValues, GCHandleType.Pinned);
      this._gch_fValues = GCHandle.Alloc((object) this._lock_fValues, GCHandleType.Pinned);
      this.iValuesPtr = this._gch_iValues.AddrOfPinnedObject();
      this.fValuesPtr = this._gch_fValues.AddrOfPinnedObject();
    }

    public void Unlock()
    {
      this._gch_fValues.Free();
      this._gch_iValues.Free();
      this.fValuesPtr = IntPtr.Zero;
      this.iValuesPtr = IntPtr.Zero;
    }
  }

  public class BinaryReader
  {
    private byte[] _fileBytes;
    private int _fourCC;
    private int _structIntValueListPosition;
    private int _structFloatValueListPosition;
    private int _structByteValueListPosition;
    private int _intValueListPosition;
    private int _floatValueListPosition;
    private int _byteValueListPosition;
    private int _nameLengthPosition;
    private int _namePosition;
    private int[] _header;
    private int[] _structList;
    private int[] _intPool;
    private float[] _floatPool;
    private byte[] _bytePool;
    private string[] _nameList;
    private int _bytePoolPosition;
    private MMD4MecanimCommon.BinaryReader.ReadMode _readMode;
    private bool _isError;
    private int _currentHeaderIntValueIndex;
    private int _currentHeaderFloatValueIndex;
    private int _currentHeaderByteValueIndex;
    private int _currentStructListIndex;
    private int _currentStructIndex;
    private int _currentStructFourCC;
    private int _currentStructFlags;
    private int _currentStructLength;
    private int _currentStructIntValueLength;
    private int _currentStructFloatValueLength;
    private int _currentStructByteValueLength;
    private int _currentStructIntValueIndex;
    private int _currentStructFloatValueIndex;
    private int _currentStructByteValueIndex;
    private int _currentIntPoolPosition;
    private int _currentIntPoolRemain;
    private int _currentFloatPoolPosition;
    private int _currentFloatPoolRemain;
    private int _currentBytePoolPosition;
    private int _currentBytePoolRemain;

    public int structListLength => this._header == null ? 0 : this._header[6];

    public int currentStructFourCC => this._currentStructFourCC;

    public int currentStructFlags => this._currentStructFlags;

    public int currentStructLength => this._currentStructLength;

    public int currentStructIndex => this._currentStructIndex;

    public static int MakeFourCC(string str) => (int) str[0] | (int) str[1] << 8 | (int) str[2] << 16 | (int) str[3] << 24;

    public int GetFourCC() => this._fourCC;

    public BinaryReader(byte[] fileBytes) => this._fileBytes = fileBytes;

    public bool Preparse()
    {
      if (this._fileBytes == null || this._fileBytes.Length == 0)
      {
        Debug.LogError((object) "(BinaryReader) fileBytes is Nothing.");
        this._isError = true;
        return false;
      }
      int num1 = 0;
      this._fourCC = MMD4MecanimCommon.ReadInt(this._fileBytes, 0);
      int srcOffset1 = num1 + 4;
      this._header = new int[15];
      Buffer.BlockCopy((Array) this._fileBytes, srcOffset1, (Array) this._header, 0, this._header.Length * 4);
      int srcOffset2 = srcOffset1 + this._header.Length * 4;
      this._structList = new int[this._header[6] * 9];
      Buffer.BlockCopy((Array) this._fileBytes, srcOffset2, (Array) this._structList, 0, this._structList.Length * 4);
      int srcOffset3 = srcOffset2 + this._structList.Length * 4;
      int length1 = this._header[0] + this._header[7] + this._header[10] + this._header[13];
      this._structIntValueListPosition = this._header[0];
      this._intValueListPosition = this._structIntValueListPosition + this._header[7];
      this._nameLengthPosition = this._intValueListPosition + this._header[10];
      this._intPool = new int[length1];
      Buffer.BlockCopy((Array) this._fileBytes, srcOffset3, (Array) this._intPool, 0, length1 * 4);
      int srcOffset4 = srcOffset3 + length1 * 4;
      int length2 = this._header[1] + this._header[8] + this._header[11];
      this._structFloatValueListPosition = this._header[1];
      this._floatValueListPosition = this._structFloatValueListPosition + this._header[8];
      this._floatPool = new float[length2];
      Buffer.BlockCopy((Array) this._fileBytes, srcOffset4, (Array) this._floatPool, 0, length2 * 4);
      int num2 = srcOffset4 + length2 * 4;
      int num3 = this._header[2] + this._header[9] + this._header[12] + this._header[14];
      if (num2 + num3 > this._fileBytes.Length)
      {
        Debug.LogError((object) "(BinaryReader) Overflow.");
        this._isError = true;
        return false;
      }
      this._bytePool = this._fileBytes;
      this._bytePoolPosition = num2;
      this._structByteValueListPosition = this._bytePoolPosition + this._header[2];
      this._byteValueListPosition = this._structByteValueListPosition + this._header[9];
      this._namePosition = this._byteValueListPosition + this._header[12];
      return this._PostfixPreparse();
    }

    private bool _PostfixPreparse()
    {
      if (this._fileBytes == null || this._intPool == null || this._header == null)
      {
        Debug.LogError((object) "(BinaryReader) null.");
        this._isError = true;
        return false;
      }
      if (this._nameLengthPosition + this._header[13] > this._intPool.Length)
      {
        Debug.LogError((object) "(BinaryReader) Overflow.");
        this._isError = true;
        return false;
      }
      int length = this._header[13];
      this._nameList = new string[length];
      int nameLengthPosition = this._nameLengthPosition;
      int namePosition = this._namePosition;
      for (int index = 0; index < length; ++index)
      {
        int count = this._intPool[nameLengthPosition];
        if (namePosition + count > this._fileBytes.Length)
        {
          Debug.LogError((object) "(BinaryReader) Overflow.");
          this._isError = true;
          return false;
        }
        this._nameList[index] = Encoding.UTF8.GetString(this._fileBytes, namePosition, count);
        ++nameLengthPosition;
        namePosition += count + 1;
      }
      return true;
    }

    public void Rewind()
    {
      if (this._isError)
        return;
      this._readMode = MMD4MecanimCommon.BinaryReader.ReadMode.None;
      this._currentHeaderIntValueIndex = 0;
      this._currentHeaderFloatValueIndex = 0;
      this._currentHeaderByteValueIndex = 0;
      this._currentStructListIndex = 0;
      this._currentStructIndex = 0;
      this._currentStructFourCC = 0;
      this._currentStructFlags = 0;
      this._currentStructLength = 0;
      this._currentStructIntValueLength = 0;
      this._currentStructFloatValueLength = 0;
      this._currentStructByteValueLength = 0;
      this._currentStructIntValueIndex = 0;
      this._currentStructFloatValueIndex = 0;
      this._currentStructByteValueIndex = 0;
      this._currentIntPoolPosition = 0;
      this._currentIntPoolRemain = 0;
      this._currentFloatPoolPosition = 0;
      this._currentFloatPoolRemain = 0;
      this._currentBytePoolPosition = 0;
      this._currentBytePoolRemain = 0;
    }

    public bool BeginHeader()
    {
      if (this._isError)
        return false;
      if (this._readMode != MMD4MecanimCommon.BinaryReader.ReadMode.None)
      {
        Debug.LogError((object) "(BinaryReader) invalid flow.");
        return false;
      }
      this._currentHeaderIntValueIndex = 0;
      this._currentHeaderFloatValueIndex = 0;
      this._currentHeaderByteValueIndex = 0;
      this._currentIntPoolPosition = 0;
      this._currentFloatPoolPosition = 0;
      this._currentBytePoolPosition = 0;
      this._currentIntPoolRemain = this._header[3];
      this._currentFloatPoolRemain = this._header[4];
      this._currentBytePoolRemain = this._header[5];
      this._readMode = MMD4MecanimCommon.BinaryReader.ReadMode.Header;
      return true;
    }

    public int ReadHeaderInt()
    {
      if (this._isError)
        return 0;
      if (this._readMode != MMD4MecanimCommon.BinaryReader.ReadMode.Header)
      {
        Debug.LogError((object) "(BinaryReader) invalid flow.");
        this._isError = true;
        return 0;
      }
      if (this._currentHeaderIntValueIndex >= this._header[0])
        return 0;
      int headerIntValueIndex = this._currentHeaderIntValueIndex;
      if (this._intPool == null || headerIntValueIndex >= this._intPool.Length)
      {
        Debug.LogError((object) "(BinaryReader) Overflow.");
        this._isError = true;
        return 0;
      }
      int num = this._intPool[headerIntValueIndex];
      ++this._currentHeaderIntValueIndex;
      return num;
    }

    public float ReadHeaderFloat()
    {
      if (this._isError)
        return 0.0f;
      if (this._readMode != MMD4MecanimCommon.BinaryReader.ReadMode.Header)
      {
        Debug.LogError((object) "(BinaryReader) invalid flow.");
        this._isError = true;
        return 0.0f;
      }
      if (this._currentHeaderFloatValueIndex >= this._header[1])
        return 0.0f;
      int headerFloatValueIndex = this._currentHeaderFloatValueIndex;
      if (this._floatPool == null || headerFloatValueIndex >= this._floatPool.Length)
      {
        Debug.LogError((object) "(BinaryReader) Overflow.");
        this._isError = true;
        return 0.0f;
      }
      double num = (double) this._floatPool[headerFloatValueIndex];
      ++this._currentHeaderFloatValueIndex;
      return (float) num;
    }

    public byte ReadHeaderByte()
    {
      if (this._isError)
        return 0;
      if (this._readMode != MMD4MecanimCommon.BinaryReader.ReadMode.Header)
      {
        Debug.LogError((object) "(BinaryReader) invalid flow.");
        this._isError = true;
        return 0;
      }
      if (this._currentHeaderByteValueIndex >= this._header[2])
        return 0;
      int index = this._bytePoolPosition + this._currentHeaderByteValueIndex;
      if (this._bytePool == null || index >= this._bytePool.Length)
      {
        Debug.LogError((object) "(BinaryReader) Overflow.");
        this._isError = true;
        return 0;
      }
      int num = (int) this._bytePool[index];
      ++this._currentHeaderByteValueIndex;
      return (byte) num;
    }

    public bool EndHeader()
    {
      if (this._isError)
        return false;
      if (this._readMode != MMD4MecanimCommon.BinaryReader.ReadMode.Header)
      {
        Debug.LogError((object) "(BinaryReader) invalid flow.");
        return false;
      }
      this._currentHeaderIntValueIndex = 0;
      this._currentHeaderFloatValueIndex = 0;
      this._currentHeaderByteValueIndex = 0;
      this._currentIntPoolPosition = 0;
      this._currentFloatPoolPosition = 0;
      this._currentBytePoolPosition = 0;
      this._currentIntPoolRemain = 0;
      this._currentFloatPoolRemain = 0;
      this._currentBytePoolRemain = 0;
      this._readMode = MMD4MecanimCommon.BinaryReader.ReadMode.None;
      return true;
    }

    public bool BeginStructList()
    {
      if (this._isError)
        return false;
      if (this._readMode != MMD4MecanimCommon.BinaryReader.ReadMode.None)
      {
        Debug.LogError((object) "(BinaryReader) invalid flow.");
        return false;
      }
      if (this._structList == null || this._currentStructListIndex + 1 > this._structList.Length)
      {
        Debug.LogError((object) "(BinaryReader) Overflow.");
        return false;
      }
      this._currentStructIndex = 0;
      this._currentStructFourCC = this._structList[this._currentStructListIndex * 9];
      this._currentStructFlags = this._structList[this._currentStructListIndex * 9 + 1];
      this._currentStructLength = this._structList[this._currentStructListIndex * 9 + 2];
      this._currentStructIntValueLength = this._structList[this._currentStructListIndex * 9 + 3];
      this._currentStructFloatValueLength = this._structList[this._currentStructListIndex * 9 + 4];
      this._currentStructByteValueLength = this._structList[this._currentStructListIndex * 9 + 5];
      this._currentIntPoolPosition = this._structList[this._currentStructListIndex * 9 + 6];
      this._currentFloatPoolPosition = this._structList[this._currentStructListIndex * 9 + 7];
      this._currentBytePoolPosition = this._structList[this._currentStructListIndex * 9 + 8];
      this._currentStructIntValueIndex = 0;
      this._currentStructFloatValueIndex = 0;
      this._currentStructByteValueIndex = 0;
      this._currentIntPoolRemain = 0;
      this._currentFloatPoolRemain = 0;
      this._currentBytePoolRemain = 0;
      this._readMode = MMD4MecanimCommon.BinaryReader.ReadMode.StructList;
      return true;
    }

    public bool BeginStruct()
    {
      if (this._isError)
        return false;
      if (this._readMode != MMD4MecanimCommon.BinaryReader.ReadMode.StructList)
      {
        Debug.LogError((object) "(BinaryReader) invalid flow.");
        return false;
      }
      if (this._currentStructIndex >= this._currentStructLength)
      {
        Debug.LogError((object) "(BinaryReader) Overflow.");
        return false;
      }
      if (this._currentStructIntValueIndex + 3 > this._currentStructIntValueLength)
      {
        Debug.LogError((object) "(BinaryReader) Overflow.");
        this._isError = true;
        return false;
      }
      if (this._intPool == null || this._structIntValueListPosition + 3 > this._intPool.Length)
      {
        Debug.LogError((object) "(BinaryReader) Overflow.");
        this._isError = true;
        return false;
      }
      this._currentIntPoolRemain = this._intPool[this._structIntValueListPosition];
      this._currentFloatPoolRemain = this._intPool[this._structIntValueListPosition + 1];
      this._currentBytePoolRemain = this._intPool[this._structIntValueListPosition + 2];
      this._currentStructIntValueIndex = 3;
      this._structIntValueListPosition += 3;
      this._readMode = MMD4MecanimCommon.BinaryReader.ReadMode.Struct;
      return true;
    }

    public int ReadStructInt()
    {
      if (this._isError)
        return 0;
      if (this._readMode != MMD4MecanimCommon.BinaryReader.ReadMode.Struct)
      {
        Debug.LogError((object) "(BinaryReader) invalid flow.");
        return 0;
      }
      if (this._currentStructIntValueIndex >= this._currentStructIntValueLength)
        return 0;
      if (this._intPool == null || this._structIntValueListPosition >= this._intPool.Length)
      {
        Debug.LogError((object) "(BinaryReader) Overflow.");
        this._isError = true;
        return 0;
      }
      int num = this._intPool[this._structIntValueListPosition];
      ++this._currentStructIntValueIndex;
      ++this._structIntValueListPosition;
      return num;
    }

    public float ReadStructFloat()
    {
      if (this._isError)
        return 0.0f;
      if (this._readMode != MMD4MecanimCommon.BinaryReader.ReadMode.Struct)
      {
        Debug.LogError((object) "(BinaryReader) invalid flow.");
        return 0.0f;
      }
      if (this._currentStructFloatValueIndex >= this._currentStructFloatValueLength)
        return 0.0f;
      if (this._floatPool == null || this._structFloatValueListPosition >= this._floatPool.Length)
      {
        Debug.LogError((object) "(BinaryReader) Overflow.");
        this._isError = true;
        return 0.0f;
      }
      double num = (double) this._floatPool[this._structFloatValueListPosition];
      ++this._currentStructFloatValueIndex;
      ++this._structFloatValueListPosition;
      return (float) num;
    }

    public Vector2 ReadStructVector2() => new Vector2()
    {
      x = this.ReadStructFloat(),
      y = this.ReadStructFloat()
    };

    public Vector3 ReadStructVector3() => new Vector3()
    {
      x = this.ReadStructFloat(),
      y = this.ReadStructFloat(),
      z = this.ReadStructFloat()
    };

    public byte ReadStructByte()
    {
      if (this._isError)
        return 0;
      if (this._readMode != MMD4MecanimCommon.BinaryReader.ReadMode.Struct)
      {
        Debug.LogError((object) "(BinaryReader) invalid flow.");
        return 0;
      }
      if (this._currentStructByteValueIndex >= this._currentStructByteValueLength)
        return 0;
      if (this._bytePool == null || this._structByteValueListPosition >= this._bytePool.Length)
      {
        Debug.LogError((object) "(BinaryReader) Overflow.");
        this._isError = true;
        return 0;
      }
      int num = (int) this._bytePool[this._structByteValueListPosition];
      ++this._currentStructByteValueIndex;
      ++this._structByteValueListPosition;
      return (byte) num;
    }

    public bool EndStruct()
    {
      if (this._readMode != MMD4MecanimCommon.BinaryReader.ReadMode.Struct)
      {
        Debug.LogError((object) "(BinaryReader) invalid flow.");
        return false;
      }
      if (this._currentStructIntValueIndex < this._currentStructIntValueLength)
        this._structIntValueListPosition += this._currentStructIntValueLength - this._currentStructIntValueIndex;
      if (this._currentStructFloatValueIndex < this._currentStructFloatValueLength)
        this._structFloatValueListPosition += this._currentStructFloatValueLength - this._currentStructFloatValueIndex;
      if (this._currentStructByteValueIndex < this._currentStructByteValueLength)
        this._structByteValueListPosition += this._currentStructByteValueLength - this._currentStructByteValueIndex;
      this._currentIntPoolPosition += this._currentIntPoolRemain;
      this._currentFloatPoolPosition += this._currentFloatPoolRemain;
      this._currentBytePoolPosition += this._currentBytePoolRemain;
      ++this._currentStructIndex;
      this._currentStructIntValueIndex = 0;
      this._currentStructFloatValueIndex = 0;
      this._currentStructByteValueIndex = 0;
      this._currentIntPoolRemain = 0;
      this._currentFloatPoolRemain = 0;
      this._currentBytePoolRemain = 0;
      this._readMode = MMD4MecanimCommon.BinaryReader.ReadMode.StructList;
      return true;
    }

    public bool EndStructList()
    {
      if (this._readMode != MMD4MecanimCommon.BinaryReader.ReadMode.StructList)
      {
        Debug.LogError((object) "(BinaryReader) invalid flow.");
        return false;
      }
      if (this._currentStructIndex < this._currentStructLength)
      {
        this._structIntValueListPosition += this._currentStructIntValueLength * (this._currentStructLength - this._currentStructIndex);
        this._structFloatValueListPosition += this._currentStructFloatValueLength * (this._currentStructLength - this._currentStructIndex);
        this._structByteValueListPosition += this._currentStructByteValueLength * (this._currentStructLength - this._currentStructIndex);
      }
      ++this._currentStructListIndex;
      this._currentStructIndex = 0;
      this._currentStructFourCC = 0;
      this._currentStructFlags = 0;
      this._currentStructLength = 0;
      this._currentStructIntValueLength = 0;
      this._currentStructFloatValueLength = 0;
      this._currentStructByteValueLength = 0;
      this._currentIntPoolPosition = 0;
      this._currentFloatPoolPosition = 0;
      this._currentBytePoolPosition = 0;
      this._currentStructIntValueIndex = 0;
      this._currentStructFloatValueIndex = 0;
      this._currentStructByteValueIndex = 0;
      this._currentIntPoolRemain = 0;
      this._currentFloatPoolRemain = 0;
      this._currentBytePoolRemain = 0;
      this._readMode = MMD4MecanimCommon.BinaryReader.ReadMode.None;
      return true;
    }

    public int ReadInt()
    {
      if (this._intPool == null || this._currentIntPoolRemain == 0)
        return 0;
      int num = this._intPool[this._intValueListPosition + this._currentIntPoolPosition];
      ++this._currentIntPoolPosition;
      --this._currentIntPoolRemain;
      return num;
    }

    public float ReadFloat()
    {
      if (this._floatPool == null || this._currentFloatPoolRemain == 0)
        return 0.0f;
      double num = (double) this._floatPool[this._floatValueListPosition + this._currentFloatPoolPosition];
      ++this._currentFloatPoolPosition;
      --this._currentFloatPoolRemain;
      return (float) num;
    }

    public Color ReadColor()
    {
      double r = (double) this.ReadFloat();
      float num1 = this.ReadFloat();
      float num2 = this.ReadFloat();
      float num3 = this.ReadFloat();
      double g = (double) num1;
      double b = (double) num2;
      double a = (double) num3;
      return new Color((float) r, (float) g, (float) b, (float) a);
    }

    public Color ReadColorRGB()
    {
      double r = (double) this.ReadFloat();
      float num1 = this.ReadFloat();
      float num2 = this.ReadFloat();
      double g = (double) num1;
      double b = (double) num2;
      return new Color((float) r, (float) g, (float) b, 1f);
    }

    public Vector3 ReadVector3()
    {
      double x = (double) this.ReadFloat();
      float num1 = this.ReadFloat();
      float num2 = this.ReadFloat();
      double y = (double) num1;
      double z = (double) num2;
      return new Vector3((float) x, (float) y, (float) z);
    }

    public Quaternion ReadQuaternion()
    {
      double x = (double) this.ReadFloat();
      float num1 = this.ReadFloat();
      float num2 = this.ReadFloat();
      float num3 = this.ReadFloat();
      double y = (double) num1;
      double z = (double) num2;
      double w = (double) num3;
      return new Quaternion((float) x, (float) y, (float) z, (float) w);
    }

    public byte ReadByte()
    {
      if (this._fileBytes == null || this._currentBytePoolRemain == 0)
        return 0;
      int fileByte = (int) this._fileBytes[this._byteValueListPosition + this._currentBytePoolPosition];
      ++this._currentBytePoolPosition;
      --this._currentBytePoolRemain;
      return (byte) fileByte;
    }

    public string GetName(int index) => this._nameList != null && (uint) index < (uint) this._nameList.Length ? this._nameList[index] : "";

    private enum Header
    {
      HeaderIntValueListLength,
      HeaderFloatValueListLength,
      HeaderByteValueListLength,
      IntValueLengthInHeader,
      FloatValueLengthInHeader,
      ByteValueLengthInHeader,
      StructListLength,
      StructIntValueListLength,
      StructFloatValueListLength,
      StructByteValueListLength,
      IntValueListLength,
      FloatValueListLength,
      ByteValueListLength,
      NameLengthListLength,
      NameLength,
      Max,
    }

    private enum ReadMode
    {
      None,
      Header,
      StructList,
      Struct,
    }
  }

  public class CloneMeshWork
  {
    public Mesh mesh;
    public string name;
    public Vector3[] vertices;
    public Vector3[] normals;
    public Vector4[] tangents;
    public Color32[] colors32;
    public BoneWeight[] boneWeights;
    public Bounds bounds;
    public HideFlags hideFlags;
    public Vector2[] uv;
    public Vector2[] uv2;
    public Matrix4x4[] bindposes;
    public int[] triangles;
  }

  public struct FastVector3
  {
    public Vector3 value;
    private int _isZero;

    public FastVector3(Vector3 value, bool isZero)
    {
      this.value = value;
      this._isZero = isZero ? 1 : 0;
    }

    public FastVector3(Vector3 value)
    {
      this.value = value;
      this._isZero = -1;
    }

    public FastVector3(ref Vector3 value)
    {
      this.value = value;
      this._isZero = -1;
    }

    public bool isZero
    {
      get
      {
        if (this._isZero == -1)
          this._isZero = this.value == Vector3.zero ? 1 : 0;
        return (uint) this._isZero > 0U;
      }
    }

    public override bool Equals(object obj) => obj is MMD4MecanimCommon.FastVector3 rhs && this.Equals(rhs);

    public bool Equals(MMD4MecanimCommon.FastVector3 rhs) => this.value == rhs.value;

    public override int GetHashCode() => this.value.GetHashCode();

    public static MMD4MecanimCommon.FastVector3 zero => new MMD4MecanimCommon.FastVector3(Vector3.zero, true);

    public static implicit operator Vector3(MMD4MecanimCommon.FastVector3 v) => v.value;

    public static implicit operator MMD4MecanimCommon.FastVector3(Vector3 v) => new MMD4MecanimCommon.FastVector3(ref v);

    public static bool operator ==(MMD4MecanimCommon.FastVector3 lhs, Vector3 rhs) => lhs.value == rhs;

    public static bool operator !=(MMD4MecanimCommon.FastVector3 lhs, Vector3 rhs) => lhs.value != rhs;

    public static MMD4MecanimCommon.FastVector3 operator +(
      MMD4MecanimCommon.FastVector3 lhs,
      Vector3 rhs)
    {
      return lhs.isZero ? new MMD4MecanimCommon.FastVector3(rhs) : new MMD4MecanimCommon.FastVector3(lhs.value + rhs);
    }

    public static MMD4MecanimCommon.FastVector3 operator +(
      Vector3 lhs,
      MMD4MecanimCommon.FastVector3 rhs)
    {
      return rhs.isZero ? new MMD4MecanimCommon.FastVector3(lhs) : new MMD4MecanimCommon.FastVector3(lhs + rhs.value);
    }

    public static MMD4MecanimCommon.FastVector3 operator +(
      MMD4MecanimCommon.FastVector3 lhs,
      MMD4MecanimCommon.FastVector3 rhs)
    {
      if (lhs.isZero && rhs.isZero)
        return lhs;
      if (lhs.isZero)
        return rhs;
      return rhs.isZero ? lhs : new MMD4MecanimCommon.FastVector3(lhs.value + rhs.value);
    }
  }

  public struct FastQuaternion
  {
    public Quaternion value;
    private int _isIdentity;

    public FastQuaternion(Quaternion value, bool isIdentity)
    {
      this.value = value;
      this._isIdentity = isIdentity ? 1 : 0;
    }

    public FastQuaternion(Quaternion value)
    {
      this.value = value;
      this._isIdentity = -1;
    }

    public FastQuaternion(ref Quaternion value)
    {
      this.value = value;
      this._isIdentity = -1;
    }

    public bool isIdentity
    {
      get
      {
        if (this._isIdentity == -1)
          this._isIdentity = this.value == Quaternion.identity ? 1 : 0;
        return (uint) this._isIdentity > 0U;
      }
    }

    public override bool Equals(object obj) => obj is MMD4MecanimCommon.FastQuaternion rhs && this.Equals(rhs);

    public bool Equals(MMD4MecanimCommon.FastQuaternion rhs) => this.value == rhs.value;

    public override int GetHashCode() => this.value.GetHashCode();

    public static MMD4MecanimCommon.FastQuaternion identity => new MMD4MecanimCommon.FastQuaternion(Quaternion.identity, true);

    public static implicit operator Quaternion(MMD4MecanimCommon.FastQuaternion q) => q.value;

    public static implicit operator MMD4MecanimCommon.FastQuaternion(Quaternion q) => new MMD4MecanimCommon.FastQuaternion(q);

    public static bool operator ==(MMD4MecanimCommon.FastQuaternion lhs, Quaternion rhs) => lhs.value == rhs;

    public static bool operator !=(MMD4MecanimCommon.FastQuaternion lhs, Quaternion rhs) => lhs.value != rhs;

    public static MMD4MecanimCommon.FastQuaternion operator *(
      MMD4MecanimCommon.FastQuaternion lhs,
      Quaternion rhs)
    {
      return lhs.isIdentity ? new MMD4MecanimCommon.FastQuaternion(rhs) : new MMD4MecanimCommon.FastQuaternion(lhs.value * rhs);
    }

    public static MMD4MecanimCommon.FastQuaternion operator *(
      Quaternion lhs,
      MMD4MecanimCommon.FastQuaternion rhs)
    {
      return rhs.isIdentity ? new MMD4MecanimCommon.FastQuaternion(lhs) : new MMD4MecanimCommon.FastQuaternion(lhs * rhs.value);
    }

    public static MMD4MecanimCommon.FastQuaternion operator *(
      MMD4MecanimCommon.FastQuaternion lhs,
      MMD4MecanimCommon.FastQuaternion rhs)
    {
      if (lhs.isIdentity && rhs.isIdentity)
        return lhs;
      if (lhs.isIdentity)
        return rhs;
      return rhs.isIdentity ? lhs : new MMD4MecanimCommon.FastQuaternion(lhs.value * rhs.value, false);
    }
  }
}
