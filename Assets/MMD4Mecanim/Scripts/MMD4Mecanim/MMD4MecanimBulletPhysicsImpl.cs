﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimBulletPhysicsImpl
// Assembly: MMD4Mecanim, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: ADBCBBA1-CEE8-40F4-A775-E57AB2D2693F
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Scripts\MMD4Mecanim.dll

using MMD4MecanimInternal.Bullet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class MMD4MecanimBulletPhysicsImpl : MonoBehaviour
{
  public static readonly Matrix4x4 rotateMatrixX = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0.0f, 0.0f, 90f), Vector3.one);
  public static readonly Matrix4x4 rotateMatrixXInv = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0.0f, 0.0f, -90f), Vector3.one);
  public static readonly Matrix4x4 rotateMatrixZ = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(90f, 0.0f, 0.0f), Vector3.one);
  public static readonly Matrix4x4 rotateMatrixZInv = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(-90f, 0.0f, 0.0f), Vector3.one);
  public static readonly Quaternion rotateQuaternionX = Quaternion.Euler(0.0f, 0.0f, 90f);
  public static readonly Quaternion rotateQuaternionZ = Quaternion.Euler(90f, 0.0f, 0.0f);
  public static readonly Quaternion rotateQuaternionXInv = Quaternion.Euler(0.0f, 0.0f, -90f);
  public static readonly Quaternion rotateQuaternionZInv = Quaternion.Euler(-90f, 0.0f, 0.0f);
  public WorldProperty globalWorldProperty;
  private List<MMD4MecanimBulletPhysicsImpl.MMDModel> _mmdModelList = new List<MMD4MecanimBulletPhysicsImpl.MMDModel>();
  private List<MMD4MecanimBulletPhysicsImpl.RigidBody> _rigidBodyList = new List<MMD4MecanimBulletPhysicsImpl.RigidBody>();
  private bool _isAwaked;
  private MMD4MecanimBulletPhysicsImpl.World _globalWorld;
  private static MMD4MecanimBulletPhysicsImpl _instance;
  private bool _initialized;
  public const string DllName = "MMD4MecanimBulletPhysics";

  public MMD4MecanimBulletPhysicsImpl.World globalWorld
  {
    get
    {
      this._ActivateGlobalWorld();
      return this._globalWorld;
    }
  }

  public static MMD4MecanimBulletPhysicsImpl instanceImpl
  {
    get
    {
      if ((UnityEngine.Object) MMD4MecanimBulletPhysicsImpl._instance == (UnityEngine.Object) null)
      {
        MMD4MecanimBulletPhysicsImpl._instance = (MMD4MecanimBulletPhysicsImpl) UnityEngine.Object.FindObjectOfType(typeof (MMD4MecanimBulletPhysicsImpl));
        if ((UnityEngine.Object) MMD4MecanimBulletPhysicsImpl._instance == (UnityEngine.Object) null)
        {
          MMD4MecanimBulletPhysicsImpl bulletPhysicsImpl = new GameObject("MMD4MecanimBulletPhysics").AddComponent<MMD4MecanimBulletPhysicsImpl>();
          if ((UnityEngine.Object) MMD4MecanimBulletPhysicsImpl._instance == (UnityEngine.Object) null)
            MMD4MecanimBulletPhysicsImpl._instance = bulletPhysicsImpl;
        }
        if ((UnityEngine.Object) MMD4MecanimBulletPhysicsImpl._instance != (UnityEngine.Object) null)
          MMD4MecanimBulletPhysicsImpl._instance._Initialize();
      }
      return MMD4MecanimBulletPhysicsImpl._instance;
    }
  }

  private void _Initialize()
  {
    if (this._initialized)
      return;
    this._initialized = true;
    MMD4MecanimBulletPhysicsImpl._InitializeEngine();
    this.StartCoroutine(this.DelayedAwake());
  }

  private void Awake()
  {
    if ((UnityEngine.Object) MMD4MecanimBulletPhysicsImpl._instance == (UnityEngine.Object) null)
      MMD4MecanimBulletPhysicsImpl._instance = this;
    else if ((UnityEngine.Object) MMD4MecanimBulletPhysicsImpl._instance != (UnityEngine.Object) this)
    {
      if (Application.isEditor)
      {
        if (Application.isPlaying)
        {
          UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
          return;
        }
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.gameObject);
        return;
      }
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
      return;
    }
    this._Initialize();
  }

  private void LateUpdate() => this._InternalUpdate();

  private void _InternalUpdate()
  {
    if (!this._isAwaked)
      return;
    float deltaTime = Time.deltaTime;
    for (int index = 0; index != this._rigidBodyList.Count; ++index)
      this._rigidBodyList[index].Update();
    for (int index = 0; index != this._mmdModelList.Count; ++index)
      this._mmdModelList[index].Update(deltaTime);
    MMD4MecanimBulletPhysicsImpl.World globalWorld = this.globalWorld;
    if (globalWorld != null)
    {
      globalWorld.Update(deltaTime);
      globalWorld.Synchronize();
    }
    for (int index = 0; index != this._rigidBodyList.Count; ++index)
      this._rigidBodyList[index].LateUpdate();
    for (int index = 0; index != this._mmdModelList.Count; ++index)
      this._mmdModelList[index].LateUpdate();
    MMD4MecanimBulletPhysicsImpl.DebugLog();
  }

  private void OnDestroy()
  {
    for (int index = 0; index != this._rigidBodyList.Count; ++index)
      this._rigidBodyList[index].Destroy();
    for (int index = 0; index != this._mmdModelList.Count; ++index)
      this._mmdModelList[index].Destroy();
    this._rigidBodyList.Clear();
    this._mmdModelList.Clear();
    if (this._globalWorld != null)
    {
      this._globalWorld.Destroy();
      this._globalWorld = (MMD4MecanimBulletPhysicsImpl.World) null;
    }
    if (!((UnityEngine.Object) MMD4MecanimBulletPhysicsImpl._instance == (UnityEngine.Object) this))
      return;
    MMD4MecanimBulletPhysicsImpl._instance = (MMD4MecanimBulletPhysicsImpl) null;
    MMD4MecanimBulletPhysicsImpl._FinalizeEngine();
  }

  private IEnumerator DelayedAwake()
  {
    yield return (object) new WaitForEndOfFrame();
    this._isAwaked = true;
  }

  private void _ActivateGlobalWorld()
  {
    if (this._globalWorld == null)
      this._globalWorld = new MMD4MecanimBulletPhysicsImpl.World();
    if (this.globalWorldProperty == null)
      this.globalWorldProperty = new WorldProperty();
    if (!this._globalWorld.isExpired)
      return;
    this._globalWorld.Create(this.globalWorldProperty);
  }

  public static bool Invoke(byte[] data = null)
  {
    if (data != null && data.Length != 0)
    {
      GCHandle gcHandle = GCHandle.Alloc((object) data, GCHandleType.Pinned);
      int num = MMD4MecanimBulletPhysicsImpl._Invoke(gcHandle.AddrOfPinnedObject(), data.Length) ? 1 : 0;
      gcHandle.Free();
      return num != 0;
    }
    return MMD4MecanimBulletPhysicsImpl._Invoke(IntPtr.Zero, 0);
  }

  public static void DebugLog()
  {
    IntPtr ptr = MMD4MecanimBulletPhysicsImpl._DebugLog(1);
    if (!(ptr != IntPtr.Zero))
      return;
    Debug.Log((object) Marshal.PtrToStringUni(ptr));
  }

  public MMD4MecanimBulletPhysicsImpl.MMDModel CreateMMDModel(
    MMD4MecanimModelImpl model)
  {
    MMD4MecanimBulletPhysicsImpl.MMDModel mmdModel = new MMD4MecanimBulletPhysicsImpl.MMDModel();
    if (!mmdModel.Create(model))
    {
      Debug.LogError((object) ("CreateMMDModel: Failed " + model.gameObject.name));
      return (MMD4MecanimBulletPhysicsImpl.MMDModel) null;
    }
    this._mmdModelList.Add(mmdModel);
    return mmdModel;
  }

  public void DestroyMMDModel(MMD4MecanimBulletPhysicsImpl.MMDModel mmdModel)
  {
    for (int index = 0; index < this._mmdModelList.Count; ++index)
    {
      if (this._mmdModelList[index] == mmdModel)
      {
        mmdModel.Destroy();
        this._mmdModelList.Remove(mmdModel);
        break;
      }
    }
  }

  public MMD4MecanimBulletPhysicsImpl.RigidBody CreateRigidBody(
    MMD4MecanimRigidBodyImpl rigidBody)
  {
    MMD4MecanimBulletPhysicsImpl.RigidBody rigidBody1 = new MMD4MecanimBulletPhysicsImpl.RigidBody();
    if (!rigidBody1.Create(rigidBody))
      return (MMD4MecanimBulletPhysicsImpl.RigidBody) null;
    this._rigidBodyList.Add(rigidBody1);
    return rigidBody1;
  }

  public void DestroyRigidBody(MMD4MecanimBulletPhysicsImpl.RigidBody rigidBody)
  {
    for (int index = 0; index < this._rigidBodyList.Count; ++index)
    {
      if (this._rigidBodyList[index] == rigidBody)
      {
        rigidBody.Destroy();
        this._rigidBodyList.Remove(rigidBody);
        break;
      }
    }
  }

  private static void _InitializeEngine() => MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsInitialize();

  private static void _FinalizeEngine() => MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsFinalize();

  private static bool _Invoke(IntPtr dataPtr, int dataLength) => (uint) MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsInvoke(dataPtr, dataLength) > 0U;

  private static IntPtr _DebugLog(int clanupFlag) => MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsDebugLog(clanupFlag);

  private static IntPtr _CreateWorld(
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength)
  {
    return MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsCreateWorld(iValues, iValueLength, fValues, fValueLength);
  }

  private static void _ConfigWorld(
    IntPtr worldPtr,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength)
  {
    MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsConfigWorld(worldPtr, iValues, iValueLength, fValues, fValueLength);
  }

  private static void _DestroyWorld(IntPtr worldPtr) => MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsDestroyWorld(worldPtr);

  private static void _UpdateWorld(
    IntPtr worldPtr,
    float deltaTime,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength)
  {
    MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsUpdateWorld(worldPtr, deltaTime, iValues, iValueLength, fValues, fValueLength);
  }

  private static void _SynchronizeWorld(IntPtr worldPtr) => MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsSynchronizeWorld(worldPtr);

  private static IntPtr _CreateRigidBody(
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength)
  {
    return MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsCreateRigidBody(iValues, iValueLength, fValues, fValueLength);
  }

  private static void _ConfigRigidBody(
    IntPtr rigidBodyPtr,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength)
  {
    MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsConfigRigidBody(rigidBodyPtr, iValues, iValueLength, fValues, fValueLength);
  }

  private static void _DestroyRigidBody(IntPtr rigidBodyPtr) => MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsDestroyRigidBody(rigidBodyPtr);

  private static void _JoinWorldRigidBody(IntPtr worldPtr, IntPtr rigidBodyPtr) => MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsJoinWorldRigidBody(worldPtr, rigidBodyPtr);

  private static void _LeaveWorldRigidBody(IntPtr rigidBodyPtr) => MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsLeaveWorldRigidBody(rigidBodyPtr);

  private static void _ResetWorldRigidBody(IntPtr rigidBodyPtr) => MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsResetWorldRigidBody(rigidBodyPtr);

  private static void _UpdateRigidBody(
    IntPtr rigidBodyPtr,
    int updateFlags,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength)
  {
    MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsUpdateRigidBody(rigidBodyPtr, updateFlags, iValues, iValueLength, fValues, fValueLength);
  }

  private static int _LateUpdateRigidBody(
    IntPtr rigidBodyPtr,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength)
  {
    return MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsLateUpdateRigidBody(rigidBodyPtr, iValues, iValueLength, fValues, fValueLength);
  }

  private static IntPtr _CreateMMDModel(
    IntPtr mmdModelBytes,
    int mmdModelLength,
    IntPtr mmdIndexBytes,
    int mmdIndexLength,
    IntPtr mmdVertexBytes,
    int mmdVertexLength,
    IntPtr iMeshValues,
    int meshLength,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength)
  {
    return MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsCreateMMDModel(mmdModelBytes, mmdModelLength, mmdIndexBytes, mmdIndexLength, mmdVertexBytes, mmdVertexLength, iMeshValues, meshLength, iValues, iValueLength, fValues, fValueLength);
  }

  private static int _GetFullBodyIKDataMMDModel(
    IntPtr mmdModelPtr,
    IntPtr fullBodyIKBoneID,
    IntPtr fullBodyIKPositions,
    IntPtr fullBodyIKRotations,
    int fullBodyIKLength)
  {
    return MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsGetFullBodyIKDataMMDModel(mmdModelPtr, fullBodyIKBoneID, fullBodyIKPositions, fullBodyIKRotations, fullBodyIKLength);
  }

  private static int _UploadMeshMMDModel(
    IntPtr mmdModelPtr,
    int meshID,
    IntPtr vertices,
    IntPtr normals,
    IntPtr boneWeights,
    int vertexLength,
    IntPtr bindposes,
    int boneLength)
  {
    return MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsUploadMeshMMDModel(mmdModelPtr, meshID, vertices, normals, boneWeights, vertexLength, bindposes, boneLength);
  }

  private static void _ConfigMMDModel(
    IntPtr mmdModelPtr,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength)
  {
    MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsConfigMMDModel(mmdModelPtr, iValues, iValueLength, fValues, fValueLength);
  }

  private static void _DestroyMMDModel(IntPtr mmdModelPtr) => MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsDestroyMMDModel(mmdModelPtr);

  private static void _JoinWorldMMDModel(IntPtr worldPtr, IntPtr mmdModelPtr) => MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsJoinWorldMMDModel(worldPtr, mmdModelPtr);

  private static void _LeaveWorldMMDModel(IntPtr mmdModelPtr) => MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsLeaveWorldMMDModel(mmdModelPtr);

  private static void _ResetWorldMMDModel(IntPtr mmdModelPtr) => MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsResetWorldMMDModel(mmdModelPtr);

  private static int _PreUpdateMMDModel(
    IntPtr mmdModelPtr,
    int updateFlags,
    IntPtr iBoneValues,
    int boneLength,
    IntPtr iRigidBodyValues,
    int rigidBodyLength,
    IntPtr ikWeights,
    int ikLength,
    IntPtr morphWeights,
    int morphLength,
    IntPtr iFullBodyIKValues,
    IntPtr fullBodyIKWeights,
    int fullBodyIKLength)
  {
    return MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsPreUpdateMMDModel(mmdModelPtr, updateFlags, iBoneValues, boneLength, iRigidBodyValues, rigidBodyLength, ikWeights, ikLength, morphWeights, morphLength, iFullBodyIKValues, fullBodyIKWeights, fullBodyIKLength);
  }

  private static void _UpdateMMDModel(
    IntPtr mmdModelPtr,
    int updateFlags,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength,
    IntPtr modelTransform,
    IntPtr iBoneValues,
    IntPtr boneTransforms,
    IntPtr boneLocalPositions,
    IntPtr boneLocalRotations,
    IntPtr boneUserPositions,
    IntPtr boneUserRotations,
    int boneLength,
    IntPtr iRigidBodyValues,
    int rigidBodyLength,
    IntPtr ikWeights,
    int ikLength,
    IntPtr morphWeights,
    int morphLength,
    IntPtr iFullBodyIKValues,
    IntPtr fullBodyIKTransforms,
    IntPtr fullBodyIKWeights,
    int fullBodyIKLength)
  {
    MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsUpdateMMDModel(mmdModelPtr, updateFlags, iValues, iValueLength, fValues, fValueLength, modelTransform, iBoneValues, boneTransforms, boneLocalPositions, boneLocalRotations, boneUserPositions, boneUserRotations, boneLength, iRigidBodyValues, rigidBodyLength, ikWeights, ikLength, morphWeights, morphLength, iFullBodyIKValues, fullBodyIKTransforms, fullBodyIKWeights, fullBodyIKLength);
  }

  private static int _LateUpdateMMDModel(
    IntPtr mmdModelPtr,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength,
    IntPtr iBoneValues,
    IntPtr bonePositions,
    IntPtr boneRotations,
    int boneLength,
    IntPtr iMeshValues,
    int meshLength,
    IntPtr morphWeigts,
    int morphLength)
  {
    return MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsLateUpdateMMDModel(mmdModelPtr, iValues, iValueLength, fValues, fValueLength, iBoneValues, bonePositions, boneRotations, boneLength, iMeshValues, meshLength, morphWeigts, morphLength);
  }

  private static int _LateUpdateMeshMMDModel(
    IntPtr mmdModelPtr,
    int meshID,
    IntPtr vertices,
    IntPtr normals,
    int vertexLength)
  {
    return MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsLateUpdateMeshMMDModel(mmdModelPtr, meshID, vertices, normals, vertexLength);
  }

  private static void _ConfigBoneMMDModel(
    IntPtr mmdModelPtr,
    int boneID,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength)
  {
    MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsConfigBoneMMDModel(mmdModelPtr, boneID, iValues, iValueLength, fValues, fValueLength);
  }

  private static void _ConfigRigidBodyMMDModel(
    IntPtr mmdModelPtr,
    int rigidBodyID,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength)
  {
    MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsConfigRigidBodyMMDModel(mmdModelPtr, rigidBodyID, iValues, iValueLength, fValues, fValueLength);
  }

  private static void _ConfigJointMMDModel(
    IntPtr mmdModelPtr,
    int jointID,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength)
  {
    MMD4MecanimBulletPhysicsImpl.MMD4MecanimBulletPhysicsConfigJointMMDModel(mmdModelPtr, jointID, iValues, iValueLength, fValues, fValueLength);
  }

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsInitialize();

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsFinalize();

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern int MMD4MecanimBulletPhysicsInvoke(IntPtr dataPtr, int dataLength);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern IntPtr MMD4MecanimBulletPhysicsDebugLog(int clanupFlag);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern IntPtr MMD4MecanimBulletPhysicsCreateWorld(
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsConfigWorld(
    IntPtr worldPtr,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsDestroyWorld(IntPtr worldPtr);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsUpdateWorld(
    IntPtr worldPtr,
    float deltaTime,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsSynchronizeWorld(IntPtr worldPtr);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern IntPtr MMD4MecanimBulletPhysicsCreateRigidBody(
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsConfigRigidBody(
    IntPtr rigidBodyPtr,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsDestroyRigidBody(IntPtr rigidBodyPtr);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsJoinWorldRigidBody(
    IntPtr worldPtr,
    IntPtr rigidBodyPtr);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsLeaveWorldRigidBody(IntPtr rigidBodyPtr);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsResetWorldRigidBody(IntPtr rigidBodyPtr);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsUpdateRigidBody(
    IntPtr rigidBodyPtr,
    int updateFlags,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern int MMD4MecanimBulletPhysicsLateUpdateRigidBody(
    IntPtr rigidBodyPtr,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern IntPtr MMD4MecanimBulletPhysicsCreateMMDModel(
    IntPtr mmdModelBytes,
    int mmdModelLength,
    IntPtr mmdIndexBytes,
    int mmdIndexLength,
    IntPtr mmdVertexBytes,
    int mmdVertexLength,
    IntPtr iMeshValues,
    int meshLength,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern int MMD4MecanimBulletPhysicsGetFullBodyIKDataMMDModel(
    IntPtr mmdModelPtr,
    IntPtr fullBodyIKBoneID,
    IntPtr fullBodyIKPositions,
    IntPtr fullBodyIKRotations,
    int fullBodyIKLength);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern int MMD4MecanimBulletPhysicsUploadMeshMMDModel(
    IntPtr mmdModelPtr,
    int meshID,
    IntPtr vertices,
    IntPtr normals,
    IntPtr boneWeights,
    int vertexLength,
    IntPtr bindposes,
    int boneLength);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsConfigMMDModel(
    IntPtr mmdModelPtr,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsDestroyMMDModel(IntPtr mmdModelPtr);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsJoinWorldMMDModel(
    IntPtr worldPtr,
    IntPtr mmdModelPtr);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsLeaveWorldMMDModel(IntPtr mmdModelPtr);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsResetWorldMMDModel(IntPtr mmdModelPtr);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern int MMD4MecanimBulletPhysicsPreUpdateMMDModel(
    IntPtr mmdModelPtr,
    int updateFlags,
    IntPtr iBoneValues,
    int boneLength,
    IntPtr iRigidBodyValues,
    int rigidBodyLength,
    IntPtr ikWeights,
    int ikLength,
    IntPtr morphWeights,
    int morphLength,
    IntPtr iFullBodyIKValues,
    IntPtr fullBodyIKWeights,
    int fullBodyIKLength);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsUpdateMMDModel(
    IntPtr mmdModelPtr,
    int updateFlags,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength,
    IntPtr modelTransform,
    IntPtr iBoneValues,
    IntPtr boneTransforms,
    IntPtr boneLocalPositions,
    IntPtr boneLocalRotations,
    IntPtr boneUserPositions,
    IntPtr boneUserRotations,
    int boneLength,
    IntPtr iRigidBodyValues,
    int rigidBodyLength,
    IntPtr ikWeights,
    int ikLength,
    IntPtr morphWeights,
    int morphLength,
    IntPtr iFullBodyIKValues,
    IntPtr fullBodyIKTransforms,
    IntPtr fullBodyIKWeights,
    int fullBodyIKLength);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern int MMD4MecanimBulletPhysicsLateUpdateMMDModel(
    IntPtr mmdModelPtr,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength,
    IntPtr iBoneValues,
    IntPtr bonePositions,
    IntPtr boneRotations,
    int boneLength,
    IntPtr iMeshValues,
    int meshLength,
    IntPtr morphWeigts,
    int morphLength);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern int MMD4MecanimBulletPhysicsLateUpdateMeshMMDModel(
    IntPtr mmdModelPtr,
    int meshID,
    IntPtr vertices,
    IntPtr normals,
    int vertexLength);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsConfigBoneMMDModel(
    IntPtr mmdModelPtr,
    int boneID,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsConfigRigidBodyMMDModel(
    IntPtr mmdModelPtr,
    int rigidBodyID,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength);

  [DllImport("MMD4MecanimBulletPhysics")]
  public static extern void MMD4MecanimBulletPhysicsConfigJointMMDModel(
    IntPtr mmdModelPtr,
    int jointID,
    IntPtr iValues,
    int iValueLength,
    IntPtr fValues,
    int fValueLength);

  public class World
  {
    public WorldProperty worldProperty;
    public IntPtr worldPtr = IntPtr.Zero;
    private float _gravityScaleCached = 10f;
    private float _gravityNoiseCached;
    private Vector3 _gravityDirectionCached = new Vector3(0.0f, -1f, 0.0f);
    private bool _isDirtyProperty = true;
    private MMD4MecanimCommon.PropertyWriter _updatePropertyWriter = new MMD4MecanimCommon.PropertyWriter();

    ~World() => this.Destroy();

    public bool isExpired => this.worldPtr == IntPtr.Zero;

    public bool Create() => this.Create((WorldProperty) null);

    public bool Create(WorldProperty worldProperty)
    {
      this.Destroy();
      this.worldProperty = worldProperty == null ? new WorldProperty() : worldProperty;
      this._gravityScaleCached = this.worldProperty.gravityScale;
      this._gravityNoiseCached = this.worldProperty.gravityNoise;
      this._gravityDirectionCached = this.worldProperty.gravityDirection;
      if (this.worldProperty != null)
      {
        Vector3 gravityDirection = this.worldProperty.gravityDirection;
        gravityDirection.z = -gravityDirection.z;
        MMD4MecanimCommon.PropertyWriter propertyWriter = new MMD4MecanimCommon.PropertyWriter();
        propertyWriter.Write("accurateStep", this.worldProperty.accurateStep);
        propertyWriter.Write("optimizeSettings", this.worldProperty.optimizeSettings);
        propertyWriter.Write("multiThreading", this.worldProperty.multiThreading);
        propertyWriter.Write("multiThreadingSynchronize", this.worldProperty.multiThreadingSynchronize);
        propertyWriter.Write("parallelDispatcher", this.worldProperty.parallelDispatcher);
        propertyWriter.Write("parallelSolver", this.worldProperty.parallelSolver);
        propertyWriter.Write("framePerSecond", this.worldProperty.framePerSecond);
        propertyWriter.Write("resetFrameRate", this.worldProperty.resetFrameRate);
        propertyWriter.Write("limitDeltaFrames", this.worldProperty.limitDeltaFrames);
        propertyWriter.Write("axisSweepDistance", this.worldProperty.axisSweepDistance);
        propertyWriter.Write("gravityScale", this.worldProperty.gravityScale);
        propertyWriter.Write("gravityNoise", this.worldProperty.gravityNoise);
        propertyWriter.Write("gravityDirection", gravityDirection);
        propertyWriter.Write("vertexScale", this.worldProperty.vertexScale);
        propertyWriter.Write("importScale", this.worldProperty.importScale);
        propertyWriter.Write("worldSolverInfoNumIterations", this.worldProperty.worldSolverInfoNumIterations);
        propertyWriter.Write("worldSolverInfoSplitImpulse", this.worldProperty.worldSolverInfoSplitImpulse);
        propertyWriter.Write("worldAddFloorPlane", this.worldProperty.worldAddFloorPlane);
        propertyWriter.Lock();
        this.worldPtr = MMD4MecanimBulletPhysicsImpl._CreateWorld(propertyWriter.iValuesPtr, propertyWriter.iValueLength, propertyWriter.fValuesPtr, propertyWriter.fValueLength);
        propertyWriter.Unlock();
      }
      else
        this.worldPtr = MMD4MecanimBulletPhysicsImpl._CreateWorld(IntPtr.Zero, 0, IntPtr.Zero, 0);
      MMD4MecanimBulletPhysicsImpl.DebugLog();
      return this.worldPtr != IntPtr.Zero;
    }

    public void SetGravity(float gravityScale, float gravityNoise, Vector3 gravityDirection)
    {
      if ((double) this._gravityScaleCached == (double) gravityScale && (double) this._gravityNoiseCached == (double) gravityNoise && !(this._gravityDirectionCached != gravityDirection))
        return;
      this._gravityScaleCached = gravityScale;
      this._gravityNoiseCached = gravityNoise;
      this._gravityDirectionCached = gravityDirection;
      this._isDirtyProperty = true;
      this.worldProperty.gravityScale = gravityScale;
      this.worldProperty.gravityNoise = gravityNoise;
      this.worldProperty.gravityDirection = gravityDirection;
    }

    public void Destroy()
    {
      if (this.worldPtr != IntPtr.Zero)
      {
        IntPtr worldPtr = this.worldPtr;
        this.worldPtr = IntPtr.Zero;
        MMD4MecanimBulletPhysicsImpl._DestroyWorld(worldPtr);
      }
      this.worldProperty = (WorldProperty) null;
    }

    public void Update(float deltaTime)
    {
      if ((double) this.worldProperty.gravityScale != (double) this._gravityScaleCached || (double) this.worldProperty.gravityNoise != (double) this._gravityNoiseCached || this.worldProperty.gravityDirection != this._gravityDirectionCached)
      {
        this._gravityScaleCached = this.worldProperty.gravityScale;
        this._gravityNoiseCached = this.worldProperty.gravityNoise;
        this._gravityDirectionCached = this.worldProperty.gravityDirection;
        this._isDirtyProperty = true;
      }
      if (!(this.worldPtr != IntPtr.Zero))
        return;
      if (this._isDirtyProperty)
      {
        this._isDirtyProperty = false;
        this._updatePropertyWriter.Clear();
        this._updatePropertyWriter.Write("gravityScale", this._gravityScaleCached);
        this._updatePropertyWriter.Write("gravityNoise", this._gravityNoiseCached);
        this._updatePropertyWriter.Write("gravityDirection", this._gravityDirectionCached);
        this._updatePropertyWriter.Lock();
        MMD4MecanimBulletPhysicsImpl._UpdateWorld(this.worldPtr, deltaTime, this._updatePropertyWriter.iValuesPtr, this._updatePropertyWriter.iValueLength, this._updatePropertyWriter.fValuesPtr, this._updatePropertyWriter.fValueLength);
        this._updatePropertyWriter.Unlock();
      }
      else
        MMD4MecanimBulletPhysicsImpl._UpdateWorld(this.worldPtr, deltaTime, IntPtr.Zero, 0, IntPtr.Zero, 0);
    }

    public void Synchronize()
    {
      if (!(this.worldPtr != IntPtr.Zero))
        return;
      MMD4MecanimBulletPhysicsImpl._SynchronizeWorld(this.worldPtr);
    }
  }

  public class RigidBody
  {
    public MMD4MecanimRigidBodyImpl rigidBody;
    public IntPtr rigidBodyPtr = IntPtr.Zero;
    private float[] fValues = new float[7];
    private SphereCollider _sphereCollider;
    private BoxCollider _boxCollider;
    private CapsuleCollider _capsuleCollider;

    private Vector3 _center
    {
      get
      {
        if ((UnityEngine.Object) this._sphereCollider != (UnityEngine.Object) null)
          return this._sphereCollider.center;
        if ((UnityEngine.Object) this._boxCollider != (UnityEngine.Object) null)
          return this._boxCollider.center;
        return (UnityEngine.Object) this._capsuleCollider != (UnityEngine.Object) null ? this._capsuleCollider.center : Vector3.zero;
      }
    }

    ~RigidBody() => this.Destroy();

    public bool isExpired => this.rigidBodyPtr == IntPtr.Zero;

    public bool Create(MMD4MecanimRigidBodyImpl rigidBody)
    {
      this.Destroy();
      if ((UnityEngine.Object) rigidBody == (UnityEngine.Object) null)
        return false;
      MMD4MecanimBulletPhysicsImpl.World world = (MMD4MecanimBulletPhysicsImpl.World) null;
      if ((UnityEngine.Object) MMD4MecanimBulletPhysicsImpl.instanceImpl != (UnityEngine.Object) null)
        world = MMD4MecanimBulletPhysicsImpl.instanceImpl.globalWorld;
      if (world == null)
        return false;
      MMD4MecanimCommon.PropertyWriter propertyWriter = new MMD4MecanimCommon.PropertyWriter();
      Matrix4x4 localToWorldMatrix = rigidBody.transform.localToWorldMatrix;
      Vector3 vector3_1 = rigidBody.transform.position;
      Quaternion rotation = rigidBody.transform.rotation;
      Vector3 matrixScale = MMD4MecanimCommon.ComputeMatrixScale(ref localToWorldMatrix);
      Vector3 center = this._center;
      if (center != Vector3.zero)
        vector3_1 = localToWorldMatrix.MultiplyPoint3x4(center);
      SphereCollider component1 = rigidBody.gameObject.GetComponent<SphereCollider>();
      if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
      {
        float x = component1.radius * Mathf.Max(Mathf.Max(matrixScale.x, matrixScale.y), matrixScale.z);
        if (propertyWriter != null)
        {
          propertyWriter.Write("shapeType", 0);
          propertyWriter.Write("shapeSize", new Vector3(x, 0.0f, 0.0f));
        }
      }
      BoxCollider component2 = rigidBody.gameObject.GetComponent<BoxCollider>();
      if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
      {
        Vector3 size = component2.size;
        size.x *= matrixScale.x;
        size.y *= matrixScale.y;
        size.z *= matrixScale.z;
        if (propertyWriter != null)
        {
          propertyWriter.Write("shapeType", 1);
          propertyWriter.Write("shapeSize", size * 0.5f);
        }
      }
      CapsuleCollider component3 = rigidBody.gameObject.GetComponent<CapsuleCollider>();
      if ((UnityEngine.Object) component3 != (UnityEngine.Object) null)
      {
        Vector3 vector3_2 = new Vector3(component3.radius, component3.height, 0.0f);
        vector3_2.x *= Mathf.Max(matrixScale.x, matrixScale.z);
        vector3_2.y *= matrixScale.y;
        vector3_2.y -= component3.radius * 2f;
        if (propertyWriter != null)
        {
          propertyWriter.Write("shapeType", 2);
          propertyWriter.Write("shapeSize", vector3_2);
        }
      }
      this._sphereCollider = component1;
      this._boxCollider = component2;
      this._capsuleCollider = component3;
      if ((UnityEngine.Object) component3 != (UnityEngine.Object) null)
      {
        if (component3.direction == 0)
          rotation *= MMD4MecanimBulletPhysicsImpl.rotateQuaternionX;
        else if (component3.direction == 2)
          rotation *= MMD4MecanimBulletPhysicsImpl.rotateQuaternionZ;
      }
      if (world.worldProperty != null)
        propertyWriter?.Write("worldScale", world.worldProperty.worldScale);
      else
        propertyWriter?.Write("worldScale", 1f);
      if (propertyWriter != null)
      {
        propertyWriter.Write("position", vector3_1);
        propertyWriter.Write("rotation", rotation);
      }
      int num1 = 0;
      if (rigidBody.bulletPhysicsRigidBodyProperty != null)
      {
        if (rigidBody.bulletPhysicsRigidBodyProperty.isKinematic)
          num1 |= 1;
        if (rigidBody.bulletPhysicsRigidBodyProperty.isAdditionalDamping)
          num1 |= 2;
        float num2 = rigidBody.bulletPhysicsRigidBodyProperty.mass;
        if (rigidBody.bulletPhysicsRigidBodyProperty.isKinematic)
          num2 = 0.0f;
        if (propertyWriter != null)
        {
          propertyWriter.Write("mass", num2);
          propertyWriter.Write("linearDamping", rigidBody.bulletPhysicsRigidBodyProperty.linearDamping);
          propertyWriter.Write("angularDamping", rigidBody.bulletPhysicsRigidBodyProperty.angularDamping);
          propertyWriter.Write("restitution", rigidBody.bulletPhysicsRigidBodyProperty.restitution);
          propertyWriter.Write("friction", rigidBody.bulletPhysicsRigidBodyProperty.friction);
        }
      }
      if (propertyWriter != null)
      {
        propertyWriter.Write("flags", num1);
        propertyWriter.Write("group", (int) ushort.MaxValue);
        propertyWriter.Write("mask", (int) ushort.MaxValue);
      }
      propertyWriter.Lock();
      IntPtr rigidBody1 = MMD4MecanimBulletPhysicsImpl._CreateRigidBody(propertyWriter.iValuesPtr, propertyWriter.iValueLength, propertyWriter.fValuesPtr, propertyWriter.fValueLength);
      propertyWriter.Unlock();
      if (rigidBody1 != IntPtr.Zero)
      {
        MMD4MecanimBulletPhysicsImpl._JoinWorldRigidBody(world.worldPtr, rigidBody1);
        MMD4MecanimBulletPhysicsImpl.DebugLog();
        this.rigidBody = rigidBody;
        this.rigidBodyPtr = rigidBody1;
        return true;
      }
      MMD4MecanimBulletPhysicsImpl.DebugLog();
      return false;
    }

    public void Update()
    {
      if (!((UnityEngine.Object) this.rigidBody != (UnityEngine.Object) null) || this.rigidBody.bulletPhysicsRigidBodyProperty == null)
        return;
      int num1 = this.rigidBody.bulletPhysicsRigidBodyProperty.isKinematic ? 1 : 0;
      bool isFreezed = this.rigidBody.bulletPhysicsRigidBodyProperty.isFreezed;
      int num2 = isFreezed ? 1 : 0;
      if ((num1 | num2) != 0)
      {
        int updateFlags = 0;
        if (isFreezed)
          updateFlags |= 1;
        Vector3 vector3 = this.rigidBody.transform.position;
        Quaternion rotation = this.rigidBody.transform.rotation;
        Vector3 center = this._center;
        if (center != Vector3.zero)
          vector3 = this.rigidBody.transform.localToWorldMatrix.MultiplyPoint3x4(center);
        if ((UnityEngine.Object) this._capsuleCollider != (UnityEngine.Object) null)
        {
          if (this._capsuleCollider.direction == 0)
            rotation *= MMD4MecanimBulletPhysicsImpl.rotateQuaternionX;
          else if (this._capsuleCollider.direction == 2)
            rotation *= MMD4MecanimBulletPhysicsImpl.rotateQuaternionZ;
        }
        if (!(this.rigidBodyPtr != IntPtr.Zero))
          return;
        this.fValues[0] = vector3.x;
        this.fValues[1] = vector3.y;
        this.fValues[2] = vector3.z;
        this.fValues[3] = rotation.x;
        this.fValues[4] = rotation.y;
        this.fValues[5] = rotation.z;
        this.fValues[6] = rotation.w;
        GCHandle gcHandle = GCHandle.Alloc((object) this.fValues, GCHandleType.Pinned);
        MMD4MecanimBulletPhysicsImpl._UpdateRigidBody(this.rigidBodyPtr, updateFlags, IntPtr.Zero, 0, gcHandle.AddrOfPinnedObject(), this.fValues.Length);
        gcHandle.Free();
      }
      else
      {
        if (!(this.rigidBodyPtr != IntPtr.Zero))
          return;
        GCHandle gcHandle = GCHandle.Alloc((object) this.fValues, GCHandleType.Pinned);
        MMD4MecanimBulletPhysicsImpl._UpdateRigidBody(this.rigidBodyPtr, 0, IntPtr.Zero, 0, IntPtr.Zero, 0);
        gcHandle.Free();
      }
    }

    public void LateUpdate()
    {
      if (!((UnityEngine.Object) this.rigidBody != (UnityEngine.Object) null) || this.rigidBody.bulletPhysicsRigidBodyProperty == null)
        return;
      int num1 = this.rigidBody.bulletPhysicsRigidBodyProperty.isKinematic ? 1 : 0;
      bool isFreezed = this.rigidBody.bulletPhysicsRigidBodyProperty.isFreezed;
      if (num1 != 0 || isFreezed)
        return;
      Vector3 vector3 = Vector3.one;
      Quaternion quaternion = Quaternion.identity;
      int num2 = 0;
      if (this.rigidBodyPtr != IntPtr.Zero)
      {
        GCHandle gcHandle = GCHandle.Alloc((object) this.fValues, GCHandleType.Pinned);
        num2 = MMD4MecanimBulletPhysicsImpl._LateUpdateRigidBody(this.rigidBodyPtr, IntPtr.Zero, 0, gcHandle.AddrOfPinnedObject(), this.fValues.Length);
        gcHandle.Free();
        vector3 = new Vector3(this.fValues[0], this.fValues[1], this.fValues[2]);
        quaternion = new Quaternion(this.fValues[3], this.fValues[4], this.fValues[5], this.fValues[6]);
      }
      if (num2 == 0)
        return;
      if ((UnityEngine.Object) this._capsuleCollider != (UnityEngine.Object) null)
      {
        if (this._capsuleCollider.direction == 0)
          quaternion *= MMD4MecanimBulletPhysicsImpl.rotateQuaternionXInv;
        else if (this._capsuleCollider.direction == 2)
          quaternion *= MMD4MecanimBulletPhysicsImpl.rotateQuaternionZInv;
      }
      this.rigidBody.gameObject.transform.position = vector3;
      this.rigidBody.gameObject.transform.rotation = quaternion;
      Vector3 center = this._center;
      if (!(center != Vector3.zero))
        return;
      this.rigidBody.gameObject.transform.localPosition = this.rigidBody.gameObject.transform.localPosition - center;
    }

    public void Destroy()
    {
      if (this.rigidBodyPtr != IntPtr.Zero)
      {
        IntPtr rigidBodyPtr = this.rigidBodyPtr;
        this.rigidBodyPtr = IntPtr.Zero;
        MMD4MecanimBulletPhysicsImpl._DestroyRigidBody(rigidBodyPtr);
      }
      this._sphereCollider = (SphereCollider) null;
      this._boxCollider = (BoxCollider) null;
      this._capsuleCollider = (CapsuleCollider) null;
      this.rigidBody = (MMD4MecanimRigidBodyImpl) null;
    }

    [Flags]
    public enum UpdateFlags
    {
      Freezed = 1,
    }
  }

  public class MMDModel
  {
    public MMD4MecanimBulletPhysicsImpl.World localWorld;
    public MMD4MecanimModelImpl model;
    public IntPtr mmdModelPtr = IntPtr.Zero;
    private Transform _modelTransform;
    private bool _physicsEnabled;
    private Transform[] _boneTransformList;
    private int[] _updateRigidBodyFlagsList;
    private float[] _updateIKWeightList;
    private float[] _updateMorphWeightList;
    private float[] _ikWeightList;
    private bool[] _ikDisabledList;
    private int[] _fullBodyIKBoneIDList;
    private Vector3[] _fullBodyIKPositionList;
    private Quaternion[] _fullBodyIKRotationList;
    private Vector3 _lossyScale = Vector3.one;
    private int[] _updateIValues;
    private float[] _updateFValues;
    private int[] _updateBoneFlagsList;
    private Matrix4x4[] _updateBoneTransformList;
    private Vector3[] _updateBonePositionList;
    private Quaternion[] _updateBoneRotationList;
    private Vector3[] _updateUserPositionList;
    private Quaternion[] _updateUserRotationList;
    private uint[] _updateUserPositionIsValidList;
    private uint[] _updateUserRotationIsValidList;
    private int[] _updateFullBodyIKFlagsList;
    private float[] _updateFullBodyIKTransformList;
    private float[] _updateFullBodyIKWeightList;
    private int[] _lateUpdateBoneFlagsList;
    private Vector3[] _lateUpdateBonePositionList;
    private Quaternion[] _lateUpdateBoneRotationList;
    private int[] _lateUpdateMeshFlagsList;
    private bool _isDirtyPreUpdate = true;
    public const int FullBodyIKTargetMax = 36;

    public int[] fullBodyIKBoneIDList => this._fullBodyIKBoneIDList;

    public Vector3[] fullBodyIKPositionList => this._fullBodyIKPositionList;

    public Quaternion[] fullBodyIKRotationList => this._fullBodyIKRotationList;

    public int[] updateFullBodyIKFlagsList => this._updateFullBodyIKFlagsList;

    public float[] updateFullBodyIKTransformList => this._updateFullBodyIKTransformList;

    public float[] updateFullBodyIKWeightList => this._updateFullBodyIKWeightList;

    ~MMDModel() => this.Destroy();

    public bool isExpired => this.mmdModelPtr == IntPtr.Zero && this.localWorld == null;

    private bool _Prepare(MMD4MecanimModelImpl model)
    {
      if ((UnityEngine.Object) model == (UnityEngine.Object) null)
        return false;
      this.model = model;
      this._modelTransform = model.transform;
      MMD4MecanimData.ModelData modelData = model.modelData;
      if (modelData == null || modelData.boneDataList == null || model.boneList == null || model.boneList.Length != modelData.boneDataList.Length)
      {
        Debug.LogError((object) "_Prepare: Failed.");
        return false;
      }
      if (modelData.boneDataList != null)
      {
        int length = modelData.boneDataList.Length;
        this._boneTransformList = new Transform[length];
        for (int index = 0; index < length; ++index)
        {
          if ((UnityEngine.Object) model.boneList[index] != (UnityEngine.Object) null && (UnityEngine.Object) model.boneList[index].gameObject != (UnityEngine.Object) null)
            this._boneTransformList[index] = model.boneList[index].gameObject.transform;
        }
      }
      else
        this._boneTransformList = new Transform[0];
      if (modelData.ikDataList != null)
      {
        int length = modelData.ikDataList.Length;
        this._ikWeightList = new float[length];
        this._ikDisabledList = new bool[length];
        this._updateIKWeightList = new float[length];
        for (int index = 0; index < length; ++index)
        {
          this._ikWeightList[index] = 1f;
          this._updateIKWeightList[index] = 1f;
        }
      }
      else
      {
        this._ikWeightList = new float[0];
        this._ikDisabledList = new bool[0];
        this._updateIKWeightList = new float[0];
      }
      this._updateMorphWeightList = modelData.morphDataList == null ? new float[0] : new float[modelData.morphDataList.Length];
      if (modelData.rigidBodyDataList != null)
      {
        int length = modelData.rigidBodyDataList.Length;
        this._updateRigidBodyFlagsList = new int[length];
        for (int index = 0; index < length; ++index)
        {
          if (modelData.rigidBodyDataList[index].isFreezed)
            this._updateRigidBodyFlagsList[index] |= 1;
        }
      }
      else
        this._updateRigidBodyFlagsList = new int[0];
      this._physicsEnabled = (uint) model.physicsEngine > 0U;
      this._isDirtyPreUpdate = true;
      this._PrepareWork(model);
      return true;
    }

    public bool Create(MMD4MecanimModelImpl model)
    {
      if ((UnityEngine.Object) model == (UnityEngine.Object) null)
        return false;
      byte[] modelFileBytes = model.modelFileBytes;
      if (modelFileBytes == null)
      {
        Debug.LogError((object) "");
        return false;
      }
      if (!this._Prepare(model))
      {
        Debug.LogError((object) "");
        return false;
      }
      this._lossyScale = !((UnityEngine.Object) this._modelTransform != (UnityEngine.Object) null) ? Vector3.one : this._modelTransform.transform.lossyScale;
      bool flag1 = true;
      bool flag2 = true;
      bool flag3 = false;
      float num1 = 0.0f;
      float num2 = 0.0f;
      MMDModelProperty mmdModelProperty = (MMDModelProperty) null;
      WorldProperty worldProperty = (WorldProperty) null;
      if (model.bulletPhysics != null)
      {
        mmdModelProperty = model.bulletPhysics.mmdModelProperty;
        worldProperty = model.bulletPhysics.worldProperty;
        flag1 = model.bulletPhysics.joinLocalWorld;
        flag2 = model.bulletPhysics.useOriginalScale;
        flag3 = model.bulletPhysics.useCustomResetTime;
        num1 = model.bulletPhysics.resetMorphTime;
        num2 = model.bulletPhysics.resetWaitTime;
      }
      float importScale = model.importScale;
      MMD4MecanimBulletPhysicsImpl.World world1 = (MMD4MecanimBulletPhysicsImpl.World) null;
      MMD4MecanimBulletPhysicsImpl.World world2 = (MMD4MecanimBulletPhysicsImpl.World) null;
      if ((double) importScale == 0.0)
        importScale = model.modelData.importScale;
      float num3;
      bool optimizeSettings;
      if (flag1)
      {
        if (worldProperty == null)
        {
          Debug.LogError((object) "localWorldProperty is null.");
          return false;
        }
        world2 = new MMD4MecanimBulletPhysicsImpl.World();
        world1 = world2;
        if (!world2.Create(worldProperty))
        {
          Debug.LogError((object) "");
          return false;
        }
        num3 = !flag2 ? worldProperty.worldScale : model.modelData.vertexScale * importScale;
        optimizeSettings = worldProperty.optimizeSettings;
      }
      else
      {
        if ((UnityEngine.Object) MMD4MecanimBulletPhysicsImpl.instanceImpl != (UnityEngine.Object) null)
          world1 = MMD4MecanimBulletPhysicsImpl.instanceImpl.globalWorld;
        if (world1 == null)
        {
          Debug.LogError((object) "");
          return false;
        }
        if (world1.worldProperty == null)
        {
          Debug.LogError((object) "worldProperty is null.");
          return false;
        }
        num3 = world1.worldProperty.worldScale;
        optimizeSettings = world1.worldProperty.optimizeSettings;
      }
      bool flag4 = model.xdefEnabled;
      bool vertexMorphEnabled = model.vertexMorphEnabled;
      bool flag5 = model.blendShapesEnabled;
      if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        flag4 = flag4 && model.xdefMobileEnabled;
      byte[] values1 = (byte[]) null;
      byte[] values2 = (byte[]) null;
      int[] numArray = (int[]) null;
      if (model.skinningEnabled)
      {
        if (vertexMorphEnabled | flag4)
        {
          bool blendShapesAnything = false;
          numArray = model._PrepareMeshFlags(out blendShapesAnything);
          flag5 = vertexMorphEnabled & flag5 & blendShapesAnything;
          if (((!vertexMorphEnabled ? 0 : (!flag5 ? 1 : 0)) | (flag4 ? 1 : 0)) != 0)
          {
            values1 = model.indexFileBytes;
            if (values1 != null)
              values2 = model.vertexFileBytes;
          }
        }
        else
          numArray = model._PrepareMeshFlags();
      }
      MMD4MecanimCommon.PropertyWriter propertyWriter = new MMD4MecanimCommon.PropertyWriter();
      propertyWriter.Write("worldScale", num3);
      propertyWriter.Write("importScale", importScale);
      propertyWriter.Write("lossyScale", this._lossyScale);
      propertyWriter.Write("optimizeSettings", optimizeSettings);
      if (flag3)
      {
        propertyWriter.Write("resetMorphTime", num1);
        propertyWriter.Write("resetWaitTime", num2);
      }
      if (mmdModelProperty != null)
      {
        propertyWriter.Write("isPhysicsEnabled", this._physicsEnabled);
        propertyWriter.Write("isJoinedLocalWorld", flag1);
        propertyWriter.Write("isVertexMorphEnabled", vertexMorphEnabled);
        propertyWriter.Write("isBlendShapesEnabled", flag5);
        propertyWriter.Write("isXDEFEnabled", flag4);
        propertyWriter.Write("isXDEFNormalEnabled", model.xdefNormalEnabled);
        propertyWriter.Write("rigidBodyIsAdditionalDamping", mmdModelProperty.rigidBodyIsAdditionalDamping);
        propertyWriter.Write("rigidBodyIsEnableSleeping", mmdModelProperty.rigidBodyIsEnableSleeping);
        propertyWriter.Write("rigidBodyIsUseCcd", mmdModelProperty.rigidBodyIsUseCcd);
        propertyWriter.Write("rigidBodyCcdMotionThreshold", mmdModelProperty.rigidBodyCcdMotionThreshold);
        propertyWriter.Write("rigidBodyShapeScale", mmdModelProperty.rigidBodyShapeScale);
        propertyWriter.Write("rigidBodyMassRate", mmdModelProperty.rigidBodyMassRate);
        propertyWriter.Write("rigidBodyLinearDampingRate", mmdModelProperty.rigidBodyLinearDampingRate);
        propertyWriter.Write("rigidBodyAngularDampingRate", mmdModelProperty.rigidBodyAngularDampingRate);
        propertyWriter.Write("rigidBodyRestitutionRate", mmdModelProperty.rigidBodyRestitutionRate);
        propertyWriter.Write("rigidBodyFrictionRate", mmdModelProperty.rigidBodyFrictionRate);
        propertyWriter.Write("rigidBodyAntiJitterRate", mmdModelProperty.rigidBodyAntiJitterRate);
        propertyWriter.Write("rigidBodyAntiJitterRateOnKinematic", mmdModelProperty.rigidBodyAntiJitterRateOnKinematic);
        propertyWriter.Write("rigidBodyPreBoneAlignmentLimitLength", mmdModelProperty.rigidBodyPreBoneAlignmentLimitLength);
        propertyWriter.Write("rigidBodyPreBoneAlignmentLossRate", mmdModelProperty.rigidBodyPreBoneAlignmentLossRate);
        propertyWriter.Write("rigidBodyPostBoneAlignmentLimitLength", mmdModelProperty.rigidBodyPostBoneAlignmentLimitLength);
        propertyWriter.Write("rigidBodyPostBoneAlignmentLossRate", mmdModelProperty.rigidBodyPostBoneAlignmentLossRate);
        propertyWriter.Write("rigidBodyLinearDampingLossRate", mmdModelProperty.rigidBodyLinearDampingLossRate);
        propertyWriter.Write("rigidBodyAngularDampingLossRate", mmdModelProperty.rigidBodyAngularDampingLossRate);
        propertyWriter.Write("rigidBodyLinearVelocityLimit", mmdModelProperty.rigidBodyLinearVelocityLimit);
        propertyWriter.Write("rigidBodyAngularVelocityLimit", mmdModelProperty.rigidBodyAngularVelocityLimit);
        propertyWriter.Write("rigidBodyIsUseForceAngularVelocityLimit", mmdModelProperty.rigidBodyIsUseForceAngularVelocityLimit);
        propertyWriter.Write("rigidBodyIsUseForceAngularAccelerationLimit", mmdModelProperty.rigidBodyIsUseForceAngularAccelerationLimit);
        propertyWriter.Write("rigidBodyForceAngularVelocityLimit", mmdModelProperty.rigidBodyForceAngularVelocityLimit);
        propertyWriter.Write("rigidBodyIsAdditionalCollider", mmdModelProperty.rigidBodyIsAdditionalCollider);
        propertyWriter.Write("rigidBodyAdditionalColliderBias", mmdModelProperty.rigidBodyAdditionalColliderBias);
        propertyWriter.Write("rigidBodyIsForceTranslate", mmdModelProperty.rigidBodyIsForceTranslate);
        propertyWriter.Write("jointRootAdditionalLimitAngle", mmdModelProperty.jointRootAdditionalLimitAngle);
        propertyWriter.Write("jointAdditionalLimitAngle", mmdModelProperty.jointAdditionalLimitAngle);
        propertyWriter.Write("jointAdditionalLimitForcibly", mmdModelProperty.jointAdditionalLimitForcibly);
      }
      MMD4MecanimCommon.GCHValues<byte> mmdModelBytes = MMD4MecanimCommon.MakeGCHValues<byte>(modelFileBytes);
      MMD4MecanimCommon.GCHValues<byte> mmdIndexBytes = MMD4MecanimCommon.MakeGCHValues<byte>(values1);
      MMD4MecanimCommon.GCHValues<byte> mmdVertexBytes = MMD4MecanimCommon.MakeGCHValues<byte>(values2);
      MMD4MecanimCommon.GCHValues<int> iMeshValues = MMD4MecanimCommon.MakeGCHValues<int>(numArray);
      propertyWriter.Lock();
      IntPtr mmdModel = MMD4MecanimBulletPhysicsImpl._CreateMMDModel((IntPtr) mmdModelBytes, mmdModelBytes.length, (IntPtr) mmdIndexBytes, mmdIndexBytes.length, (IntPtr) mmdVertexBytes, mmdVertexBytes.length, (IntPtr) iMeshValues, iMeshValues.length, propertyWriter.iValuesPtr, propertyWriter.iValueLength, propertyWriter.fValuesPtr, propertyWriter.fValueLength);
      propertyWriter.Unlock();
      iMeshValues.Free();
      mmdVertexBytes.Free();
      mmdIndexBytes.Free();
      mmdModelBytes.Free();
      if (mmdModel != IntPtr.Zero)
      {
        this.mmdModelPtr = mmdModel;
        if (this.model.rigidBodyList != null)
        {
          for (int rigidBodyID = 0; rigidBodyID != this.model.rigidBodyList.Length; ++rigidBodyID)
          {
            if (this.model.rigidBodyList[rigidBodyID] != null)
              this.SetRigidBodyConfigure(rigidBodyID, this.model.rigidBodyList[rigidBodyID]);
          }
        }
        MMD4MecanimBulletPhysicsImpl._JoinWorldMMDModel(world1.worldPtr, mmdModel);
        MMD4MecanimBulletPhysicsImpl.DebugLog();
        this.localWorld = world2;
        this._UploadMesh(model, numArray);
        return true;
      }
      world2?.Destroy();
      MMD4MecanimBulletPhysicsImpl.DebugLog();
      Debug.LogError((object) "");
      return false;
    }

    private void _UploadMesh(MMD4MecanimModelImpl model, int[] meshFlags)
    {
      if ((UnityEngine.Object) model == (UnityEngine.Object) null || meshFlags == null || meshFlags.Length == 0)
        return;
      model._InitializeCloneMesh(meshFlags);
      if (this.mmdModelPtr != IntPtr.Zero)
      {
        for (int index = 0; index != meshFlags.Length; ++index)
        {
          if ((meshFlags[index] & 6) != 0)
          {
            MMD4MecanimModelImpl.CloneMesh cloneMesh = model._GetCloneMesh(index);
            if (cloneMesh != null)
            {
              Vector3[] vertices1 = cloneMesh.vertices;
              Vector3[] values1 = (Vector3[]) null;
              BoneWeight[] values2 = (BoneWeight[]) null;
              Matrix4x4[] values3 = (Matrix4x4[]) null;
              if ((meshFlags[index] & 4) != 0)
              {
                if (model.xdefNormalEnabled)
                  values1 = cloneMesh.normals;
                values2 = cloneMesh.boneWeights;
                values3 = cloneMesh.bindposes;
              }
              MMD4MecanimCommon.GCHValues<Vector3> vertices2 = MMD4MecanimCommon.MakeGCHValues<Vector3>(vertices1);
              MMD4MecanimCommon.GCHValues<Vector3> normals = MMD4MecanimCommon.MakeGCHValues<Vector3>(values1);
              MMD4MecanimCommon.GCHValues<BoneWeight> boneWeights = MMD4MecanimCommon.MakeGCHValues<BoneWeight>(values2);
              MMD4MecanimCommon.GCHValues<Matrix4x4> bindposes = MMD4MecanimCommon.MakeGCHValues<Matrix4x4>(values3);
              MMD4MecanimBulletPhysicsImpl._UploadMeshMMDModel(this.mmdModelPtr, index, (IntPtr) vertices2, (IntPtr) normals, (IntPtr) boneWeights, vertices2.length, (IntPtr) bindposes, bindposes.length);
              boneWeights.Free();
              normals.Free();
              vertices2.Free();
            }
          }
        }
        MMD4MecanimBulletPhysicsImpl.DebugLog();
      }
      model._CleanupCloneMesh();
    }

    private void _PrepareWork(MMD4MecanimModelImpl model)
    {
      int length = 0;
      if (this._boneTransformList != null)
        length = this._boneTransformList.Length;
      this._fullBodyIKBoneIDList = new int[36];
      this._fullBodyIKPositionList = new Vector3[36];
      this._fullBodyIKRotationList = new Quaternion[36];
      for (int index = 0; index != 36; ++index)
      {
        this._fullBodyIKBoneIDList[index] = -1;
        this._fullBodyIKPositionList[index] = Vector3.zero;
        this._fullBodyIKRotationList[index] = Quaternion.identity;
      }
      this._updateIValues = new int[1];
      this._updateFValues = new float[2];
      this._updateBoneFlagsList = new int[length];
      this._updateBoneTransformList = new Matrix4x4[length];
      this._updateBonePositionList = new Vector3[length];
      this._updateBoneRotationList = new Quaternion[length];
      this._updateUserPositionList = new Vector3[length];
      this._updateUserRotationList = new Quaternion[length];
      this._updateUserPositionIsValidList = new uint[(length + 31) / 32];
      this._updateUserRotationIsValidList = new uint[(length + 31) / 32];
      for (int index = 0; index != length; ++index)
      {
        this._updateUserPositionList[index] = Vector3.zero;
        this._updateUserRotationList[index] = Quaternion.identity;
      }
      this._updateFullBodyIKFlagsList = new int[36];
      this._updateFullBodyIKTransformList = new float[144];
      this._updateFullBodyIKWeightList = new float[36];
      for (int index = 0; index != 36; ++index)
        this._updateFullBodyIKWeightList[index] = 1f;
    }

    public void Destroy()
    {
      if (this.mmdModelPtr != IntPtr.Zero)
      {
        IntPtr mmdModelPtr = this.mmdModelPtr;
        this.mmdModelPtr = IntPtr.Zero;
        MMD4MecanimBulletPhysicsImpl._DestroyMMDModel(mmdModelPtr);
      }
      if (this.localWorld != null)
      {
        this.localWorld.Destroy();
        this.localWorld = (MMD4MecanimBulletPhysicsImpl.World) null;
      }
      this._modelTransform = (Transform) null;
      this.model = (MMD4MecanimModelImpl) null;
    }

    public void SetRigidBodyConfigure(int rigidBodyID, MMD4MecanimModelImpl.RigidBody rigidBody)
    {
      if (rigidBody == null || (double) rigidBody.mass < 0.0 && (double) rigidBody.linearDamping < 0.0 && (double) rigidBody.angularDamping < 0.0 && (double) rigidBody.restitution < 0.0 && (double) rigidBody.friction < 0.0 || !(this.mmdModelPtr != IntPtr.Zero))
        return;
      MMD4MecanimCommon.PropertyWriter propertyWriter = new MMD4MecanimCommon.PropertyWriter();
      propertyWriter.Write("mass", rigidBody.mass);
      propertyWriter.Write("linearDamping", rigidBody.linearDamping);
      propertyWriter.Write("angularDamping", rigidBody.angularDamping);
      propertyWriter.Write("restitution", rigidBody.restitution);
      propertyWriter.Write("friction", rigidBody.friction);
      propertyWriter.Lock();
      MMD4MecanimBulletPhysicsImpl._ConfigRigidBodyMMDModel(this.mmdModelPtr, rigidBodyID, propertyWriter.iValuesPtr, propertyWriter.iValueLength, propertyWriter.fValuesPtr, propertyWriter.fValueLength);
      propertyWriter.Unlock();
    }

    public void SetRigidBodyFreezed(int rigidBodyID, bool isFreezed)
    {
      if (this._updateRigidBodyFlagsList == null || (uint) rigidBodyID >= (uint) this._updateRigidBodyFlagsList.Length)
        return;
      int updateRigidBodyFlags = this._updateRigidBodyFlagsList[rigidBodyID];
      bool flag = (uint) (updateRigidBodyFlags & 1) > 0U;
      if (isFreezed == flag)
        return;
      this._isDirtyPreUpdate = true;
      int num = !isFreezed ? updateRigidBodyFlags & -2 : updateRigidBodyFlags | 1;
      this._updateRigidBodyFlagsList[rigidBodyID] = num;
    }

    public void SetIKProperty(int ikID, bool isEnabled, float ikWeight)
    {
      if (this._ikWeightList == null || this._ikDisabledList == null || this._updateIKWeightList == null || (uint) ikID >= (uint) this._ikWeightList.Length || this._ikDisabledList[ikID] == !isEnabled && (double) this._ikWeightList[ikID] == (double) ikWeight)
        return;
      this._isDirtyPreUpdate = true;
      this._ikDisabledList[ikID] = !isEnabled;
      this._ikWeightList[ikID] = ikWeight;
      if (isEnabled)
        this._updateIKWeightList[ikID] = ikWeight;
      else
        this._updateIKWeightList[ikID] = 0.0f;
    }

    private static void _Swap<Type>(ref Type[] lhs, ref Type[] rhs)
    {
      Type[] ypeArray = lhs;
      lhs = rhs;
      rhs = ypeArray;
    }

    public void Update(float deltaTime)
    {
      if (this._boneTransformList == null || this._updateIValues == null || this._updateFValues == null || this._updateBoneFlagsList == null || this._updateBoneTransformList == null || this._updateBonePositionList == null || this._updateBoneRotationList == null || this._updateUserPositionList == null || this._updateUserRotationList == null || this._updateRigidBodyFlagsList == null || this._updateIKWeightList == null || this._updateMorphWeightList == null || (UnityEngine.Object) this._modelTransform == (UnityEngine.Object) null)
        return;
      if ((UnityEngine.Object) this.model != (UnityEngine.Object) null)
      {
        if (this.localWorld != null)
          this.localWorld.SetGravity(this.model.bulletPhysics.worldProperty.gravityScale, this.model.bulletPhysics.worldProperty.gravityNoise, this.model.bulletPhysics.worldProperty.gravityDirection);
        if (this.model.ikList != null)
        {
          for (int ikID = 0; ikID != this.model.ikList.Length; ++ikID)
          {
            if (this.model.ikList[ikID] != null)
              this.SetIKProperty(ikID, this.model.ikList[ikID].ikEnabled, this.model.ikList[ikID].ikWeight);
          }
        }
        if (this.model.morphList != null && this.model.morphList.Length == this._updateMorphWeightList.Length)
        {
          for (int index = 0; index != this.model.morphList.Length; ++index)
            this._updateMorphWeightList[index] = this.model.morphList[index]._updatedWeight;
        }
        if (this.model.rigidBodyList != null)
        {
          for (int rigidBodyID = 0; rigidBodyID != this.model.rigidBodyList.Length; ++rigidBodyID)
          {
            if (this.model.rigidBodyList[rigidBodyID] != null)
              this.SetRigidBodyFreezed(rigidBodyID, this.model.rigidBodyList[rigidBodyID].freezed);
          }
        }
      }
      int length = this._boneTransformList.Length;
      int updateFlags = 0 | (this.model.ikEnabled ? 1 : 0) | (this.model.boneInherenceEnabled ? 2 : 0) | (this.model.boneMorphEnabled ? 4 : 0) | (this.model.fullBodyIKEnabled ? 16 : 0);
      if ((UnityEngine.Object) this.model != (UnityEngine.Object) null)
      {
        float fixRateImmediately = this.model.pphShoulderFixRateImmediately;
        if ((double) fixRateImmediately > 0.0)
        {
          updateFlags |= 8;
          if ((double) this._updateFValues[1] != (double) fixRateImmediately)
          {
            this._updateFValues[1] = fixRateImmediately;
            this._isDirtyPreUpdate = true;
          }
        }
        MMD4MecanimBoneImpl[] boneList = this.model.boneList;
        if (boneList != null)
        {
          for (int index = 0; index != length; ++index)
          {
            MMD4MecanimBoneImpl d4MecanimBoneImpl = boneList[index];
            if ((UnityEngine.Object) d4MecanimBoneImpl != (UnityEngine.Object) null)
            {
              bool flag1 = (this._updateUserPositionIsValidList[index >> 5] >> index & 1U) > 0U;
              bool flag2 = (this._updateUserRotationIsValidList[index >> 5] >> index & 1U) > 0U;
              if (!d4MecanimBoneImpl._userPositionIsZero != flag1)
              {
                this._isDirtyPreUpdate = true;
                if (d4MecanimBoneImpl._userPositionIsZero)
                {
                  this._updateUserPositionIsValidList[index >> 5] &= (uint) ~(1 << index);
                  this._updateUserPositionList[index] = Vector3.zero;
                }
                else
                {
                  this._updateUserPositionIsValidList[index >> 5] |= (uint) (1 << index);
                  this._updateUserPositionList[index] = d4MecanimBoneImpl._userPosition;
                }
              }
              else if (flag1)
                this._updateUserPositionList[index] = d4MecanimBoneImpl._userPosition;
              if (!d4MecanimBoneImpl._userRotationIsIdentity != flag2)
              {
                this._isDirtyPreUpdate = true;
                if (d4MecanimBoneImpl._userRotationIsIdentity)
                {
                  this._updateUserRotationIsValidList[index >> 5] &= (uint) ~(1 << index);
                  this._updateUserRotationList[index] = Quaternion.identity;
                }
                else
                {
                  this._updateUserRotationIsValidList[index >> 5] |= (uint) (1 << index);
                  this._updateUserRotationList[index] = d4MecanimBoneImpl._userRotation;
                }
              }
              else if (flag2)
                this._updateUserRotationList[index] = d4MecanimBoneImpl._userRotation;
            }
          }
        }
      }
      if (this._isDirtyPreUpdate)
      {
        for (int index = 0; index != length; ++index)
          this._updateBoneFlagsList[index] = 0;
        if ((UnityEngine.Object) this.model != (UnityEngine.Object) null)
        {
          MMD4MecanimBoneImpl[] boneList = this.model.boneList;
          if (boneList != null && boneList.Length == length)
          {
            for (int index = 0; index != length; ++index)
            {
              if ((UnityEngine.Object) boneList[index] != (UnityEngine.Object) null)
              {
                switch (boneList[index].humanBodyBones)
                {
                  case 11:
                    this._updateBoneFlagsList[index] |= 16777216;
                    break;
                  case 12:
                    this._updateBoneFlagsList[index] |= 50331648;
                    break;
                  case 13:
                    this._updateBoneFlagsList[index] |= 33554432;
                    break;
                  case 14:
                    this._updateBoneFlagsList[index] |= 67108864;
                    break;
                }
              }
              int num = (this._updateUserPositionIsValidList[index >> 5] >> index & 1U) > 0U ? 1 : 0;
              bool flag = (this._updateUserRotationIsValidList[index >> 5] >> index & 1U) > 0U;
              if (num != 0)
                this._updateBoneFlagsList[index] |= 128;
              if (flag)
                this._updateBoneFlagsList[index] |= 256;
            }
          }
        }
        if (this.mmdModelPtr != IntPtr.Zero)
        {
          MMD4MecanimCommon.GCHValues<int> iBoneValues = MMD4MecanimCommon.MakeGCHValues<int>(this._updateBoneFlagsList);
          MMD4MecanimCommon.GCHValues<int> iRigidBodyValues = MMD4MecanimCommon.MakeGCHValues<int>(this._updateRigidBodyFlagsList);
          MMD4MecanimCommon.GCHValues<float> ikWeights = MMD4MecanimCommon.MakeGCHValues<float>(this._updateIKWeightList);
          MMD4MecanimCommon.GCHValues<float> morphWeights = MMD4MecanimCommon.MakeGCHValues<float>(this._updateMorphWeightList);
          MMD4MecanimCommon.GCHValues<int> iFullBodyIKValues = MMD4MecanimCommon.MakeGCHValues<int>(this._updateFullBodyIKFlagsList);
          MMD4MecanimCommon.GCHValues<float> fullBodyIKWeights = MMD4MecanimCommon.MakeGCHValues<float>(this._updateFullBodyIKWeightList);
          MMD4MecanimBulletPhysicsImpl._PreUpdateMMDModel(this.mmdModelPtr, updateFlags, (IntPtr) iBoneValues, iBoneValues.length, (IntPtr) iRigidBodyValues, iRigidBodyValues.length, (IntPtr) ikWeights, ikWeights.length, (IntPtr) morphWeights, morphWeights.length, (IntPtr) iFullBodyIKValues, (IntPtr) fullBodyIKWeights, fullBodyIKWeights.length);
          fullBodyIKWeights.Free();
          iFullBodyIKValues.Free();
          morphWeights.Free();
          ikWeights.Free();
          iRigidBodyValues.Free();
          iBoneValues.Free();
        }
      }
      for (int index = 0; index != length; ++index)
      {
        Transform boneTransform = this._boneTransformList[index];
        if ((UnityEngine.Object) boneTransform != (UnityEngine.Object) null)
        {
          if ((this._updateBoneFlagsList[index] & 1) != 0)
            this._updateBoneTransformList[index] = boneTransform.localToWorldMatrix;
          if ((this._updateBoneFlagsList[index] & 2) != 0)
            this._updateBonePositionList[index] = boneTransform.localPosition;
          if ((this._updateBoneFlagsList[index] & 4) != 0)
            this._updateBoneRotationList[index] = boneTransform.localRotation;
        }
      }
      Matrix4x4 localToWorldMatrix = this._modelTransform.localToWorldMatrix;
      if (this.mmdModelPtr != IntPtr.Zero)
      {
        MMD4MecanimCommon.GCHValues<int> iValues = MMD4MecanimCommon.MakeGCHValues<int>(this._updateIValues);
        MMD4MecanimCommon.GCHValues<float> fValues = MMD4MecanimCommon.MakeGCHValues<float>(this._updateFValues);
        MMD4MecanimCommon.GCHValue<Matrix4x4> modelTransform = MMD4MecanimCommon.MakeGCHValue<Matrix4x4>(ref localToWorldMatrix);
        MMD4MecanimCommon.GCHValues<int> iBoneValues = MMD4MecanimCommon.MakeGCHValues<int>(this._updateBoneFlagsList);
        MMD4MecanimCommon.GCHValues<Matrix4x4> boneTransforms = MMD4MecanimCommon.MakeGCHValues<Matrix4x4>(this._updateBoneTransformList);
        MMD4MecanimCommon.GCHValues<Vector3> boneLocalPositions = MMD4MecanimCommon.MakeGCHValues<Vector3>(this._updateBonePositionList);
        MMD4MecanimCommon.GCHValues<Quaternion> boneLocalRotations = MMD4MecanimCommon.MakeGCHValues<Quaternion>(this._updateBoneRotationList);
        MMD4MecanimCommon.GCHValues<Vector3> boneUserPositions = MMD4MecanimCommon.MakeGCHValues<Vector3>(this._updateUserPositionList);
        MMD4MecanimCommon.GCHValues<Quaternion> boneUserRotations = MMD4MecanimCommon.MakeGCHValues<Quaternion>(this._updateUserRotationList);
        MMD4MecanimCommon.GCHValues<int> iRigidBodyValues = MMD4MecanimCommon.MakeGCHValues<int>(this._updateRigidBodyFlagsList);
        MMD4MecanimCommon.GCHValues<float> ikWeights = MMD4MecanimCommon.MakeGCHValues<float>(this._updateIKWeightList);
        MMD4MecanimCommon.GCHValues<float> morphWeights = MMD4MecanimCommon.MakeGCHValues<float>(this._updateMorphWeightList);
        MMD4MecanimCommon.GCHValues<int> iFullBodyIKValues = MMD4MecanimCommon.MakeGCHValues<int>(this._updateFullBodyIKFlagsList);
        MMD4MecanimCommon.GCHValues<float> fullBodyIKTransforms = MMD4MecanimCommon.MakeGCHValues<float>(this._updateFullBodyIKTransformList);
        MMD4MecanimCommon.GCHValues<float> fullBodyIKWeights = MMD4MecanimCommon.MakeGCHValues<float>(this._updateFullBodyIKWeightList);
        MMD4MecanimBulletPhysicsImpl._UpdateMMDModel(this.mmdModelPtr, updateFlags, (IntPtr) iValues, iValues.length, (IntPtr) fValues, fValues.length, (IntPtr) modelTransform, (IntPtr) iBoneValues, (IntPtr) boneTransforms, (IntPtr) boneLocalPositions, (IntPtr) boneLocalRotations, (IntPtr) boneUserPositions, (IntPtr) boneUserRotations, iBoneValues.length, (IntPtr) iRigidBodyValues, iRigidBodyValues.length, (IntPtr) ikWeights, ikWeights.length, (IntPtr) morphWeights, morphWeights.length, (IntPtr) iFullBodyIKValues, (IntPtr) fullBodyIKTransforms, (IntPtr) fullBodyIKWeights, fullBodyIKWeights.length);
        fullBodyIKWeights.Free();
        fullBodyIKTransforms.Free();
        iFullBodyIKValues.Free();
        morphWeights.Free();
        ikWeights.Free();
        iRigidBodyValues.Free();
        boneUserRotations.Free();
        boneUserPositions.Free();
        boneLocalRotations.Free();
        boneLocalPositions.Free();
        boneTransforms.Free();
        iBoneValues.Free();
        modelTransform.Free();
        fValues.Free();
        iValues.Free();
      }
      if (this.localWorld == null)
        return;
      this.localWorld.Update(deltaTime);
    }

    public void LateUpdate()
    {
      if (this._boneTransformList == null || this._updateBoneTransformList == null || this._updateBoneFlagsList == null || this._updateBonePositionList == null || this._updateBoneRotationList == null)
        return;
      if (this.localWorld != null)
        this.localWorld.Synchronize();
      int num1 = 0;
      int[] numArray1 = (int[]) null;
      Vector3[] vector3Array1 = (Vector3[]) null;
      Quaternion[] quaternionArray = (Quaternion[]) null;
      int[] numArray2 = (int[]) null;
      if (this.mmdModelPtr != IntPtr.Zero)
      {
        if (this._lateUpdateBoneFlagsList == null || this._lateUpdateBonePositionList == null || this._lateUpdateBoneRotationList == null)
        {
          int length = this._boneTransformList != null ? this._boneTransformList.Length : 0;
          this._lateUpdateBoneFlagsList = new int[length];
          this._lateUpdateBonePositionList = new Vector3[length];
          this._lateUpdateBoneRotationList = new Quaternion[length];
        }
        if (this._lateUpdateMeshFlagsList == null)
          this._lateUpdateMeshFlagsList = new int[(UnityEngine.Object) this.model != (UnityEngine.Object) null ? this.model._skinnedMeshCount : 0];
        MMD4MecanimCommon.GCHValues<int> iBoneValues = MMD4MecanimCommon.MakeGCHValues<int>(this._lateUpdateBoneFlagsList);
        MMD4MecanimCommon.GCHValues<Vector3> bonePositions = MMD4MecanimCommon.MakeGCHValues<Vector3>(this._lateUpdateBonePositionList);
        MMD4MecanimCommon.GCHValues<Quaternion> boneRotations = MMD4MecanimCommon.MakeGCHValues<Quaternion>(this._lateUpdateBoneRotationList);
        MMD4MecanimCommon.GCHValues<int> iMeshValues = MMD4MecanimCommon.MakeGCHValues<int>(this._lateUpdateMeshFlagsList);
        num1 = MMD4MecanimBulletPhysicsImpl._LateUpdateMMDModel(this.mmdModelPtr, IntPtr.Zero, 0, IntPtr.Zero, 0, (IntPtr) iBoneValues, (IntPtr) bonePositions, (IntPtr) boneRotations, this._lateUpdateBoneFlagsList.Length, (IntPtr) iMeshValues, iMeshValues.length, IntPtr.Zero, 0);
        iMeshValues.Free();
        boneRotations.Free();
        bonePositions.Free();
        iBoneValues.Free();
        numArray1 = this._lateUpdateBoneFlagsList;
        vector3Array1 = this._lateUpdateBonePositionList;
        quaternionArray = this._lateUpdateBoneRotationList;
        numArray2 = this._lateUpdateMeshFlagsList;
      }
      if ((num1 & 1) != 0 && numArray1 != null && vector3Array1 != null && quaternionArray != null)
      {
        int length = this._boneTransformList.Length;
        for (int index = 0; index != length; ++index)
        {
          Transform boneTransform = this._boneTransformList[index];
          if ((UnityEngine.Object) boneTransform != (UnityEngine.Object) null && (numArray1[index] & 1) != 0)
          {
            if ((numArray1[index] & 2) != 0)
              boneTransform.localPosition = vector3Array1[index];
            if ((numArray1[index] & 4) != 0)
              boneTransform.localRotation = quaternionArray[index];
          }
        }
      }
      if ((num1 & 2) == 0 || numArray2 == null || !(this.mmdModelPtr != IntPtr.Zero))
        return;
      int cloneMeshCount = this.model._cloneMeshCount;
      for (int index = 0; index != cloneMeshCount; ++index)
      {
        int num2 = numArray2[index];
        if ((num2 & 3) != 0)
        {
          MMD4MecanimModelImpl.CloneMesh cloneMesh = this.model._GetCloneMesh(index);
          if (cloneMesh != null)
          {
            Vector3[] vector3Array2 = (Vector3[]) null;
            Vector3[] vector3Array3 = (Vector3[]) null;
            if ((num2 & 1) != 0)
              vector3Array2 = cloneMesh.vertices;
            if ((num2 & 2) != 0)
              vector3Array3 = cloneMesh.normals;
            MMD4MecanimCommon.GCHValues<Vector3> vertices = MMD4MecanimCommon.MakeGCHValues<Vector3>(vector3Array2);
            MMD4MecanimCommon.GCHValues<Vector3> normals = MMD4MecanimCommon.MakeGCHValues<Vector3>(vector3Array3);
            int num3 = MMD4MecanimBulletPhysicsImpl._LateUpdateMeshMMDModel(this.mmdModelPtr, index, (IntPtr) vertices, (IntPtr) normals, vertices.length);
            normals.Free();
            vertices.Free();
            if (num3 != 0)
              this.model._UploadMeshVertex(index, vector3Array2, vector3Array3);
          }
        }
      }
    }

    public enum UpdateIValues
    {
      AnimationHashName,
      Max,
    }

    public enum UpdateFValues
    {
      AnimationTime,
      PPHShoulderFixRate,
      Max,
    }

    [Flags]
    public enum LateUpdateMeshFlags
    {
      Vertices = 1,
      Normals = 2,
    }

    [Flags]
    public enum UpdateFlags
    {
      IKEnabled = 1,
      BoneInherenceEnabled = 2,
      BoneMorphEnabled = 4,
      PPHShoulderEnabled = 8,
      FullBodyIKEnabled = 16, // 0x00000010
    }

    [Flags]
    public enum UpdateBoneFlags
    {
      WorldTransform = 1,
      Position = 2,
      Rotation = 4,
      UserPosition = 128, // 0x00000080
      UserRotation = 256, // 0x00000100
      SkeletonMask = -16777216, // 0xFF000000
      SkeletonLeftShoulder = 16777216, // 0x01000000
      SkeletonLeftUpperArm = 33554432, // 0x02000000
      SkeletonRightShoulder = SkeletonLeftUpperArm | SkeletonLeftShoulder, // 0x03000000
      SkeletonRightUpperArm = 67108864, // 0x04000000
      UserTransform = UserRotation | UserPosition, // 0x00000180
    }

    [Flags]
    public enum UpdateRigidBodyFlags
    {
      Freezed = 1,
    }

    [Flags]
    public enum LateUpdateFlags
    {
      Bone = 1,
      Mesh = 2,
    }

    [Flags]
    public enum LateUpdateBoneFlags
    {
      LateUpdated = 1,
      Position = 2,
      Rotation = 4,
    }
  }
}
