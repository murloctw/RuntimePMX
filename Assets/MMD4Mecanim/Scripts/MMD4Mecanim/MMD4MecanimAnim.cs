﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimAnim
// Assembly: MMD4Mecanim, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: ADBCBBA1-CEE8-40F4-A775-E57AB2D2693F
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Scripts\MMD4Mecanim.dll

using System;
using UnityEngine;

public static class MMD4MecanimAnim
{
  private static int _isAnimTimeLegacy = -1;

  public static void InitializeAnimModel(MMD4MecanimAnim.IAnimModel animModel)
  {
    if (animModel == null)
      return;
    if (!MMD4MecanimCommon.versionChecked)
      MMD4MecanimCommon.VersionCheck();
    int animCount = animModel.animCount;
    for (int index1 = 0; index1 < animCount; ++index1)
    {
      MMD4MecanimAnim.IAnim animAt = animModel.GetAnimAt(index1);
      if (animAt != null)
      {
        if (animAt.animatorStateName != null)
          animAt.animatorStateNameHash = Animator.StringToHash(animAt.animatorStateName);
        if (animAt.animData == null)
        {
          animAt.animData = MMD4MecanimData.BuildAnimData(animAt.animFile);
          if (animAt.animData == null)
            continue;
        }
        MMD4MecanimData.MorphMotionData[] morphMotionDataList = animAt.animData.morphMotionDataList;
        if (morphMotionDataList != null)
        {
          animAt.morphMotionList = new MMD4MecanimAnim.MorphMotion[morphMotionDataList.Length];
          if (animAt.animData.supportNameIsFull)
          {
            for (int index2 = 0; index2 != morphMotionDataList.Length; ++index2)
              animAt.morphMotionList[index2].morph = animModel.GetMorph(morphMotionDataList[index2].name, morphMotionDataList[index2].nameIsFull);
          }
          else
          {
            for (int index3 = 0; index3 != morphMotionDataList.Length; ++index3)
              animAt.morphMotionList[index3].morph = animModel.GetMorph(morphMotionDataList[index3].name, false);
            for (int index4 = 0; index4 != morphMotionDataList.Length; ++index4)
            {
              if (animAt.morphMotionList[index4].morph == null)
              {
                MMD4MecanimAnim.IMorph morph = animModel.GetMorph(morphMotionDataList[index4].name, true);
                if (morph != null)
                {
                  bool flag = false;
                  for (int index5 = 0; index5 != morphMotionDataList.Length && !flag; ++index5)
                    flag = animAt.morphMotionList[index5].morph == morph;
                  if (!flag)
                    animAt.morphMotionList[index4].morph = morph;
                }
              }
            }
          }
        }
      }
    }
  }

  public static void PreUpdateAnimModel(MMD4MecanimAnim.IAnimModel animModel)
  {
    if (animModel == null || (double) animModel.prevDeltaTime != 0.0)
      return;
    animModel.prevDeltaTime = Time.deltaTime;
  }

  public static void PostUpdateAnimModel(MMD4MecanimAnim.IAnimModel animModel)
  {
    if (animModel == null)
      return;
    animModel.prevDeltaTime = Time.deltaTime;
  }

  private static void _UpdateAnimTimeLegacy()
  {
    if (MMD4MecanimAnim._isAnimTimeLegacy != -1)
      return;
    string unityVersion = Application.unityVersion;
    if (unityVersion.StartsWith("4.") || unityVersion.StartsWith("5.0") || unityVersion.StartsWith("5.1"))
      MMD4MecanimAnim._isAnimTimeLegacy = 1;
    else
      MMD4MecanimAnim._isAnimTimeLegacy = 0;
  }

  public static void UpdateAnimModel(MMD4MecanimAnim.IAnimModel animModel)
  {
    if (animModel == null)
      return;
    Animator animator = animModel.animator;
    if (!MMD4MecanimCommon.animatorInitializedEnabled ? (UnityEngine.Object) animator != (UnityEngine.Object) null && animator.enabled && (UnityEngine.Object) animator.runtimeAnimatorController != (UnityEngine.Object) null && animator.layerCount > 0 : (UnityEngine.Object) animator != (UnityEngine.Object) null && MMD4MecanimCommon._IsAnimatorInitialized(animator) && animator.enabled && (UnityEngine.Object) animator.runtimeAnimatorController != (UnityEngine.Object) null && animator.layerCount > 0)
    {
      AnimatorStateInfo animatorStateInfo = animator.GetCurrentAnimatorStateInfo(0);
      int fullPathHash = animatorStateInfo.fullPathHash;
      float normalizedTime = animatorStateInfo.normalizedTime;
      if ((double) normalizedTime > 1.0 && animatorStateInfo.loop)
        normalizedTime %= 1f;
      float length = animatorStateInfo.length;
      float animationTime = !float.IsInfinity(length) ? normalizedTime * length : 0.0f;
      if (MMD4MecanimAnim._isAnimTimeLegacy == -1)
        MMD4MecanimAnim._UpdateAnimTimeLegacy();
      if (MMD4MecanimAnim._isAnimTimeLegacy == 0)
        animationTime *= animator.speed;
      int animCount = animModel.animCount;
      for (int index = 0; index < animCount; ++index)
      {
        MMD4MecanimAnim.IAnim animAt = animModel.GetAnimAt(index);
        if (animAt != null && animAt.animatorStateNameHash == fullPathHash)
        {
          MMD4MecanimAnim.UpdateAnimModel(animModel, animAt, animationTime);
          return;
        }
      }
      MMD4MecanimAnim.StopAnimModel(animModel);
    }
    else
      MMD4MecanimAnim.StopAnimModel(animModel);
  }

  public static void UpdateAnimModel(
    MMD4MecanimAnim.IAnimModel animModel,
    MMD4MecanimAnim.IAnim anim,
    float animationTime)
  {
    if (animModel == null)
      return;
    if (anim == null)
    {
      MMD4MecanimAnim.StopAnimModel(animModel);
    }
    else
    {
      float f_frameNo = animationTime * 30f;
      int frameNo1 = (int) f_frameNo;
      bool flag1 = animModel.playingAnim != anim;
      if (animModel.playingAnim != anim)
        MMD4MecanimAnim.StopAnimModel(animModel);
      bool isPlayAudioSourceAtFirst = false;
      if (animModel.playingAnim == null && (UnityEngine.Object) anim.audioClip != (UnityEngine.Object) null)
      {
        AudioSource audioSource = animModel.audioSource;
        if ((UnityEngine.Object) audioSource != (UnityEngine.Object) null)
        {
          if ((UnityEngine.Object) audioSource.clip != (UnityEngine.Object) anim.audioClip)
          {
            audioSource.clip = anim.audioClip;
            audioSource.Play();
            isPlayAudioSourceAtFirst = true;
          }
          else if (!audioSource.isPlaying)
          {
            audioSource.Play();
            isPlayAudioSourceAtFirst = true;
          }
        }
      }
      animModel.playingAnim = anim;
      MMD4MecanimAnim._SyncToAudio(animModel, animationTime, isPlayAudioSourceAtFirst);
      MMD4MecanimAnim.MorphMotion[] morphMotionList = anim.morphMotionList;
      MMD4MecanimData.AnimData animData = anim.animData;
      if (morphMotionList == null || animData == null || animData.morphMotionDataList == null)
        return;
      for (int index = 0; index != morphMotionList.Length; ++index)
      {
        MMD4MecanimData.MorphMotionData morphMotionData = animData.morphMotionDataList[index];
        if (morphMotionList[index].morph != null && morphMotionData.frameNos != null && morphMotionData.f_frameNos != null && morphMotionData.weights != null)
        {
          if (flag1 || morphMotionList[index].lastKeyFrameIndex >= morphMotionData.frameNos.Length || morphMotionData.frameNos[morphMotionList[index].lastKeyFrameIndex] > frameNo1)
            morphMotionList[index].lastKeyFrameIndex = 0;
          bool flag2 = false;
          for (int lastKeyFrameIndex = morphMotionList[index].lastKeyFrameIndex; lastKeyFrameIndex != morphMotionData.frameNos.Length; ++lastKeyFrameIndex)
          {
            int frameNo2 = morphMotionData.frameNos[lastKeyFrameIndex];
            if (frameNo1 >= frameNo2)
            {
              morphMotionList[index].lastKeyFrameIndex = lastKeyFrameIndex;
            }
            else
            {
              if (morphMotionList[index].lastKeyFrameIndex + 1 < morphMotionData.frameNos.Length)
              {
                MMD4MecanimAnim._ProcessKeyFrame2(animModel, morphMotionList[index].morph, morphMotionData, morphMotionList[index].lastKeyFrameIndex, morphMotionList[index].lastKeyFrameIndex + 1, frameNo1, f_frameNo);
                flag2 = true;
                break;
              }
              break;
            }
          }
          if (!flag2 && morphMotionList[index].lastKeyFrameIndex < morphMotionData.frameNos.Length)
            MMD4MecanimAnim._ProcessKeyFrame(animModel, morphMotionList[index].morph, morphMotionData, morphMotionList[index].lastKeyFrameIndex);
        }
      }
    }
  }

  private static void _ProcessKeyFrame2(
    MMD4MecanimAnim.IAnimModel animModel,
    MMD4MecanimAnim.IMorph morph,
    MMD4MecanimData.MorphMotionData motionMorphData,
    int keyFrameIndex0,
    int keyFrameIndex1,
    int frameNo,
    float f_frameNo)
  {
    int frameNo1 = motionMorphData.frameNos[keyFrameIndex0];
    int frameNo2 = motionMorphData.frameNos[keyFrameIndex1];
    float fFrameNo1 = motionMorphData.f_frameNos[keyFrameIndex0];
    float fFrameNo2 = motionMorphData.f_frameNos[keyFrameIndex1];
    if (frameNo <= frameNo1 || frameNo2 - frameNo1 == 1)
      animModel._SetAnimMorphWeight(morph, motionMorphData.weights[keyFrameIndex0]);
    else if (frameNo >= frameNo2)
    {
      animModel._SetAnimMorphWeight(morph, motionMorphData.weights[keyFrameIndex1]);
    }
    else
    {
      float num = (float) (((double) f_frameNo - (double) fFrameNo1) / ((double) fFrameNo2 - (double) fFrameNo1));
      float weight1 = motionMorphData.weights[keyFrameIndex0];
      float weight2 = motionMorphData.weights[keyFrameIndex1];
      animModel._SetAnimMorphWeight(morph, weight1 + (weight2 - weight1) * num);
    }
  }

  private static void _ProcessKeyFrame(
    MMD4MecanimAnim.IAnimModel animModel,
    MMD4MecanimAnim.IMorph morph,
    MMD4MecanimData.MorphMotionData motionMorphData,
    int keyFrameIndex)
  {
    animModel._SetAnimMorphWeight(morph, motionMorphData.weights[keyFrameIndex]);
  }

  public static void StopAnimModel(MMD4MecanimAnim.IAnimModel animModel)
  {
    if (animModel == null)
      return;
    MMD4MecanimAnim.IAnim playingAnim = animModel.playingAnim;
    if (playingAnim == null)
      return;
    if ((UnityEngine.Object) playingAnim.audioClip != (UnityEngine.Object) null)
    {
      AudioSource audioSource = animModel.audioSource;
      if ((UnityEngine.Object) audioSource != (UnityEngine.Object) null && (UnityEngine.Object) audioSource.clip == (UnityEngine.Object) playingAnim.audioClip)
      {
        audioSource.Stop();
        audioSource.clip = (AudioClip) null;
        if (animModel.animSyncToAudio)
        {
          Animator animator = animModel.animator;
          if ((UnityEngine.Object) animator != (UnityEngine.Object) null)
            animator.speed = 1f;
        }
      }
    }
    animModel.playingAnim = (MMD4MecanimAnim.IAnim) null;
  }

  private static void _SyncToAudio(
    MMD4MecanimAnim.IAnimModel animModel,
    float animationTime,
    bool isPlayAudioSourceAtFirst)
  {
    if (!animModel.animSyncToAudio)
      return;
    MMD4MecanimAnim.IAnim playingAnim = animModel.playingAnim;
    if (playingAnim == null || (UnityEngine.Object) playingAnim.audioClip == (UnityEngine.Object) null)
      return;
    AudioSource audioSource = animModel.audioSource;
    if ((UnityEngine.Object) audioSource == (UnityEngine.Object) null)
      return;
    Animator animator = animModel.animator;
    if (!((UnityEngine.Object) animator != (UnityEngine.Object) null) || !animator.enabled)
      return;
    float prevDeltaTime = animModel.prevDeltaTime;
    if (audioSource.isPlaying)
    {
      float time = audioSource.time;
      if ((double) time == 0.0)
      {
        animator.speed = 0.0f;
      }
      else
      {
        float num1 = (float) (((double) prevDeltaTime + (double) Time.deltaTime) * 0.5);
        float f = time - animationTime;
        if ((double) Mathf.Abs(f) <= (double) num1)
          animator.speed = 1f;
        else if (!isPlayAudioSourceAtFirst && (double) num1 > (double) Mathf.Epsilon && (double) num1 < 0.100000001490116)
        {
          float num2 = Mathf.Clamp((float) (1.0 + (double) f / (double) num1), 0.5f, 2f);
          if ((double) animator.speed == 0.0)
            animator.speed = num2;
          else
            animator.speed = (float) ((double) animator.speed * 0.949999988079071 + (double) num2 * 0.0500000007450581);
        }
        else
        {
          AnimatorStateInfo animatorStateInfo = animator.GetCurrentAnimatorStateInfo(0);
          if ((double) animatorStateInfo.length <= (double) Mathf.Epsilon)
            return;
          int fullPathHash = animatorStateInfo.fullPathHash;
          animator.Play(fullPathHash, 0, time / animatorStateInfo.length);
          animator.speed = 1f;
        }
      }
    }
    else
      animator.speed = 1f;
  }

  [Serializable]
  public struct MorphMotion
  {
    public MMD4MecanimAnim.IMorph morph;
    public int lastKeyFrameIndex;
  }

  public interface IAnim
  {
    string animatorStateName { get; set; }

    int animatorStateNameHash { get; set; }

    TextAsset animFile { get; set; }

    AudioClip audioClip { get; set; }

    MMD4MecanimData.AnimData animData { get; set; }

    MMD4MecanimAnim.MorphMotion[] morphMotionList { get; set; }
  }

  [Serializable]
  public class Anim : MMD4MecanimAnim.IAnim
  {
    public string animatorStateName;
    [NonSerialized]
    public int animatorStateNameHash;
    public TextAsset animFile;
    public AudioClip audioClip;
    [NonSerialized]
    public MMD4MecanimData.AnimData animData;
    [NonSerialized]
    public MMD4MecanimAnim.MorphMotion[] morphMotionList;

    string MMD4MecanimAnim.IAnim.animatorStateName
    {
      get => this.animatorStateName;
      set => this.animatorStateName = value;
    }

    int MMD4MecanimAnim.IAnim.animatorStateNameHash
    {
      get => this.animatorStateNameHash;
      set => this.animatorStateNameHash = value;
    }

    TextAsset MMD4MecanimAnim.IAnim.animFile
    {
      get => this.animFile;
      set => this.animFile = value;
    }

    AudioClip MMD4MecanimAnim.IAnim.audioClip
    {
      get => this.audioClip;
      set => this.audioClip = value;
    }

    MMD4MecanimData.AnimData MMD4MecanimAnim.IAnim.animData
    {
      get => this.animData;
      set => this.animData = value;
    }

    MMD4MecanimAnim.MorphMotion[] MMD4MecanimAnim.IAnim.morphMotionList
    {
      get => this.morphMotionList;
      set => this.morphMotionList = value;
    }
  }

  public interface IMorph
  {
    string name { get; }

    float weight { get; set; }
  }

  public interface IAnimModel
  {
    MMD4MecanimAnim.IMorph GetMorph(string name);

    MMD4MecanimAnim.IMorph GetMorph(string name, bool startsWith);

    int morphCount { get; }

    MMD4MecanimAnim.IMorph GetMorphAt(int index);

    int animCount { get; }

    MMD4MecanimAnim.IAnim GetAnimAt(int index);

    Animator animator { get; }

    AudioSource audioSource { get; }

    bool animSyncToAudio { get; }

    float prevDeltaTime { get; set; }

    MMD4MecanimAnim.IAnim playingAnim { get; set; }

    void _SetAnimMorphWeight(MMD4MecanimAnim.IMorph morph, float weight);
  }
}
