﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimBoneImpl
// Assembly: MMD4Mecanim, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: ADBCBBA1-CEE8-40F4-A775-E57AB2D2693F
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Scripts\MMD4Mecanim.dll

using System;
using UnityEngine;

public class MMD4MecanimBoneImpl : MonoBehaviour
{
  public MMD4MecanimModelImpl model;
  public int boneID = -1;
  public bool ikEnabled;
  public float ikWeight = 1f;
  [NonSerialized]
  public int humanBodyBones = -1;
  private MMD4MecanimData.BoneData _boneData;
  [NonSerialized]
  public Vector3 _userPosition = Vector3.zero;
  [NonSerialized]
  public Vector3 _userEulerAngles = Vector3.zero;
  [NonSerialized]
  public Quaternion _userRotation = Quaternion.identity;
  [NonSerialized]
  public bool _userPositionIsZero = true;
  [NonSerialized]
  public bool _userRotationIsIdentity = true;

  public MMD4MecanimData.BoneData boneData => this._boneData;

  public Vector3 userPosition
  {
    get => this._userPosition;
    set
    {
      if (!(this._userPosition != value))
        return;
      this._userPosition = value;
      this._userPositionIsZero = MMD4MecanimCommon.FuzzyZero(value);
    }
  }

  public Vector3 userEulerAngles
  {
    get => this._userEulerAngles;
    set
    {
      if (!(this._userEulerAngles != value))
        return;
      if (MMD4MecanimCommon.FuzzyZero(value))
      {
        this._userRotation = Quaternion.identity;
        this._userEulerAngles = Vector3.zero;
        this._userRotationIsIdentity = true;
      }
      else
      {
        this._userRotation = Quaternion.Euler(value);
        this._userEulerAngles = value;
        this._userRotationIsIdentity = false;
      }
    }
  }

  public Quaternion userRotation
  {
    get => this._userRotation;
    set
    {
      if (!(this._userRotation != value))
        return;
      if (MMD4MecanimCommon.FuzzyIdentity(value))
      {
        this._userRotation = Quaternion.identity;
        this._userEulerAngles = Vector3.zero;
        this._userRotationIsIdentity = true;
      }
      else
      {
        this._userRotation = value;
        this._userEulerAngles = value.eulerAngles;
        this._userRotationIsIdentity = false;
      }
    }
  }

  public void Setup()
  {
    if ((UnityEngine.Object) this.model == (UnityEngine.Object) null || this.model.modelData == null || this.model.modelData.boneDataList == null || this.boneID < 0 || this.boneID >= this.model.modelData.boneDataList.Length)
      return;
    this._boneData = this.model.modelData.boneDataList[this.boneID];
  }

  public void Bind()
  {
    if ((UnityEngine.Object) this.model == (UnityEngine.Object) null)
      return;
    MMD4MecanimData.BoneData boneData = this._boneData;
  }

  public void Destroy() => this._boneData = (MMD4MecanimData.BoneData) null;
}
