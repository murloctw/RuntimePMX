﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimImporterImplEditor
// Assembly: MMD4MecanimEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DD6A7AB2-B1E4-4A37-85A9-48A7C8E1BAF8
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Editor\MMD4MecanimEditor.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
[CanEditMultipleObjects, CustomEditor(typeof(MMD4MecanimImporterImpl))]
public class MMD4MecanimImporterImplEditor : Editor
{
    private static bool _isStartupUnityEditor = true;
    private static bool _isHierarchyWindowChanged = false;
    private static int _isPlaymodeChangedDelay = 0;
    private static int _importedMMDAssetDelay = 0;
    private static int _importedFBXAssetDelay = 0;
    private static List<string> _importedMMDAssetPaths = new List<string>();
    private static List<string> _importedFBXAssetPaths = new List<string>();
    public static volatile bool _isProcessedPMX2FBX = false;
    private static readonly string _compilingLockTempFile = "Temp/MMD4MecanimLockfile";
    private static readonly string _licenseWarningLockTempFile = "Temp/MMD4MecanimLicenseWarningLockfile";
    public static bool _isEnabledPMX2FBX;
    private static bool _showWarningAtLeastOnce;
    private static bool _intializeChecked = false;
    public static List<MMD4MecanimImporterImpl> _cache_importers;

    private static void _InvokeEliminate() => Shader.SetGlobalFloat("___Eliminate", 1f);

    private static void _InvokeLicenseCheck()
    {
        BuildTarget activeBuildTarget = EditorUserBuildSettings.activeBuildTarget;
        int num = activeBuildTarget == BuildTarget.iOS ? 1 : 0;
        if (!MMD4MecanimEditorCommon.GetBulletPhysicsPluginState().hasManaged && (activeBuildTarget.ToString().StartsWith("Web") || activeBuildTarget == BuildTarget.WebGL))
            MMD4MecanimImporterImplEditor._InternalLicenseCheck((string)null);
        if (num == 0 && activeBuildTarget != BuildTarget.Android)
            return;
        MMD4MecanimImporterImplEditor._InternalWarning();
    }

    private static void _InternalWarning()
    {
        if (MMD4MecanimImporterImplEditor._showWarningAtLeastOnce)
            return;
        MMD4MecanimImporterImplEditor._showWarningAtLeastOnce = true;
        if (MMD4MecanimEditorCommon.IsLicenseAccepted())
            return;
        if (File.Exists(MMD4MecanimImporterImplEditor._licenseWarningLockTempFile))
        {
            MMD4MecanimImporterImplEditor._showWarningAtLeastOnce = true;
        }
        else
        {
            try
            {
                File.Create(_licenseWarningLockTempFile);
            }
            catch (Exception ex)
            {
            }
            if (MMD4MecanimImporterImpl.IsJapanese())
            {
                string message = "場合によって、ソフトウェアの公開を停止することがあります。\n詳細は Readme_jp.txt をご参照ください。";
                Debug.LogWarning((object)"著作者・権利者の明示的な許可がない限り、ビルド・動画・静止画のアップロード・SNS投稿など、私的利用外での利用はお控えください。\nまた、ハック的に動作させたり、BlogやSNSなどを通じて不特定多数にその手段の情報共有もしないで下さい。\n");
                Debug.LogWarning((object)message);
            }
            else
            {
                string message = "If someone can't save it, I may discontinue to software developing and publishing.\nSee also Readme_en.txt.";
                Debug.LogWarning((object)"Please refrain from uploading a binary, movie or screenshot without explicit permission by authors.\nAnd don't hack for forcely supporting platform, so don't share that technique, too.\n");
                Debug.LogWarning((object)message);
            }
        }
    }

    private static void _InternalLicenseCheck(string dllName)
    {
        if (dllName != null)
        {
            string[] allAssetPaths = AssetDatabase.GetAllAssetPaths();
            if (allAssetPaths != null)
            {
                foreach (string str in allAssetPaths)
                {
                    if (str != null && str.Contains(dllName))
                        return;
                }
            }
        }
        if (MMD4MecanimImporterImpl.IsJapanese())
        {
            string message = "場合によって、ソフトウェアの公開を停止することがあります。\n詳細は Readme_jp.txt をご参照ください。";
            Debug.LogError((object)"サポート外のプラットホームです。ビルド・動画・静止画のアップロード・SNS投稿など、私的利用外での利用はお控えください。\nまた、ハック的に動作させたり、BlogやSNSなどを通じて不特定多数にその手段の情報共有もしないで下さい。\n");
            Debug.LogError((object)message);
        }
        else
        {
            string message = "If someone can't save it, I may discontinue to software developing and publishing.\nSee also Readme_en.txt.";
            Debug.LogError((object)"Unsupported platform. Please refrain from uploading a binary, movie or screenshot.\nAnd don't hack for forcely supporting platform, so don't share that technique, too.\n");
            Debug.LogError((object)message);
        }
    }

    private static void _PMX2FBXCheck() => _isEnabledPMX2FBX = MMD4MecanimImporterImpl.GetPMX2FBXRootConfigPath() != null || MMD4MecanimImporterImpl.GetPMX2FBXPath(false, MMD4MecanimImporterImpl.PMX2FBXConfig.BulletPhysicsVersion.Latest) != null || MMD4MecanimImporterImpl.GetPMX2FBXPath(false, MMD4MecanimImporterImpl.PMX2FBXConfig.BulletPhysicsVersion._275) != null;

    static MMD4MecanimImporterImplEditor()
    {
        if (!MMD4MecanimEditorCommon.ValidateUnityVersion())
            return;
        Type type = typeof(EditorApplication);
        EventInfo eventInfo = type.GetEvent("playModeStateChanged", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.GetField | BindingFlags.GetProperty);
        FieldInfo field = type.GetField("playmodeStateChanged", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.GetField | BindingFlags.GetProperty);
        if (eventInfo != null)
        {
            MethodInfo method = typeof(MMD4MecanimImporterImplEditor).GetMethod("_OnPlayModeStateChanged", BindingFlags.Static | BindingFlags.NonPublic);
            Delegate handler = Delegate.CreateDelegate(eventInfo.EventHandlerType, null, method);
            eventInfo.AddEventHandler(null, handler);
        }
        else if (field != null)
        {
            MethodInfo method = typeof(MMD4MecanimImporterImplEditor).GetMethod("_OnPlaymodeStateChanged", BindingFlags.Static | BindingFlags.NonPublic);
            Delegate b = Delegate.CreateDelegate(field.FieldType, null, method);
            Delegate a = (Delegate)field.GetValue(null);
            if (a != null)
                field.SetValue(null, Delegate.Combine(a, b));
            else
                field.SetValue(null, b);
        }
        EditorApplication.projectChanged += _InvokeEliminate;
        EditorApplication.hierarchyChanged += () =>
        {
            _InvokeEliminate();
            _isHierarchyWindowChanged = true;
        };
        EditorApplication.update += (() =>
        {
            if (!_intializeChecked)
            {
                Debug.Log("111");
                _intializeChecked = true;
                _InitializeCheck();
            }
            if (_isProcessedPMX2FBX)
            {
                Debug.Log("222");
                _isProcessedPMX2FBX = false;
                AssetDatabase.Refresh();
            }
            else if (EditorApplication.isCompiling)
            {
                //Debug.Log("333");
                if (File.Exists(_compilingLockTempFile))
                    return;
                try
                {
                    File.Create(_compilingLockTempFile);
                }
                catch (Exception ex)
                {
                }
            }
            else
            {
                if (_isStartupUnityEditor && File.Exists(_compilingLockTempFile))
                {
                    Debug.Log("555");
                    MMD4MecanimEditorCommon.ValidateScriptExecutionOrder();
                    _isStartupUnityEditor = false;
                    _isHierarchyWindowChanged = false;
                    File.Delete(_compilingLockTempFile);
                }
                if (_isPlaymodeChangedDelay > 0)
                {
                    Debug.Log("666");
                    --_isPlaymodeChangedDelay;
                    _isStartupUnityEditor = false;
                    _isHierarchyWindowChanged = false;
                }
                else if (!EditorApplication.isCompiling && !EditorApplication.isPlaying && !EditorApplication.isPlayingOrWillChangePlaymode && MMD4MecanimImporterImpl._pmx2fbxProcessingCount == 0)
                {
                    if (_isStartupUnityEditor)
                    {
                        _isStartupUnityEditor = false;
                        _OnStartupUnityEditor();
                    }
                    if (_importedMMDAssetDelay > 0)
                    {
                        --_importedMMDAssetDelay;
                    }
                    else
                    {
                        while (_importedMMDAssetPaths.Count > 0)
                        {
                            string importedMmdAssetPath = _importedMMDAssetPaths[0];
                            _importedMMDAssetPaths.RemoveAt(0);
                            _OnImportedMMDAsset(importedMmdAssetPath);
                        }
                    }
                    if (_importedFBXAssetDelay > 0)
                    {
                        --_importedFBXAssetDelay;
                    }
                    else
                    {
                        while (_importedFBXAssetPaths.Count > 0)
                        {
                            string importedFbxAssetPath = _importedFBXAssetPaths[0];
                            _importedFBXAssetPaths.RemoveAt(0);
                            _OnImportedFBXAsset(importedFbxAssetPath);
                        }
                    }
                    if (_isHierarchyWindowChanged)
                    {
                        _isHierarchyWindowChanged = false;
                        _ForceAllCheckModelInScene();
                    }
                    _cache_importers = null;
                }
                else
                {
                    _isStartupUnityEditor = false;
                    _isHierarchyWindowChanged = false;
                }
            }
        });
    }

    private static void _InitializeCheck()
    {
        _PMX2FBXCheck();
        _InvokeEliminate();
        _InvokeLicenseCheck();
        if (Application.isPlaying)
            return;
        MMD4MecanimEditorCommon.CheckAndroidDevices();
    }

    private static void _OnPlaymodeStateChanged()
    {
        _isPlaymodeChangedDelay = 2;
        _InvokeEliminate();
        if (Application.isPlaying)
            _InvokeLicenseCheck();
        if (Application.isPlaying)
            return;
        MMD4MecanimEditorCommon.CheckAndroidDevices();
    }

    private static void _OnPlayModeStateChanged(object playModeStateChange) => MMD4MecanimImporterImplEditor._OnPlaymodeStateChanged();

    private static void _OnStartupUnityEditor()
    {
        MMD4MecanimEditorCommon.SetBulletPhysicsPluginType(MMD4MecanimEditorCommon.GetBulletPhysicsPluginState().bulletPhysicsPluginType);
        MMD4MecanimEditorCommon.ValidateScriptExecutionOrder();
        string[] allAssetPaths = AssetDatabase.GetAllAssetPaths();
        if (allAssetPaths == null)
            return;
        _cache_GetImporters();
        foreach (string str in allAssetPaths)
        {
            if (MMD4MecanimEditorCommon.IsExtensionPMDorPMX(str))
                _OnImportedMMDAsset(str);
        }
        foreach (string str in allAssetPaths)
        {
            if (MMD4MecanimEditorCommon.IsExtensionFBX(str))
            {
                MMD4MecanimImporterImpl fromFbxAssetPath = _GetImporterFromFBXAssetPath(str);
                if (fromFbxAssetPath != null)
                    fromFbxAssetPath._OnImportedFBXAsset(str);
            }
        }
        _cache_importers = null;
    }

    private static void _MoveDependedAssets(string[] dependedPaths, string[] dependedFromPaths)
    {
        if (dependedPaths == null || dependedFromPaths == null)
            return;
        for (int index = 0; index < dependedPaths.Length; ++index)
        {
            if (File.Exists(dependedFromPaths[index]))
                AssetDatabase.MoveAsset(dependedFromPaths[index], dependedPaths[index]);
        }
    }

    private static void _DeleteDependedAssets(string[] dependedPaths)
    {
        if (dependedPaths == null)
            return;
        foreach (string dependedPath in dependedPaths)
        {
            if (File.Exists(dependedPath))
                AssetDatabase.DeleteAsset(dependedPath);
        }
    }

    private static void _ForceRefreshImportScale()
    {
        if (!(FindObjectsOfType(typeof(MMD4MecanimModelImpl)) is MMD4MecanimModelImpl[] objectsOfType))
            return;
        for (int index = 0; index < objectsOfType.Length; ++index)
        {
            MMD4MecanimModelImpl mecanimModelImpl = objectsOfType[index];
            string assetPath = MMD4MecanimImporterImpl.GetAssetPath(mecanimModelImpl);
            if (assetPath != null)
            {
                float modelImportScale = MMD4MecanimEditorCommon.GetModelImportScale((ModelImporter)AssetImporter.GetAtPath(assetPath));
                if ((double)mecanimModelImpl.importScale != (double)modelImportScale)
                {
                    mecanimModelImpl.importScale = modelImportScale;
                    EditorUtility.SetDirty(mecanimModelImpl);
                }
            }
        }
    }

    private static void _ForceAllCheckModelInScene()
    {
        if (UnityEngine.Object.FindObjectsOfType(typeof(Animator)) is Animator[] objectsOfType1)
        {
            for (int index = 0; index < objectsOfType1.Length; ++index)
                MMD4MecanimImporterImplEditor._ForceAllCheckModelInScene(objectsOfType1[index].gameObject);
        }
        if (!(UnityEngine.Object.FindObjectsOfType(typeof(Animation)) is Animation[] objectsOfType2))
            return;
        for (int index = 0; index < objectsOfType2.Length; ++index)
            MMD4MecanimImporterImplEditor._ForceAllCheckModelInScene(objectsOfType2[index].gameObject);
    }

    private static void _ForceAllCheckModelInScene(GameObject gameObject)
    {
        if (!((UnityEngine.Object)gameObject.GetComponent<MMD4MecanimModelImpl>() == (UnityEngine.Object)null))
            return;
        SkinnedMeshRenderer skinnedMeshRenderer = MMD4MecanimCommon.GetSkinnedMeshRenderer(gameObject);
        if ((UnityEngine.Object)skinnedMeshRenderer != (UnityEngine.Object)null && (UnityEngine.Object)skinnedMeshRenderer.sharedMesh != (UnityEngine.Object)null)
        {
            string assetPath = AssetDatabase.GetAssetPath((UnityEngine.Object)skinnedMeshRenderer.sharedMesh);
            if (MMD4MecanimEditorCommon.IsExtensionFBX(assetPath))
                MMD4MecanimImporterImplEditor._CheckModelInScene(gameObject, assetPath, true);
        }
        if (false)
            return;
        MeshRenderer meshRenderer = MMD4MecanimCommon.GetMeshRenderer(gameObject);
        if (!((UnityEngine.Object)meshRenderer != (UnityEngine.Object)null))
            return;
        MeshFilter component = meshRenderer.gameObject.GetComponent<MeshFilter>();
        if (!((UnityEngine.Object)component != (UnityEngine.Object)null) || !((UnityEngine.Object)component.sharedMesh != (UnityEngine.Object)null))
            return;
        string assetPath1 = AssetDatabase.GetAssetPath((UnityEngine.Object)component.sharedMesh);
        if (!MMD4MecanimEditorCommon.IsExtensionFBX(assetPath1))
            return;
        MMD4MecanimImporterImplEditor._CheckModelInScene(gameObject, assetPath1, false);
    }

    private static void _CheckModelInScene(
      GameObject gameObject,
      string assetPath,
      bool isSkinningMesh)
    {
        string modelDataPath = MMD4MecanimImporterImpl.GetModelDataPath(assetPath);
        if (!File.Exists(modelDataPath))
            return;
        TextAsset modelData = (TextAsset)AssetDatabase.LoadAssetAtPath(modelDataPath, typeof(TextAsset));
        if (isSkinningMesh)
        {
            string indexDataPath = MMD4MecanimImporterImpl.GetIndexDataPath(assetPath);
            string vertexDataPath = MMD4MecanimImporterImpl.GetVertexDataPath(assetPath);
            if (!File.Exists(indexDataPath))
                return;
            TextAsset indexData = (TextAsset)AssetDatabase.LoadAssetAtPath(indexDataPath, typeof(TextAsset));
            TextAsset vertexData = (TextAsset)AssetDatabase.LoadAssetAtPath(vertexDataPath, typeof(TextAsset));
            MMD4MecanimImporterImplEditor._MakeModel(gameObject, assetPath, modelData, indexData, vertexData, isSkinningMesh);
        }
        else
            MMD4MecanimImporterImplEditor._MakeModel(gameObject, assetPath, modelData, (TextAsset)null, (TextAsset)null, isSkinningMesh);
    }

    private static System.Type _GetMMD4MecanimModelType() => MMD4MecanimEditorCommon.GetAssemblyCSharpType("MMD4MecanimModel");

    public static string _GetAnimStateName(UnityEngine.Object[] modelAssets, string animAssetName)
    {
        if (string.IsNullOrEmpty(animAssetName))
            return "";
        if (modelAssets != null)
        {
            foreach (UnityEngine.Object modelAsset in modelAssets)
            {
                AnimationClip animationClip = modelAsset as AnimationClip;
                if (animationClip != null)
                {
                    string name = animationClip.name;
                    if ((name.EndsWith(".vmd") || name.EndsWith("_vmd")) && animAssetName == name.Substring(0, name.Length - 4) || animAssetName == name)
                        return name;
                }
            }
        }
        return animAssetName + "_vmd";
    }

    private static void _MakeModel(
      GameObject modelGameObject,
      string fbxAssetPath,
      TextAsset modelData,
      TextAsset indexData,
      TextAsset vertexData,
      bool isSkinned)
    {
        if (!(modelData != null) || isSkinned && !(indexData != null))
            return;
        ModelImporter atPath = (ModelImporter)AssetImporter.GetAtPath(fbxAssetPath);
        if (!(atPath != null))
            return;
        MMD4MecanimModelImpl mecanimModelImpl = modelGameObject.AddComponent(_GetMMD4MecanimModelType()) as MMD4MecanimModelImpl;
        float modelImportScale = MMD4MecanimEditorCommon.GetModelImportScale(atPath);
        mecanimModelImpl.importScale = modelImportScale;
        mecanimModelImpl.modelFile = modelData;
        mecanimModelImpl.indexFile = indexData;
        mecanimModelImpl.vertexFile = vertexData;
        UnityEngine.Object[] modelAssets = AssetDatabase.LoadAllAssetsAtPath(fbxAssetPath);
        MMD4MecanimImporterImpl fromFbxAssetPath = MMD4MecanimImporterImplEditor._GetImporterFromFBXAssetPath(fbxAssetPath);
        if (!((UnityEngine.Object)fromFbxAssetPath != (UnityEngine.Object)null) || fromFbxAssetPath.pmx2fbxProperty == null || fromFbxAssetPath.pmx2fbxProperty.vmdAssetList == null)
            return;
        foreach (UnityEngine.Object vmdAsset in fromFbxAssetPath.pmx2fbxProperty.vmdAssetList)
        {
            string assetPath = AssetDatabase.GetAssetPath(vmdAsset);
            if (!string.IsNullOrEmpty(assetPath))
            {
                TextAsset textAsset = (TextAsset)AssetDatabase.LoadAssetAtPath(MMD4MecanimImporterImpl.GetAnimDataPath(assetPath), typeof(TextAsset));
                if ((UnityEngine.Object)textAsset != (UnityEngine.Object)null)
                {
                    if (mecanimModelImpl.animList == null)
                        mecanimModelImpl.animList = new MMD4MecanimModelImpl.Anim[1];
                    else
                        Array.Resize<MMD4MecanimModelImpl.Anim>(ref mecanimModelImpl.animList, mecanimModelImpl.animList.Length + 1);
                    MMD4MecanimModelImpl.Anim anim = new MMD4MecanimModelImpl.Anim();
                    anim.animFile = textAsset;
                    string animStateName = MMD4MecanimImporterImplEditor._GetAnimStateName(modelAssets, Path.GetFileNameWithoutExtension(textAsset.name));
                    anim.animatorStateName = "Base Layer." + animStateName;
                    mecanimModelImpl.animList[mecanimModelImpl.animList.Length - 1] = anim;
                }
            }
        }
    }

    private static string[] _GetMMDAssetDependedPaths(string mmdAssetPath) => new string[2]
    {
        MMD4MecanimImporterImpl.GetImporterAssetPath(mmdAssetPath),
        MMD4MecanimImporterImpl.GetImporterPropertyAssetPath(mmdAssetPath)
    };

    public static void _OnRegistImportedMMDAsset(string mmdAssetPath)
    {
        _importedMMDAssetDelay = 1;
        _importedMMDAssetPaths.Add(mmdAssetPath);
    }

    public static void _OnImportedMMDAsset(string mmdAssetPath)
    {
        RichLog.Log($"_OnImportedMMDAsset {mmdAssetPath}", Color.blue);

        if (mmdAssetPath == null)
            return;
        string importerAssetPath = MMD4MecanimImporterImpl.GetImporterAssetPath(mmdAssetPath);
        if (File.Exists(importerAssetPath))
            return;
        MMD4MecanimImporterImpl instance = CreateInstance<MMD4MecanimImporterImpl>();
        AssetDatabase.CreateAsset(instance, importerAssetPath);
        AssetDatabase.Refresh();
        if (_cache_importers == null)
            return;
        _cache_importers.Add(instance);
    }

    public static void _OnMovedMMDAsset(string mmdAssetPath, string mmdAssetFromPath)
    {
        if (mmdAssetPath == null || mmdAssetFromPath == null)
            return;
        MMD4MecanimImporterImplEditor._MoveDependedAssets(MMD4MecanimImporterImplEditor._GetMMDAssetDependedPaths(mmdAssetPath), MMD4MecanimImporterImplEditor._GetMMDAssetDependedPaths(mmdAssetFromPath));
    }

    public static void _OnDeletedMMDAsset(string mmdAssetPath)
    {
        if (mmdAssetPath == null)
            return;
        string[] assetDependedPaths = _GetMMDAssetDependedPaths(mmdAssetPath);
        string str = assetDependedPaths[0];
        if (_cache_importers != null)
        {
            for (int index = 0; index < _cache_importers.Count; ++index)
            {
                if (_cache_importers[index] != null && AssetDatabase.GetAssetPath(_cache_importers[index]) == str)
                {
                    _cache_importers.RemoveAt(index);
                    break;
                }
            }
        }
        _DeleteDependedAssets(assetDependedPaths);
    }

    private static string[] _GetFBXAssetDependedPaths(string fbxAssetPath) => new string[5]
    {
    MMD4MecanimImporterImpl.GetMMDModelPath(fbxAssetPath),
    MMD4MecanimImporterImpl.GetModelDataPath(fbxAssetPath),
    MMD4MecanimImporterImpl.GetExtraDataPath(fbxAssetPath),
    MMD4MecanimImporterImpl.GetIndexDataPath(fbxAssetPath),
    MMD4MecanimImporterImpl.GetVertexDataPath(fbxAssetPath)
    };

    private static List<MMD4MecanimImporterImpl> _cache_GetImporters(
      string[] assetPaths)
    {
        if (_cache_importers == null)
        {
            _cache_importers = new List<MMD4MecanimImporterImpl>();
            if (assetPaths == null)
                assetPaths = AssetDatabase.GetAllAssetPaths();
            if (assetPaths != null)
            {
                foreach (string assetPath in assetPaths)
                {
                    if (assetPath.EndsWith(".MMD4Mecanim.asset"))
                    {
                        MMD4MecanimImporterImpl mecanimImporterImpl = (MMD4MecanimImporterImpl)AssetDatabase.LoadAssetAtPath(assetPath, typeof(MMD4MecanimImporterImpl));
                        if (mecanimImporterImpl != null)
                        {
                            mecanimImporterImpl.Setup();
                            _cache_importers.Add(mecanimImporterImpl);
                        }
                    }
                }
            }
        }
        else
        {
            int index = 0;
            while (index < _cache_importers.Count)
            {
                if (_cache_importers[index] != null)
                    ++index;
                else
                    _cache_importers.RemoveAt(index);
            }
        }
        return _cache_importers;
    }

    private static List<MMD4MecanimImporterImpl> _cache_GetImporters() => _cache_GetImporters(null);

    private static MMD4MecanimImporterImpl _GetImporterFromFBXAssetPath(
      string fbxAssetPath)
    {
        if (fbxAssetPath == null)
            return null;
        if (_cache_importers != null)
            return _cache_GetImporterFromFBXAssetPath(fbxAssetPath);
        string importerAssetPath = MMD4MecanimImporterImpl.GetImporterAssetPath(fbxAssetPath);
        if (!File.Exists(importerAssetPath))
            return _cache_GetImporterFromFBXAssetPath(fbxAssetPath);
        MMD4MecanimImporterImpl fromFbxAssetPath = (MMD4MecanimImporterImpl)AssetDatabase.LoadAssetAtPath(importerAssetPath, typeof(MMD4MecanimImporterImpl));
        if (fromFbxAssetPath != null)
            fromFbxAssetPath.Setup();
        return fromFbxAssetPath;
    }

    private static MMD4MecanimImporterImpl _cache_GetImporterFromFBXAssetPath(
      string fbxAssetPath)
    {
        if (fbxAssetPath == null)
            return null;
        foreach (MMD4MecanimImporterImpl importer in _cache_GetImporters())
        {
            if (fbxAssetPath == importer.fbxAssetPath)
                return importer;
        }
        return null;
    }

    public static void _OnImportingFBXAsset(string fbxAssetPath)
    {
        if (fbxAssetPath == null || !(_GetImporterFromFBXAssetPath(fbxAssetPath) != null))
            return;
        ModelImporter atPath = (ModelImporter)AssetImporter.GetAtPath(fbxAssetPath);
        if (!(atPath != null))
            return;
        if (atPath.materialSearch == ModelImporterMaterialSearch.RecursiveUp)
            atPath.materialSearch = ModelImporterMaterialSearch.Local;
        if (atPath.animationCompression != ModelImporterAnimationCompression.KeyframeReduction)
            return;
        atPath.animationCompression = ModelImporterAnimationCompression.Optimal;
    }

    public static void _OnRegistImportedFBXAsset(string fbxAssetPath)
    {
        _importedFBXAssetDelay = 1;
        _importedFBXAssetPaths.Add(fbxAssetPath);
    }

    public static void _OnImportedFBXAsset(string fbxAssetPath)
    {
        if (fbxAssetPath == null)
            return;
        MMD4MecanimImporterImpl fromFbxAssetPath = _GetImporterFromFBXAssetPath(fbxAssetPath);
        if (!(fromFbxAssetPath != null))
            return;

        fromFbxAssetPath._OnImportedFBXAsset(fbxAssetPath);
        _ForceRefreshImportScale();
    }

    public static void _OnMovedFBXAsset(string fbxAssetPath, string fbxAssetFromPath)
    {
        if (fbxAssetPath == null)
            return;
        _MoveDependedAssets(_GetFBXAssetDependedPaths(fbxAssetPath), _GetFBXAssetDependedPaths(fbxAssetFromPath));
        MMD4MecanimImporterImpl fromFbxAssetPath = _GetImporterFromFBXAssetPath(fbxAssetFromPath);
        if (!(fromFbxAssetPath != null))
            return;
        fromFbxAssetPath.fbxAssetPath = fbxAssetPath;
        fromFbxAssetPath.SavePMX2FBXConfig();
    }

    public static void _OnDeletedFBXAsset(string fbxAssetPath)
    {
        if (fbxAssetPath == null)
            return;
        _DeleteDependedAssets(_GetFBXAssetDependedPaths(fbxAssetPath));
        MMD4MecanimImporterImpl fromFbxAssetPath = _GetImporterFromFBXAssetPath(fbxAssetPath);
        if (!(fromFbxAssetPath != null))
            return;
        fromFbxAssetPath.fbxAsset = null;
        fromFbxAssetPath.fbxAssetPath = null;
        fromFbxAssetPath.SavePMX2FBXConfig();
    }

    public override void OnInspectorGUI()
    {
        MMD4MecanimImporterImpl impl = (MMD4MecanimImporterImpl)target;
        impl.OnInspectorGUI();
    }
}
