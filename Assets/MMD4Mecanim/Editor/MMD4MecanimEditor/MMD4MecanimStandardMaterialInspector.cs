﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimStandardMaterialInspector
// Assembly: MMD4MecanimEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DD6A7AB2-B1E4-4A37-85A9-48A7C8E1BAF8
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Editor\MMD4MecanimEditor.dll

using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class MMD4MecanimStandardMaterialInspector : MaterialEditor
{
  private static void SetKeyword(UnityEngine.Material m, string keyword, bool state)
  {
    if (state)
      m.EnableKeyword(keyword);
    else
      m.DisableKeyword(keyword);
  }

  private static MMD4MecanimStandardMaterialInspector.SmoothnessMapChannel GetSmoothnessMapChannel(
    UnityEngine.Material material)
  {
    return (int) material.GetFloat("_SmoothnessTextureChannel") == 1 ? MMD4MecanimStandardMaterialInspector.SmoothnessMapChannel.AlbedoAlpha : MMD4MecanimStandardMaterialInspector.SmoothnessMapChannel.SpecularMetallicAlpha;
  }

  private static bool ShouldEmissionBeEnabled(UnityEngine.Material mat, Color color)
  {
    bool flag = (mat.globalIlluminationFlags & MaterialGlobalIlluminationFlags.RealtimeEmissive) > MaterialGlobalIlluminationFlags.None;
    return (double) Mathf.Max(Mathf.Max(color.r, color.g), color.b) > 0.000392156856833026 | flag;
  }

  private static void SetMaterialKeywords(UnityEngine.Material material)
  {
    MMD4MecanimStandardMaterialInspector.SetKeyword(material, "_NORMALMAP", (bool) (Object) material.GetTexture("_BumpMap") || (bool) (Object) material.GetTexture("_DetailNormalMap"));
    MMD4MecanimStandardMaterialInspector.SetKeyword(material, "_SPECGLOSSMAP", (bool) (Object) material.GetTexture("_SpecGlossMap"));
    MMD4MecanimStandardMaterialInspector.SetKeyword(material, "_METALLICGLOSSMAP", (bool) (Object) material.GetTexture("_MetallicGlossMap"));
    MMD4MecanimStandardMaterialInspector.SetKeyword(material, "_PARALLAXMAP", (bool) (Object) material.GetTexture("_ParallaxMap"));
    MMD4MecanimStandardMaterialInspector.SetKeyword(material, "_DETAIL_MULX2", (bool) (Object) material.GetTexture("_DetailAlbedoMap") || (bool) (Object) material.GetTexture("_DetailNormalMap"));
    bool state = MMD4MecanimStandardMaterialInspector.ShouldEmissionBeEnabled(material, material.GetColor("_EmissionColor"));
    MMD4MecanimStandardMaterialInspector.SetKeyword(material, "_EMISSION", state);
    if (material.HasProperty("_SmoothnessTextureChannel"))
      MMD4MecanimStandardMaterialInspector.SetKeyword(material, "_SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A", MMD4MecanimStandardMaterialInspector.GetSmoothnessMapChannel(material) == MMD4MecanimStandardMaterialInspector.SmoothnessMapChannel.AlbedoAlpha);
    MaterialGlobalIlluminationFlags illuminationFlags1 = material.globalIlluminationFlags;
    if ((illuminationFlags1 & MaterialGlobalIlluminationFlags.AnyEmissive) == MaterialGlobalIlluminationFlags.None)
      return;
    MaterialGlobalIlluminationFlags illuminationFlags2 = illuminationFlags1 & ~MaterialGlobalIlluminationFlags.EmissiveIsBlack;
    if (!state)
      illuminationFlags2 |= MaterialGlobalIlluminationFlags.EmissiveIsBlack;
    material.globalIlluminationFlags = illuminationFlags2;
  }

  public override void OnInspectorGUI()
  {
    if (!MMD4MecanimEditorCommon.ValidateUnityVersion())
      return;
    base.OnInspectorGUI();
    if (!this.isVisible)
      return;
    UnityEngine.Material target = this.target as UnityEngine.Material;
    string[] shaderKeywords = target.shaderKeywords;
    EditorGUILayout.Separator();
    EditorGUILayout.LabelField("Tags", EditorStyles.boldLabel);
    MMD4MecanimStandardMaterialInspector.SetMaterialKeywords(target);
    bool flag1 = target.GetInt("_DstBlend") == 10;
    EditorGUI.BeginChangeCheck();
    bool flag2 = EditorGUILayout.Toggle("Transparency", flag1);
    if (EditorGUI.EndChangeCheck())
    {
      MMD4MecanimEditorCommon.SetOverrideTag(target, "RenderType", "");
      target.SetInt("_ZWrite", 1);
      target.DisableKeyword("_ALPHATEST_ON");
      target.DisableKeyword("_ALPHAPREMULTIPLY_ON");
      target.SetFloat("_Cutoff", 0.003921569f);
      if (flag2)
      {
        target.SetInt("_SrcBlend", 5);
        target.SetInt("_DstBlend", 10);
        target.EnableKeyword("_ALPHABLEND_ON");
        target.SetFloat("_Mode", 3f);
      }
      else
      {
        target.SetInt("_SrcBlend", 1);
        target.SetInt("_DstBlend", 0);
        target.DisableKeyword("_ALPHABLEND_ON");
        target.SetFloat("_Mode", 0.0f);
      }
    }
    bool flag3 = target.GetInt("_Cull") == 0;
    EditorGUI.BeginChangeCheck();
    bool flag4 = EditorGUILayout.Toggle("BothFaces", flag3);
    if (EditorGUI.EndChangeCheck())
    {
      if (flag4)
        target.SetInt("_Cull", 0);
      else
        target.SetInt("_Cull", 2);
    }
    int renderQueue = target.renderQueue;
    EditorGUI.BeginChangeCheck();
    int num = EditorGUILayout.IntField("RenderQueue", renderQueue);
    if (EditorGUI.EndChangeCheck())
      target.renderQueue = num;
    EditorGUILayout.Separator();
    EditorGUILayout.LabelField("Shader Keywords", EditorStyles.boldLabel);
    bool flag5 = ((IEnumerable<string>) shaderKeywords).Contains<string>("_ADD_SPECULAR");
    EditorGUI.BeginChangeCheck();
    bool flag6 = EditorGUILayout.Toggle("Add Specular", flag5);
    if (EditorGUI.EndChangeCheck())
    {
      if (flag6)
        target.EnableKeyword("_ADD_SPECULAR");
      else
        target.DisableKeyword("_ADD_SPECULAR");
      EditorUtility.SetDirty((Object) target);
    }
    bool flag7 = ((IEnumerable<string>) shaderKeywords).Contains<string>("SPECULAR_ON");
    EditorGUI.BeginChangeCheck();
    bool flag8 = EditorGUILayout.Toggle("Specular(Legacy)", flag7);
    if (!EditorGUI.EndChangeCheck())
      return;
    if (flag8)
      target.EnableKeyword("SPECULAR_ON");
    else
      target.DisableKeyword("SPECULAR_ON");
    EditorUtility.SetDirty((Object) target);
  }

  public enum SmoothnessMapChannel
  {
    SpecularMetallicAlpha,
    AlbedoAlpha,
  }
}
