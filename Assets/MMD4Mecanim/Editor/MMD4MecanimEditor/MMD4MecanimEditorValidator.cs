﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimEditorValidator
// Assembly: MMD4MecanimEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DD6A7AB2-B1E4-4A37-85A9-48A7C8E1BAF8
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Editor\MMD4MecanimEditor.dll

using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class MMD4MecanimEditorValidator
{
  private const string Message_Invalid_VRChat_Jp = "VRChatへの利用は禁止されています。詳細は Readme をご参照ください。";
  private const string Message_Invalid_VRChat_En = "Use to VRChat is prohibited. Please refer to the Readme for details.";
  private const string Message_Invalid_VRChat2_Jp = "VRChatの機能の一部を削除しました。復帰するには、別プロジェクトで再インポートしてください。";
  private const string Message_Invalid_VRChat2_En = "A part of VRChat extensions was deleted. If you need to restore it, please reimport package in other project.";
  private const string Message_Invalid_VRChat3_Jp = "ほとんどのモデルは再配布（アップロード）が禁止されています。";
  private const string Message_Invalid_VRChat3_En = "Most models are prohibited to redistribution (or uploading).";
  private const string Message_Invalid_VRChat4_Jp = "もしアップロード済みのモデルがある場合は、削除をお願いします。";
  private const string Message_Invalid_VRChat4_En = "If you've uploaded some models, please delete some models.";

  static MMD4MecanimEditorValidator()
  {
    bool flag = false;
    List<string> targetList = new List<string>();
    foreach (string allAssetPath in AssetDatabase.GetAllAssetPaths())
    {
      if (allAssetPath.Contains("/VRChat/") || allAssetPath.Contains("\\VRChat\\"))
      {
        flag = true;
        if (allAssetPath.Contains("/Editor/") || allAssetPath.Contains("\\Editor\\"))
          targetList.Add(allAssetPath);
      }
    }
    if (MMD4MecanimEditorValidator._WeakFix(targetList))
      AssetDatabase.Refresh();
    if (!flag)
      return;
    if (Application.systemLanguage == SystemLanguage.Japanese)
    {
      Debug.LogError((object) "VRChatへの利用は禁止されています。詳細は Readme をご参照ください。");
      Debug.LogError((object) "VRChatの機能の一部を削除しました。復帰するには、別プロジェクトで再インポートしてください。");
      Debug.LogError((object) "ほとんどのモデルは再配布（アップロード）が禁止されています。");
      Debug.LogError((object) "もしアップロード済みのモデルがある場合は、削除をお願いします。");
    }
    else
    {
      Debug.LogError((object) "Use to VRChat is prohibited. Please refer to the Readme for details.");
      Debug.LogError((object) "A part of VRChat extensions was deleted. If you need to restore it, please reimport package in other project.");
      Debug.LogError((object) "Most models are prohibited to redistribution (or uploading).");
      Debug.LogError((object) "If you've uploaded some models, please delete some models.");
    }
  }

  private static bool _WeakFix(List<string> targetList)
  {
    foreach (string target in targetList)
    {
      try
      {
        File.Delete(target);
      }
      catch (Exception ex)
      {
      }
    }
    return targetList.Count > 0;
  }
}
