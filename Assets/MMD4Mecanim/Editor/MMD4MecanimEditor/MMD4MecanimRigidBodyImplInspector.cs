﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimRigidBodyImplInspector
// Assembly: MMD4MecanimEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DD6A7AB2-B1E4-4A37-85A9-48A7C8E1BAF8
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Editor\MMD4MecanimEditor.dll

using MMD4MecanimInternal.Bullet;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof (MMD4MecanimRigidBodyImpl))]
public class MMD4MecanimRigidBodyImplInspector : Editor
{
  public override void OnInspectorGUI()
  {
    if (!MMD4MecanimEditorCommon.ValidateUnityVersion())
      return;
    MMD4MecanimRigidBodyImpl target = this.target as MMD4MecanimRigidBodyImpl;
    EditorGUI.BeginChangeCheck();
    RigidBodyProperty rigidBodyProperty = target.bulletPhysicsRigidBodyProperty;
    if (rigidBodyProperty == null)
    {
      rigidBodyProperty = new RigidBodyProperty();
      target.bulletPhysicsRigidBodyProperty = rigidBodyProperty;
    }
    GUILayout.Label("Bullet Physics Rigid Body", EditorStyles.boldLabel);
    rigidBodyProperty.isKinematic = EditorGUILayout.Toggle("isKinematic", rigidBodyProperty.isKinematic);
    rigidBodyProperty.isFreezed = EditorGUILayout.Toggle("isFreezed", rigidBodyProperty.isFreezed);
    rigidBodyProperty.isAdditionalDamping = EditorGUILayout.Toggle("isAdditionalDamping", rigidBodyProperty.isAdditionalDamping);
    if (rigidBodyProperty.isKinematic)
      GUI.enabled = false;
    rigidBodyProperty.mass = EditorGUILayout.FloatField("Mass", rigidBodyProperty.mass);
    if (rigidBodyProperty.isKinematic)
      GUI.enabled = true;
    rigidBodyProperty.linearDamping = EditorGUILayout.FloatField("LinearDamping", rigidBodyProperty.linearDamping);
    rigidBodyProperty.angularDamping = EditorGUILayout.FloatField("AngularDamping", rigidBodyProperty.angularDamping);
    rigidBodyProperty.restitution = EditorGUILayout.FloatField("Restitution", rigidBodyProperty.restitution);
    rigidBodyProperty.friction = EditorGUILayout.FloatField("Friction", rigidBodyProperty.friction);
    if (!EditorGUI.EndChangeCheck())
      return;
    EditorUtility.SetDirty(this.target);
  }
}
