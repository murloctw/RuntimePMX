﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimMaterialInspector
// Assembly: MMD4MecanimEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DD6A7AB2-B1E4-4A37-85A9-48A7C8E1BAF8
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Editor\MMD4MecanimEditor.dll

using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class MMD4MecanimMaterialInspector : MaterialEditor
{
    public override void OnInspectorGUI()
    {
        if (!MMD4MecanimEditorCommon.ValidateUnityVersion())
            return;
        base.OnInspectorGUI();
        if (!this.isVisible)
            return;
        UnityEngine.Material target = this.target as UnityEngine.Material;
        string[] shaderKeywords = target.shaderKeywords;
        EditorGUILayout.Separator();
        EditorGUILayout.LabelField("Shader Keywords", EditorStyles.boldLabel);
        bool flag1 = ((IEnumerable<string>) shaderKeywords).Contains<string>("SPECULAR_ON");
        EditorGUI.BeginChangeCheck();
        bool flag2 = EditorGUILayout.Toggle("Specular", flag1);
        if (!EditorGUI.EndChangeCheck())
            return;
        if (flag2)
        {
            target.EnableKeyword("SPECULAR_ON");
            target.DisableKeyword("SPECULAR_OFF");
        }
        else
        {
            target.DisableKeyword("SPECULAR_ON");
            target.EnableKeyword("SPECULAR_OFF");
        }
        EditorUtility.SetDirty((Object) target);
    }
}