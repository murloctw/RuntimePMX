﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimAssetPostprocessor
// Assembly: MMD4MecanimEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DD6A7AB2-B1E4-4A37-85A9-48A7C8E1BAF8
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Editor\MMD4MecanimEditor.dll

using UnityEditor;

public class MMD4MecanimAssetPostprocessor : AssetPostprocessor
{
  private void OnPreprocessModel()
  {
    if (!MMD4MecanimEditorCommon.ValidateUnityVersion() || !MMD4MecanimEditorCommon.IsExtensionFBX(assetPath))
      return;
    MMD4MecanimImporterImplEditor._OnImportingFBXAsset(assetPath);
  }

  private static void OnPostprocessAllAssets(
    string[] importedAssets,
    string[] deletedAssets,
    string[] movedAssets,
    string[] movedFromPaths)
  {
    if (!MMD4MecanimEditorCommon.ValidateUnityVersion())
      return;
    if (importedAssets != null)
    {
      foreach (string importedAsset in importedAssets)
      {
        if (MMD4MecanimEditorCommon.IsExtensionFBX(importedAsset))
          MMD4MecanimImporterImplEditor._OnRegistImportedFBXAsset(importedAsset);
        else if (MMD4MecanimEditorCommon.IsExtensionPMDorPMX(importedAsset))
          MMD4MecanimImporterImplEditor._OnRegistImportedMMDAsset(importedAsset);
      }
    }
    if (deletedAssets != null)
    {
      foreach (string deletedAsset in deletedAssets)
      {
        if (MMD4MecanimEditorCommon.IsExtensionFBX(deletedAsset))
          MMD4MecanimImporterImplEditor._OnDeletedFBXAsset(deletedAsset);
        else if (MMD4MecanimEditorCommon.IsExtensionPMDorPMX(deletedAsset))
          MMD4MecanimImporterImplEditor._OnDeletedMMDAsset(deletedAsset);
      }
    }
    if (movedAssets == null)
      return;
    for (int index = 0; index < movedAssets.Length; ++index)
    {
      string movedAsset = movedAssets[index];
      string movedFromPath = movedFromPaths[index];
      if (MMD4MecanimEditorCommon.IsExtensionFBX(movedAsset))
        MMD4MecanimImporterImplEditor._OnMovedFBXAsset(movedAsset, movedFromPath);
      else if (MMD4MecanimEditorCommon.IsExtensionPMDorPMX(movedAsset))
        MMD4MecanimImporterImplEditor._OnMovedMMDAsset(movedAsset, movedFromPath);
    }
  }
}
