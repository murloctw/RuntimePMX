﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimEditorCommon
// Assembly: MMD4MecanimEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DD6A7AB2-B1E4-4A37-85A9-48A7C8E1BAF8
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Editor\MMD4MecanimEditor.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using UnityEditor;
using UnityEngine;

public class MMD4MecanimEditorCommon
{
    public const int BulletPhysicsExecutionOrder = 20001;
    private static string _unityVersion;
    private static PropertyInfo _textureImporter_textureShape;
    private static MethodInfo _material_SetOverrideTag;
    private static MMD4MecanimEditorCommon._BulletPhysicsPluginState _bulletPhysicsPluginState = (MMD4MecanimEditorCommon._BulletPhysicsPluginState)null;
    private static bool _isCheckedAcceptanceAtLeastOnce;
    private static bool _isLicenseAccpeted;
    private static readonly string _androidDeviceLoggedLockTempFile = "Temp/MMD4MecanimAndroidDeviceLoggedLockfile";
    private static PropertyInfo _modelImporter_fileScale;
    private static PropertyInfo _modelImporter_useFileScale;

    public static bool IsUnity2017_2_or_Later()
    {
        MMD4MecanimEditorCommon.ValidateUnityVersion();
        return !MMD4MecanimEditorCommon._unityVersion.StartsWith("4.") && !MMD4MecanimEditorCommon._unityVersion.StartsWith("5.") && !MMD4MecanimEditorCommon._unityVersion.StartsWith("2017.1.");
    }

    public static bool IsNewTextureImporter()
    {
        MMD4MecanimEditorCommon.ValidateUnityVersion();
        return !MMD4MecanimEditorCommon._unityVersion.StartsWith("5.0.") && !MMD4MecanimEditorCommon._unityVersion.StartsWith("5.1.") && !MMD4MecanimEditorCommon._unityVersion.StartsWith("5.2.") && !MMD4MecanimEditorCommon._unityVersion.StartsWith("5.3.") && !MMD4MecanimEditorCommon._unityVersion.StartsWith("5.4.");
    }

    public static bool IsMultiSceneEditing()
    {
        MMD4MecanimEditorCommon.ValidateUnityVersion();
        return !MMD4MecanimEditorCommon._unityVersion.StartsWith("5.0.") && !MMD4MecanimEditorCommon._unityVersion.StartsWith("5.1.") && !MMD4MecanimEditorCommon._unityVersion.StartsWith("5.2.");
    }

    public static System.Type GetTypeInCurrentDomain(string typeName)
    {
        System.Type type1 = System.Type.GetType(typeName);
        if (type1 != null)
            return type1;
        foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
        {
            System.Type type2 = assembly.GetType(typeName);
            if (type2 != null)
                return type2;
        }
        return (System.Type)null;
    }

    public static void SetDirty(Component target)
    {
        if (MMD4MecanimEditorCommon.IsMultiSceneEditing())
        {
            try
            {
                MMD4MecanimEditorCommon.GetTypeInCurrentDomain("UnityEditor.SceneManagement.EditorSceneManager")?.GetMethod("MarkAllScenesDirty", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)?.Invoke((object)null, (object[])null);
            }
            catch (Exception ex)
            {
            }
        }
        EditorUtility.SetDirty((UnityEngine.Object)target);
        EditorUtility.SetDirty((UnityEngine.Object)target.gameObject);
    }

    public static void RecordObject(UnityEngine.Object obj, string name)
    {
        if (!MMD4MecanimEditorCommon.IsMultiSceneEditing())
            return;
        Undo.RecordObject(obj, name);
    }

    private static PropertyInfo _GetPropertyInfo_textureImporter_textureShape()
    {
        if (MMD4MecanimEditorCommon._textureImporter_textureShape == null)
            MMD4MecanimEditorCommon._textureImporter_textureShape = typeof(TextureImporter).GetProperty("textureShape");
        return MMD4MecanimEditorCommon._textureImporter_textureShape;
    }

    public static MMD4MecanimEditorCommon._TextureImporterShape GetTextureShape(
      TextureImporter textureImporter)
    {
        if (MMD4MecanimEditorCommon.IsNewTextureImporter() && (UnityEngine.Object)textureImporter != (UnityEngine.Object)null)
        {
            PropertyInfo importerTextureShape = MMD4MecanimEditorCommon._GetPropertyInfo_textureImporter_textureShape();
            if (importerTextureShape != null)
            {
                try
                {
                    return (MMD4MecanimEditorCommon._TextureImporterShape)Convert.ChangeType(importerTextureShape.GetValue((object)textureImporter, (object[])null), typeof(int));
                }
                catch (Exception ex)
                {
                    UnityEngine.Debug.LogError((object)ex.ToString());
                }
            }
        }
        return MMD4MecanimEditorCommon._TextureImporterShape.Texture2D;
    }

    public static void SetTextureShape(
      TextureImporter textureImporter,
      MMD4MecanimEditorCommon._TextureImporterShape textureShape)
    {
        if (!MMD4MecanimEditorCommon.IsNewTextureImporter() || !((UnityEngine.Object)textureImporter != (UnityEngine.Object)null))
            return;
        PropertyInfo importerTextureShape = MMD4MecanimEditorCommon._GetPropertyInfo_textureImporter_textureShape();
        if (importerTextureShape == null)
            return;
        try
        {
            object obj = Enum.Parse(importerTextureShape.PropertyType, textureShape.ToString());
            importerTextureShape.SetValue((object)textureImporter, obj, (object[])null);
        }
        catch (Exception ex)
        {
            UnityEngine.Debug.LogError((object)ex.ToString());
        }
    }

    public static void SetGenerateCubemapToNone(TextureImporter textureImporter)
    {
        if (!((UnityEngine.Object)textureImporter != (UnityEngine.Object)null) || MMD4MecanimEditorCommon.IsNewTextureImporter())
            return;
        textureImporter.generateCubemap = TextureImporterGenerateCubemap.None;
    }

    public static void SetTextureTypeToDefault(TextureImporter textureImporter)
    {
        if (!((UnityEngine.Object)textureImporter != (UnityEngine.Object)null))
            return;
        textureImporter.textureType = TextureImporterType.Image;
    }

    public static bool IsTextureTypeDefault(TextureImporter textureImporter) => (UnityEngine.Object)textureImporter != (UnityEngine.Object)null && textureImporter.textureType == TextureImporterType.Image;

    public static bool _Legacy_IsTextureTypeAdvancedOrCubemap(TextureImporter textureImporter)
    {
        if (MMD4MecanimEditorCommon.IsNewTextureImporter())
            return false;
        try
        {
            if ((UnityEngine.Object)textureImporter != (UnityEngine.Object)null)
            {
                int textureType = (int)textureImporter.textureType;
                return textureType == 3 || textureType == 5;
            }
        }
        catch (Exception ex)
        {
        }
        return false;
    }

    public static void _Legacy_SetTextureTypeToCubemap(TextureImporter textureImporter)
    {
        if (MMD4MecanimEditorCommon.IsNewTextureImporter())
            return;
        try
        {
            if (!((UnityEngine.Object)textureImporter != (UnityEngine.Object)null))
                return;
            textureImporter.textureType = TextureImporterType.Reflection;
        }
        catch (Exception ex)
        {
        }
    }

    public static void SetGenerateCubemap(TextureImporter textureImporter, bool generateCubemap)
    {
        if (MMD4MecanimEditorCommon.IsNewTextureImporter())
            MMD4MecanimEditorCommon.SetTextureShape(textureImporter, MMD4MecanimEditorCommon._TextureImporterShape.TextureCube);
        if (!((UnityEngine.Object)textureImporter != (UnityEngine.Object)null))
            return;
        if (generateCubemap)
            textureImporter.generateCubemap = TextureImporterGenerateCubemap.Spheremap;
        else
            textureImporter.generateCubemap = TextureImporterGenerateCubemap.Spheremap;
    }

    public static bool ValidateUnityVersion()
    {
        if (string.IsNullOrEmpty(MMD4MecanimEditorCommon._unityVersion))
            MMD4MecanimEditorCommon._unityVersion = Application.unityVersion;
        return !MMD4MecanimEditorCommon._unityVersion.StartsWith("4.");
    }

    public static void RestoreLibraries()
    {
        if (MMD4MecanimEditorCommon.ValidateUnityVersion())
            return;
        UnityEngine.Debug.Log((object)"This DLL is for Unity 5 or later. Back to Unity 4.");
        string path2 = "Unity4";
        string path1 = Path.Combine(MMD4MecanimImporterImpl.GetEditorPath(), path2);
        string str1 = Path.Combine(path1, "MMD4MecanimEditor.dll_");
        string str2 = Path.Combine(path1, "MMD4Mecanim.dll_");
        string str3 = Path.Combine(MMD4MecanimImporterImpl.GetEditorPath(), "MMD4MecanimEditor.dll");
        string str4 = Path.Combine(MMD4MecanimImporterImpl.GetScriptsPath(), "MMD4Mecanim.dll");
        bool flag1 = File.Exists(str1);
        bool flag2 = File.Exists(str2);
        if (flag1)
        {
            UnityEngine.Debug.Log((object)("Copy from " + str1 + " to " + str3));
            try
            {
                File.Copy(str1, str3, true);
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.LogError((object)ex.ToString());
            }
        }
        if (flag2)
        {
            UnityEngine.Debug.Log((object)("Copy from " + str2 + " to " + str4));
            try
            {
                File.Copy(str2, str4, true);
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.LogError((object)ex.ToString());
            }
        }
        if (flag2)
            AssetDatabase.ImportAsset(str4, ImportAssetOptions.ForceUpdate);
        if (!flag1)
            return;
        AssetDatabase.ImportAsset(str3, ImportAssetOptions.ForceUpdate);
    }

    public static string GetTag(
      UnityEngine.Material material,
      string tag,
      bool searchFallbacks,
      string defaultValue)
    {
        string unityVersion = Application.unityVersion;
        return unityVersion.StartsWith("4.") || unityVersion.StartsWith("5.0.") || unityVersion.StartsWith("5.1.") ? defaultValue : MMD4MecanimEditorCommon._GetTag(material, tag, searchFallbacks, defaultValue);
    }

    private static string _GetTag(
      UnityEngine.Material material,
      string tag,
      bool searchFallbacks,
      string defaultValue)
    {
        return material.GetTag(tag, searchFallbacks, defaultValue);
    }

    public static void SetOverrideTag(UnityEngine.Material material, string tag, string val)
    {
        string unityVersion = Application.unityVersion;
        if (unityVersion.StartsWith("4.") || unityVersion.StartsWith("5.0.") || unityVersion.StartsWith("5.1."))
            return;
        MMD4MecanimEditorCommon._SetOverrideTag(material, tag, val);
    }

    private static MethodInfo _GetMaterialSetOverrideTag()
    {
        if (MMD4MecanimEditorCommon._material_SetOverrideTag == null)
        {
            try
            {
                MMD4MecanimEditorCommon._material_SetOverrideTag = typeof(UnityEngine.Material).GetMethod("SetOverrideTag");
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.LogError((object)ex.ToString());
                return (MethodInfo)null;
            }
        }
        if (MMD4MecanimEditorCommon._material_SetOverrideTag == null)
            UnityEngine.Debug.LogError((object)"SetOverrideTag is not found.");
        return MMD4MecanimEditorCommon._material_SetOverrideTag;
    }

    private static void _SetOverrideTag(UnityEngine.Material material, string name, string val) => material.SetOverrideTag(name, val);

    private static bool _IsContainDirName(string assetPath, string dirName) => assetPath != null && dirName != null && assetPath.Contains(dirName) && (assetPath.Contains("/" + dirName + "/") || assetPath.Contains("\\" + dirName + "\\"));

    public static void SetBulletPhysicsPluginType(
      MMD4MecanimEditorCommon.BulletPhysicsPluginType bulletPhysicsPluginType)
    {
        if (Application.isPlaying)
        {
            UnityEngine.Debug.LogError((object)"Can't process in playing.");
        }
        else
        {
            string[] allAssetPaths = AssetDatabase.GetAllAssetPaths();
            if (allAssetPaths == null)
                return;
            bool useNative = bulletPhysicsPluginType != MMD4MecanimEditorCommon.BulletPhysicsPluginType.Managed;
            bool flag1 = bulletPhysicsPluginType == MMD4MecanimEditorCommon.BulletPhysicsPluginType.Native_275;
            foreach (string str in allAssetPaths)
            {
                string fileName1 = Path.GetFileName(str);
                if (MMD4MecanimEditorCommon._PluginImporterSetter.IsTargetFileName(fileName1))
                {
                    MMD4MecanimEditorCommon._PluginImporterSetter pluginImporterSetter = new MMD4MecanimEditorCommon._PluginImporterSetter(str);
                    bool flag2 = str.Contains("MMD4MecanimBulletPhysics 2.75");
                    string fileName2 = Path.GetFileName(Path.GetDirectoryName(str));
                    if (fileName1 == "MMD4Mecanim.dll")
                    {
                        if (fileName2 == "Internal")
                            pluginImporterSetter.SetPlugin_RuntimeInternal(useNative);
                        else if (fileName2 == "Managed")
                            pluginImporterSetter.SetPlugin_RuntimeManaged(useNative);
                        else
                            pluginImporterSetter.SetPlugin_Runtime(useNative);
                    }
                    else if (fileName1 == "BulletXNA-Unity.dll")
                        pluginImporterSetter.SetPlugin_RuntimeManaged(useNative);
                    else if (fileName1.EndsWith(".a"))
                    {
                        if (useNative && flag1 == flag2)
                            pluginImporterSetter.SetPlugin_iOS();
                        else
                            pluginImporterSetter.ResetCompatibleWithPlatform();
                    }
                    else if (fileName1.EndsWith(".so"))
                    {
                        if (useNative && flag1 == flag2)
                        {
                            if (fileName2 == "armeabi-v7a")
                                pluginImporterSetter.SetPlugin_Android_ARMv7();
                            else if (fileName2 == "x86")
                                pluginImporterSetter.SetPlugin_Android_x86();
                        }
                        else
                            pluginImporterSetter.ResetCompatibleWithPlatform();
                    }
                    else if (fileName1.EndsWith(".bundle"))
                    {
                        if (useNative && flag1 == flag2)
                            pluginImporterSetter.SetPlugin_OSX();
                        else
                            pluginImporterSetter.ResetCompatibleWithPlatform();
                    }
                    else if (fileName1.EndsWith(".dll"))
                    {
                        if (MMD4MecanimEditorCommon._IsContainDirName(str, "WSA"))
                        {
                            if (useNative && flag1 == flag2)
                            {
                                if (fileName2 == "X64")
                                    pluginImporterSetter.SetPlugin_WSA_X64();
                                else if (fileName2 == "X86")
                                    pluginImporterSetter.SetPlugin_WSA_X86();
                                else if (fileName2 == "ARM")
                                    pluginImporterSetter.SetPlugin_WSA_ARM();
                            }
                            else
                                pluginImporterSetter.ResetCompatibleWithPlatform();
                        }
                        else if (useNative && flag1 == flag2)
                        {
                            if (fileName2 == "x86_64")
                                pluginImporterSetter.SetPlugin_x86_64();
                            else if (fileName2 == "x86")
                                pluginImporterSetter.SetPlugin_x86();
                        }
                        else
                            pluginImporterSetter.ResetCompatibleWithPlatform();
                    }
                    pluginImporterSetter.WriteImportSettingsIfDirty();
                }
            }
            AssetDatabase.Refresh();
        }
    }

    public static MMD4MecanimEditorCommon._BulletPhysicsPluginState GetBulletPhysicsPluginState()
    {
        if (MMD4MecanimEditorCommon._bulletPhysicsPluginState == null)
            MMD4MecanimEditorCommon._bulletPhysicsPluginState = new MMD4MecanimEditorCommon._BulletPhysicsPluginState();
        return MMD4MecanimEditorCommon._bulletPhysicsPluginState;
    }

    public static void EnumPopupBulletPhysicsPluginType(
      ref MMD4MecanimEditorCommon.BulletPhysicsPluginType bulletPhysicsPluginType)
    {
        MMD4MecanimEditorCommon._BulletPhysicsPluginState physicsPluginState = MMD4MecanimEditorCommon.GetBulletPhysicsPluginState();
        if (physicsPluginState.enumStrings == null || physicsPluginState.enumStrings.Length == 0)
            return;
        if (Application.isPlaying)
            bulletPhysicsPluginType = physicsPluginState.bulletPhysicsPluginType;
        int enumIndex = EditorGUILayout.Popup(" Plugin Type", physicsPluginState.ToEnumIndex(bulletPhysicsPluginType), physicsPluginState.enumStrings);
        if (Application.isPlaying)
            return;
        bulletPhysicsPluginType = physicsPluginState.ToEnumValue(enumIndex);
    }

    public static string GetLoadedBulletPhysicsModule()
    {
        if (Process.GetCurrentProcess() == null)
            return (string)null;
        ProcessModuleCollection modules = Process.GetCurrentProcess().Modules;
        if (modules == null)
            return (string)null;
        foreach (ProcessModule processModule in (ReadOnlyCollectionBase)modules)
        {
            if (processModule.FileName.Contains("MMD4Mecanim"))
            {
                if (processModule.FileName.Contains("MMD4MecanimBulletPhysics 2.75"))
                    return "2.75";
                if (processModule.FileName.EndsWith("MMD4Mecanim.Managed.dll"))
                    return "Managed";
                if (processModule.FileName.Contains("Plugins"))
                    return "Latest (2.83)";
            }
        }
        return (string)null;
    }

    public static bool IsLicenseAccepted()
    {
        if (MMD4MecanimEditorCommon._isCheckedAcceptanceAtLeastOnce)
            return MMD4MecanimEditorCommon._isLicenseAccpeted;
        MMD4MecanimEditorCommon._isCheckedAcceptanceAtLeastOnce = true;
        string path = "";
        string str = "athkctag";
        for (int index = 0; index < 5; ++index)
            path += ((char)((uint)str[index] + 1U)).ToString();
        for (int index = 5; index < 8; ++index)
            path += str[index].ToString();
        UnityEngine.Object assetToUnload = Resources.Load(path, typeof(TextAsset));
        if (assetToUnload != (UnityEngine.Object)null)
        {
            MMD4MecanimEditorCommon._isLicenseAccpeted = true;
            Resources.UnloadAsset(assetToUnload);
        }
        return MMD4MecanimEditorCommon._isLicenseAccpeted;
    }

    public static void CheckAndroidDevices(bool isLogging = false)
    {
        if (EditorUserBuildSettings.activeBuildTarget != BuildTarget.Android)
            return;
        MMD4MecanimEditorCommon.AndroidSupport.logEnabled = isLogging;
        if (!File.Exists(MMD4MecanimEditorCommon._androidDeviceLoggedLockTempFile))
        {
            MMD4MecanimEditorCommon.AndroidSupport.logEnabled = true;
            try
            {
                using (File.Create(MMD4MecanimEditorCommon._androidDeviceLoggedLockTempFile))
                    ;
            }
            catch (Exception ex)
            {
            }
        }
        MMD4MecanimEditorCommon.AndroidSupport.GenerateAndroidDevicesFile();
    }

    private static bool _IsExtension(string name, string lowerExt, string upperExt)
    {
        if (name != null)
        {
            int length1 = name.Length;
            int length2 = lowerExt.Length;
            if (length1 >= length2)
            {
                for (int index = 0; index < length2; ++index)
                {
                    if ((int)name[length1 - length2 + index] != (int)lowerExt[index] && (int)name[length1 - length2 + index] != (int)upperExt[index])
                        return false;
                }
                return true;
            }
        }
        return false;
    }

    public static bool IsExtensionAnimBytes(string name) => MMD4MecanimEditorCommon._IsExtension(name, ".anim.bytes", ".ANIM.BYTES");

    public static bool IsExtensionDDS(string name) => MMD4MecanimEditorCommon._IsExtension(name, ".dds", ".DDS");

    public static bool IsExtensionVMD(string name) => MMD4MecanimEditorCommon._IsExtension(name, ".vmd", ".VMD");

    public static bool IsExtensionFBX(string name) => MMD4MecanimEditorCommon._IsExtension(name, ".fbx", ".FBX");

    public static bool IsExtensionXML(string name) => MMD4MecanimEditorCommon._IsExtension(name, ".xml", ".XML");

    public static bool IsExtensionPMDorPMX(string name)
    {
        if (name != null)
        {
            int length = name.Length;
            if (length >= 4 && name[length - 4] == '.' && (name[length - 3] == 'p' || name[length - 3] == 'P') && (name[length - 2] == 'm' || name[length - 2] == 'M'))
                return name[length - 1] == 'd' || name[length - 1] == 'D' || name[length - 1] == 'x' || name[length - 1] == 'X';
        }
        return false;
    }

    public static string GetPathWithoutExtension(string assetPath, string ext) => ext != null && ext.Length > 0 ? MMD4MecanimEditorCommon.GetPathWithoutExtension(assetPath, ext.Length) : assetPath;

    public static string GetPathWithoutExtension(string assetPath) => GetPathWithoutExtension(assetPath, 4);

    public static string GetPathWithoutExtension(string fbxAssetPath, int periodPos)
    {
        if (string.IsNullOrEmpty(fbxAssetPath))
            return null;

        int length = fbxAssetPath.Length;

        if (length >= periodPos && fbxAssetPath[length - periodPos] == '.')
            return fbxAssetPath.Substring(0, length - periodPos);
        else
            return Path.Combine(Path.GetDirectoryName(fbxAssetPath), Path.GetFileNameWithoutExtension(fbxAssetPath));
    }

    public static DateTime GetLastWriteTime(string path)
    {
        try
        {
            return File.GetLastWriteTime(path);
        }
        catch (Exception ex)
        {
            return new DateTime();
        }
    }

    public static float GetModelImportScale(ModelImporter modelImporter)
    {
        if ((UnityEngine.Object)modelImporter == (UnityEngine.Object)null)
            return 0.01f;
        if (Application.unityVersion.StartsWith("4."))
            return modelImporter.globalScale;
        try
        {
            if (MMD4MecanimEditorCommon._modelImporter_fileScale == null)
                MMD4MecanimEditorCommon._modelImporter_fileScale = typeof(ModelImporter).GetProperty("fileScale");
            float num = 0.01f;
            if (MMD4MecanimEditorCommon._modelImporter_fileScale != null)
                num = (float)MMD4MecanimEditorCommon._modelImporter_fileScale.GetValue((object)modelImporter, (object[])null);
            if (MMD4MecanimEditorCommon._modelImporter_useFileScale == null)
                MMD4MecanimEditorCommon._modelImporter_useFileScale = typeof(ModelImporter).GetProperty("useFileScale");
            bool flag = true;
            if (MMD4MecanimEditorCommon._modelImporter_useFileScale != null)
                flag = (bool)MMD4MecanimEditorCommon._modelImporter_useFileScale.GetValue((object)modelImporter, (object[])null);
            return flag ? modelImporter.globalScale * num : modelImporter.globalScale;
        }
        catch (Exception ex)
        {
            return modelImporter.globalScale;
        }
    }

    public static void UpdateImportSettings(string path)
    {
        AssetDatabase.WriteImportSettingsIfDirty(path);
        AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate | ImportAssetOptions.ForceSynchronousImport);
    }

    public static System.Type GetAssemblyCSharpType(string typeName) => Assembly.Load("Assembly-CSharp")?.GetType(typeName);

    public static void ValidateScriptExecutionOrder()
    {
        if (EditorApplication.isPlaying)
            return;
        foreach (MonoScript runtimeMonoScript in MonoImporter.GetAllRuntimeMonoScripts())
        {
            if (runtimeMonoScript.name == "MMD4MecanimBulletPhysics" && MonoImporter.GetExecutionOrder(runtimeMonoScript) == 0)
                MonoImporter.SetExecutionOrder(runtimeMonoScript, 20001);
        }
    }

    private static uint _Swap(uint v) => (uint)((int)(v >> 24) | (int)(v >> 8) & 65280 | (int)v << 8 & 16711680 | (int)v << 24);

    private static uint _MakeFourCC(char a, char b, char c, char d) => (uint)((int)(byte)a | (int)(byte)b << 8 | (int)(byte)c << 16 | (int)(byte)d << 24);

    public static MMD4MecanimEditorCommon.TextureFileSign GetTextureFileSign(
      string path)
    {
        try
        {
            using (FileStream input = new FileStream(path, System.IO.FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                byte[] buffer = new byte[(int)input.Length];
                input.Read(buffer, 0, (int)input.Length);
                if ((int)input.Length >= 8)
                {
                    bool flag = true;
                    byte[] numArray = new byte[8]
                    {
            (byte) 137,
            (byte) 80,
            (byte) 78,
            (byte) 71,
            (byte) 13,
            (byte) 10,
            (byte) 26,
            (byte) 10
                    };
                    for (int index = 0; index < 8; ++index)
                    {
                        if ((int)buffer[index] != (int)numArray[index])
                        {
                            flag = false;
                            break;
                        }
                    }
                    if (flag)
                    {
                        input.Seek(0L, SeekOrigin.Begin);
                        using (System.IO.BinaryReader binaryReader = new System.IO.BinaryReader((Stream)input))
                        {
                            binaryReader.ReadInt32();
                            binaryReader.ReadInt32();
                            while (true)
                            {
                                try
                                {
                                    uint num1 = MMD4MecanimEditorCommon._Swap(binaryReader.ReadUInt32());
                                    uint num2 = binaryReader.ReadUInt32();
                                    if ((int)num2 == (int)MMD4MecanimEditorCommon._MakeFourCC('I', 'H', 'D', 'R'))
                                    {
                                        if (num1 < 13U)
                                            return MMD4MecanimEditorCommon.TextureFileSign.Png;
                                        int num3 = (int)binaryReader.ReadUInt32();
                                        int num4 = (int)binaryReader.ReadUInt32();
                                        int num5 = (int)binaryReader.ReadByte();
                                        switch (binaryReader.ReadByte())
                                        {
                                            case 4:
                                            case 6:
                                                return MMD4MecanimEditorCommon.TextureFileSign.PngWithAlpha;
                                            default:
                                                num1 -= 10U;
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        if ((int)num2 == (int)MMD4MecanimEditorCommon._MakeFourCC('t', 'R', 'N', 'S'))
                                            return MMD4MecanimEditorCommon.TextureFileSign.PngWithAlpha;
                                        if ((int)num2 == (int)MMD4MecanimEditorCommon._MakeFourCC('I', 'E', 'N', 'D') || (int)num2 == (int)MMD4MecanimEditorCommon._MakeFourCC('I', 'D', 'A', 'T'))
                                            return MMD4MecanimEditorCommon.TextureFileSign.Png;
                                    }
                                    for (uint index = 0; index < num1 / 4U; ++index)
                                    {
                                        int num6 = (int)binaryReader.ReadUInt32();
                                    }
                                    for (uint index = 0; index < num1 % 4U; ++index)
                                    {
                                        int num7 = (int)binaryReader.ReadByte();
                                    }
                                    int num8 = (int)binaryReader.ReadUInt32();
                                }
                                catch (Exception ex)
                                {
                                    return MMD4MecanimEditorCommon.TextureFileSign.PngWithAlpha;
                                }
                            }
                        }
                    }
                }
                if ((int)input.Length >= 18 && buffer[0] == (byte)66 && buffer[1] == (byte)77)
                {
                    uint num = (uint)((int)buffer[14] | (int)buffer[15] << 8 | (int)buffer[16] << 16 | (int)buffer[17] << 24);
                    switch (num)
                    {
                        case 12:
                            return MMD4MecanimEditorCommon.TextureFileSign.Bmp;
                        case 40:
                        case 52:
                        case 56:
                        case 60:
                        case 96:
                        case 108:
                        case 112:
                        case 120:
                        case 124:
                            if (num >= 56U && ((int)buffer[66] | (int)buffer[67] << 8 | (int)buffer[68] << 16 | (int)buffer[69] << 9) != 0)
                                return MMD4MecanimEditorCommon.TextureFileSign.BmpWithAlpha;
                            switch ((uint)((int)buffer[30] | (int)buffer[31] << 8 | (int)buffer[32] << 16 | (int)buffer[33] << 9))
                            {
                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                    return MMD4MecanimEditorCommon.TextureFileSign.Bmp;
                                case 5:
                                    return MMD4MecanimEditorCommon.TextureFileSign.BmpWithAlpha;
                            }
                            break;
                    }
                }
                if ((int)input.Length >= 2 && buffer[0] == byte.MaxValue && buffer[1] == (byte)216)
                    return MMD4MecanimEditorCommon.TextureFileSign.Jpeg;
                if ((int)input.Length >= 18)
                {
                    byte num9 = buffer[1];
                    byte num10 = buffer[17];
                    byte num11 = (byte)(((int)num10 & 48) >> 4);
                    if (num9 != (byte)0)
                    {
                        if (num9 != (byte)1)
                            goto label_61;
                    }
                    if (num11 != (byte)0)
                    {
                        if (num11 != (byte)2)
                            goto label_61;
                    }
                    byte num12 = buffer[2];
                    byte num13 = buffer[16];
                    byte num14 = (byte)((uint)num10 & 15U);
                    if ((num12 == (byte)1 || num12 == (byte)9) && (num13 == (byte)1 || num13 == (byte)2 || num13 == (byte)4 || num13 == (byte)8) || (num12 == (byte)3 || num12 == (byte)11) && (num13 == (byte)1 || num13 == (byte)2 || num13 == (byte)4 || num13 == (byte)8))
                        return num14 > (byte)0 ? MMD4MecanimEditorCommon.TextureFileSign.TargaWithAlpha : MMD4MecanimEditorCommon.TextureFileSign.Targa;
                    if (num12 != (byte)2)
                    {
                        if (num12 != (byte)10)
                            goto label_61;
                    }
                    if (num13 != (byte)16 && num13 != (byte)24)
                    {
                        if (num13 != (byte)32)
                            goto label_61;
                    }
                    if (num13 == (byte)24)
                        return MMD4MecanimEditorCommon.TextureFileSign.Targa;
                    return num13 == (byte)32 ? MMD4MecanimEditorCommon.TextureFileSign.TargaWithAlpha : (num14 > (byte)0 ? MMD4MecanimEditorCommon.TextureFileSign.TargaWithAlpha : MMD4MecanimEditorCommon.TextureFileSign.Targa);
                }
            }
        label_61:
            return MMD4MecanimEditorCommon.TextureFileSign.None;
        }
        catch (Exception ex)
        {
            return MMD4MecanimEditorCommon.TextureFileSign.None;
        }
    }

    public enum _TextureImporterShape
    {
        Texture2D = 1,
        TextureCube = 2,
    }

    public enum _Legacy_TextureImporterType
    {
        Cubemap = 3,
        Advanced = 5,
    }

    public enum BulletPhysicsPluginType
    {
        Native_Latest,
        Native_275,
        Managed,
    }

    private class _PluginImporterSetter
    {
        private static HashSet<BuildTarget> _nativePlatforms = new HashSet<BuildTarget>();
        private static HashSet<string> _targetFileNames;
        private string _assetPath;
        private PluginImporter _pluginImporter;
        private bool _isWritten;

        static _PluginImporterSetter()
        {
            MMD4MecanimEditorCommon._PluginImporterSetter._nativePlatforms.Add(BuildTarget.StandaloneWindows);
            MMD4MecanimEditorCommon._PluginImporterSetter._nativePlatforms.Add(BuildTarget.StandaloneWindows64);
            MMD4MecanimEditorCommon._PluginImporterSetter._nativePlatforms.Add(BuildTarget.StandaloneOSXIntel);
            MMD4MecanimEditorCommon._PluginImporterSetter._nativePlatforms.Add(BuildTarget.StandaloneOSXIntel64);
            MMD4MecanimEditorCommon._PluginImporterSetter._nativePlatforms.Add(BuildTarget.StandaloneOSX);
            MMD4MecanimEditorCommon._PluginImporterSetter._nativePlatforms.Add(BuildTarget.iOS);
            MMD4MecanimEditorCommon._PluginImporterSetter._nativePlatforms.Add(BuildTarget.Android);
            MMD4MecanimEditorCommon._PluginImporterSetter._nativePlatforms.Add(BuildTarget.WSAPlayer);
            MMD4MecanimEditorCommon._PluginImporterSetter._targetFileNames = new HashSet<string>();
            MMD4MecanimEditorCommon._PluginImporterSetter._targetFileNames.Add("MMD4MecanimBulletPhysics.dll");
            MMD4MecanimEditorCommon._PluginImporterSetter._targetFileNames.Add("MMD4MecanimBulletPhysics.bundle");
            MMD4MecanimEditorCommon._PluginImporterSetter._targetFileNames.Add("libBulletPhysics.a");
            MMD4MecanimEditorCommon._PluginImporterSetter._targetFileNames.Add("MMD4Mecanim.dll");
            MMD4MecanimEditorCommon._PluginImporterSetter._targetFileNames.Add("BulletXNA-Unity.dll");
            MMD4MecanimEditorCommon._PluginImporterSetter._targetFileNames.Add("libMMD4MecanimBulletPhysics.a");
            MMD4MecanimEditorCommon._PluginImporterSetter._targetFileNames.Add("libMMD4MecanimBulletPhysics.so");
        }

        private static bool IsNativePlatform(BuildTarget buildTarget) => MMD4MecanimEditorCommon._PluginImporterSetter._nativePlatforms.Contains(buildTarget);

        private static bool IsNativeInternalPlatform(BuildTarget buildTarget) => buildTarget == BuildTarget.iOS;

        public static bool IsTargetFileName(string fileName) => MMD4MecanimEditorCommon._PluginImporterSetter._targetFileNames.Contains(fileName);

        public _PluginImporterSetter(string assetPath)
        {
            this._assetPath = assetPath;
            this._pluginImporter = (PluginImporter)AssetImporter.GetAtPath(assetPath);
        }

        public void WriteImportSettingsIfDirty()
        {
            if (!this._isWritten)
                return;
            this._isWritten = false;
            AssetDatabase.WriteImportSettingsIfDirty(this._assetPath);
        }

        public void SetCompatibleWithAnyPlatform(bool enable)
        {
            if (!((UnityEngine.Object)this._pluginImporter != (UnityEngine.Object)null) || this._pluginImporter.GetCompatibleWithAnyPlatform() == enable)
                return;
            this._pluginImporter.SetCompatibleWithAnyPlatform(enable);
            this._isWritten = true;
        }

        public bool GetCompatibleWithAnyPlatform() => (UnityEngine.Object)this._pluginImporter != (UnityEngine.Object)null && this._pluginImporter.GetCompatibleWithAnyPlatform();

        public void SetCompatibleWithPlatform(string platformName, bool enable)
        {
            if (!((UnityEngine.Object)this._pluginImporter != (UnityEngine.Object)null) || this._pluginImporter.GetCompatibleWithPlatform(platformName) == enable)
                return;
            this._pluginImporter.SetCompatibleWithPlatform(platformName, enable);
            this._isWritten = true;
        }

        public void SetCompatibleWithPlatform(BuildTarget platform, bool enable)
        {
            if (!((UnityEngine.Object)this._pluginImporter != (UnityEngine.Object)null) || platform < ~BuildTarget.iOS || this._pluginImporter.GetCompatibleWithPlatform(platform) == enable)
                return;
            this._pluginImporter.SetCompatibleWithPlatform(platform, enable);
            this._isWritten = true;
        }

        public bool GetCompatibleWithPlatform(BuildTarget platform) => (UnityEngine.Object)this._pluginImporter != (UnityEngine.Object)null && platform >= ~BuildTarget.iOS && this._pluginImporter.GetCompatibleWithPlatform(platform);

        public bool GetCompatibleWithPlatform_AllPlatforms()
        {
            foreach (BuildTarget platform in Enum.GetValues(typeof(BuildTarget)))
            {
                if (MMD4MecanimEditorCommon._PluginImporterSetter._IsSupportPlatform(platform) && this.GetCompatibleWithPlatform(platform))
                    return true;
            }
            return false;
        }

        public void SetPlatformData(string platformName, string key, string value)
        {
            if (!((UnityEngine.Object)this._pluginImporter != (UnityEngine.Object)null) || !(this._pluginImporter.GetPlatformData(platformName, key) != value))
                return;
            this._pluginImporter.SetPlatformData(platformName, key, value);
            this._isWritten = true;
        }

        public void SetPlatformData(BuildTarget platform, string key, string value)
        {
            if (!((UnityEngine.Object)this._pluginImporter != (UnityEngine.Object)null) || platform < ~BuildTarget.iOS || !(this._pluginImporter.GetPlatformData(platform, key) != value))
                return;
            this._pluginImporter.SetPlatformData(platform, key, value);
            this._isWritten = true;
        }

        public void SetCompatibleWithPlatform_Editor(bool enable)
        {
            if (!((UnityEngine.Object)this._pluginImporter != (UnityEngine.Object)null) || this._pluginImporter.GetCompatibleWithEditor() == enable)
                return;
            this._pluginImporter.SetCompatibleWithEditor(enable);
            this._isWritten = true;
        }

        public void ResetCompatibleWithPlatform()
        {
            this.SetCompatibleWithAnyPlatform(false);
            this.SetCompatibleWithPlatform_Editor(false);
            foreach (BuildTarget platform in Enum.GetValues(typeof(BuildTarget)))
            {
                if (MMD4MecanimEditorCommon._PluginImporterSetter._IsSupportPlatform(platform))
                    this.SetCompatibleWithPlatform(platform, false);
            }
        }

        public void SetPlugin_OSX()
        {
            this.SetCompatibleWithAnyPlatform(false);
            this.SetCompatibleWithPlatform_Editor(true);
            this.SetPlatformData("Editor", "OS", "OSX");
            this.SetCompatibleWithPlatform(BuildTarget.StandaloneOSXIntel, true);
            this.SetPlatformData(BuildTarget.StandaloneOSXIntel, "CPU", "AnyCPU");
            this.SetCompatibleWithPlatform(BuildTarget.StandaloneOSXIntel64, true);
            this.SetPlatformData(BuildTarget.StandaloneOSXIntel64, "CPU", "AnyCPU");
        }

        public void SetPlugin_x86()
        {
            this.SetCompatibleWithAnyPlatform(false);
            this.SetCompatibleWithPlatform_Editor(true);
            this.SetPlatformData("Editor", "OS", "Windows");
            this.SetPlatformData("Editor", "CPU", "x86");
            this.SetCompatibleWithPlatform(BuildTarget.StandaloneWindows, true);
            this.SetPlatformData(BuildTarget.StandaloneWindows, "CPU", "x86");
        }

        public void SetPlugin_x86_64()
        {
            this.SetCompatibleWithAnyPlatform(false);
            this.SetCompatibleWithPlatform_Editor(true);
            this.SetPlatformData("Editor", "OS", "Windows");
            this.SetPlatformData("Editor", "CPU", "x86_64");
            this.SetCompatibleWithPlatform(BuildTarget.StandaloneWindows64, true);
            this.SetPlatformData(BuildTarget.StandaloneWindows64, "CPU", "x86_64");
        }

        public void SetPlugin_WSA_X86()
        {
            this.SetCompatibleWithAnyPlatform(false);
            this.SetCompatibleWithPlatform_Editor(false);
            this.SetCompatibleWithPlatform(BuildTarget.WSAPlayer, true);
            this.SetPlatformData(BuildTarget.WSAPlayer, "CPU", "X86");
        }

        public void SetPlugin_WSA_X64()
        {
            this.SetCompatibleWithAnyPlatform(false);
            this.SetCompatibleWithPlatform_Editor(false);
            this.SetCompatibleWithPlatform(BuildTarget.WSAPlayer, true);
            this.SetPlatformData(BuildTarget.WSAPlayer, "CPU", "X64");
        }

        public void SetPlugin_WSA_ARM()
        {
            this.SetCompatibleWithAnyPlatform(false);
            this.SetCompatibleWithPlatform_Editor(false);
            this.SetCompatibleWithPlatform(BuildTarget.WSAPlayer, true);
            this.SetPlatformData(BuildTarget.WSAPlayer, "CPU", "ARM");
        }

        public void SetPlugin_Android_x86()
        {
            this.SetCompatibleWithAnyPlatform(false);
            this.SetCompatibleWithPlatform_Editor(false);
            this.SetCompatibleWithPlatform(BuildTarget.Android, true);
            this.SetPlatformData(BuildTarget.Android, "CPU", "x86");
        }

        public void SetPlugin_Android_ARMv7()
        {
            this.SetCompatibleWithAnyPlatform(false);
            this.SetCompatibleWithPlatform_Editor(false);
            this.SetCompatibleWithPlatform(BuildTarget.Android, true);
            this.SetPlatformData(BuildTarget.Android, "CPU", "ARMv7");
        }

        public void SetPlugin_iOS()
        {
            this.SetCompatibleWithAnyPlatform(false);
            this.SetCompatibleWithPlatform_Editor(false);
            this.SetCompatibleWithPlatform(BuildTarget.iOS, true);
        }

        private static bool _IsSupportPlatform(BuildTarget platform) => (!MMD4MecanimEditorCommon.IsUnity2017_2_or_Later() || platform != BuildTarget.WebGL) && platform != BuildTarget.XBOX360;

        public void SetPlugin_Runtime(bool useNative)
        {
            this.SetCompatibleWithAnyPlatform(false);
            this.SetCompatibleWithPlatform_Editor(useNative);
            this.SetPlatformData("Editor", "OS", "AnyOS");
            this.SetPlatformData("Editor", "CPU", "AnyCPU");
            foreach (BuildTarget buildTarget in Enum.GetValues(typeof(BuildTarget)))
            {
                if (MMD4MecanimEditorCommon._PluginImporterSetter._IsSupportPlatform(buildTarget))
                {
                    if (useNative)
                        this.SetCompatibleWithPlatform(buildTarget, MMD4MecanimEditorCommon._PluginImporterSetter.IsNativePlatform(buildTarget) && !MMD4MecanimEditorCommon._PluginImporterSetter.IsNativeInternalPlatform(buildTarget));
                    else
                        this.SetCompatibleWithPlatform(buildTarget, false);
                    if (buildTarget != BuildTarget.Android)
                        this.SetPlatformData(buildTarget, "CPU", "AnyCPU");
                }
            }
        }

        public void SetPlugin_RuntimeInternal(bool useNative)
        {
            this.SetCompatibleWithAnyPlatform(false);
            this.SetCompatibleWithPlatform_Editor(false);
            this.SetPlatformData("Editor", "OS", "AnyOS");
            this.SetPlatformData("Editor", "CPU", "AnyCPU");
            foreach (BuildTarget buildTarget in Enum.GetValues(typeof(BuildTarget)))
            {
                if (MMD4MecanimEditorCommon._PluginImporterSetter._IsSupportPlatform(buildTarget))
                {
                    if (useNative)
                        this.SetCompatibleWithPlatform(buildTarget, MMD4MecanimEditorCommon._PluginImporterSetter.IsNativePlatform(buildTarget) && MMD4MecanimEditorCommon._PluginImporterSetter.IsNativeInternalPlatform(buildTarget));
                    else
                        this.SetCompatibleWithPlatform(buildTarget, false);
                    if (buildTarget != BuildTarget.Android)
                        this.SetPlatformData(buildTarget, "CPU", "AnyCPU");
                }
            }
        }

        public void SetPlugin_RuntimeManaged(bool useNative)
        {
            this.SetCompatibleWithAnyPlatform(!useNative);
            this.SetCompatibleWithPlatform_Editor(!useNative);
            this.SetPlatformData("Editor", "OS", "AnyOS");
            this.SetPlatformData("Editor", "CPU", "AnyCPU");
            foreach (BuildTarget buildTarget in Enum.GetValues(typeof(BuildTarget)))
            {
                if (MMD4MecanimEditorCommon._PluginImporterSetter._IsSupportPlatform(buildTarget))
                {
                    if (useNative)
                        this.SetCompatibleWithPlatform(buildTarget, !MMD4MecanimEditorCommon._PluginImporterSetter.IsNativePlatform(buildTarget));
                    else
                        this.SetCompatibleWithPlatform(buildTarget, true);
                    if (buildTarget != BuildTarget.Android)
                        this.SetPlatformData(buildTarget, "CPU", "AnyCPU");
                }
            }
        }
    }

    public class _BulletPhysicsPluginState
    {
        public bool hasLatest;
        public bool has275;
        public bool hasManaged;
        public MMD4MecanimEditorCommon.BulletPhysicsPluginType bulletPhysicsPluginType;
        public string[] enumStrings;
        public MMD4MecanimEditorCommon.BulletPhysicsPluginType[] enumValues;

        private static bool _IsContainDirName(string assetPath, string dirName)
        {
            if (!assetPath.Contains(dirName))
                return false;
            return assetPath.Contains("/" + dirName + "/") || assetPath.Contains("\\" + dirName + "\\");
        }

        public _BulletPhysicsPluginState()
        {
            this.bulletPhysicsPluginType = MMD4MecanimEditorCommon.BulletPhysicsPluginType.Native_Latest;
            string[] allAssetPaths = AssetDatabase.GetAllAssetPaths();
            if (allAssetPaths == null)
                return;
            bool flag1 = false;
            bool flag2 = false;
            bool flag3 = false;
            foreach (string str in allAssetPaths)
            {
                string fileName = Path.GetFileName(str);
                if (MMD4MecanimEditorCommon._PluginImporterSetter.IsTargetFileName(fileName))
                {
                    if (fileName == "MMD4Mecanim.dll")
                    {
                        if (Path.GetFileName(Path.GetDirectoryName(str)) == "Managed")
                        {
                            if (!flag3 && new MMD4MecanimEditorCommon._PluginImporterSetter(str).GetCompatibleWithAnyPlatform())
                                flag1 = true;
                            this.hasManaged = true;
                        }
                    }
                    else if (MMD4MecanimEditorCommon._BulletPhysicsPluginState._IsContainDirName(str, "MMD4MecanimBulletPhysics 2.75"))
                    {
                        if (!flag2 && new MMD4MecanimEditorCommon._PluginImporterSetter(str).GetCompatibleWithPlatform_AllPlatforms())
                            flag2 = true;
                        this.has275 = true;
                    }
                    else if (MMD4MecanimEditorCommon._BulletPhysicsPluginState._IsContainDirName(str, "MMD4MecanimBulletPhysics"))
                    {
                        if (!flag1 && new MMD4MecanimEditorCommon._PluginImporterSetter(str).GetCompatibleWithPlatform_AllPlatforms())
                            flag1 = true;
                        this.hasLatest = true;
                    }
                }
            }
            if (flag1)
                this.bulletPhysicsPluginType = MMD4MecanimEditorCommon.BulletPhysicsPluginType.Native_Latest;
            else if (flag2)
                this.bulletPhysicsPluginType = MMD4MecanimEditorCommon.BulletPhysicsPluginType.Native_275;
            else if (flag3)
                this.bulletPhysicsPluginType = MMD4MecanimEditorCommon.BulletPhysicsPluginType.Managed;
            List<string> stringList = new List<string>();
            List<MMD4MecanimEditorCommon.BulletPhysicsPluginType> physicsPluginTypeList = new List<MMD4MecanimEditorCommon.BulletPhysicsPluginType>();
            if (this.hasLatest)
            {
                stringList.Add("Latest (2.83)");
                physicsPluginTypeList.Add(MMD4MecanimEditorCommon.BulletPhysicsPluginType.Native_Latest);
            }
            if (this.has275)
            {
                stringList.Add("2.75");
                physicsPluginTypeList.Add(MMD4MecanimEditorCommon.BulletPhysicsPluginType.Native_275);
            }
            if (this.hasManaged)
            {
                stringList.Add("Managed");
                physicsPluginTypeList.Add(MMD4MecanimEditorCommon.BulletPhysicsPluginType.Managed);
            }
            this.enumStrings = stringList.ToArray();
            this.enumValues = physicsPluginTypeList.ToArray();
        }

        public int ToEnumIndex(
          MMD4MecanimEditorCommon.BulletPhysicsPluginType pluginType)
        {
            if (this.enumValues != null)
            {
                for (int enumIndex = 0; enumIndex < this.enumValues.Length; ++enumIndex)
                {
                    if (this.enumValues[enumIndex] == pluginType)
                        return enumIndex;
                }
            }
            return 0;
        }

        public MMD4MecanimEditorCommon.BulletPhysicsPluginType ToEnumValue(
          int enumIndex)
        {
            return this.enumValues != null && enumIndex >= 0 && enumIndex < this.enumValues.Length ? this.enumValues[enumIndex] : MMD4MecanimEditorCommon.BulletPhysicsPluginType.Native_Latest;
        }
    }

    public class GUIBulletPhysicsPlugin
    {
        private bool _bulletPhysicsPluginTypeInitialized;
        private MMD4MecanimEditorCommon.BulletPhysicsPluginType _bulletPhysicsPluginType;
        private bool _bulletPhysicsLoadedModuleInitialized;
        private string _bulletPhysicsLoadedModule;

        public GUIBulletPhysicsPlugin()
        {
            MMD4MecanimEditorCommon._BulletPhysicsPluginState physicsPluginState = MMD4MecanimEditorCommon.GetBulletPhysicsPluginState();
            if (!this._bulletPhysicsPluginTypeInitialized)
            {
                this._bulletPhysicsPluginTypeInitialized = true;
                this._bulletPhysicsPluginType = physicsPluginState.bulletPhysicsPluginType;
            }
            if (this._bulletPhysicsLoadedModuleInitialized)
                return;
            this._bulletPhysicsLoadedModuleInitialized = true;
            this._bulletPhysicsLoadedModule = MMD4MecanimEditorCommon.GetLoadedBulletPhysicsModule();
        }

        public void OnGUI()
        {
            MMD4MecanimEditorCommon._BulletPhysicsPluginState physicsPluginState = MMD4MecanimEditorCommon.GetBulletPhysicsPluginState();
            int num = GUI.enabled ? 1 : 0;
            GUI.enabled = num != 0 && !Application.isPlaying;
            MMD4MecanimEditorCommon.EnumPopupBulletPhysicsPluginType(ref this._bulletPhysicsPluginType);
            GUI.enabled = num != 0;
            bool enabled = GUI.enabled;
            GUI.enabled = enabled && !Application.isPlaying && physicsPluginState.bulletPhysicsPluginType != this._bulletPhysicsPluginType;
            EditorGUILayout.BeginHorizontal();
            if (this._bulletPhysicsLoadedModule != null)
                GUILayout.Label(" Loaded " + this._bulletPhysicsLoadedModule);
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Apply"))
            {
                MMD4MecanimEditorCommon.SetBulletPhysicsPluginType(this._bulletPhysicsPluginType);
                physicsPluginState.bulletPhysicsPluginType = this._bulletPhysicsPluginType;
            }
            EditorGUILayout.EndHorizontal();
            GUI.enabled = enabled;
        }
    }

    private static class AndroidSupport
    {
        private const string _devicesDirectory = "Assets/Resources";
        private const string _devicesFilePath = "Assets/Resources/devices.txt";
        public static bool logEnabled;

        private static string _GetAdbPath()
        {
            string pathB = Application.platform == RuntimePlatform.WindowsEditor ? "adb.exe" : "adb";
            string pathA = EditorPrefs.GetString("AndroidSdkRoot");
            return string.IsNullOrEmpty(pathA) ? (string)null : MMD4MecanimEditorCommon.AndroidSupport.PathCombine(MMD4MecanimEditorCommon.AndroidSupport.PathCombine(pathA, "platform-tools"), pathB);
        }

        private static string _ExecuteAdbProcess(string adbPath, string arguments)
        {
            try
            {
                return Process.Start(new ProcessStartInfo()
                {
                    FileName = adbPath,
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    Arguments = arguments
                }).StandardOutput.ReadToEnd();
            }
            catch (Exception ex)
            {
                if (MMD4MecanimEditorCommon.AndroidSupport.logEnabled)
                    UnityEngine.Debug.LogError((object)ex.ToString());
                return (string)null;
            }
        }

        public static bool GenerateAndroidDevicesFile()
        {
            if (MMD4MecanimEditorCommon.IsLicenseAccepted())
                return true;
            string adbPath = MMD4MecanimEditorCommon.AndroidSupport._GetAdbPath();
            if (string.IsNullOrEmpty(adbPath))
            {
                if (MMD4MecanimEditorCommon.AndroidSupport.logEnabled)
                    UnityEngine.Debug.LogError((object)"Android SDK is not found. You should install Android SDK at first. (See also, Preferences.)");
                return false;
            }
            if (!File.Exists(adbPath))
            {
                if (MMD4MecanimEditorCommon.AndroidSupport.logEnabled)
                    UnityEngine.Debug.LogError((object)"adb is not found in Android SDK. You should re-install Android SDK. (See also, Preferences.)");
                return false;
            }
            StringBuilder stringBuilder = new StringBuilder();
            string firstColumn = MMD4MecanimEditorCommon.AndroidSupport._ParseFirstColumn(MMD4MecanimEditorCommon.AndroidSupport._ExecuteAdbProcess(adbPath, "shell settings get secure android_id"));
            if (!string.IsNullOrEmpty(firstColumn))
            {
                stringBuilder.Append("Android.ANDROID_ID=");
                stringBuilder.Append(firstColumn);
            }
            string[] devices = MMD4MecanimEditorCommon.AndroidSupport._ParseDevices(MMD4MecanimEditorCommon.AndroidSupport._ExecuteAdbProcess(adbPath, "devices"));
            if (devices != null && devices.Length != 0 && !string.IsNullOrEmpty(devices[0]))
            {
                if (stringBuilder.Length != 0)
                    stringBuilder.Append(",");
                stringBuilder.Append("Android.SERIAL=");
                stringBuilder.Append(devices[0]);
            }
            if (stringBuilder.Length == 0)
            {
                if (MMD4MecanimEditorCommon.AndroidSupport.logEnabled && !File.Exists("Assets/Resources/devices.txt"))
                    UnityEngine.Debug.Log((object)"Android device is not found. You should reopen Unity as some android device is connected.");
            }
            else
            {
                string contents = stringBuilder.ToString();
                try
                {
                    if (File.Exists("Assets/Resources/devices.txt"))
                    {
                        string str = File.ReadAllText("Assets/Resources/devices.txt");
                        if (str != null)
                        {
                            if (str == contents)
                                return false;
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                try
                {
                    if (!Directory.Exists("Assets/Resources"))
                        Directory.CreateDirectory("Assets/Resources");
                }
                catch (Exception ex)
                {
                    if (MMD4MecanimEditorCommon.AndroidSupport.logEnabled)
                        UnityEngine.Debug.LogError((object)ex.ToString());
                    return false;
                }
                try
                {
                    File.WriteAllText("Assets/Resources/devices.txt", contents);
                    AssetDatabase.ImportAsset("Assets/Resources/devices.txt", ImportAssetOptions.ForceUpdate | ImportAssetOptions.ForceSynchronousImport);
                }
                catch (Exception ex)
                {
                    if (MMD4MecanimEditorCommon.AndroidSupport.logEnabled)
                        UnityEngine.Debug.LogError((object)ex.ToString());
                    return false;
                }
            }
            return true;
        }

        private static string[] _ParseLines(string outputString)
        {
            if (string.IsNullOrEmpty(outputString))
                return (string[])null;
            List<string> stringList = new List<string>();
            int index = 0;
            int startIndex = 0;
            for (; index < outputString.Length; ++index)
            {
                switch (outputString[index])
                {
                    case '\n':
                    case '\r':
                        if (startIndex != -1)
                        {
                            stringList.Add(outputString.Substring(startIndex, index - startIndex));
                            startIndex = -1;
                            break;
                        }
                        break;
                    default:
                        if (startIndex == -1)
                        {
                            startIndex = index;
                            break;
                        }
                        break;
                }
            }
            return stringList.ToArray();
        }

        private static string _ParseFirstColumn(string line)
        {
            if (string.IsNullOrEmpty(line))
                return (string)null;
            for (int index = 0; index < line.Length; ++index)
            {
                switch (line[index])
                {
                    case '\t':
                    case '\n':
                    case '\r':
                    case ' ':
                        return line.Substring(0, index);
                    default:
                        continue;
                }
            }
            return line;
        }

        private static string[] _ParseDevices(string outputString)
        {
            string[] lines = MMD4MecanimEditorCommon.AndroidSupport._ParseLines(outputString);
            if (lines == null || lines.Length == 0)
                return (string[])null;
            List<string> stringList = new List<string>();
            for (int index = 1; index < lines.Length; ++index)
            {
                string firstColumn = MMD4MecanimEditorCommon.AndroidSupport._ParseFirstColumn(lines[index]);
                if (!string.IsNullOrEmpty(firstColumn))
                    stringList.Add(firstColumn);
            }
            return stringList.ToArray();
        }

        public static string PathCombine(string pathA, string pathB)
        {
            if (pathA == null)
                return pathB;
            if (pathB == null)
                return pathA;
            int length = pathA.Length;
            if (length == 0)
                return pathB;
            if (pathB.Length == 0)
                return pathA;
            if (pathA[length - 1] == '\\' || pathA[length - 1] == '/' || pathB[0] == '\\' || pathB[0] == '/')
                return pathA + pathB;
            for (int index = 0; index < length; ++index)
            {
                switch (pathA[index])
                {
                    case '/':
                        return pathA + "/" + pathB;
                    case '\\':
                        return pathA + "\\" + pathB;
                    default:
                        continue;
                }
            }
            return pathA + Path.DirectorySeparatorChar.ToString() + pathB;
        }
    }

    public class TextureAccessor
    {
        public int width;
        public int height;
        public Color32[] pixels;
        public bool isTransparency;

        public TextureAccessor()
        {
        }

        public TextureAccessor(UnityEngine.Texture texture) => this.Lock(texture);

        public bool Lock(UnityEngine.Texture texture)
        {
            this.width = 0;
            this.height = 0;
            this.pixels = (Color32[])null;
            this.isTransparency = false;
            if ((UnityEngine.Object)texture == (UnityEngine.Object)null)
            {
                UnityEngine.Debug.LogWarning((object)"Texture is null.");
                return false;
            }
            this.width = texture.width;
            this.height = texture.height;
            string assetPath = AssetDatabase.GetAssetPath((UnityEngine.Object)texture);
            if (string.IsNullOrEmpty(assetPath))
            {
                UnityEngine.Debug.LogWarning((object)"Texture is null.");
                return false;
            }
            if (MMD4MecanimEditorCommon.IsExtensionDDS(assetPath))
            {
                this.isTransparency = this._HasTextureTransparency(texture);
                return true;
            }
            TextureImporter atPath = AssetImporter.GetAtPath(assetPath) as TextureImporter;
            if ((UnityEngine.Object)atPath == (UnityEngine.Object)null)
            {
                UnityEngine.Debug.LogWarning((object)"Texture is null.");
                return false;
            }
            if (atPath.textureType == TextureImporterType.Sprite || atPath.textureType == TextureImporterType.GUI)
            {
                MMD4MecanimEditorCommon.SetTextureTypeToDefault(atPath);
                MMD4MecanimEditorCommon.UpdateImportSettings(assetPath);
            }
            bool isReadable = atPath.isReadable;
            if (!isReadable)
            {
                atPath.isReadable = true;
                MMD4MecanimEditorCommon.UpdateImportSettings(assetPath);
            }
            this.isTransparency = this._HasTextureTransparency(texture);
            if (this.isTransparency && !atPath.alphaIsTransparency)
            {
                atPath.alphaIsTransparency = true;
                if (!isReadable)
                    MMD4MecanimEditorCommon.UpdateImportSettings(assetPath);
            }
            if (!isReadable)
            {
                atPath.isReadable = isReadable;
                MMD4MecanimEditorCommon.UpdateImportSettings(assetPath);
            }
            return true;
        }

        private bool _HasTextureTransparency(UnityEngine.Texture texture)
        {
            if ((UnityEngine.Object)texture == (UnityEngine.Object)null)
            {
                UnityEngine.Debug.LogError((object)"_HasTextureTransparency:Unknown Flow.");
                return false;
            }
            Texture2D texture2D = texture as Texture2D;
            if ((UnityEngine.Object)texture2D == (UnityEngine.Object)null)
                return false;
            try
            {
                this.pixels = texture2D.GetPixels32();
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.LogError((object)ex.ToString());
                UnityEngine.Debug.LogError((object)AssetDatabase.GetAssetPath((UnityEngine.Object)texture));
                return true;
            }
            if (this.pixels == null)
                return true;
            for (int index = 0; index < this.pixels.Length; ++index)
            {
                if (this.pixels[index].a != byte.MaxValue)
                    return true;
            }
            return false;
        }
    }

    public class AlphaUVChecker
    {
        public bool[] pixels;
        public int width;
        public int height;
        private float f_width;
        private float f_height;
        private List<MMD4MecanimEditorCommon.AlphaUVChecker.Point> lineMinMax = new List<MMD4MecanimEditorCommon.AlphaUVChecker.Point>();

        private static void _InitLineMinMax(
          List<MMD4MecanimEditorCommon.AlphaUVChecker.Point> lineMinMax,
          int yMin,
          int yMax)
        {
            lineMinMax.Clear();
            int num = yMax - yMin + 1;
            for (int index = 0; index < num; ++index)
            {
                int minValue = int.MinValue;
                int x_ = minValue - 1;
                lineMinMax.Add(new MMD4MecanimEditorCommon.AlphaUVChecker.Point(x_, minValue));
            }
        }

        private static void _ComputeLineMinMax(
          List<MMD4MecanimEditorCommon.AlphaUVChecker.Point> lineMinMax,
          int yMin,
          MMD4MecanimEditorCommon.AlphaUVChecker.Point pt1,
          MMD4MecanimEditorCommon.AlphaUVChecker.Point pt2)
        {
            uint count = (uint)lineMinMax.Count;
            int num1 = Mathf.Abs(pt1.x - pt2.x);
            int num2 = Mathf.Abs(pt1.y - pt2.y);
            if (num1 > num2)
            {
                int num3;
                if (pt1.x > pt2.x)
                {
                    num3 = pt1.y > pt2.y ? 1 : -1;
                    int x = pt1.x;
                    pt1.x = pt2.x;
                    pt2.x = x;
                    pt1.y = pt2.y;
                }
                else
                    num3 = pt1.y < pt2.y ? 1 : -1;
                uint index1 = (uint)(pt1.y - yMin);
                if (index1 < count)
                {
                    MMD4MecanimEditorCommon.AlphaUVChecker.Point point;
                    point.x = Mathf.Min(lineMinMax[(int)index1].x, pt1.x);
                    point.y = Mathf.Max(lineMinMax[(int)index1].y, pt1.x);
                    lineMinMax[(int)index1] = point;
                }
                int num4 = num1 >> 1;
                while (++pt1.x <= pt2.x)
                {
                    if ((num4 -= num2) < 0)
                    {
                        num4 += num1;
                        pt1.y += num3;
                    }
                    uint index2 = (uint)(pt1.y - yMin);
                    if (index2 < count)
                    {
                        MMD4MecanimEditorCommon.AlphaUVChecker.Point point;
                        point.x = Mathf.Min(lineMinMax[(int)index2].x, pt1.x);
                        point.y = Mathf.Max(lineMinMax[(int)index2].y, pt1.x);
                        lineMinMax[(int)index2] = point;
                    }
                }
            }
            else
            {
                int num5;
                if (pt1.y > pt2.y)
                {
                    num5 = pt1.x > pt2.x ? 1 : -1;
                    int y = pt1.y;
                    pt1.y = pt2.y;
                    pt2.y = y;
                    pt1.x = pt2.x;
                }
                else
                    num5 = pt1.x < pt2.x ? 1 : -1;
                uint index3 = (uint)(pt1.y - yMin);
                if (index3 < count)
                {
                    MMD4MecanimEditorCommon.AlphaUVChecker.Point point;
                    point.x = Mathf.Min(lineMinMax[(int)index3].x, pt1.x);
                    point.y = Mathf.Max(lineMinMax[(int)index3].y, pt1.x);
                    lineMinMax[(int)index3] = point;
                }
                int num6 = num2 >> 1;
                while (++pt1.y <= pt2.y)
                {
                    if ((num6 -= num1) < 0)
                    {
                        num6 += num2;
                        pt1.x += num5;
                    }
                    uint index4 = (uint)(pt1.y - yMin);
                    if (index4 < count)
                    {
                        MMD4MecanimEditorCommon.AlphaUVChecker.Point point;
                        point.x = Mathf.Min(lineMinMax[(int)index4].x, pt1.x);
                        point.y = Mathf.Max(lineMinMax[(int)index4].y, pt1.x);
                        lineMinMax[(int)index4] = point;
                    }
                }
            }
        }

        private static void _Swap(
          ref MMD4MecanimEditorCommon.AlphaUVChecker.Point xy0,
          ref MMD4MecanimEditorCommon.AlphaUVChecker.Point xy1)
        {
            MMD4MecanimEditorCommon.AlphaUVChecker.Point point = xy0;
            xy0 = xy1;
            xy1 = point;
        }

        private static void _SortTriangleByHeight(
          ref MMD4MecanimEditorCommon.AlphaUVChecker.Point xy0,
          ref MMD4MecanimEditorCommon.AlphaUVChecker.Point xy1,
          ref MMD4MecanimEditorCommon.AlphaUVChecker.Point xy2)
        {
            if (xy0.y > xy1.y)
                MMD4MecanimEditorCommon.AlphaUVChecker._Swap(ref xy0, ref xy1);
            if (xy0.y > xy2.y)
                MMD4MecanimEditorCommon.AlphaUVChecker._Swap(ref xy0, ref xy2);
            if (xy1.y <= xy2.y)
                return;
            MMD4MecanimEditorCommon.AlphaUVChecker._Swap(ref xy1, ref xy2);
        }

        public void Create(int width, int height)
        {
            this.width = width;
            this.height = height;
            this.f_width = (float)width;
            this.f_height = (float)height;
            this.pixels = new bool[width * height];
        }

        private MMD4MecanimEditorCommon.AlphaUVChecker.Point _ToPoint(Vector2 uv) => new MMD4MecanimEditorCommon.AlphaUVChecker.Point((int)((double)uv.x * (double)this.f_width), (int)((double)uv.y * (double)this.f_height));

        public void SetTriangle(Vector2 uv0, Vector2 uv1, Vector2 uv2)
        {
            if (this.width == 0 || this.height == 0)
                return;
            MMD4MecanimEditorCommon.AlphaUVChecker.Point point1 = this._ToPoint(uv0);
            MMD4MecanimEditorCommon.AlphaUVChecker.Point point2 = this._ToPoint(uv1);
            MMD4MecanimEditorCommon.AlphaUVChecker.Point point3 = this._ToPoint(uv2);
            MMD4MecanimEditorCommon.AlphaUVChecker._SortTriangleByHeight(ref point1, ref point2, ref point3);
            this._SetPixel(point1.x, point1.y);
            this._SetPixel(point2.x, point2.y);
            this._SetPixel(point3.x, point3.y);
            MMD4MecanimEditorCommon.AlphaUVChecker._InitLineMinMax(this.lineMinMax, point1.y, point3.y);
            int y1 = point1.y;
            MMD4MecanimEditorCommon.AlphaUVChecker._ComputeLineMinMax(this.lineMinMax, y1, point1, point2);
            MMD4MecanimEditorCommon.AlphaUVChecker._ComputeLineMinMax(this.lineMinMax, y1, point2, point3);
            MMD4MecanimEditorCommon.AlphaUVChecker._ComputeLineMinMax(this.lineMinMax, y1, point1, point3);
            for (int y2 = point1.y; y2 <= point3.y; ++y2)
            {
                int x1 = this.lineMinMax[y2 - y1].x;
                int y3 = this.lineMinMax[y2 - y1].y;
                for (int x2 = x1; x2 < y3; ++x2)
                    this._SetPixel(x2, y2);
            }
        }

        private static int _Wrap(int x, int w)
        {
            if (x < 0)
                x = -x;
            if (x >= w)
            {
                int num1 = x / w;
                int num2 = x % w;
                x = num1 % 2 != 0 ? w - 1 - num2 : num2;
            }
            return x;
        }

        private void _SetPixel(int x, int y)
        {
            x = MMD4MecanimEditorCommon.AlphaUVChecker._Wrap(x, this.width);
            y = MMD4MecanimEditorCommon.AlphaUVChecker._Wrap(y, this.height);
            if ((uint)x >= (uint)this.width || (uint)y >= (uint)this.height)
                return;
            this.pixels[x + y * this.width] = true;
        }

        public bool HasTransparency(Color32[] colors)
        {
            if (this.pixels == null)
                return true;
            for (int index = 0; index < this.pixels.Length; ++index)
            {
                if (this.pixels[index] && colors[index].a != byte.MaxValue)
                    return true;
            }
            return false;
        }

        public void Dump(Color32[] colors, string path)
        {
            if (this.pixels == null || colors == null)
                return;
            Texture2D texture2D = new Texture2D(this.width, this.height, TextureFormat.ARGB32, false);
            Color32[] colors1 = (Color32[])colors.Clone();
            for (int index = 0; index < colors1.Length; ++index)
                colors1[index] = !this.pixels[index] ? new Color32((byte)0, (byte)0, byte.MaxValue, byte.MaxValue) : (colors[index].a == byte.MaxValue ? new Color32((byte)0, byte.MaxValue, (byte)0, byte.MaxValue) : new Color32(byte.MaxValue, (byte)0, (byte)0, byte.MaxValue));
            texture2D.SetPixels32(colors1);
            texture2D.Apply();
            File.WriteAllBytes(path, texture2D.EncodeToPNG());
        }

        private struct Point
        {
            public int x;
            public int y;

            public Point(int x_, int y_)
            {
                this.x = x_;
                this.y = y_;
            }
        }
    }

    public class MMDMesh
    {
        private MeshRenderer[] _meshRenderers;
        private SkinnedMeshRenderer[] _skinnedMeshRenderers;

        public MMDMesh(GameObject gameObject)
        {
            if (!((UnityEngine.Object)gameObject != (UnityEngine.Object)null))
                return;
            this._meshRenderers = MMD4MecanimCommon.GetMeshRenderers(gameObject);
            this._skinnedMeshRenderers = MMD4MecanimCommon.GetSkinnedMeshRenderers(gameObject);
        }

        private static bool _CheckTransparency(
          Mesh mesh,
          int subMeshIndex,
          UnityEngine.Material material,
          MMD4MecanimEditorCommon.TextureAccessor textureAccessor)
        {
            if ((UnityEngine.Object)mesh == (UnityEngine.Object)null || subMeshIndex >= mesh.subMeshCount || textureAccessor == null)
            {
                UnityEngine.Debug.LogError((object)"");
                return true;
            }
            if (!textureAccessor.isTransparency)
                return false;
            MMD4MecanimEditorCommon.AlphaUVChecker alphaUvChecker = new MMD4MecanimEditorCommon.AlphaUVChecker();
            alphaUvChecker.Create(textureAccessor.width, textureAccessor.height);
            Vector2[] uv = mesh.uv;
            int[] triangles = mesh.GetTriangles(subMeshIndex);
            if (uv == null || uv.Length == 0 || triangles == null || triangles.Length == 0)
            {
                UnityEngine.Debug.LogError((object)"");
                return false;
            }
            for (int index = 0; index + 2 < triangles.Length; index += 3)
                alphaUvChecker.SetTriangle(uv[triangles[index]], uv[triangles[index + 1]], uv[triangles[index + 2]]);
            return alphaUvChecker.HasTransparency(textureAccessor.pixels);
        }

        public bool CheckTransparency(
          string materialName,
          MMD4MecanimEditorCommon.TextureAccessor textureAcccessor)
        {
            if (materialName == null)
                return true;
            bool flag = false;
            if (this._meshRenderers != null)
            {
                foreach (MeshRenderer meshRenderer in this._meshRenderers)
                {
                    UnityEngine.Material[] sharedMaterials = meshRenderer.sharedMaterials;
                    if (sharedMaterials != null)
                    {
                        for (int subMeshIndex = 0; subMeshIndex < sharedMaterials.Length; ++subMeshIndex)
                        {
                            UnityEngine.Material material = sharedMaterials[subMeshIndex];
                            if (material.name == materialName)
                            {
                                MeshFilter component = meshRenderer.gameObject.GetComponent<MeshFilter>();
                                if ((UnityEngine.Object)component != (UnityEngine.Object)null && (UnityEngine.Object)component.sharedMesh != (UnityEngine.Object)null)
                                {
                                    flag = true;
                                    if (MMD4MecanimEditorCommon.MMDMesh._CheckTransparency(component.sharedMesh, subMeshIndex, material, textureAcccessor))
                                        return true;
                                }
                            }
                        }
                    }
                }
            }
            if (this._skinnedMeshRenderers != null)
            {
                foreach (SkinnedMeshRenderer skinnedMeshRenderer in this._skinnedMeshRenderers)
                {
                    UnityEngine.Material[] sharedMaterials = skinnedMeshRenderer.sharedMaterials;
                    if (sharedMaterials != null)
                    {
                        for (int subMeshIndex = 0; subMeshIndex < sharedMaterials.Length; ++subMeshIndex)
                        {
                            UnityEngine.Material material = sharedMaterials[subMeshIndex];
                            if (material.name == materialName)
                            {
                                flag = true;
                                if (MMD4MecanimEditorCommon.MMDMesh._CheckTransparency(skinnedMeshRenderer.sharedMesh, subMeshIndex, material, textureAcccessor))
                                    return true;
                            }
                        }
                    }
                }
            }
            return !flag;
        }
    }

    public enum TextureFileSign
    {
        None,
        Bmp,
        BmpWithAlpha,
        Png,
        PngWithAlpha,
        Jpeg,
        Targa,
        TargaWithAlpha,
    }

    public class StreamBuilder
    {
        public List<int> intValues = new List<int>();
        public List<float> floatValues = new List<float>();
        public List<byte> byteValues = new List<byte>();

        public int intPos => this.intValues.Count;

        public int floatPos => this.floatValues.Count;

        public int bytePos => this.byteValues.Count;

        public int AddInt(int v)
        {
            int count = this.intValues.Count;
            this.intValues.Add(v);
            return count;
        }

        public int AddFloat(float v)
        {
            int count = this.floatValues.Count;
            this.floatValues.Add(v);
            return count;
        }

        public int AddByte(byte v)
        {
            int count = this.byteValues.Count;
            this.byteValues.Add(v);
            return count;
        }

        public void SetInt(int pos, int v) => this.intValues[pos] = v;

        public void SetFloat(int pos, float v) => this.floatValues[pos] = v;

        public void SetByte(int pos, byte v) => this.byteValues[pos] = v;

        public int AddVector2(Vector2 v)
        {
            int floatPos = this.floatPos;
            this.floatValues.Add(v.x);
            this.floatValues.Add(v.y);
            return floatPos;
        }

        public int AddVector3(Vector3 v)
        {
            int floatPos = this.floatPos;
            this.floatValues.Add(v.x);
            this.floatValues.Add(v.y);
            this.floatValues.Add(v.z);
            return floatPos;
        }

        public void SetVector2(int pos, Vector2 v)
        {
            this.floatValues[pos] = v.x;
            this.floatValues[pos + 1] = v.y;
        }

        public void SetVector3(int pos, Vector3 v)
        {
            this.floatValues[pos] = v.x;
            this.floatValues[pos + 1] = v.y;
            this.floatValues[pos + 2] = v.z;
        }

        public void WriteToFile(string path)
        {
            int count1 = this.intValues.Count;
            int count2 = this.floatValues.Count;
            int count3 = this.byteValues.Count;
            byte[] numArray = new byte[count1 * 4 + count2 * 4 + count3];
            int num = 0;
            if (count1 > 0)
            {
                Buffer.BlockCopy((Array)this.intValues.ToArray(), 0, (Array)numArray, num, count1 * 4);
                num += count1 * 4;
            }
            if (count2 > 0)
            {
                Buffer.BlockCopy((Array)this.floatValues.ToArray(), 0, (Array)numArray, num, count2 * 4);
                num += count2 * 4;
            }
            if (count3 > 0)
            {
                Buffer.BlockCopy((Array)this.byteValues.ToArray(), 0, (Array)numArray, num, count3);
                num += count3;
            }
            FileStream fileStream = File.Open(path, System.IO.FileMode.Create, FileAccess.Write, FileShare.None);
            fileStream.Write(numArray, 0, num);
            fileStream.Close();
        }
    }

    [Serializable]
    public struct Bool3
    {
        public bool x;
        public bool y;
        public bool z;

        public Bool3(bool x, bool y, bool z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public void SetValue(bool x, bool y, bool z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

    public static class Localizer
    {
        public static string Do(string text) => text;

        public static string Do(object obj)
        {
            if (obj == null)
                return (string)null;
            return obj is string ? obj as string : obj.ToString();
        }
    }

    public static class UI
    {
        private static List<bool> _guiEnabledStack = new List<bool>();
        private static List<int> _guiLeadTabCountStack = new List<int>();
        private static int _guiLeadTabCount;
        private static string _guiLeadTabString;
        private static int _calcTabCount;
        private static float _calcTabSpace;
        private static int _lineCount;
        private static bool _leadSpaced;
        private static MethodInfo _enumPopup;
        private static MethodInfo _popup;
        private static Dictionary<System.Type, Array> _enumValues;
        private static GUIStyle _materialTitlebar_labelStyle;
        private static GUIStyle _materialBlockTitlebar_labelStyle;
        private static GUIStyle _propertyTitlebar_foldoutStyle;
        private static GUIStyle _propertyTitlebar_toggleStyle;
        private static GUIStyle _propertyTitlebar_labelStyle;

        public static void PushEnabled(bool enabled)
        {
            MMD4MecanimEditorCommon.UI._guiEnabledStack.Add(GUI.enabled);
            GUI.enabled &= enabled;
        }

        public static void PopEnabled()
        {
            if (MMD4MecanimEditorCommon.UI._guiEnabledStack.Count <= 0)
                return;
            int num = MMD4MecanimEditorCommon.UI._guiEnabledStack[MMD4MecanimEditorCommon.UI._guiEnabledStack.Count - 1] ? 1 : 0;
            MMD4MecanimEditorCommon.UI._guiEnabledStack.RemoveAt(MMD4MecanimEditorCommon.UI._guiEnabledStack.Count - 1);
            GUI.enabled = num != 0;
        }

        public static void DrawSeparator() => GUILayout.Box(new GUIContent(), GUILayout.Height(1f), GUILayout.ExpandWidth(true));

        public static void PushLead() => MMD4MecanimEditorCommon.UI.PushLead(4);

        public static void PushLead(int tabCount)
        {
            MMD4MecanimEditorCommon.UI._guiLeadTabCount += tabCount;
            MMD4MecanimEditorCommon.UI._guiLeadTabCountStack.Add(tabCount);
            MMD4MecanimEditorCommon.UI._guiLeadTabString = new string(' ', MMD4MecanimEditorCommon.UI._guiLeadTabCount);
        }

        public static void PopLead()
        {
            if (MMD4MecanimEditorCommon.UI._guiLeadTabCountStack.Count <= 0)
                return;
            MMD4MecanimEditorCommon.UI._guiLeadTabCount -= MMD4MecanimEditorCommon.UI._guiLeadTabCountStack[MMD4MecanimEditorCommon.UI._guiLeadTabCountStack.Count - 1];
            MMD4MecanimEditorCommon.UI._guiLeadTabCountStack.RemoveAt(MMD4MecanimEditorCommon.UI._guiLeadTabCountStack.Count - 1);
            MMD4MecanimEditorCommon.UI._guiLeadTabString = new string(' ', MMD4MecanimEditorCommon.UI._guiLeadTabCount);
        }

        public static void LeadSpace()
        {
            if (MMD4MecanimEditorCommon.UI._leadSpaced)
                return;
            MMD4MecanimEditorCommon.UI._leadSpaced = true;
            MMD4MecanimEditorCommon.UI.InternalLeadSpace();
        }

        private static void InternalLeadSpace()
        {
            if (MMD4MecanimEditorCommon.UI._guiLeadTabCount <= 0)
                return;
            MMD4MecanimEditorCommon.UI.TabSpace(MMD4MecanimEditorCommon.UI._guiLeadTabCount);
        }

        public static void TabSpace() => MMD4MecanimEditorCommon.UI.TabSpace(4);

        public static void TabSpace(int tabCount)
        {
            if (MMD4MecanimEditorCommon.UI._calcTabCount != tabCount)
            {
                MMD4MecanimEditorCommon.UI._calcTabCount = tabCount;
                MMD4MecanimEditorCommon.UI._calcTabSpace = GUI.skin.label.CalcSize(new GUIContent(new string(' ', tabCount))).x;
            }
            GUILayout.Space(MMD4MecanimEditorCommon.UI._calcTabSpace);
        }

        public static void BeginLine() => MMD4MecanimEditorCommon.UI.BeginLine(false);

        public static void BeginLineWithLeadSpacing() => MMD4MecanimEditorCommon.UI.BeginLine(true);

        public static void BeginLine(bool leadSpacing)
        {
            if (MMD4MecanimEditorCommon.UI._lineCount == 0)
                EditorGUILayout.BeginHorizontal();
            if (leadSpacing)
                MMD4MecanimEditorCommon.UI.LeadSpace();
            ++MMD4MecanimEditorCommon.UI._lineCount;
        }

        public static void EndLine()
        {
            if (--MMD4MecanimEditorCommon.UI._lineCount != 0)
                return;
            MMD4MecanimEditorCommon.UI._leadSpaced = false;
            EditorGUILayout.EndHorizontal();
        }

        public static void ResetStyles()
        {
            MMD4MecanimEditorCommon.UI._calcTabCount = 0;
            MMD4MecanimEditorCommon.UI._calcTabSpace = 0.0f;
            MMD4MecanimEditorCommon.UI._lineCount = 0;
            MMD4MecanimEditorCommon.UI._leadSpaced = false;
            MMD4MecanimEditorCommon.UI._guiLeadTabCountStack.Clear();
            MMD4MecanimEditorCommon.UI._guiLeadTabCount = 0;
            MMD4MecanimEditorCommon.UI._guiLeadTabString = (string)null;
        }

        public static float GetTextWidth(int count) => GUI.skin.label.CalcSize(new GUIContent(new string('A', count))).x;

        public static string LeadText(string text)
        {
            if (MMD4MecanimEditorCommon.UI._leadSpaced || MMD4MecanimEditorCommon.UI._guiLeadTabCount == 0)
                return text;
            MMD4MecanimEditorCommon.UI._leadSpaced = true;
            return MMD4MecanimEditorCommon.UI._guiLeadTabString + text;
        }

        public static Type EnumPopupNoLocalized<Type>(
          object label,
          ref Type enumValue,
          params GUILayoutOption[] options)
        {
            if (MMD4MecanimEditorCommon.UI._enumPopup == null)
                MMD4MecanimEditorCommon.UI._enumPopup = typeof(EditorGUILayout).GetMethod("EnumPopup", BindingFlags.Static | BindingFlags.Public, (Binder)null, new System.Type[3]
                {
          typeof (string),
          typeof (Enum),
          typeof (GUILayoutOption[])
                }, (ParameterModifier[])null);
            MMD4MecanimEditorCommon.UI.BeginLine(false);
            enumValue = (Type)MMD4MecanimEditorCommon.UI._enumPopup.Invoke((object)null, new object[3]
            {
        (object) MMD4MecanimEditorCommon.UI.LeadText(MMD4MecanimEditorCommon.Localizer.Do(label)),
        (object) enumValue,
        (object) options
            });
            MMD4MecanimEditorCommon.UI.EndLine();
            return enumValue;
        }

        public static Type EnumPopup<Type>(
          object label,
          ref Type enumValue,
          params GUILayoutOption[] options)
          where Type : struct, IComparable, IFormattable, IConvertible
        {
            if (MMD4MecanimEditorCommon.UI._popup == null)
                MMD4MecanimEditorCommon.UI._popup = typeof(EditorGUILayout).GetMethod("Popup", BindingFlags.Static | BindingFlags.Public, (Binder)null, new System.Type[4]
                {
          typeof (string),
          typeof (int),
          typeof (string[]),
          typeof (GUILayoutOption[])
                }, (ParameterModifier[])null);
            if (MMD4MecanimEditorCommon.UI._enumValues == null)
                MMD4MecanimEditorCommon.UI._enumValues = new Dictionary<System.Type, Array>();
            Array values;
            if (!MMD4MecanimEditorCommon.UI._enumValues.TryGetValue(typeof(Type), out values))
            {
                values = Enum.GetValues(typeof(Type));
                MMD4MecanimEditorCommon.UI._enumValues.Add(typeof(Type), values);
            }
            string[] strArray = new string[values.Length];
            int index = 0;
            foreach (object obj in values)
            {
                strArray[index] = MMD4MecanimEditorCommon.Localizer.Do(obj);
                ++index;
            }
            MMD4MecanimEditorCommon.UI.BeginLine(false);
            enumValue = (Type)MMD4MecanimEditorCommon.UI._popup.Invoke((object)null, new object[4]
            {
        (object) MMD4MecanimEditorCommon.UI.LeadText(MMD4MecanimEditorCommon.Localizer.Do(label)),
        (object) (int) (ValueType) enumValue,
        (object) strArray,
        (object) options
            });
            MMD4MecanimEditorCommon.UI.EndLine();
            return enumValue;
        }

        public static Color ColorField(
          object label,
          ref Color color,
          params GUILayoutOption[] options)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(true);
            color = EditorGUILayout.ColorField(MMD4MecanimEditorCommon.Localizer.Do(label), color, options);
            MMD4MecanimEditorCommon.UI.EndLine();
            return color;
        }

        public static Color ColorField_RGB(
          object label,
          ref Color color,
          params GUILayoutOption[] options)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(true);
            color = EditorGUILayout.ColorField(new GUIContent(MMD4MecanimEditorCommon.Localizer.Do(label)), color, true, false, false, (ColorPickerHDRConfig)null, options);
            MMD4MecanimEditorCommon.UI.EndLine();
            return color;
        }

        public static bool Foldout(ref bool foldout, object label)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(true);
            foldout = EditorGUILayout.Foldout(foldout, MMD4MecanimEditorCommon.Localizer.Do(label));
            MMD4MecanimEditorCommon.UI.EndLine();
            return foldout;
        }

        public static bool ToggleFoldout(ref bool foldout, ref bool toggle, object label)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(true);
            foldout = MMD4MecanimEditorCommon.UI.InternalPropertyTitlebar(ref foldout, true, ref toggle, label, EditorStyles.label, (UnityEngine.Texture)null, true);
            MMD4MecanimEditorCommon.UI.EndLine();
            return foldout;
        }

        public static int IntField(object label, ref int intValue, params GUILayoutOption[] options)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(false);
            intValue = EditorGUILayout.IntField(MMD4MecanimEditorCommon.UI.LeadText(MMD4MecanimEditorCommon.Localizer.Do(label)), intValue, options);
            MMD4MecanimEditorCommon.UI.EndLine();
            return intValue;
        }

        public static float IntSlider(object label, ref int intValue, int minValue, int maxValue)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(false);
            intValue = EditorGUILayout.IntSlider(MMD4MecanimEditorCommon.UI.LeadText(MMD4MecanimEditorCommon.Localizer.Do(label)), intValue, minValue, maxValue);
            MMD4MecanimEditorCommon.UI.EndLine();
            return (float)intValue;
        }

        public static float FloatField(object label, ref float floatValue)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(false);
            floatValue = EditorGUILayout.FloatField(MMD4MecanimEditorCommon.UI.LeadText(MMD4MecanimEditorCommon.Localizer.Do(label)), floatValue);
            MMD4MecanimEditorCommon.UI.EndLine();
            return floatValue;
        }

        public static float Slider(object label, ref float floatValue, float minValue, float maxValue)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(false);
            floatValue = EditorGUILayout.Slider(MMD4MecanimEditorCommon.UI.LeadText(MMD4MecanimEditorCommon.Localizer.Do(label)), floatValue, minValue, maxValue);
            MMD4MecanimEditorCommon.UI.EndLine();
            return floatValue;
        }

        public static float FloatField_Max(
          object label,
          ref float floatValue,
          float maxValue,
          params GUILayoutOption[] options)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(false);
            floatValue = EditorGUILayout.FloatField(MMD4MecanimEditorCommon.UI.LeadText(MMD4MecanimEditorCommon.Localizer.Do(label)), floatValue, options);
            if ((double)floatValue > (double)maxValue)
                floatValue = maxValue;
            MMD4MecanimEditorCommon.UI.EndLine();
            return floatValue;
        }

        public static float FloatField_Min(
          object label,
          ref float floatValue,
          float minValue,
          params GUILayoutOption[] options)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(false);
            floatValue = EditorGUILayout.FloatField(MMD4MecanimEditorCommon.UI.LeadText(MMD4MecanimEditorCommon.Localizer.Do(label)), floatValue, options);
            if ((double)floatValue < (double)minValue)
                floatValue = minValue;
            MMD4MecanimEditorCommon.UI.EndLine();
            return floatValue;
        }

        public static bool ToggleFloatField(ref bool toggle, object label, ref float floatValue)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(true);
            toggle = (GUILayout.Toggle((toggle ? 1 : 0) != 0, MMD4MecanimEditorCommon.Localizer.Do(label), GUILayout.ExpandWidth(false)) ? 1 : 0) != 0;
            MMD4MecanimEditorCommon.UI.PushEnabled(toggle);
            GUILayout.FlexibleSpace();
            floatValue = EditorGUILayout.FloatField("", floatValue);
            MMD4MecanimEditorCommon.UI.PopEnabled();
            MMD4MecanimEditorCommon.UI.EndLine();
            return toggle;
        }

        public static Type ObjectField<Type>(object label, ref Type obj, bool allowSceneObjects) where Type : UnityEngine.Object
        {
            MMD4MecanimEditorCommon.UI.BeginLine(false);
            obj = (Type)EditorGUILayout.ObjectField(MMD4MecanimEditorCommon.UI.LeadText(MMD4MecanimEditorCommon.Localizer.Do(label)), (UnityEngine.Object)obj, typeof(Type), allowSceneObjects);
            MMD4MecanimEditorCommon.UI.EndLine();
            return obj;
        }

        public static bool ToggleLeft(object label, ref bool toggle, params GUILayoutOption[] options)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(true);
            toggle = GUILayout.Toggle(toggle, MMD4MecanimEditorCommon.Localizer.Do(label), options);
            MMD4MecanimEditorCommon.UI.EndLine();
            return toggle;
        }

        public static bool Toggle(object label, ref bool toggle, params GUILayoutOption[] options)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(false);
            toggle = EditorGUILayout.Toggle(MMD4MecanimEditorCommon.UI.LeadText(MMD4MecanimEditorCommon.Localizer.Do(label)), toggle, options);
            MMD4MecanimEditorCommon.UI.EndLine();
            return toggle;
        }

        public static void Toggle3(object label, ref MMD4MecanimEditorCommon.Bool3 toggle3)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(true);
            EditorGUILayout.LabelField(MMD4MecanimEditorCommon.Localizer.Do(label));
            toggle3.x = (GUILayout.Toggle((toggle3.x ? 1 : 0) != 0, "X", GUILayout.ExpandWidth(false)) ? 1 : 0) != 0;
            toggle3.y = (GUILayout.Toggle((toggle3.y ? 1 : 0) != 0, "Y", GUILayout.ExpandWidth(false)) ? 1 : 0) != 0;
            toggle3.z = (GUILayout.Toggle((toggle3.z ? 1 : 0) != 0, "Z", GUILayout.ExpandWidth(false)) ? 1 : 0) != 0;
            MMD4MecanimEditorCommon.UI.EndLine();
        }

        public static bool ToggleToggle3(
          ref bool toggle,
          object label,
          ref MMD4MecanimEditorCommon.Bool3 toggle3)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(true);
            toggle = (GUILayout.Toggle((toggle ? 1 : 0) != 0, "", GUILayout.ExpandWidth(false)) ? 1 : 0) != 0;
            MMD4MecanimEditorCommon.UI.PushEnabled(toggle);
            EditorGUILayout.LabelField(MMD4MecanimEditorCommon.Localizer.Do(label));
            toggle3.x = (GUILayout.Toggle((toggle3.x ? 1 : 0) != 0, "X", GUILayout.ExpandWidth(false)) ? 1 : 0) != 0;
            toggle3.y = (GUILayout.Toggle((toggle3.y ? 1 : 0) != 0, "Y", GUILayout.ExpandWidth(false)) ? 1 : 0) != 0;
            toggle3.z = (GUILayout.Toggle((toggle3.z ? 1 : 0) != 0, "Z", GUILayout.ExpandWidth(false)) ? 1 : 0) != 0;
            MMD4MecanimEditorCommon.UI.PopEnabled();
            MMD4MecanimEditorCommon.UI.EndLine();
            return toggle;
        }

        public static void Label(string text, params GUILayoutOption[] options)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(false);
            GUILayout.Label(MMD4MecanimEditorCommon.UI.LeadText(MMD4MecanimEditorCommon.Localizer.Do(text)), options);
            MMD4MecanimEditorCommon.UI.EndLine();
        }

        public static void Label(string text, GUIStyle style, params GUILayoutOption[] options)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(false);
            GUILayout.Label(MMD4MecanimEditorCommon.UI.LeadText(MMD4MecanimEditorCommon.Localizer.Do(text)), style, options);
            MMD4MecanimEditorCommon.UI.EndLine();
        }

        public static void LabelField(string text, params GUILayoutOption[] options)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(false);
            EditorGUILayout.LabelField(new GUIContent(MMD4MecanimEditorCommon.UI.LeadText(MMD4MecanimEditorCommon.Localizer.Do(text))), options);
            MMD4MecanimEditorCommon.UI.EndLine();
        }

        public static void LabelField(string text, GUIStyle style, params GUILayoutOption[] options)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(false);
            EditorGUILayout.LabelField(new GUIContent(MMD4MecanimEditorCommon.UI.LeadText(MMD4MecanimEditorCommon.Localizer.Do(text))), style, options);
            MMD4MecanimEditorCommon.UI.EndLine();
        }

        public static void LabelField(GUIContent content, params GUILayoutOption[] options)
        {
            MMD4MecanimEditorCommon.UI.BeginLine((UnityEngine.Object)content.image != (UnityEngine.Object)null);
            EditorGUILayout.LabelField(new GUIContent(MMD4MecanimEditorCommon.UI.LeadText(MMD4MecanimEditorCommon.Localizer.Do(content.text)), content.image), options);
            MMD4MecanimEditorCommon.UI.EndLine();
        }

        public static void LabelField(
          GUIContent content,
          GUIStyle style,
          params GUILayoutOption[] options)
        {
            MMD4MecanimEditorCommon.UI.BeginLine((UnityEngine.Object)content.image != (UnityEngine.Object)null);
            EditorGUILayout.LabelField(new GUIContent(MMD4MecanimEditorCommon.UI.LeadText(MMD4MecanimEditorCommon.Localizer.Do(content.text)), content.image), style, options);
            MMD4MecanimEditorCommon.UI.EndLine();
        }

        public static void BoldLabel(string text, params GUILayoutOption[] options)
        {
            MMD4MecanimEditorCommon.UI.BeginLine();
            EditorGUILayout.LabelField(MMD4MecanimEditorCommon.UI.LeadText(MMD4MecanimEditorCommon.Localizer.Do(text)), EditorStyles.boldLabel, options);
            MMD4MecanimEditorCommon.UI.EndLine();
        }

        public static void BoldLabel(GUIContent content, params GUILayoutOption[] options)
        {
            MMD4MecanimEditorCommon.UI.BeginLine((UnityEngine.Object)content.image != (UnityEngine.Object)null);
            EditorGUILayout.LabelField(new GUIContent(MMD4MecanimEditorCommon.UI.LeadText(MMD4MecanimEditorCommon.Localizer.Do(content.text)), content.image), EditorStyles.boldLabel, options);
            MMD4MecanimEditorCommon.UI.EndLine();
        }

        public static void BoldLabelSpaced(string text, params GUILayoutOption[] options)
        {
            MMD4MecanimEditorCommon.UI.BeginLine();
            EditorGUILayout.LabelField(MMD4MecanimEditorCommon.UI.LeadText(" " + MMD4MecanimEditorCommon.Localizer.Do(text)), EditorStyles.boldLabel, options);
            MMD4MecanimEditorCommon.UI.EndLine();
        }

        public static void BoldLabelSpaced(GUIContent content, params GUILayoutOption[] options)
        {
            MMD4MecanimEditorCommon.UI.BeginLine((UnityEngine.Object)content.image != (UnityEngine.Object)null);
            EditorGUILayout.LabelField(new GUIContent(MMD4MecanimEditorCommon.UI.LeadText(" " + MMD4MecanimEditorCommon.Localizer.Do(content.text)), content.image), EditorStyles.boldLabel, options);
            MMD4MecanimEditorCommon.UI.EndLine();
        }

        public static void LineSeparator() => GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(1f));

        public static bool Button(object label)
        {
            MMD4MecanimEditorCommon.UI.BeginLine(false);
            int num = GUILayout.Button(MMD4MecanimEditorCommon.Localizer.Do(label)) ? 1 : 0;
            MMD4MecanimEditorCommon.UI.EndLine();
            return num != 0;
        }

        public static bool MaterialTitlebar(ref bool foldout, ref bool toggle, string visibleName)
        {
            MMD4MecanimEditorCommon.UI._Internal_SetupPropertyTitlebarGUIStyles();
            if (MMD4MecanimEditorCommon.UI._materialTitlebar_labelStyle == null)
            {
                MMD4MecanimEditorCommon.UI._materialTitlebar_labelStyle = new GUIStyle(EditorStyles.label);
                MMD4MecanimEditorCommon.UI._ZeroMarginLeft(MMD4MecanimEditorCommon.UI._materialTitlebar_labelStyle);
                MMD4MecanimEditorCommon.UI._ZeroPaddingLeft(MMD4MecanimEditorCommon.UI._materialTitlebar_labelStyle);
            }
            EditorGUILayout.BeginHorizontal();
            foldout = (GUILayout.Toggle((foldout ? 1 : 0) != 0, "", MMD4MecanimEditorCommon.UI._propertyTitlebar_foldoutStyle, GUILayout.ExpandWidth(false)) ? 1 : 0) != 0;
            toggle = (GUILayout.Toggle((toggle ? 1 : 0) != 0, "", GUILayout.ExpandWidth(false)) ? 1 : 0) != 0;
            EditorGUILayout.LabelField(visibleName, EditorStyles.label, GUILayout.ExpandWidth(false));
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
            return foldout;
        }

        public static bool MaterialBlockTitlebar(
          ref bool foldout,
          int index,
          out bool isDelete,
          out bool isAdd)
        {
            MMD4MecanimEditorCommon.UI._Internal_SetupPropertyTitlebarGUIStyles();
            if (MMD4MecanimEditorCommon.UI._materialBlockTitlebar_labelStyle == null)
            {
                MMD4MecanimEditorCommon.UI._materialBlockTitlebar_labelStyle = new GUIStyle(EditorStyles.label);
                MMD4MecanimEditorCommon.UI._ZeroMarginLeft(MMD4MecanimEditorCommon.UI._materialBlockTitlebar_labelStyle);
                MMD4MecanimEditorCommon.UI._ZeroPaddingLeft(MMD4MecanimEditorCommon.UI._materialBlockTitlebar_labelStyle);
            }
            float width = GUI.skin.button.CalcHeight(new GUIContent(), 0.0f);
            EditorGUILayout.BeginHorizontal();
            foldout = (GUILayout.Toggle((foldout ? 1 : 0) != 0, "", MMD4MecanimEditorCommon.UI._propertyTitlebar_foldoutStyle, GUILayout.ExpandWidth(false)) ? 1 : 0) != 0;
            EditorGUILayout.LabelField("Material " + (object)index, EditorStyles.label, GUILayout.ExpandWidth(false));
            GUILayout.FlexibleSpace();
            if (index != 0)
                isDelete = (GUILayout.Button("-", GUILayout.Width(width)) ? 1 : 0) != 0;
            else
                isDelete = false;
            isAdd = (GUILayout.Button("+", GUILayout.Width(width)) ? 1 : 0) != 0;
            EditorGUILayout.EndHorizontal();
            return foldout;
        }

        public static bool PropertyTitlebar(ref bool foldout, GUIContent content)
        {
            bool toggle = false;
            return MMD4MecanimEditorCommon.UI.InternalPropertyTitlebar(ref foldout, false, ref toggle, (object)content.text, content.image, true);
        }

        public static bool PropertyTitlebar(ref bool foldout, GUIContent content, bool enabled)
        {
            bool toggle = false;
            return MMD4MecanimEditorCommon.UI.InternalPropertyTitlebar(ref foldout, false, ref toggle, (object)content.text, content.image, enabled);
        }

        public static bool PropertyTitlebar(ref bool foldout, ref bool toggle, GUIContent content) => MMD4MecanimEditorCommon.UI.InternalPropertyTitlebar(ref foldout, true, ref toggle, (object)content.text, content.image, true);

        public static bool PropertyTitlebar(
          ref bool foldout,
          ref bool toggle,
          GUIContent content,
          bool enabled)
        {
            return MMD4MecanimEditorCommon.UI.InternalPropertyTitlebar(ref foldout, true, ref toggle, (object)content.text, content.image, enabled);
        }

        private static void _ZeroMarginLeft(GUIStyle guiStyle) => guiStyle.margin.left = 0;

        private static void _ZeroMarginRight(GUIStyle guiStyle) => guiStyle.margin.right = 0;

        private static void _ZeroPaddingLeft(GUIStyle guiStyle) => guiStyle.padding.left = 0;

        private static void _ZeroPaddingRight(GUIStyle guiStyle) => guiStyle.padding.right = 0;

        private static bool InternalPropertyTitlebar(
          ref bool foldout,
          bool toggleEnabled,
          ref bool toggle,
          object text,
          UnityEngine.Texture image,
          bool enabled)
        {
            return MMD4MecanimEditorCommon.UI.InternalPropertyTitlebar(ref foldout, toggleEnabled, ref toggle, text, EditorStyles.boldLabel, image, enabled);
        }

        private static void _Internal_SetupPropertyTitlebarGUIStyles()
        {
            if (MMD4MecanimEditorCommon.UI._propertyTitlebar_foldoutStyle == null)
            {
                MMD4MecanimEditorCommon.UI._propertyTitlebar_foldoutStyle = new GUIStyle(EditorStyles.foldout);
                MMD4MecanimEditorCommon.UI._ZeroMarginRight(MMD4MecanimEditorCommon.UI._propertyTitlebar_foldoutStyle);
                MMD4MecanimEditorCommon.UI._ZeroPaddingRight(MMD4MecanimEditorCommon.UI._propertyTitlebar_foldoutStyle);
            }
            if (MMD4MecanimEditorCommon.UI._propertyTitlebar_toggleStyle != null)
                return;
            MMD4MecanimEditorCommon.UI._propertyTitlebar_toggleStyle = new GUIStyle(EditorStyles.toggle);
            MMD4MecanimEditorCommon.UI._ZeroMarginLeft(MMD4MecanimEditorCommon.UI._propertyTitlebar_toggleStyle);
            MMD4MecanimEditorCommon.UI._ZeroMarginRight(MMD4MecanimEditorCommon.UI._propertyTitlebar_toggleStyle);
            MMD4MecanimEditorCommon.UI._ZeroPaddingRight(MMD4MecanimEditorCommon.UI._propertyTitlebar_toggleStyle);
        }

        private static bool InternalPropertyTitlebar(
          ref bool foldout,
          bool toggleEnabled,
          ref bool toggle,
          object text,
          GUIStyle textStyle,
          UnityEngine.Texture image,
          bool enabled)
        {
            MMD4MecanimEditorCommon.UI._Internal_SetupPropertyTitlebarGUIStyles();
            if (MMD4MecanimEditorCommon.UI._propertyTitlebar_labelStyle == null)
            {
                MMD4MecanimEditorCommon.UI._propertyTitlebar_labelStyle = new GUIStyle(textStyle);
                MMD4MecanimEditorCommon.UI._ZeroMarginLeft(MMD4MecanimEditorCommon.UI._propertyTitlebar_labelStyle);
                MMD4MecanimEditorCommon.UI._ZeroPaddingLeft(MMD4MecanimEditorCommon.UI._propertyTitlebar_labelStyle);
            }
            EditorGUILayout.BeginHorizontal();
            foldout = (GUILayout.Toggle((foldout ? 1 : 0) != 0, "", MMD4MecanimEditorCommon.UI._propertyTitlebar_foldoutStyle, GUILayout.ExpandWidth(false)) ? 1 : 0) != 0;
            MMD4MecanimEditorCommon.UI.PushEnabled(enabled);
            if (toggleEnabled)
                toggle = (GUILayout.Toggle((toggle ? 1 : 0) != 0, "", MMD4MecanimEditorCommon.UI._propertyTitlebar_toggleStyle, GUILayout.ExpandWidth(false)) ? 1 : 0) != 0;
            else
                GUILayout.Space(MMD4MecanimEditorCommon.UI._propertyTitlebar_toggleStyle.CalcSize(new GUIContent()).x);
            EditorGUILayout.LabelField(new GUIContent(" " + MMD4MecanimEditorCommon.Localizer.Do(text), image), textStyle, GUILayout.ExpandWidth(false));
            MMD4MecanimEditorCommon.UI.PopEnabled();
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
            return foldout;
        }

        public static Font GetMiniFont()
        {
            GUIStyle guiStyle = new GUIStyle(GUI.skin.FindStyle("MiniLabel"));
            return guiStyle != null ? guiStyle.font : GUI.skin.font;
        }

        public static Vector2 BeginVerticalScrollView(
          Vector2 scrollPosition,
          bool alwaysShowVertical,
          GUIStyle verticalScrollbar,
          GUIStyle background,
          params GUILayoutOption[] options)
        {
            return EditorGUILayout.BeginScrollView(scrollPosition, alwaysShowVertical, alwaysShowVertical, GUI.skin.horizontalScrollbar, verticalScrollbar, background, options);
        }
    }
}
