﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimImporterImpl
// Assembly: MMD4MecanimEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DD6A7AB2-B1E4-4A37-85A9-48A7C8E1BAF8
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Editor\MMD4MecanimEditor.dll

using MMD4MecanimInternal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

public class MMD4MecanimImporterImpl : ScriptableObject
{
    private static readonly string[] toolbarTitlesBasic = new string[2]
    {
        "PMX2FBX",
        "Material"
    };

    private static readonly string[] toolbarTitlesAdvanced = new string[4]
    {
        "PMX2FBX",
        "Material",
        "Rig",
        "Animations"
    };

    private static readonly string[] no_pmx2fbx_toolbarTitlesBasic = new string[1]
    {
        "Material"
    };

    private static readonly string[] no_pmx2fbx_toolbarTitlesAdvanced = new string[3]
    {
        "Material",
        "Rig",
        "Animations"
    };

    public GameObject fbxAsset;
    public string fbxAssetPath;
    [NonSerialized] public List<Material> materialList;
    [NonSerialized] public List<int[]> materialNoLists;
    [NonSerialized] public MMDModel mmdModel;
    [NonSerialized] public DateTime mmdModelLastWriteTime;
    [NonSerialized] public TextAsset indexAsset;
    [NonSerialized] public AuxData.IndexData indexData;
    [NonSerialized] public TextAsset vertexAsset;
    [NonSerialized] public AuxData.VertexData vertexData;
    public EditorViewPage editorViewPage;
    [NonSerialized] public PMX2FBXProperty pmx2fbxProperty;
    [NonSerialized] public PMX2FBXConfig pmx2fbxConfig;
    private bool _prepareDependencyAtLeastOnce;
    private LicenseAgree _licenseAgree;
    private string[] _readmeFiles;
    private string[] _readmeTexts;
    private bool _isPiapro;
    private bool _isNitro;
    private bool _isKancolle;
    private string[] _comments;
    private Vector2 _readmeScrollView;
    private int _readmeSelected;
    private bool _isAgreedWeak;
    private bool _isCheckedLicense;
    private bool _isCheckedWarning;
    private bool _isCheckedMorality;
    private bool _isCheckedPiapro;
    private bool _isCheckedNitro;
    private bool _isCheckedKancolle;
    private bool _isCheckedPublish;
    private GUIStyle _guiStyle_toggleButtons;
    private GUIStyle _guiStyle_labelField;
    private GUIStyle _guiStyle_textArea;
    public static readonly string ScriptExtension = ".MMD4Mecanim.asset";
    public static volatile int _pmx2fbxProcessingCount;
    private volatile System.Diagnostics.Process _pmx2fbxProcess;

    public static readonly string[] ReadmeExtNames = new string[3]
    {
        ".txt",
        ".htm",
        ".html"
    };

    public static readonly string[] ReadmeFileNames_StartsWith = new string[2]
    {
        "readme",
        "read"
    };

    public static readonly string[] ReadmeFileNames_Contains_Japanese = new string[14]
    {
        "モデル利用のガイドライン",
        "お読みください",
        "よんでね",
        "よんで",
        "説明",
        "読",
        "りーどみー",
        "リードミー",
        "れどめ",
        "りどみ",
        "れあどめ",
        "利用規約",
        "利用",
        "使用"
    };

    #region LicenseText
    
    public static readonly string LicenceImportantString = "利用規約";

    public static readonly string[] LicenseAgreeStrings = new string[5]
    {
        "",
        "ライセンス状況を確認できませんでした。",
        "利用規約が見つかりませんでした。利用規約をご確認ください。",
        "利用規約をご確認ください。",
        "利用規約をご確認ください。"
    };

    public static readonly string[] LicenseDeniedStrings = new string[37]
    {
        "ＭＭＤ及びＭＭＭ以外のアプリケーションで出力した画像、映像の公開は原則として不可です",
        "ＭＭＤ及びＭＭＭ以外のアプリケーションによって出力した画像、映像の公開は禁止です",
        "(自作ゲームやソフトウェア等)で使用することは出来ません",
        "モデルの使用はＭＭＤ、ＭＭＭ限定でお願いします。",
        "当データはＭＭＤ並びにＭＭＤ派生ソフト専用のＰＭＸデータです。",
        "MMD(MMD後継ツール含む)以外でのデータの使用",
        "MMD以外での使用。",
        "MMDおよびその派生ソフト（MMMなど）以外での使用は控えてください。",
        "MMDやMMM以外のソフトやサービスでの使用",
        "MMD以外の3Dソフトでの使用。",
        "本モデルデータのMikuMikuDance及びデータフォーマットを変換せずに利用可能な類似の動画作成ツール以外での利用",
        "MikuMikuDance MikuMikuMoving MikuMikuOnline以外では原則使用不可です。",
        "当データはＭＭＤ並びにＭＭＤ派生ソフト専用のＰＭＸデータです。",
        "ＭＭＤ以外のアプリケーションで出力した画像、映像の公開は原則として不可です。",
        "他のソフトへの移植やデータ形式へのコンバートは一切禁止します。",
        "MMD、MMM、MMDAI、nanoem以外の３Ｄソフトでの使用",
        "MMD/MMM以外でのモデルデータの利用",
        "MMD及びMMM以外のアニメーションツールへの使用",
        "MMD、MMM、Mac用MMD互換ソフト以外での使用",
        "MMD/MMM以外の媒体での使用は原則禁止",
        "MMD、MME、MMM、MMDAI2、nanoem、PMXE以外のソフト、アプリケーションでの使用",
        "MMD･MMM以外での使用(Mac用などのMMD互換ソフトでの使用は可）",
        "MMD/MMM以外のツールでの使用・コンテンツ制作",
        "MMD,MMM,Mac用ＭＭＤ互換ソフト,PMDEditor,PMXEditor以外での使用",
        "MMD、MMM以外での使用",
        "MMD以外の動画静止画の製作・他のアプリケーション、サイトでの使用",
        "MMD以外の他ソフト・アプリでの使用は禁止",
        "MMD、MMM以外の３Dソフトでの使用禁止",
        "MMD、MMM以外のソフト及びサービスでの使用",
        "MMD、MMM、MMDAIなどのMMD動画作成ツール以外での使用",
        "MMD/MMEこれに準ずるソフト(後継ソフトやMac用MMDソフト/MMM)以外での利用",
        "MMD.MMM以外の3Dソフト＆ツールへの使用禁止",
        "ＭＭＤ、ＭＭＭ、nanoem（MMDAI）以外での使用",
        "MMD・MMM以外の目的に使用すること",
        "PMX形式以外へのコンバート",
        "MMD、MMM、Mac用MMD互換ソフト、PMDE、PMXE以外のソフトやアプリでの使用",
        "MMD、MMM以外のツールでの使用、コンテンツ制作"
    };

    public static readonly string[] LicensePiaproStrings = new string[4]
    {
        "ピアプロ",
        "piapro",
        "ＣＲＹＰＴＯＮ",
        "CRYPTON"
    };

    public static readonly string[] LicenseNitroStrings = new string[3]
    {
        "ニトロプラス",
        "nitroplus",
        "Nitroplus"
    };

    public static readonly string[] LicenseKancolleStrings = new string[2]
    {
        "艦隊これくしょん",
        "艦これ"
    };

    public static readonly string LicenceAgreedWarning = "利用規約に同意の上、ご使用ください。";

    public static readonly string LicenceWarningNotice =
        "公開・配布を目的とするゲーム及びコンテンツでの利用は、明示的に許可されている場合を除いて、制作されている方に問い合わせましょう。";

    public static readonly string LicenceDeniedWarning = "私的利用の範囲内で、ご使用ください。";
    public static readonly string LicenceDeniedNotice = "画像、映像の公開は原則として不可です。";
    public static readonly string LicenseWarningPiapro = "PCLの遵守をお願いします。";
    public static readonly string LicenseWarningPiaproLink = "http://piapro.jp/license/pcl";
    public static readonly string LicenseWarningKancolle = "公序良俗に反する表現、及びゲーム性のある\nコンテンツの配布については制限されています。";
    public static readonly string LicenseWarningNitro = "公式のガイドラインの遵守をお願いします。";
    public static readonly string LicenseWarningNitroLink = "http://www.nitroplus.co.jp/license/";
    public static readonly string LicenseWarningMorality = "公序良俗を十分に配慮頂けるようお願いします。";
    public static readonly string LicenseDeniedMorality = "公序良俗に反する表現は禁止されています。";
    public static readonly string LicenseMorality = "（家族がそろって視聴した場合、露骨な表現描写をすることによって困惑、嫌悪の感じを抱かせないように注意をしてください。）";
    public static readonly string LicenseMoralityLink = "https://ec.crypton.co.jp/support/faq/362";

    public static readonly string LicensePublish =
        "ストア、オンラインストレージ、githubなど不特定多数に向けてのアップロードは行わないでください。（著作者及び権利者から特別に許可を得た場合は除く。）";

    public static readonly string LicenceAgreedWarning_En =
        "You must agree to these Terms of Use before using the model/motion.";

    public static readonly string LicenceWarningNotice_En =
        "If it is intended to distribute and games, need a permit creators.";

    public static readonly string LicenceDeniedWarning_En = "Within the scope of private use, please use.";
    public static readonly string LicenceDeniedNotice_En = "Public image, the image is impossible in principle.";

    public static readonly string LicenseWarningPiapro_En =
        "These existing artworks include copyrights of the creators who made them.\nSee more CC BY-NC.";

    public static readonly string LicenseWarningPiaproLink_En = "http://piapro.net/en_for_creators.html";
    public static readonly string LicenseWarningNitro_En = "Please observe guidelines.";
    public static readonly string LicenseWarningNitroLink_En = "http://www.nitroplus.co.jp/license/";

    public static readonly string LicenseWarningKancolle_En =
        "Public order and morality in contrary representation,\nand has been restricted for distribution of content that game properties.";

    public static readonly string LicenseWarningMorality_En = "Please consider public order and morals enough.";

    public static readonly string LicenseDeniedMorality_En =
        "The expression against public order and morals is prohibited.";

    public static readonly string LicenseMorality_En =
        "(When a family watches it all together, please warn him not to let you hold perplexity, an aversive feeling by doing open expression description.)";

    public static readonly string LicenseMoralityLink_En = "https://ec.crypton.co.jp/support/faq/362";

    public static readonly string LicensePublish_En =
        "Don't upload to public space(online storages, stores, etc...) without author's specially permission.";
    #endregion
    
    public const int ChangedDelayFrame = 60;
    public const float ShaderRevision = 2f;
    private List<MaterialHelper> _materialHelpers;
    private int _changedDelay;
    private static bool _standardFounded;
    private static bool _standardEnabled;

    private static readonly string[] toolbarMaterialPageTitle = new string[2]
    {
        "MaterialBlocks",
        "Materials"
    };

    private bool _initializeMaterialAtLeastOnce;
    private bool _initializeMaterialAfterPMX2FBX;
    [NonSerialized] public bool _forceProcessConvertMaterial = true;

    private static readonly string[] WinePaths = new string[4]
    {
        "/Applications/NXWine.app/Contents/Resources/bin/wine",
        "/Applications/Wine.app/Contents/Resources/bin/wine",
        "/opt/local/bin/wine",
        "/opt/local/bin/wine"
    };

    private const string _RigDetail1 = "Humanoidのスケルトンマッピングを自動補正します。";
    private const string _RigDetail2 = "一旦Rig>HumanoidのConfigureを行ってから実行してください。";
    private const string _RigDetail3 = ".metaを直接書き換えるため、Unityバージョンによっては不整合を起こします。";
    private const string _RigWarning1 = "非推奨機能です。必ずバックアップを取ってから実行してください。";
    private bool _rigAccept1;
    private const string HumanDescriptionLine = "humanDescription:";
    private const string HumanLine = "human:";
    private const string Human0Line = "human: []";
    private const string SkeletonLine = "skeleton:";
    private const string Skeleton0Line = "skeleton: []";

    public void _OnImportedFBXAsset(string fbxAssetPath)
    {
        RichLog.Log($"_OnImportedFBXAsset {fbxAssetPath}", Color.red);

        this.Setup();
        if (fbxAsset == null)
        {
            this.fbxAssetPath = null;
            this.fbxAsset = (GameObject) AssetDatabase.LoadAssetAtPath(fbxAssetPath, typeof(GameObject));
            if (fbxAsset == null)
            {
                Debug.LogError(("Nothing fbxAsset. " + fbxAssetPath));
                return;
            }

            this.fbxAssetPath = fbxAssetPath;
        }

        _PrepareDependency();
    }

    private bool _editorAdvancedMode
    {
        get => this.pmx2fbxConfig != null && this.pmx2fbxConfig.globalSettings != null &&
               this.pmx2fbxConfig.globalSettings.editorAdvancedMode;
        set
        {
            if (this.pmx2fbxConfig == null)
                this.pmx2fbxConfig = new PMX2FBXConfig();
            if (this.pmx2fbxConfig.globalSettings == null)
                this.pmx2fbxConfig.globalSettings = new PMX2FBXConfig.GlobalSettings();
            this.pmx2fbxConfig.globalSettings.editorAdvancedMode = value;
        }
    }

    public static bool IsJapanese() => Application.systemLanguage == SystemLanguage.Japanese;

    private static bool IsEnableShakeHands() =>
        MMD4MecanimGlobalSettings.instance.materialShakeHandsEnabled == 51966;

    public void OnInspectorGUI()
    {
        if (!MMD4MecanimEditorCommon.ValidateUnityVersion())
            return;
        MMD4MecanimEditorCommon.UI.ResetStyles();
        if (!Application.isPlaying && !this._prepareDependencyAtLeastOnce)
        {
            this._prepareDependencyAtLeastOnce = true;
            this._PrepareDependency();
        }

        if (!this.Setup())
            return;
        if (this.pmx2fbxConfig != null)
        {
            if (!this._isAgreedWeak && IsEnableShakeHands() &&
                this.pmx2fbxConfig.mmd4MecanimProperty != null)
                this._isAgreedWeak = this.pmx2fbxConfig.mmd4MecanimProperty.materialShakeHanded;
            if (!this._isAgreedWeak)
            {
                if (this._licenseAgree == LicenseAgree.NotProcess)
                {
                    this._licenseAgree = this.CheckLicenseAgree(ref this._readmeFiles, ref this._readmeTexts,
                        ref this._isPiapro, ref this._isNitro, ref this._isKancolle);
                    this._comments = this.DecodeModelComments();
                }

                if (this._guiStyle_toggleButtons == null)
                {
                    this._guiStyle_toggleButtons = new GUIStyle(GUI.skin.toggle);
                    this._guiStyle_toggleButtons.wordWrap = true;
                    this._guiStyle_toggleButtons.fontSize = 12;
                }

                if (this._guiStyle_labelField == null)
                {
                    this._guiStyle_labelField = new GUIStyle(EditorStyles.label);
                    this._guiStyle_labelField.wordWrap = true;
                    this._guiStyle_toggleButtons.fontSize = 12;
                }

                if (this._guiStyle_textArea == null)
                {
                    this._guiStyle_textArea = new GUIStyle(EditorStyles.textArea);
                    this._guiStyle_textArea.fontSize = 14;
                    this._guiStyle_textArea.wordWrap = true;
                }

                EditorGUILayout.TextArea(LicenseAgreeStrings[(int) this._licenseAgree],
                    this._guiStyle_textArea);
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.BeginVertical();
                if (this._readmeFiles != null)
                {
                    for (int index = 0; index < this._readmeFiles.Length; ++index)
                    {
                        string fileName = Path.GetFileName(this._readmeFiles[index]);
                        int num = GUILayout.Toolbar(index == this._readmeSelected ? 0 : -1, new string[1]
                        {
                            fileName
                        });
                        if (this._readmeSelected != index && num == 0)
                            this._readmeSelected = index;
                    }
                }

                EditorGUILayout.EndVertical();
                GUILayout.FlexibleSpace();
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.Separator();
                string text1 = (string) null;
                if (this._readmeTexts != null && this._readmeSelected < this._readmeTexts.Length)
                    text1 = this._readmeTexts[this._readmeSelected];
                this._readmeScrollView = EditorGUILayout.BeginScrollView(this._readmeScrollView);
                if (this._comments != null)
                {
                    if (this._comments.Length != 0)
                    {
                        string comment = this._comments[0];
                        if (comment != null)
                            EditorGUILayout.TextArea(comment, this._guiStyle_textArea);
                    }

                    if (2 < this._comments.Length)
                    {
                        string comment = this._comments[2];
                        if (comment != null)
                            EditorGUILayout.TextArea(comment, this._guiStyle_textArea);
                    }

                    EditorGUILayout.Separator();
                }

                if (!string.IsNullOrEmpty(text1))
                    EditorGUILayout.TextArea(text1, this._guiStyle_textArea);
                string label1 = LicenceWarningNotice;
                string text2 = LicenceAgreedWarning;
                string text3 = LicenseWarningMorality;
                string text4 = LicenseDeniedMorality;
                string label2 = LicenseMorality;
                string str1 = LicenseMoralityLink;
                string text5 = LicensePublish;
                string text6 = LicenseWarningPiapro;
                string str2 = LicenseWarningPiaproLink;
                string text7 = LicenseWarningNitro;
                string str3 = LicenseWarningNitroLink;
                string text8 = LicenseWarningKancolle;
                string text9 = LicenceDeniedWarning;
                string text10 = LicenceDeniedNotice;
                if (!IsJapanese())
                {
                    label1 = LicenceWarningNotice_En;
                    text2 = LicenceAgreedWarning_En;
                    text3 = LicenseWarningMorality_En;
                    text4 = LicenseDeniedMorality_En;
                    label2 = LicenseMorality_En;
                    str1 = LicenseMoralityLink_En;
                    text5 = LicensePublish_En;
                    text6 = LicenseWarningPiapro_En;
                    str2 = LicenseWarningPiaproLink_En;
                    text7 = LicenseWarningNitro_En;
                    str3 = LicenseWarningNitroLink_En;
                    text8 = LicenseWarningKancolle_En;
                    text9 = LicenceDeniedWarning_En;
                    text10 = LicenceDeniedNotice_En;
                }

                EditorGUILayout.Separator();
                if (this._licenseAgree != LicenseAgree.Denied)
                {
                    EditorGUILayout.LabelField(label1, this._guiStyle_labelField);
                    EditorGUILayout.Separator();
                }

                this._isCheckedLicense = GUILayout.Toggle(this._isCheckedLicense, text2, this._guiStyle_toggleButtons);
                bool isCheckedLicense = this._isCheckedLicense;
                EditorGUILayout.Separator();
                this._isCheckedMorality = this._isPiapro || this._isNitro
                    ? GUILayout.Toggle(this._isCheckedMorality, text4, this._guiStyle_toggleButtons)
                    : GUILayout.Toggle(this._isCheckedMorality, text3, this._guiStyle_toggleButtons);
                EditorGUILayout.BeginHorizontal();
                GUILayout.Space(18f);
                EditorGUILayout.LabelField(label2, this._guiStyle_labelField);
                EditorGUILayout.EndHorizontal();
                if (IsJapanese())
                {
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Space(18f);
                    if (GUILayout.Button(str1))
                        System.Diagnostics.Process.Start(str1);
                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.Separator();
                this._isCheckedPublish = GUILayout.Toggle(this._isCheckedPublish, text5, this._guiStyle_toggleButtons);
                bool flag = isCheckedLicense && this._isCheckedPublish;
                if (this._isPiapro)
                {
                    this._isCheckedPiapro =
                        GUILayout.Toggle(this._isCheckedPiapro, text6, this._guiStyle_toggleButtons);
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Space(18f);
                    if (GUILayout.Button(str2))
                        System.Diagnostics.Process.Start(str2);
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Separator();
                    flag = flag && this._isCheckedPiapro;
                }

                if (this._isNitro)
                {
                    this._isCheckedNitro = GUILayout.Toggle(this._isCheckedNitro, text7, this._guiStyle_toggleButtons);
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Space(18f);
                    if (GUILayout.Button(str3))
                        System.Diagnostics.Process.Start(str3);
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Separator();
                    flag = flag && this._isCheckedNitro;
                }

                if (this._isKancolle)
                {
                    this._isCheckedKancolle =
                        GUILayout.Toggle(this._isCheckedKancolle, text8, this._guiStyle_toggleButtons);
                    EditorGUILayout.Separator();
                    flag = flag && this._isCheckedKancolle;
                }

                if (this._licenseAgree == LicenseAgree.Denied)
                {
                    this._isCheckedWarning =
                        GUILayout.Toggle(this._isCheckedWarning, text9, this._guiStyle_toggleButtons);
                    EditorGUILayout.TextArea(text10);
                    EditorGUILayout.Separator();
                    flag = flag && this._isCheckedWarning;
                }

                GUI.enabled = flag;
                EditorGUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                int num1 = GUILayout.Button(IsJapanese() ? "同意する" : "Agree",
                    GUILayout.ExpandWidth(false))
                    ? 1
                    : 0;
                GUI.enabled = true;
                EditorGUILayout.EndHorizontal();
                if (num1 != 0)
                {
                    this._isAgreedWeak = true;
                    if (IsEnableShakeHands())
                    {
                        if (this.pmx2fbxConfig.mmd4MecanimProperty == null)
                            this.pmx2fbxConfig.mmd4MecanimProperty =
                                new PMX2FBXConfig.MMD4MecanimProperty();
                        if (!this.pmx2fbxConfig.mmd4MecanimProperty.materialShakeHanded)
                        {
                            this.pmx2fbxConfig.mmd4MecanimProperty.materialShakeHanded = true;
                            this.SavePMX2FBXConfig();
                        }
                    }
                }

                EditorGUILayout.EndScrollView();
                return;
            }
        }

        bool inspectorViewAdvanced =
            MMD4MecanimGlobalSettings.instance.importerInspectorViewAdvanced;
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        _editorAdvancedMode = GUILayout.Toggle(inspectorViewAdvanced, "Advanced Mode");
        GUILayout.EndHorizontal();
        if (_editorAdvancedMode !=
            MMD4MecanimGlobalSettings.instance.importerInspectorViewAdvanced)
        {
            MMD4MecanimGlobalSettings.instance.importerInspectorViewAdvanced =
                _editorAdvancedMode;
            MMD4MecanimGlobalSettings.Sync();
        }

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        int selected = MMD4MecanimGlobalSettings.instance.importerInspectorViewPage;
        string[] texts = !MMD4MecanimImporterImplEditor._isEnabledPMX2FBX
            ? (inspectorViewAdvanced
                ? no_pmx2fbx_toolbarTitlesAdvanced
                : no_pmx2fbx_toolbarTitlesBasic)
            : (inspectorViewAdvanced
                ? toolbarTitlesAdvanced
                : toolbarTitlesBasic);
        if (selected < 0 || selected >= texts.Length)
            selected = 0;
        int num2 = GUILayout.Toolbar(selected, texts);
        editorViewPage = !MMD4MecanimImporterImplEditor._isEnabledPMX2FBX
            ? (EditorViewPage) (num2 + 1)
            : (EditorViewPage) num2;
        if (num2 != MMD4MecanimGlobalSettings.instance.importerInspectorViewPage)
        {
            MMD4MecanimGlobalSettings.instance.importerInspectorViewPage = num2;
            MMD4MecanimGlobalSettings.Sync();
        }

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        EditorGUILayout.Separator();
        if (editorViewPage != EditorViewPage.Material)
            ClearStandardFounded();
        switch (editorViewPage)
        {
            case EditorViewPage.PMX2FBX:
                _OnInspectorGUI_PMX2FBX();
                break;
            case EditorViewPage.Material:
                _OnInspectorGUI_Material();
                break;
            case EditorViewPage.Rig:
                _OnInspectorGUI_Rig();
                break;
            case EditorViewPage.Animations:
                _OnInspectorGUI_Animations();
                break;
        }
    }

    private void _OnInspectorGUI_ShowFBXField()
    {
        GameObject assetObject =
            EditorGUILayout.ObjectField(fbxAsset, typeof(GameObject), false) as GameObject;
        if (!(assetObject != null) ||
            !(assetObject != fbxAsset))
            return;
        string assetPath = AssetDatabase.GetAssetPath(assetObject);
        if (MMD4MecanimEditorCommon.IsExtensionFBX(assetPath))
        {
            fbxAsset = assetObject;
            fbxAssetPath = assetPath;
            materialList = null;
            mmdModel = null;
            mmdModelLastWriteTime = new DateTime();
            indexAsset = null;
            indexData = null;
            vertexAsset = null;
            vertexData = null;
            PrepareDependency();
        }
    }

    public static string GetAssetPath(Animator animator) => animator != null
        ? AssetDatabase.GetAssetPath(animator.avatar)
        : null;

    public static string GetAssetPath(MMD4MecanimModelImpl model) =>
        model != null
        ? GetAssetPath(model.gameObject.GetComponent<Animator>())
        : null;

    public string ScriptAssetPath;

    private string _GetScriptAssetPath()
    {
        if(string.IsNullOrEmpty(ScriptAssetPath))
            ScriptAssetPath = AssetDatabase.GetAssetPath(this);
        Debug.Log("GetScriptAssetPath: " + ScriptAssetPath);
        return ScriptAssetPath;
    }

    private string _GetScriptAssetPathWithoutExtension() =>
        MMD4MecanimEditorCommon.GetPathWithoutExtension(this._GetScriptAssetPath(),
            ScriptExtension.Length);

    public static string GetBasePath()
    {
        Shader assetObject = Shader.Find("MMD4Mecanim/MMDLit");
        return assetObject != null
            ? Path.GetDirectoryName(Path.GetDirectoryName(AssetDatabase.GetAssetPath((Object) assetObject)))
            : Path.Combine("Assets", "MMD4Mecanim");
    }

    public static string GetEditorPath() => Path.Combine(GetBasePath(), "Editor");

    public static string GetScriptsPath() => Path.Combine(GetBasePath(), "Scripts");

    public static string GetPMX2FBXRootConfigPath()
    {
        string path = Path.Combine(Path.Combine(GetEditorPath(), "PMX2FBX"), "pmx2fbx.xml");
        return File.Exists(path) ? path : null;
    }

    public static string GetPMX2FBXPath(
        bool useWineFlag,
        PMX2FBXConfig.BulletPhysicsVersion bulletPhysicsVersion)
    {
        string path = Path.Combine(Path.Combine(GetEditorPath(), "PMX2FBX"), "pmx2fbx");
        if (bulletPhysicsVersion != PMX2FBXConfig.BulletPhysicsVersion.Latest)
            path += bulletPhysicsVersion.ToString();
        if (Application.platform == RuntimePlatform.WindowsEditor | useWineFlag)
            path += ".exe";
        return File.Exists(path) ? path : (string) null;
    }

    public bool PrepareDependency()
    {
        if (this.isProcessing)
            return false;
        this._PrepareDependency();
        return true;
    }

    private void _PrepareDependency()
    {
        if (this.isProcessing || !this.Setup())
            return;
        if (this.fbxAsset == null &&
            this.pmx2fbxConfig.mmd4MecanimProperty != null)
        {
            string fbxOutputPath = this.pmx2fbxConfig.mmd4MecanimProperty.fbxOutputPath;
            if (File.Exists(fbxOutputPath))
            {
                this.fbxAsset = AssetDatabase.LoadAssetAtPath(fbxOutputPath, typeof(GameObject)) as GameObject;
                this.fbxAssetPath = fbxOutputPath;
            }
        }

        if (fbxAsset != null)
        {
            Debug.Log("fbxAssetPath: " + fbxAssetPath);
            string mmdModelPath = GetMMDModelPath(fbxAssetPath);
            RichLog.Log("mmdModelPath: " + mmdModelPath, Color.blue);

            if (File.Exists(mmdModelPath))
            {
                if (this.mmdModel == null ||
                    this.mmdModelLastWriteTime != MMD4MecanimEditorCommon.GetLastWriteTime(mmdModelPath))
                {
                    this.mmdModel = GetMMDModel(mmdModelPath);
                    this.mmdModelLastWriteTime = MMD4MecanimEditorCommon.GetLastWriteTime(mmdModelPath);
                }
            }
            else
            {
                this.mmdModel = null;
                this.mmdModelLastWriteTime = new DateTime();
            }
        }
        else
        {
            this.mmdModel = null;
            this.mmdModelLastWriteTime = new DateTime();
        }

        if (this.fbxAsset != null)
        {
            if (this.indexAsset == null || this.indexData == null)
            {
                string indexDataPath = GetIndexDataPath(this.fbxAssetPath);
                if (File.Exists(indexDataPath))
                {
                    TextAsset indexFile = AssetDatabase.LoadAssetAtPath(indexDataPath, typeof(TextAsset)) as TextAsset;
                    if (indexFile != null)
                    {
                        this.indexData = AuxData.BuildIndexData(indexFile);
                        if (this.indexData != null)
                            this.indexAsset = indexFile;
                    }
                }
            }

            if (this.vertexAsset == null || this.vertexData == null)
            {
                string vertexDataPath = GetVertexDataPath(this.fbxAssetPath);
                if (File.Exists(vertexDataPath))
                {
                    TextAsset vertexFile =
                        AssetDatabase.LoadAssetAtPath(vertexDataPath, typeof(TextAsset)) as TextAsset;
                    if (vertexFile != null)
                    {
                        this.vertexData = AuxData.BuildVertexData(vertexFile);
                        if (this.vertexData != null)
                            this.vertexAsset = vertexFile;
                    }
                }
            }
        }
        else
        {
            this.indexData = null;
            this.vertexData = null;
        }

        this._CheckFBXMaterial();
    }

    public bool CheckFBXMaterialVertex()
    {
        if (this.isProcessing)
            return false;
        this._CheckFBXMaterialVertex();
        return true;
    }

    public void _CheckModelInScene(
        List<Animator> assetAnimators,
        List<string> assetPaths,
        List<bool> assetIsSkinned)
    {
        if (this.isProcessing || !this.Setup() || string.IsNullOrEmpty(this.fbxAssetPath))
            return;
        for (int index = 0; index < assetPaths.Count; ++index)
        {
            string assetPath = assetPaths[index];
            if (!string.IsNullOrEmpty(assetPath) && assetPath == this.fbxAssetPath)
            {
                if (assetIsSkinned[index])
                {
                    string modelDataPath = GetModelDataPath(assetPath);
                    string indexDataPath = GetIndexDataPath(assetPath);
                    string vertexDataPath = GetVertexDataPath(assetPath);
                    if (File.Exists(modelDataPath) && File.Exists(indexDataPath))
                    {
                        TextAsset modelData =
                            AssetDatabase.LoadAssetAtPath(modelDataPath, typeof(TextAsset)) as TextAsset;
                        TextAsset indexData =
                            AssetDatabase.LoadAssetAtPath(indexDataPath, typeof(TextAsset)) as TextAsset;
                        TextAsset vertexData =
                            AssetDatabase.LoadAssetAtPath(vertexDataPath, typeof(TextAsset)) as TextAsset;
                        this._MakeModel(assetAnimators[index].gameObject, modelData, indexData, vertexData,
                            assetIsSkinned[index]);
                    }
                }
                else
                {
                    string modelDataPath = GetModelDataPath(assetPath);
                    if (File.Exists(modelDataPath))
                    {
                        TextAsset modelData =
                            AssetDatabase.LoadAssetAtPath(modelDataPath, typeof(TextAsset)) as TextAsset;
                        this._MakeModel(assetAnimators[index].gameObject, modelData, (TextAsset) null, (TextAsset) null,
                            assetIsSkinned[index]);
                    }
                }
            }
        }
    }

    private void _MakeModel(
        GameObject modelGameObject,
        TextAsset modelData,
        TextAsset indexData,
        TextAsset vertexData,
        bool isSkinned)
    {
        if (!(modelData != null) ||
            isSkinned && !(indexData != null))
            return;
        MMD4MecanimModelImpl mecanimModelImpl = modelGameObject.AddComponent<MMD4MecanimModelImpl>();
        mecanimModelImpl.importScale = this.fbxAssertImportScale;
        mecanimModelImpl.modelFile = modelData;
        mecanimModelImpl.indexFile = indexData;
        mecanimModelImpl.vertexFile = vertexData;
        if (this.pmx2fbxProperty.vmdAssetList == null)
            return;
        foreach (Object vmdAsset in this.pmx2fbxProperty.vmdAssetList)
        {
            string assetPath = AssetDatabase.GetAssetPath(vmdAsset);
            if (!string.IsNullOrEmpty(assetPath))
            {
                TextAsset textAsset =
                    AssetDatabase.LoadAssetAtPath(GetAnimDataPath(assetPath),
                        typeof(TextAsset)) as TextAsset;
                if (textAsset != null)
                {
                    if (mecanimModelImpl.animList == null)
                        mecanimModelImpl.animList = new MMD4MecanimModelImpl.Anim[1];
                    else
                        Array.Resize(ref mecanimModelImpl.animList,
                            mecanimModelImpl.animList.Length + 1);
                    MMD4MecanimModelImpl.Anim anim = new MMD4MecanimModelImpl.Anim();
                    anim.animFile = textAsset;
                    anim.animatorStateName = "Base Layer." + Path.GetFileNameWithoutExtension(textAsset.name) + "_vmd";
                    mecanimModelImpl.animList[mecanimModelImpl.animList.Length - 1] = anim;
                }
            }
        }
    }

    public bool Setup() => this.pmx2fbxProperty != null && this.pmx2fbxConfig != null || this._Setup();

    public bool SetupWithReload() => _Setup();

    private bool _Setup()
    {
        string withoutExtension = this._GetScriptAssetPathWithoutExtension();
        Debug.Log("*****withoutExtension: " + withoutExtension);
        if (string.IsNullOrEmpty(withoutExtension))
            return false;
        if (this.pmx2fbxProperty == null)
            this.pmx2fbxProperty = new PMX2FBXProperty();
        if (this.pmx2fbxConfig == null)
        {
            this.pmx2fbxConfig =
                GetPMX2FBXConfig(
                    (withoutExtension + ".MMD4Mecanim.xml").Normalize(NormalizationForm.FormC));
            if (this.pmx2fbxConfig == null)
            {
                this.pmx2fbxConfig =
                    GetPMX2FBXConfig(GetPMX2FBXRootConfigPath());
                if (this.pmx2fbxConfig == null)
                    this.pmx2fbxConfig = new PMX2FBXConfig();
                else
                    this.pmx2fbxConfig.renameList = null;
            }

            if (this.pmx2fbxConfig != null && this.pmx2fbxConfig.mmd4MecanimProperty != null)
            {
                PMX2FBXConfig.MMD4MecanimProperty mmd4MecanimProperty =
                    this.pmx2fbxConfig.mmd4MecanimProperty;
                if (this.pmx2fbxProperty.pmxAsset == null)
                {
                    string pmxAssetPath = mmd4MecanimProperty.pmxAssetPath;
                    if (File.Exists(pmxAssetPath))
                        this.pmx2fbxProperty.pmxAsset =
                            AssetDatabase.LoadAssetAtPath(pmxAssetPath, typeof(Object));
                }

                this.pmx2fbxProperty.vmdAssetList = new List<Object>();
                if (mmd4MecanimProperty.vmdAssetPathList != null)
                {
                    for (int index = 0; index < mmd4MecanimProperty.vmdAssetPathList.Count; ++index)
                    {
                        Object @object =
                            AssetDatabase.LoadAssetAtPath(mmd4MecanimProperty.vmdAssetPathList[index],
                                typeof(Object));
                        if (@object != (Object) null)
                            this.pmx2fbxProperty.vmdAssetList.Add(@object);
                    }
                }

                if (this.fbxAsset == null)
                {
                    string fbxAssetPath = mmd4MecanimProperty.fbxAssetPath;
                    if (!string.IsNullOrEmpty(fbxAssetPath) && File.Exists(fbxAssetPath))
                    {
                        this.fbxAsset = AssetDatabase.LoadAssetAtPath(fbxAssetPath, typeof(GameObject)) as GameObject;
                        this.fbxAssetPath = fbxAssetPath;
                    }

                    if (this.fbxAsset == null)
                    {
                        string fbxOutputPath = mmd4MecanimProperty.fbxOutputPath;
                        if (!string.IsNullOrEmpty(fbxOutputPath) && File.Exists(fbxOutputPath))
                        {
                            this.fbxAsset =
                                AssetDatabase.LoadAssetAtPath(fbxOutputPath, typeof(GameObject)) as GameObject;
                            this.fbxAssetPath = fbxOutputPath;
                        }
                    }
                }
            }
        }

        if (this.pmx2fbxConfig == null)
            this.pmx2fbxConfig = new PMX2FBXConfig();
        if (this.pmx2fbxConfig.globalSettings == null)
            this.pmx2fbxConfig.globalSettings = new PMX2FBXConfig.GlobalSettings();
        if (this.pmx2fbxConfig.bulletPhysics == null)
            this.pmx2fbxConfig.bulletPhysics = new PMX2FBXConfig.BulletPhysics();
        if (this.pmx2fbxConfig.renameList == null)
            this.pmx2fbxConfig.renameList = new List<PMX2FBXConfig.Rename>();
        if (this.pmx2fbxConfig.freezeRigidBodyList == null)
            this.pmx2fbxConfig.freezeRigidBodyList = new List<PMX2FBXConfig.FreezeRigidBody>();
        if (this.pmx2fbxConfig.freezeMotionList == null)
            this.pmx2fbxConfig.freezeMotionList = new List<PMX2FBXConfig.FreezeMotion>();
        if (this.pmx2fbxConfig.mmd4MecanimProperty == null)
            this.pmx2fbxConfig.mmd4MecanimProperty = new PMX2FBXConfig.MMD4MecanimProperty();
        if (string.IsNullOrEmpty(this.pmx2fbxConfig.mmd4MecanimProperty.fbxOutputPath))
            this.pmx2fbxConfig.mmd4MecanimProperty.fbxOutputPath =
                (withoutExtension + ".fbx").Normalize(NormalizationForm.FormC);
        if (this.pmx2fbxProperty.pmxAsset == null)
        {
            string str = withoutExtension + ".pmx";
            if (File.Exists(str))
                this.pmx2fbxProperty.pmxAsset = AssetDatabase.LoadAssetAtPath(str, typeof(Object));
        }

        if (this.pmx2fbxProperty.pmxAsset == null)
        {
            string str = withoutExtension + ".pmd";
            if (File.Exists(str))
                this.pmx2fbxProperty.pmxAsset = AssetDatabase.LoadAssetAtPath(str, typeof(Object));
        }

        if (fbxAsset == null)
        {
            string str = withoutExtension + ".fbx";
            if (!string.IsNullOrEmpty(str) && File.Exists(str))
            {
                this.fbxAsset = AssetDatabase.LoadAssetAtPath(str, typeof(GameObject)) as GameObject;
                this.fbxAssetPath = str;
            }
        }

        if (this.mmdModel == null && fbxAsset != null)
        {
            string mmdModelPath = GetMMDModelPath(this.fbxAssetPath);
            if (!string.IsNullOrEmpty(mmdModelPath) && File.Exists(mmdModelPath))
            {
                this.mmdModel = GetMMDModel(mmdModelPath);
                this.mmdModelLastWriteTime = MMD4MecanimEditorCommon.GetLastWriteTime(mmdModelPath);
            }
        }

        if (string.IsNullOrEmpty(this.pmx2fbxConfig.mmd4MecanimProperty.winePath))
        {
            this.pmx2fbxConfig.mmd4MecanimProperty.wine = PMX2FBXConfig.Wine.Manual;
            this.pmx2fbxConfig.mmd4MecanimProperty.winePath = WinePaths[3];
            for (int index = 0; index < WinePaths.Length; ++index)
            {
                if (File.Exists(WinePaths[index]))
                {
                    this.pmx2fbxConfig.mmd4MecanimProperty.wine = (PMX2FBXConfig.Wine) index;
                    this.pmx2fbxConfig.mmd4MecanimProperty.winePath = WinePaths[index];
                    break;
                }
            }
        }

        return true;
    }

    public void SavePMX2FBXConfig()
    {
        if (this.pmx2fbxConfig == null || this.pmx2fbxProperty == null)
        {
            Debug.LogError("");
        }
        else
        {
            string withoutExtension = this._GetScriptAssetPathWithoutExtension();
            if (string.IsNullOrEmpty(withoutExtension))
            {
                Debug.LogWarning("Not found script.");
            }
            else
            {
                string str = (withoutExtension + ".MMD4Mecanim.xml").Normalize(NormalizationForm.FormC);
                Debug.Log("xmlPath: " + str);
                if (this.pmx2fbxConfig.mmd4MecanimProperty != null)
                {
                    this.pmx2fbxConfig.mmd4MecanimProperty.pmxAssetPath =
                        !(this.pmx2fbxProperty.pmxAsset != null)
                            ? (string) null
                            : AssetDatabase.GetAssetPath(this.pmx2fbxProperty.pmxAsset);
                    if (this.pmx2fbxProperty.vmdAssetList != null)
                    {
                        this.pmx2fbxConfig.mmd4MecanimProperty.vmdAssetPathList = new List<string>();
                        foreach (Object vmdAsset in this.pmx2fbxProperty.vmdAssetList)
                            this.pmx2fbxConfig.mmd4MecanimProperty.vmdAssetPathList.Add(
                                AssetDatabase.GetAssetPath(vmdAsset));
                    }
                    else
                        this.pmx2fbxConfig.mmd4MecanimProperty.vmdAssetPathList = (List<string>) null;

                    this.pmx2fbxConfig.mmd4MecanimProperty.fbxAssetPath =
                        !(fbxAsset != null)
                            ? null
                            : AssetDatabase.GetAssetPath(fbxAsset);
                }

                WritePMX2FBXConfig(str, this.pmx2fbxConfig);
                AssetDatabase.ImportAsset(str,
                    ImportAssetOptions.ForceUpdate | ImportAssetOptions.ForceSynchronousImport);
                AssetDatabase.Refresh();
            }
        }
    }

    public bool isProcessing => this._pmx2fbxProcess != null || _pmx2fbxProcessingCount > 0;

    public void ProcessPMX2FBX()
    {
        if (this.pmx2fbxConfig == null || this.pmx2fbxConfig.mmd4MecanimProperty == null ||
            this.pmx2fbxProperty == null)
        {
            Debug.LogError((object) "");
        }
        else
        {
            bool useWineFlag = this.pmx2fbxConfig.mmd4MecanimProperty.useWineFlag;
            string pmX2FbxPath = GetPMX2FBXPath(useWineFlag,
                this.pmx2fbxConfig.mmd4MecanimProperty.bulletPhysicsVersion);
            if (pmX2FbxPath == null)
                Debug.LogError((object) "");
            else if (this._pmx2fbxProcess != null || _pmx2fbxProcessingCount > 0)
            {
                Debug.LogWarning((object) "Already processing pmx2fbx. Please wait.");
            }
            else
            {
                string withoutExtension = this._GetScriptAssetPathWithoutExtension();
                if (string.IsNullOrEmpty(withoutExtension))
                {
                    Debug.LogWarning((object) "Not found script.");
                }
                else
                {
                    string str = (withoutExtension + ".MMD4Mecanim.xml").Normalize(NormalizationForm.FormC);
                    if (string.IsNullOrEmpty(this.pmx2fbxConfig.mmd4MecanimProperty.pmxAssetPath))
                    {
                        Debug.LogError((object) "PMX/PMD Path is null.");
                    }
                    else
                    {
                        string path = Application.dataPath;
                        if (path.EndsWith("Assets"))
                            path = Path.GetDirectoryName(path);
                        StringBuilder stringBuilder = new StringBuilder();
                        if (Application.platform != RuntimePlatform.WindowsEditor && useWineFlag)
                        {
                            stringBuilder.Append("\"");
                            stringBuilder.Append(path + "/" + pmX2FbxPath);
                            stringBuilder.Append("\" ");
                        }

                        stringBuilder.Append("-o \"");
                        stringBuilder.Append(path + "/" + this.pmx2fbxConfig.mmd4MecanimProperty.fbxOutputPath);
                        stringBuilder.Append("\" -conf \"");
                        stringBuilder.Append(path + "/" + str);
                        stringBuilder.Append("\" \"");
                        stringBuilder.Append(path + "/" + this.pmx2fbxConfig.mmd4MecanimProperty.pmxAssetPath);
                        stringBuilder.Append("\"");
                        if (this.pmx2fbxConfig.mmd4MecanimProperty.vmdAssetPathList != null)
                        {
                            foreach (string vmdAssetPath in this.pmx2fbxConfig.mmd4MecanimProperty.vmdAssetPathList)
                            {
                                stringBuilder.Append(" \"");
                                stringBuilder.Append(path + "/" + vmdAssetPath);
                                stringBuilder.Append("\"");
                            }
                        }

                        this._pmx2fbxProcess = new System.Diagnostics.Process();
                        ++_pmx2fbxProcessingCount;
                        if (Application.platform == RuntimePlatform.WindowsEditor || !useWineFlag)
                        {
                            this._pmx2fbxProcess.StartInfo.FileName = path + "/" + pmX2FbxPath;
                        }
                        else
                        {
                            string winePath =
                                WinePaths[(int) this.pmx2fbxConfig.mmd4MecanimProperty.wine];
                            if (this.pmx2fbxConfig.mmd4MecanimProperty.wine ==
                                PMX2FBXConfig.Wine.Manual)
                                winePath = this.pmx2fbxConfig.mmd4MecanimProperty.winePath;
                            this._pmx2fbxProcess.StartInfo.FileName = winePath;
                        }

                        this._pmx2fbxProcess.StartInfo.Arguments = stringBuilder.ToString();
                        Debug.Log("-----Arg-----");
                        Debug.Log(stringBuilder.ToString());
                        this._pmx2fbxProcess.EnableRaisingEvents = true;
                        this._pmx2fbxProcess.Exited += new EventHandler(this._pmx2fbx_OnExited);
                        if (this._pmx2fbxProcess.Start())
                            return;
                        this._pmx2fbxProcess.Dispose();
                        this._pmx2fbxProcess = null;
                    }
                }
            }
        }
    }

    private void _pmx2fbx_OnExited(object sender, EventArgs e)
    {
        Debug.Log("Processed pmx2fbx.");
        this._pmx2fbxProcess.Dispose();
        this._pmx2fbxProcess = null;
        if (--_pmx2fbxProcessingCount < 0)
            _pmx2fbxProcessingCount = 0;
        MMD4MecanimImporterImplEditor._isProcessedPMX2FBX = true;
    }

    public float fbxAssertImportScale
    {
        get
        {
            if (!string.IsNullOrEmpty(this.fbxAssetPath))
            {
                ModelImporter atPath = (ModelImporter) AssetImporter.GetAtPath(this.fbxAssetPath);
                if ((Object) atPath != (Object) null)
                    return MMD4MecanimEditorCommon.GetModelImportScale(atPath);
                Debug.LogWarning((object) ("ModelImporter is not found. " + this.fbxAssetPath));
            }

            return 0.0f;
        }
    }

    public static string[] FindReadmeFiles(string assetPath)
    {
        if (string.IsNullOrEmpty(assetPath))
        {
            Debug.LogError((object) "");
            return (string[]) null;
        }

        string directoryName = Path.GetDirectoryName(assetPath);
        if (string.IsNullOrEmpty(directoryName))
        {
            Debug.LogError((object) "");
            return (string[]) null;
        }

        string[] files = Directory.GetFiles(directoryName);
        if (files == null)
        {
            Debug.LogError((object) "");
            return (string[]) null;
        }

        List<string> stringList1 = new List<string>();
        List<string> stringList2 = new List<string>();
        List<string> stringList3 = new List<string>();
        try
        {
            foreach (string path in files)
            {
                bool flag1 = false;
                string extension = Path.GetExtension(path);
                if (!string.IsNullOrEmpty(extension))
                {
                    string lower1 = extension.ToLower();
                    for (int index = 0; index < ReadmeExtNames.Length; ++index)
                    {
                        if (lower1 == ReadmeExtNames[index])
                        {
                            flag1 = true;
                            break;
                        }
                    }

                    if (flag1)
                    {
                        string withoutExtension = Path.GetFileNameWithoutExtension(path);
                        bool flag2 = false;
                        for (int index = 0;
                             index < ReadmeFileNames_Contains_Japanese.Length;
                             ++index)
                        {
                            if (withoutExtension.Contains(
                                    ReadmeFileNames_Contains_Japanese[index]))
                            {
                                stringList1.Add(path);
                                flag2 = true;
                                break;
                            }
                        }

                        if (!flag2)
                        {
                            string lower2 = withoutExtension.ToLower();
                            for (int index = 0;
                                 index < ReadmeFileNames_StartsWith.Length;
                                 ++index)
                            {
                                if (lower2.StartsWith(ReadmeFileNames_StartsWith[index]))
                                {
                                    if (lower2.Contains("english"))
                                        stringList2.Add(path);
                                    else
                                        stringList2.Insert(0, path);
                                    flag2 = true;
                                    break;
                                }
                            }

                            if (!flag2)
                                stringList3.Add(path);
                        }
                    }
                }
            }

            for (int index = 0; index < stringList2.Count; ++index)
                stringList1.Add(stringList2[index]);
            for (int index = 0; index < stringList3.Count; ++index)
                stringList1.Add(stringList3[index]);
        }
        catch (Exception ex)
        {
            stringList1.Clear();
            stringList2.Clear();
        }

        return stringList1.ToArray();
    }

    public string[] FindReadmeFiles()
    {
        if (this.pmx2fbxProperty == null || this.pmx2fbxProperty.pmxAsset == (Object) null)
        {
            Debug.LogError((object) "");
            return (string[]) null;
        }

        string assetPath = AssetDatabase.GetAssetPath(this.pmx2fbxProperty.pmxAsset);
        if (string.IsNullOrEmpty(assetPath))
        {
            Debug.LogError((object) "");
            return (string[]) null;
        }

        string[] readmeFiles1 = FindReadmeFiles(assetPath);
        if (readmeFiles1 != null)
        {
            for (int index = 0; index < readmeFiles1.Length; ++index)
            {
                try
                {
                    string str = _ReadReadmeText(readmeFiles1[index]);
                    if (!string.IsNullOrEmpty(str))
                    {
                        if (str.Contains(LicenceImportantString))
                            return readmeFiles1;
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }

        string[] readmeFiles2 = FindReadmeFiles(Path.GetDirectoryName(assetPath));
        if (readmeFiles2 == null || readmeFiles2.Length == 0)
            return readmeFiles1;
        if (readmeFiles1 == null)
            return readmeFiles2;
        string[] destinationArray = new string[readmeFiles2.Length + readmeFiles1.Length];
        Array.Copy((Array) readmeFiles2, 0, (Array) destinationArray, 0, readmeFiles2.Length);
        Array.Copy((Array) readmeFiles1, 0, (Array) destinationArray, readmeFiles2.Length, readmeFiles1.Length);
        return destinationArray;
    }

    public static Encoding GetCode(byte[] bytes)
    {
        int length = bytes.Length;
        bool flag1 = false;
        for (int index = 0; index < length; ++index)
        {
            byte num = bytes[index];
            if (num <= (byte) 6 || num == (byte) 127 || num == byte.MaxValue)
            {
                flag1 = true;
                if (num == (byte) 0 && index < length - 1 && bytes[index + 1] <= (byte) 127)
                    return Encoding.Unicode;
            }
        }

        if (flag1)
            return (Encoding) null;
        bool flag2 = true;
        for (int index = 0; index < length; ++index)
        {
            byte num = bytes[index];
            if (num == (byte) 27 || (byte) 128 <= num)
            {
                flag2 = false;
                break;
            }
        }

        if (flag2)
            return Encoding.ASCII;
        for (int index = 0; index < length - 2; ++index)
        {
            byte num1 = bytes[index];
            byte num2 = bytes[index + 1];
            byte num3 = bytes[index + 2];
            if (num1 == (byte) 27)
            {
                if (num2 == (byte) 36 && num3 == (byte) 64 || num2 == (byte) 36 && num3 == (byte) 66 ||
                    num2 == (byte) 40 && (num3 == (byte) 66 || num3 == (byte) 74) ||
                    num2 == (byte) 40 && num3 == (byte) 73)
                    return Encoding.GetEncoding(50220);
                if (index < length - 3)
                {
                    byte num4 = bytes[index + 3];
                    if (num2 == (byte) 36 && num3 == (byte) 40 && num4 == (byte) 68 || index < length - 5 &&
                        num2 == (byte) 38 && num3 == (byte) 64 && num4 == (byte) 27 && bytes[index + 4] == (byte) 36 &&
                        bytes[index + 5] == (byte) 66)
                        return Encoding.GetEncoding(50220);
                }
            }
        }

        int num5 = 0;
        int num6 = 0;
        int num7 = 0;
        for (int index = 0; index < length - 1; ++index)
        {
            byte num8 = bytes[index];
            byte num9 = bytes[index + 1];
            if (((byte) 129 <= num8 && num8 <= (byte) 159 || (byte) 224 <= num8 && num8 <= (byte) 252) &&
                ((byte) 64 <= num9 && num9 <= (byte) 126 || (byte) 128 <= num9 && num9 <= (byte) 252))
            {
                num5 += 2;
                ++index;
            }
        }

        for (int index = 0; index < length - 1; ++index)
        {
            byte num10 = bytes[index];
            byte num11 = bytes[index + 1];
            if ((byte) 161 <= num10 && num10 <= (byte) 254 && (byte) 161 <= num11 && num11 <= (byte) 254 ||
                num10 == (byte) 142 && (byte) 161 <= num11 && num11 <= (byte) 223)
            {
                num6 += 2;
                ++index;
            }
            else if (index < length - 2)
            {
                byte num12 = bytes[index + 2];
                if (num10 == (byte) 143 && (byte) 161 <= num11 && num11 <= (byte) 254 && (byte) 161 <= num12 &&
                    num12 <= (byte) 254)
                {
                    num6 += 3;
                    index += 2;
                }
            }
        }

        for (int index = 0; index < length - 1; ++index)
        {
            byte num13 = bytes[index];
            byte num14 = bytes[index + 1];
            if ((byte) 192 <= num13 && num13 <= (byte) 223 && (byte) 128 <= num14 && num14 <= (byte) 191)
            {
                num7 += 2;
                ++index;
            }
            else if (index < length - 2)
            {
                byte num15 = bytes[index + 2];
                if ((byte) 224 <= num13 && num13 <= (byte) 239 && (byte) 128 <= num14 && num14 <= (byte) 191 &&
                    (byte) 128 <= num15 && num15 <= (byte) 191)
                {
                    num7 += 3;
                    index += 2;
                }
            }
        }

        if (num6 > num5 && num6 > num7)
            return Encoding.GetEncoding(51932);
        if (num5 > num6 && num5 > num7)
            return Encoding.GetEncoding(932);
        return num7 > num6 && num7 > num5 ? Encoding.UTF8 : (Encoding) null;
    }

    public LicenseAgree CheckLicenseAgree(
        ref string[] readmeFiles,
        ref string[] readmeTexts,
        ref bool isPiapro,
        ref bool isNitro,
        ref bool isKancolle)
    {
        readmeFiles = this.FindReadmeFiles();
        readmeTexts = (string[]) null;
        return CheckLicenseAgree(readmeFiles, ref readmeTexts, ref isPiapro, ref isNitro,
            ref isKancolle);
    }

    private static string _ReadReadmeText(string readmeFile)
    {
        string str = (string) null;
        if (string.IsNullOrEmpty(str))
        {
            try
            {
                byte[] bytes = File.ReadAllBytes(readmeFile);
                if (bytes != null)
                {
                    Encoding code = GetCode(bytes);
                    if (code != null)
                        str = code.GetString(bytes);
                }
            }
            catch (Exception ex)
            {
                str = (string) null;
            }
        }

        if (string.IsNullOrEmpty(str))
        {
            try
            {
                str = File.ReadAllText(readmeFile);
            }
            catch (Exception ex)
            {
                str = (string) null;
            }
        }

        return str;
    }

    private static void _Swap(string[] a, int lhs, int rhs)
    {
        string str = a[lhs];
        a[lhs] = a[rhs];
        a[rhs] = str;
    }

    public static LicenseAgree CheckLicenseAgree(
        string[] readmeFiles,
        ref string[] readmeTexts,
        ref bool isPiapro,
        ref bool isNitro,
        ref bool isKancolle)
    {
        isPiapro = false;
        isKancolle = false;
        readmeTexts = (string[]) null;
        if (readmeFiles == null)
            return LicenseAgree.Error;
        readmeTexts = new string[readmeFiles.Length];
        if (readmeFiles.Length == 0)
            return LicenseAgree.Unknown;
        bool flag = false;
        for (int rhs = 0; rhs < readmeFiles.Length; ++rhs)
        {
            readmeTexts[rhs] = _ReadReadmeText(readmeFiles[rhs]);
            try
            {
                for (int index = 0; index < LicensePiaproStrings.Length; ++index)
                {
                    if (readmeTexts[rhs].Contains(LicensePiaproStrings[index]))
                    {
                        isPiapro = true;
                        break;
                    }
                }

                for (int index = 0; index < LicenseNitroStrings.Length; ++index)
                {
                    if (readmeTexts[rhs].Contains(LicenseNitroStrings[index]))
                    {
                        isNitro = true;
                        break;
                    }
                }

                for (int index = 0; index < LicenseKancolleStrings.Length; ++index)
                {
                    if (readmeTexts[rhs].Contains(LicenseKancolleStrings[index]))
                    {
                        isKancolle = true;
                        break;
                    }
                }

                for (int index = 0; index < LicenseDeniedStrings.Length; ++index)
                {
                    if (readmeTexts[rhs].Contains(LicenseDeniedStrings[index]))
                    {
                        flag = true;
                        if (rhs > 0)
                        {
                            _Swap(readmeFiles, 0, rhs);
                            _Swap(readmeTexts, 0, rhs);
                            break;
                        }

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        return flag ? LicenseAgree.Denied : LicenseAgree.Warning;
    }

    public string[] DecodeModelComments()
    {
        if (this.pmx2fbxProperty == null || this.pmx2fbxProperty.pmxAsset == (Object) null)
        {
            Debug.LogError((object) "");
            return (string[]) null;
        }

        string assetPath = AssetDatabase.GetAssetPath(this.pmx2fbxProperty.pmxAsset);
        if (string.IsNullOrEmpty(assetPath))
        {
            Debug.LogError((object) "");
            return (string[]) null;
        }

        byte[] fileBytes;
        try
        {
            fileBytes = File.ReadAllBytes(assetPath);
            if (fileBytes == null)
            {
                Debug.LogError((object) "");
                return (string[]) null;
            }
        }
        catch (Exception ex)
        {
            return (string[]) null;
        }

        return DecodeModelComments(fileBytes, Path.GetExtension(assetPath));
    }

    public static string ReadPMDString(System.IO.BinaryReader reader, int maxLen)
    {
        if (reader == null)
        {
            Debug.LogError((object) "");
            return (string) null;
        }

        if (maxLen > 0)
        {
            byte[] bytes = reader.ReadBytes(maxLen);
            if (bytes != null)
            {
                int count = 0;
                int index = 0;
                while (index < maxLen && bytes[index] != (byte) 0)
                {
                    ++index;
                    ++count;
                }

                Encoding encoding = Encoding.GetEncoding(932);
                if (encoding != null)
                    return encoding.GetString(bytes, 0, count);
            }
        }

        return "";
    }

    public static string ReadPMXString(System.IO.BinaryReader reader, PMXEncoding enc)
    {
        if (reader == null)
        {
            Debug.LogError((object) "");
            return (string) null;
        }

        uint count = reader.ReadUInt32();
        if (count > 0U)
        {
            byte[] bytes = reader.ReadBytes((int) count);
            if (bytes != null)
            {
                if (enc == PMXEncoding.UTF16)
                    return Encoding.Unicode.GetString(bytes);
                if (enc == PMXEncoding.UTF8)
                    return Encoding.UTF8.GetString(bytes);
            }
        }

        return "";
    }

    public static string[] DecodeModelComments(byte[] fileBytes, string extension)
    {
        if (string.IsNullOrEmpty(extension))
        {
            Debug.LogError((object) "");
            return (string[]) null;
        }

        using (MemoryStream input = new MemoryStream(fileBytes))
        {
            using (System.IO.BinaryReader reader = new System.IO.BinaryReader((Stream) input))
            {
                try
                {
                    extension = extension.ToLower();
                    if (extension == ".pmx")
                    {
                        byte[] numArray1 = reader.ReadBytes(4);
                        if (numArray1 == null || numArray1.Length != 4 || numArray1[0] != (byte) 80 ||
                            numArray1[1] != (byte) 77 || numArray1[2] != (byte) 88 || numArray1[3] != (byte) 32)
                        {
                            Debug.LogError((object) "");
                            return (string[]) null;
                        }

                        double num = (double) reader.ReadSingle();
                        byte count = reader.ReadByte();
                        if (count == (byte) 0)
                        {
                            Debug.LogError((object) "");
                            return (string[]) null;
                        }

                        byte[] numArray2 = reader.ReadBytes((int) count);
                        if (numArray2 == null || numArray2.Length != (int) count)
                        {
                            Debug.LogError((object) "");
                            return (string[]) null;
                        }

                        PMXEncoding enc = (PMXEncoding) numArray2[0];
                        switch (enc)
                        {
                            case PMXEncoding.UTF16:
                            case PMXEncoding.UTF8:
                                return new string[4]
                                {
                                    ReadPMXString(reader, enc),
                                    ReadPMXString(reader, enc),
                                    ReadPMXString(reader, enc),
                                    ReadPMXString(reader, enc)
                                };
                            default:
                                Debug.LogError((object) "");
                                return (string[]) null;
                        }
                    }
                    else
                    {
                        if (extension == ".pmd")
                        {
                            byte[] numArray = reader.ReadBytes(3);
                            if (numArray == null || numArray.Length != 3 || numArray[0] != (byte) 80 ||
                                numArray[1] != (byte) 109 || numArray[2] != (byte) 100)
                            {
                                Debug.LogError((object) "");
                                return (string[]) null;
                            }

                            double num = (double) reader.ReadSingle();
                            return new string[4]
                            {
                                ReadPMDString(reader, 20),
                                null,
                                ReadPMDString(reader, 256),
                                null
                            };
                        }

                        Debug.LogError((object) "");
                        return (string[]) null;
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogError((object) "");
                    return (string[]) null;
                }
            }
        }
    }

    private void _OnInspectorGUI_Animations()
    {
        if (this.pmx2fbxConfig == null || this.pmx2fbxConfig.mmd4MecanimProperty == null)
            return;
        GUILayout.Label("FBX", EditorStyles.boldLabel);
        GUILayout.BeginHorizontal();
        GUILayout.Space(26f);
        this._OnInspectorGUI_ShowFBXField();
        GUILayout.EndHorizontal();
        EditorGUILayout.Separator();
        EditorGUILayout.LabelField("Split Animations", EditorStyles.boldLabel);
        AnimationClip[] animationClips = this.GetAnimationClips();
        if (animationClips != null)
        {
            foreach (Object @object in animationClips)
                EditorGUILayout.TextField(@object.name);
        }
        else
            EditorGUILayout.TextField("No Animation.");

        GUI.enabled = animationClips != null;
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Process"))
            this.SplitAnimations(animationClips);
        GUILayout.EndHorizontal();
        GUI.enabled = true;
    }

    public AnimationClip[] GetAnimationClips()
    {
        if (!this.Setup())
        {
            Debug.LogError((object) "");
            return (AnimationClip[]) null;
        }

        if ((Object) this.fbxAsset == (Object) null || string.IsNullOrEmpty(this.fbxAssetPath))
            return (AnimationClip[]) null;
        Object[] objectArray = AssetDatabase.LoadAllAssetsAtPath(this.fbxAssetPath);
        if (objectArray == null || objectArray.Length == 0)
            return (AnimationClip[]) null;
        int length = 0;
        foreach (Object @object in objectArray)
        {
            AnimationClip animationClip = @object as AnimationClip;
            if ((Object) animationClip != (Object) null && animationClip.name != "Null" &&
                !animationClip.name.StartsWith("__preview__"))
                ++length;
        }

        if (length == 0)
            return (AnimationClip[]) null;
        AnimationClip[] animationClips = new AnimationClip[length];
        int index = 0;
        foreach (Object @object in objectArray)
        {
            AnimationClip animationClip = @object as AnimationClip;
            if ((Object) animationClip != (Object) null && animationClip.name != "Null" &&
                !animationClip.name.StartsWith("__preview__"))
            {
                animationClips[index] = animationClip;
                ++index;
            }
        }

        return animationClips;
    }

    public void SplitAnimations(AnimationClip[] animationClips)
    {
        if (!this.Setup())
            Debug.LogError((object) "");
        else if ((Object) this.fbxAsset == (Object) null ||
                 string.IsNullOrEmpty(this.fbxAssetPath))
        {
            Debug.LogError((object) "");
        }
        else
        {
            string directoryName = Path.GetDirectoryName(this.fbxAssetPath);
            string str = Path.Combine(directoryName, "Animations");
            if (!Directory.Exists(str))
                AssetDatabase.CreateFolder(directoryName, "Animations");
            foreach (AnimationClip animationClip in animationClips)
            {
                string path = Path.Combine(str, animationClip.name + ".anim");
                AssetDatabase.CreateAsset(
                    (Object) Object.Instantiate<AnimationClip>(animationClip), path);
            }
        }
    }

    private static bool IsNotZeroRGB(Color color) =>
        (double) color.r > 0.0 || (double) color.g > 0.0 || (double) color.b > 0.0;

    private static double _Clamp01(double r)
    {
        if (r > 1.0)
            return 1.0;
        return r < 0.0 ? 0.0 : r;
    }

    private static float _Clamp01(float r)
    {
        if ((double) r > 1.0)
            return 1f;
        return (double) r < 0.0 ? 0.0f : r;
    }

    private static Color _Clamp01(Color c) => new Color(_Clamp01(c.r),
        _Clamp01(c.g), _Clamp01(c.b),
        _Clamp01(c.a));

    private static bool _HasTextureTransparency(Texture texture)
    {
        if ((Object) texture == (Object) null)
        {
            Debug.LogError((object) "_HasTextureTransparency:Unknown Flow.");
            return false;
        }

        Texture2D texture2D = texture as Texture2D;
        if ((Object) texture2D == (Object) null)
        {
            Debug.LogError((object) ("_HasTextureTransparency:Unknown Flow." + texture.name));
            return false;
        }

        foreach (Color32 color32 in texture2D.GetPixels32())
        {
            if (color32.a != byte.MaxValue)
                return true;
        }

        return false;
    }

    public static bool HasTextureTransparency(Texture texture)
    {
        if ((Object) texture == (Object) null)
        {
            Debug.LogWarning((object) "Texture is null.");
            return false;
        }

        string assetPath = AssetDatabase.GetAssetPath((Object) texture);
        TextureImporter atPath = AssetImporter.GetAtPath(assetPath) as TextureImporter;
        if ((Object) atPath == (Object) null)
            return false;
        if (atPath.alphaIsTransparency)
            return true;
        bool isReadable = atPath.isReadable;
        atPath.isReadable = true;
        MMD4MecanimEditorCommon.UpdateImportSettings(assetPath);
        int num = _HasTextureTransparency(texture) ? 1 : 0;
        if (num != 0 && !atPath.alphaIsTransparency)
            atPath.alphaIsTransparency = true;
        atPath.isReadable = isReadable;
        MMD4MecanimEditorCommon.UpdateImportSettings(assetPath);
        return num != 0;
    }

    private static void _ComputeSphereCubeToStandardParameters(
        Texture texture,
        out StandardParameters standardParameters)
    {
        standardParameters = new StandardParameters();
        if ((Object) texture == (Object) null)
        {
            Debug.LogError((object) "_ComputeGlossiness:Unknown Flow. (Texture is null.)");
        }
        else
        {
            Texture2D texture2D = texture as Texture2D;
            if ((Object) texture2D == (Object) null)
            {
                Debug.LogError((object) ("_ComputeGlossiness:Unknown Flow. (Texture is not 2D) " +
                                                     texture.name));
            }
            else
            {
                int width = texture2D.width;
                int height = texture2D.height;
                double num1 = (double) (width - 1);
                double num2 = (double) (height - 1);
                double num3 = num1 / 2.0;
                double num4 = num2 / 2.0;
                Color32[] pixels32 = texture2D.GetPixels32();
                ulong num5 = 0;
                ulong num6 = 0;
                ulong num7 = 0;
                ulong num8 = 0;
                ulong num9 = 0;
                int num10 = 0;
                int index = 0;
                for (; num10 < height; ++num10)
                {
                    double num11 = ((double) num10 - num4) / num4;
                    int num12 = 0;
                    while (num12 < width)
                    {
                        double num13 = ((double) num12 - num3) / num3;
                        double d = num13 * num13 + num11 * num11;
                        if ((d > double.Epsilon ? Math.Sqrt(d) : 0.0) < 0.98)
                        {
                            Color32 color32 = pixels32[index];
                            num5 += (ulong) ((int) color32.r + (int) color32.g + (int) color32.b) / 3UL;
                            num7 += (ulong) color32.r;
                            num8 += (ulong) color32.g;
                            num9 += (ulong) color32.b;
                            ++num6;
                        }

                        ++num12;
                        ++index;
                    }
                }

                if (num6 == 0UL)
                    return;
                double num14 = _Clamp01((double) (num5 / num6) / (double) byte.MaxValue);
                double r = _Clamp01((double) (num7 / num6) / (double) byte.MaxValue);
                double g = _Clamp01((double) (num8 / num6) / (double) byte.MaxValue);
                double b = _Clamp01((double) (num9 / num6) / (double) byte.MaxValue);
                standardParameters.metallicGlossiness = (float) num14;
                standardParameters.metallic = 0.0f;
                standardParameters.specularGlossiness = 1f;
                standardParameters.specularColor = new Color((float) r, (float) g, (float) b, 1f);
            }
        }
    }

    public static void ComputeSphereCubeToStandardParameters(
        Texture texture,
        out StandardParameters standardParameters)
    {
        standardParameters = new StandardParameters();
        if ((Object) texture == (Object) null)
            return;
        string assetPath = AssetDatabase.GetAssetPath((Object) texture);
        TextureImporter atPath = AssetImporter.GetAtPath(assetPath) as TextureImporter;
        if ((Object) atPath == (Object) null)
            return;
        bool isReadable = atPath.isReadable;
        TextureImporterType textureType = atPath.textureType;
        TextureImporterGenerateCubemap generateCubemap = atPath.generateCubemap;
        MMD4MecanimEditorCommon._TextureImporterShape textureShape = MMD4MecanimEditorCommon.GetTextureShape(atPath);
        atPath.isReadable = true;
        MMD4MecanimEditorCommon.SetTextureTypeToDefault(atPath);
        MMD4MecanimEditorCommon.SetGenerateCubemapToNone(atPath);
        MMD4MecanimEditorCommon.SetTextureShape(atPath, MMD4MecanimEditorCommon._TextureImporterShape.Texture2D);
        MMD4MecanimEditorCommon.UpdateImportSettings(assetPath);
        texture = (Texture) AssetDatabase.LoadAssetAtPath(assetPath, typeof(Texture));
        _ComputeSphereCubeToStandardParameters(texture, out standardParameters);
        atPath.isReadable = isReadable;
        atPath.textureType = textureType;
        atPath.generateCubemap = generateCubemap;
        MMD4MecanimEditorCommon.SetTextureShape(atPath, textureShape);
        MMD4MecanimEditorCommon.UpdateImportSettings(assetPath);
    }

    private static int _ToMaterialIndex(
        MMDModel mmdModel,
        string materialName)
    {
        if (mmdModel != null && mmdModel.materialList != null && !string.IsNullOrEmpty(materialName))
        {
            for (int materialIndex = 0; materialIndex < mmdModel.materialList.Length; ++materialIndex)
            {
                MMDModel.Material material = mmdModel.materialList[materialIndex];
                if (material != null && !string.IsNullOrEmpty(material.surfaceName) &&
                    material.surfaceName == materialName)
                    return materialIndex;
            }
        }

        return -1;
    }

    private static int _ComputeFBXMaterialList(
        Material[] materials,
        string fbxAssetPath,
        string fbxAssetDirectoryName,
        Dictionary<int, Material> materialDict,
        out int[] materialNoList,
        MMDModel mmdModel)
    {
        List<int> intList = new List<int>();
        int fbxMaterialList = 0;
        if (materials != null)
        {
            foreach (Material material in materials)
            {
                if (!((Object) material == (Object) null))
                {
                    string assetPath = AssetDatabase.GetAssetPath((Object) material);
                    if (string.IsNullOrEmpty(assetPath))
                    {
                        Debug.LogWarning((object) ("Material is invalid. Skip this material." +
                                                               material.name));
                    }
                    else
                    {
                        string str = (string) null;
                        if (fbxAssetPath == assetPath)
                            str = material.name;
                        else if (fbxAssetDirectoryName == Path.GetDirectoryName(Path.GetDirectoryName(assetPath)))
                            str = Path.GetFileNameWithoutExtension(assetPath);
                        if (str != null)
                        {
                            int materialIndex = _ToMaterialIndex(mmdModel, str);
                            if (materialIndex < 0)
                                materialIndex = MMD4MecanimCommon.ToInt(str);
                            materialDict[materialIndex] = material;
                            fbxMaterialList = materialIndex + 1 > fbxMaterialList ? materialIndex + 1 : fbxMaterialList;
                            intList.Add(materialIndex);
                        }
                    }
                }
            }
        }

        materialNoList = intList.ToArray();
        return fbxMaterialList;
    }

    public static List<Material> _GetFBXMaterialList(
        GameObject fbxAsset,
        MMDModel mmdModel,
        out List<int[]> materialNoLists)
    {
        materialNoLists = new List<int[]>();
        if ((Object) fbxAsset == (Object) null)
            return (List<Material>) null;
        string assetPath = AssetDatabase.GetAssetPath((Object) fbxAsset);
        string directoryName = Path.GetDirectoryName(assetPath);
        Dictionary<int, Material> materialDict = new Dictionary<int, Material>();
        int num = 0;
        MeshRenderer[] meshRenderers = MMD4MecanimCommon.GetMeshRenderers(fbxAsset);
        if (meshRenderers != null)
        {
            foreach (Renderer renderer in meshRenderers)
            {
                int[] materialNoList;
                int fbxMaterialList = _ComputeFBXMaterialList(renderer.sharedMaterials,
                    assetPath, directoryName, materialDict, out materialNoList, mmdModel);
                if (num < fbxMaterialList)
                    num = fbxMaterialList;
                if (materialNoList != null && materialNoList.Length != 0)
                    materialNoLists.Add(materialNoList);
            }
        }

        SkinnedMeshRenderer[] skinnedMeshRenderers = MMD4MecanimCommon.GetSkinnedMeshRenderers(fbxAsset);
        if (skinnedMeshRenderers != null)
        {
            foreach (Renderer renderer in skinnedMeshRenderers)
            {
                int[] materialNoList;
                int fbxMaterialList = _ComputeFBXMaterialList(renderer.sharedMaterials,
                    assetPath, directoryName, materialDict, out materialNoList, mmdModel);
                if (num < fbxMaterialList)
                    num = fbxMaterialList;
                if (materialNoList != null && materialNoList.Length != 0)
                    materialNoLists.Add(materialNoList);
            }
        }

        List<Material> fbxMaterialList1 = new List<Material>();
        for (int key = 0; key < num; ++key)
        {
            Material material = (Material) null;
            if (materialDict.TryGetValue(key, out material))
                fbxMaterialList1.Add(material);
            else
                fbxMaterialList1.Add((Material) null);
        }

        return fbxMaterialList1;
    }

    private static List<MaterialHelper> _CreateMaterialHelpers(
        PMX2FBXConfig.MMD4MecanimProperty mmd4MecanimProperty,
        MMDModel mmdModel,
        List<Material> materialList,
        List<int[]> materialNoLists)
    {
        if (mmd4MecanimProperty == null || mmdModel == null || materialList == null)
            return (List<MaterialHelper>) null;
        List<MaterialHelper> materialHelpers =
            new List<MaterialHelper>(materialList.Count);
        List<MMDModel.Material> xmlMaterials =
            new List<MMDModel.Material>();
        for (int index = 0; index < materialList.Count; ++index)
            xmlMaterials.Add((Object) materialList[index] != (Object) null
                ? GetXmlMaterial(mmdModel, materialList[index].name)
                : (MMDModel.Material) null);
        for (int index = 0; index < materialList.Count; ++index)
            materialHelpers.Add(new MaterialHelper(xmlMaterials, materialNoLists, index,
                materialList[index], mmd4MecanimProperty));
        return materialHelpers;
    }

    private static void ClearStandardFounded() => _standardFounded = false;

    private static bool CheckStandard(bool isUseCache = false)
    {
        if (!_standardFounded || !isUseCache)
        {
            _standardFounded = true;
            _standardEnabled =
                (Object) Shader.Find("MMD4Mecanim/Standard/MMDLit") != (Object) null;
        }

        return _standardEnabled;
    }

    private void _OnInspectorGUI_Material()
    {
        if (this.pmx2fbxConfig == null || this.pmx2fbxConfig.mmd4MecanimProperty == null ||
            (Object) this.fbxAsset == (Object) null || this.mmdModel == null)
            return;
        CheckStandard(true);
        if (this.pmx2fbxConfig.mmd4MecanimProperty.materialGlobalProperty == null)
            this.pmx2fbxConfig.mmd4MecanimProperty.materialGlobalProperty =
                new PMX2FBXConfig.MaterialGlobalProperty();
        if (this.materialList == null)
            this.materialList =
                _GetFBXMaterialList(this.fbxAsset, this.mmdModel, out this.materialNoLists);
        if (this.materialList != null)
        {
            this.pmx2fbxConfig.PrepareMaterialPropertyList(this.materialList);
            this.pmx2fbxConfig.PrepareMaterialBlockPropertyList(this.materialList);
            if (this._materialHelpers == null || this._materialHelpers.Count != this.materialList.Count)
                this._materialHelpers = _CreateMaterialHelpers(
                    this.pmx2fbxConfig.mmd4MecanimProperty, this.mmdModel, this.materialList, this.materialNoLists);
        }

        bool flag1 = false;
        bool flag2 = false;
        GUILayout.Label("FBX", EditorStyles.boldLabel);
        GUILayout.BeginHorizontal();
        GUILayout.Space(26f);
        this._OnInspectorGUI_ShowFBXField();
        GUILayout.EndHorizontal();
        EditorGUILayout.Separator();
        PMX2FBXConfig.MaterialGlobalProperty materialGlobalProperty =
            this.pmx2fbxConfig.mmd4MecanimProperty.materialGlobalProperty;
        if (_standardEnabled)
        {
            GUILayout.Label("Shader Properties", EditorStyles.boldLabel);
            MMD4MecanimEditorCommon.UI.PushLead();
            MMD4MecanimEditorCommon.UI.PopLead();
            EditorGUI.BeginChangeCheck();
            int num1 = (int) MMD4MecanimEditorCommon.UI.EnumPopup<PMX2FBXConfig.ShaderType>(
                (object) "Shader Type", ref materialGlobalProperty.shaderType);
            if (EditorGUI.EndChangeCheck())
                flag1 = true;
            EditorGUI.BeginChangeCheck();
            if (materialGlobalProperty.shaderType == PMX2FBXConfig.ShaderType.Standard)
            {
                MMD4MecanimEditorCommon.UI.PushLead();
                int num2 =
                    (int) MMD4MecanimEditorCommon.UI
                        .EnumPopup<PMX2FBXConfig.GlobalIlluminationEmissiveType>(
                            (object) "GI Emissive", ref materialGlobalProperty.globalIlluminationEmissiveType);
                MMD4MecanimEditorCommon.UI.Toggle((object) "Toon", ref materialGlobalProperty.toonEnabled);
                MMD4MecanimEditorCommon.UI.PopLead();
            }

            if (EditorGUI.EndChangeCheck())
            {
                this._materialHelpers.ForEach(
                    (Action<MaterialHelper>) (m => m.UpdateMaterial_Toon()));
                this._materialHelpers.ForEach(
                    (Action<MaterialHelper>) (m => m.UpdateMaterial_GlobalIllumination()));
                flag2 = true;
            }
        }

        EditorGUILayout.Separator();
        EditorGUI.BeginChangeCheck();
        MMD4MecanimEditorCommon.UI.LabelField("Render Queue", EditorStyles.boldLabel);
        MMD4MecanimEditorCommon.UI.PushLead();
        MMD4MecanimEditorCommon.UI.Toggle((object) "Prefix", ref materialGlobalProperty.prefixRenderQueue);
        MMD4MecanimEditorCommon.UI.PushLead();
        MMD4MecanimEditorCommon.UI.PushEnabled(materialGlobalProperty.prefixRenderQueue);
        MMD4MecanimEditorCommon.UI.Toggle((object) "After Skybox", ref materialGlobalProperty.renderQueueAfterSkybox);
        MMD4MecanimEditorCommon.UI.PopEnabled();
        MMD4MecanimEditorCommon.UI.PopLead();
        MMD4MecanimEditorCommon.UI.PopLead();
        if (EditorGUI.EndChangeCheck())
            flag1 = true;
        EditorGUILayout.Separator();
        EditorGUI.BeginChangeCheck();
        MMD4MecanimEditorCommon.UI.LabelField("Toon", EditorStyles.boldLabel);
        MMD4MecanimEditorCommon.UI.PushLead();
        double num3 =
            (double) MMD4MecanimEditorCommon.UI.Slider((object) "Toon Tone", ref materialGlobalProperty.toonTone, 1f,
                40f);
        MMD4MecanimEditorCommon.UI.PopLead();
        if (EditorGUI.EndChangeCheck())
        {
            this._materialHelpers.ForEach(
                (Action<MaterialHelper>) (m => m.UpdateMaterial_ToonTone()));
            flag2 = true;
        }

        EditorGUILayout.Separator();
        EditorGUI.BeginChangeCheck();
        MMD4MecanimEditorCommon.UI.LabelField("Shadow", EditorStyles.boldLabel);
        MMD4MecanimEditorCommon.UI.PushLead();
        double num4 =
            (double) MMD4MecanimEditorCommon.UI.FloatField_Min((object) "Shadow Lum",
                ref materialGlobalProperty.shadowLum, 0.0f);
        MMD4MecanimEditorCommon.UI.PopLead();
        if (EditorGUI.EndChangeCheck())
        {
            this._materialHelpers.ForEach(
                (Action<MaterialHelper>) (m => m.UpdateMaterial_Shadow()));
            flag2 = true;
        }

        EditorGUILayout.Separator();
        EditorGUI.BeginChangeCheck();
        MMD4MecanimEditorCommon.UI.LabelField("Shadow (Forward Add)", EditorStyles.boldLabel);
        MMD4MecanimEditorCommon.UI.PushLead();
        double num5 = (double) MMD4MecanimEditorCommon.UI.Slider((object) "Add Light Toon Cen",
            ref materialGlobalProperty.addLightToonCen, -1f, 1f);
        double num6 = (double) MMD4MecanimEditorCommon.UI.Slider((object) "Add Light Toon Min",
            ref materialGlobalProperty.addLightToonMin, 0.0f, 1f);
        MMD4MecanimEditorCommon.UI.PopLead();
        if (EditorGUI.EndChangeCheck())
        {
            this._materialHelpers.ForEach(
                (Action<MaterialHelper>) (m => m.UpdateMaterial_AddLight()));
            flag2 = true;
        }

        EditorGUILayout.Separator();
        MMD4MecanimEditorCommon.UI.LabelField("Tessellation", EditorStyles.boldLabel);
        MMD4MecanimEditorCommon.UI.PushLead();
        EditorGUI.BeginChangeCheck();
        MMD4MecanimEditorCommon.UI.Toggle((object) "Enabled", ref materialGlobalProperty.tessEnabled);
        if (EditorGUI.EndChangeCheck())
        {
            this._materialHelpers.ForEach(
                (Action<MaterialHelper>) (m => m.UpdateMaterial_Shader()));
            flag2 = true;
        }

        MMD4MecanimEditorCommon.UI.PushEnabled(materialGlobalProperty.tessEnabled);
        EditorGUI.BeginChangeCheck();
        double num7 = (double) MMD4MecanimEditorCommon.UI.Slider((object) "Edge Length",
            ref materialGlobalProperty.tessEdgeLength, 2f, 50f);
        double num8 = (double) MMD4MecanimEditorCommon.UI.Slider((object) "Phong Strength",
            ref materialGlobalProperty.tessPhongStrength, 0.0f, 1f);
        if (EditorGUI.EndChangeCheck())
        {
            this._materialHelpers.ForEach(
                (Action<MaterialHelper>) (m => m.UpdateMaterial_Tess()));
            flag2 = true;
        }

        MMD4MecanimEditorCommon.UI.PopEnabled();
        MMD4MecanimEditorCommon.UI.PopLead();
        EditorGUILayout.Separator();
        EditorGUI.BeginChangeCheck();
        MMD4MecanimEditorCommon.UI.LabelField("Ambient", EditorStyles.boldLabel);
        MMD4MecanimEditorCommon.UI.PushLead();
        MMD4MecanimEditorCommon.UI.BeginLineWithLeadSpacing();
        materialGlobalProperty.ambientToDiffuseEnabled =
            (GUILayout.Toggle((materialGlobalProperty.ambientToDiffuseEnabled ? 1 : 0) != 0, "Ambient To Diffuse",
                GUILayout.ExpandWidth(false))
                ? 1
                : 0) != 0;
        MMD4MecanimEditorCommon.UI.PushEnabled(materialGlobalProperty.ambientToDiffuseEnabled);
        materialGlobalProperty.ambientToDiffuse =
            GUILayout.HorizontalSlider(materialGlobalProperty.ambientToDiffuse, 0.0f, 1f);
        MMD4MecanimEditorCommon.UI.PopEnabled();
        MMD4MecanimEditorCommon.UI.EndLine();
        MMD4MecanimEditorCommon.UI.PopLead();
        if (EditorGUI.EndChangeCheck())
        {
            this._materialHelpers.ForEach(
                (Action<MaterialHelper>) (m => m.UpdateMaterial_Ambient()));
            flag2 = true;
        }

        EditorGUILayout.Separator();
        EditorGUI.BeginChangeCheck();
        MMD4MecanimEditorCommon.UI.Toggle((object) "Specular Enabled", ref materialGlobalProperty.specularEnabled);
        if (EditorGUI.EndChangeCheck())
        {
            this._materialHelpers.ForEach(
                (Action<MaterialHelper>) (m => m.UpdateMaterial_Specular()));
            flag2 = true;
        }

        EditorGUILayout.Separator();
        EditorGUI.BeginChangeCheck();
        MMD4MecanimEditorCommon.UI.Toggle((object) "Edge Enabled", ref materialGlobalProperty.edgeEnabled);
        MMD4MecanimEditorCommon.UI.PushLead();
        MMD4MecanimEditorCommon.UI.PushEnabled(materialGlobalProperty.edgeEnabled);
        double num9 =
            (double) MMD4MecanimEditorCommon.UI.FloatField_Min((object) "Scale", ref materialGlobalProperty.edgeScale,
                0.0f);
        MMD4MecanimEditorCommon.UI.PopEnabled();
        MMD4MecanimEditorCommon.UI.PopLead();
        if (EditorGUI.EndChangeCheck())
        {
            this._materialHelpers.ForEach(
                (Action<MaterialHelper>) (m => m.UpdateMaterial_Edge()));
            flag2 = true;
        }

        EditorGUILayout.Separator();
        EditorGUI.BeginChangeCheck();
        int num10 = (int) MMD4MecanimEditorCommon.UI.EnumPopup<PMX2FBXConfig.AutoLuminous>(
            (object) "Auto Luminous", ref materialGlobalProperty.autoLuminous);
        MMD4MecanimEditorCommon.UI.PushLead();
        MMD4MecanimEditorCommon.UI.PushEnabled((uint) materialGlobalProperty.autoLuminous > 0U);
        double num11 = (double) MMD4MecanimEditorCommon.UI.FloatField_Min((object) "Power",
            ref materialGlobalProperty.autoLuminousPower, 0.0f);
        MMD4MecanimEditorCommon.UI.PopEnabled();
        MMD4MecanimEditorCommon.UI.PopLead();
        if (EditorGUI.EndChangeCheck())
        {
            this._materialHelpers.ForEach(
                (Action<MaterialHelper>) (m => m.UpdateMaterial_Emission()));
            flag2 = true;
        }

        EditorGUILayout.Separator();
        EditorGUI.BeginChangeCheck();
        MMD4MecanimEditorCommon.UI.LabelField("Etcetera", EditorStyles.boldLabel);
        MMD4MecanimEditorCommon.UI.PushLead();
        int num12 = (int) MMD4MecanimEditorCommon.UI.EnumPopup<PMX2FBXConfig.TransparencyMode>(
            (object) "Transparency", ref materialGlobalProperty.transparencyMode);
        if (EditorGUI.EndChangeCheck())
            flag1 = true;
        if (_standardEnabled && materialGlobalProperty.shaderType ==
            PMX2FBXConfig.ShaderType.Standard)
        {
            EditorGUI.BeginChangeCheck();
            MMD4MecanimEditorCommon.UI.PushLead();
            double num13 = (double) MMD4MecanimEditorCommon.UI.FloatField((object) "Offset Factor",
                ref materialGlobalProperty.transparentOffsetFactor);
            double num14 = (double) MMD4MecanimEditorCommon.UI.FloatField((object) "Offset Units",
                ref materialGlobalProperty.transparentOffsetUnits);
            MMD4MecanimEditorCommon.UI.PopLead();
            if (EditorGUI.EndChangeCheck())
            {
                this._materialHelpers.ForEach(
                    (Action<MaterialHelper>) (m => m.UpdateMaterial_TransparentOffset()));
                flag2 = true;
            }
        }

        EditorGUI.BeginChangeCheck();
        MMD4MecanimEditorCommon.UI.Toggle((object) "Self Shadow", ref materialGlobalProperty.selfShadowEnabled);
        if (EditorGUI.EndChangeCheck())
            flag1 = true;
        EditorGUI.BeginChangeCheck();
        MMD4MecanimEditorCommon.UI.PushEnabled(materialGlobalProperty.shaderType ==
                                               PMX2FBXConfig.ShaderType.Compatible);
        MMD4MecanimEditorCommon.UI.Toggle((object) "Sphere Map", ref materialGlobalProperty.sphereMapEnabled);
        MMD4MecanimEditorCommon.UI.PopEnabled();
        if (EditorGUI.EndChangeCheck())
        {
            this._materialHelpers.ForEach(
                (Action<MaterialHelper>) (m => m.UpdateMaterial_SphereMap()));
            flag2 = true;
        }

        MMD4MecanimEditorCommon.UI.PopLead();
        EditorGUILayout.Separator();
        if (_standardEnabled)
        {
            MMD4MecanimEditorCommon.UI.DrawSeparator();
            GUILayout.Label("Materials", EditorStyles.boldLabel);
            MMD4MecanimEditorCommon.UI.BeginLine();
            int num15 =
                (int) MMD4MecanimEditorCommon.UI.EnumPopup<PMX2FBXConfig.StandardSetupType>(
                    (object) "Automatic Setup", ref materialGlobalProperty.standardSetupType);
            MMD4MecanimEditorCommon.UI.EndLine();
            MMD4MecanimEditorCommon.UI.PushLead();
            MMD4MecanimEditorCommon.UI.PushEnabled(materialGlobalProperty.standardSetupType ==
                                                   PMX2FBXConfig.StandardSetupType.Metalic);
            double num16 = (double) MMD4MecanimEditorCommon.UI.FloatField_Min((object) "Sph To Metallic Glossiness",
                ref materialGlobalProperty.sphereCubeToMetallicGlossinessRate, 0.0f);
            MMD4MecanimEditorCommon.UI.PopEnabled();
            MMD4MecanimEditorCommon.UI.PushEnabled(materialGlobalProperty.standardSetupType ==
                                                   PMX2FBXConfig.StandardSetupType.Specular);
            double num17 = (double) MMD4MecanimEditorCommon.UI.Slider((object) "Sph To Specular Glossiness",
                ref materialGlobalProperty.sphereCubeToSpecularGlossiness, 0.0f, 1f);
            double num18 = (double) MMD4MecanimEditorCommon.UI.FloatField_Min((object) "Sph To Specular Color",
                ref materialGlobalProperty.sphereCubeToSpecularColorRate, 0.0f);
            MMD4MecanimEditorCommon.UI.PopEnabled();
            MMD4MecanimEditorCommon.UI.PopLead();
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            MMD4MecanimEditorCommon.UI.BeginLine();
            if (MMD4MecanimEditorCommon.UI.Button((object) "Setup"))
            {
                new MaterialBlocksConfigurator()
                {
                    fbxAsset = this.fbxAsset,
                    mmdModel = this.mmdModel,
                    mmd4MecanimProperty = this.pmx2fbxConfig.mmd4MecanimProperty
                }.Configure(materialGlobalProperty.standardSetupType);
                this._materialHelpers.ForEach(
                    (Action<MaterialHelper>) (m => m.UpdateMaterial()));
                flag2 = true;
            }

            MMD4MecanimEditorCommon.UI.EndLine();
            EditorGUILayout.EndHorizontal();
        }

        MMD4MecanimEditorCommon.UI.DrawSeparator();
        EditorGUILayout.Separator();
        EditorGUI.BeginChangeCheck();
        this.pmx2fbxConfig.mmd4MecanimProperty.materialViewPage = GUILayout.Toolbar(
            this.pmx2fbxConfig.mmd4MecanimProperty.materialViewPage, toolbarMaterialPageTitle);
        if (EditorGUI.EndChangeCheck())
            flag2 = true;
        if (this.pmx2fbxConfig.mmd4MecanimProperty.materialViewPage == 0)
        {
            List<PMX2FBXConfig.MaterialBlockProperty> blockPropertyList =
                this.pmx2fbxConfig.mmd4MecanimProperty.materialBlockPropertyList;
            PMX2FBXConfig.MaterialBlockProperty materialBlockProperty1 = blockPropertyList[0];
            materialBlockProperty1.Repair();
            int num19 = 0;
            while (num19 < blockPropertyList.Count)
            {
                PMX2FBXConfig.MaterialBlockProperty materialBlockProperty2 =
                    blockPropertyList[num19];
                materialBlockProperty2.Repair();
                bool isDelete;
                bool isAdd;
                MMD4MecanimEditorCommon.UI.MaterialBlockTitlebar(ref materialBlockProperty2.isUIFoldout, num19,
                    out isDelete, out isAdd);
                if (materialBlockProperty2.isUIFoldout)
                    flag2 |= this.DrawMaterialBlockProperty(materialBlockProperty2, num19);
                if (isDelete)
                {
                    if (materialBlockProperty2.materialNames != null)
                        materialBlockProperty1.materialNames.AddRange(
                            (IEnumerable<string>) materialBlockProperty2.materialNames);
                    blockPropertyList.RemoveAt(num19);
                }
                else
                {
                    if (isAdd)
                    {
                        PMX2FBXConfig.MaterialBlockProperty materialBlockProperty3 =
                            new PMX2FBXConfig.MaterialBlockProperty();
                        blockPropertyList.Add(materialBlockProperty3);
                    }

                    ++num19;
                }
            }
        }
        else
        {
            for (int index = 0; index < this._materialHelpers.Count; ++index)
                flag2 |= this.DrawMaterialProperty(this._materialHelpers[index], index);
        }

        EditorGUILayout.Separator();
        if (flag2)
        {
            this._materialHelpers.ForEach(m => m.SetDirty());
            this._changedDelay = 60;
        }
        else if (this._changedDelay > 0 && --this._changedDelay == 0)
            this.SavePMX2FBXConfig();

        if (!flag1)
            return;
        this._changedDelay = 0;
        this.SavePMX2FBXConfig();
        this.Process();
    }

    public static MMDModel.Material GetXmlMaterial(
        MMDModel mmdModel,
        string materialName)
    {
        if (mmdModel != null)
        {
            MMDModel.Material[] materialList = mmdModel.materialList;
            if (materialList != null && materialName != null)
            {
                foreach (MMDModel.Material xmlMaterial in materialList)
                {
                    if (xmlMaterial != null && xmlMaterial.materialName == materialName)
                        return xmlMaterial;
                }
            }
        }

        return (MMDModel.Material) null;
    }

    public MaterialHelper GetMaterialHelper(
        string materialName)
    {
        return materialName == null
            ? (MaterialHelper) null
            : this._materialHelpers.Find(
                (Predicate<MaterialHelper>) (m => m.materialName == materialName));
    }

    public List<MaterialHelper> GetMaterialHelpers(
        List<string> materialNames)
    {
        List<MaterialHelper> materialHelpers =
            new List<MaterialHelper>();
        foreach (string materialName in materialNames)
        {
            MaterialHelper materialHelper = this.GetMaterialHelper(materialName);
            if (materialHelper != null)
                materialHelpers.Add(materialHelper);
        }

        materialHelpers.Sort(
            (Comparison<MaterialHelper>) ((a, b) => a.materialIndex - b.materialIndex));
        return materialHelpers;
    }

    public bool DrawMaterialProperty(
        MaterialHelper materialHelper,
        int materialIndex)
    {
        bool flag = false;
        int num1 = Application.systemLanguage == SystemLanguage.Japanese ? 1 : 0;
        PMX2FBXConfig.MaterialProperty materialProperty = materialHelper.materialProperty;
        MMDModel.Material xmlMaterial = materialHelper.xmlMaterial;
        string str = num1 != 0 ? xmlMaterial.nameJp : xmlMaterial.nameEn;
        string visibleName;
        if (!string.IsNullOrEmpty(str))
        {
            visibleName = materialHelper.materialIndex.ToString() + "." + str;
        }
        else
        {
            visibleName = materialHelper.materialName;
            if (string.IsNullOrEmpty(visibleName))
                visibleName = string.Concat((object) materialHelper.materialIndex);
        }

        MMD4MecanimEditorCommon.UI.BeginLine();
        EditorGUI.BeginChangeCheck();
        int num2 = materialProperty.isVisible ? 1 : 0;
        MMD4MecanimEditorCommon.UI.MaterialTitlebar(ref materialProperty.isUIFoldout, ref materialProperty.isVisible,
            visibleName);
        int num3 = materialProperty.isVisible ? 1 : 0;
        if (num2 != num3)
        {
            materialHelper.UpdateMaterial_Color();
            materialHelper.UpdateMaterial_Edge();
            flag = true;
        }

        MMD4MecanimEditorCommon.UI.EndLine();
        if (materialProperty.isUIFoldout)
        {
            MMD4MecanimEditorCommon.UI.PushLead();
            MMD4MecanimEditorCommon.UI.PushLead();
            MMD4MecanimEditorCommon.UI.BeginLineWithLeadSpacing();
            EditorGUI.BeginChangeCheck();
            MMD4MecanimEditorCommon.UI.ToggleLeft((object) "", ref materialProperty.isLocked);
            MMD4MecanimEditorCommon.UI.PushEnabled(materialProperty.isLocked);
            MMD4MecanimEditorCommon.UI.LabelField("Locked (Don't processing)");
            MMD4MecanimEditorCommon.UI.PopEnabled();
            GUILayout.FlexibleSpace();
            if (EditorGUI.EndChangeCheck())
                flag = true;
            MMD4MecanimEditorCommon.UI.EndLine();
            MMD4MecanimEditorCommon.UI.PushEnabled(!materialProperty.isLocked);
            MMD4MecanimEditorCommon.UI.BeginLineWithLeadSpacing();
            EditorGUI.BeginChangeCheck();
            MMD4MecanimEditorCommon.UI.ToggleLeft((object) "", ref materialProperty.overrideToonTone,
                GUILayout.ExpandWidth(false));
            MMD4MecanimEditorCommon.UI.PushEnabled(materialProperty.overrideToonTone);
            float toonTone = materialHelper.toonTone;
            double num4 =
                (double) MMD4MecanimEditorCommon.UI.Slider((object) "Override Toon Tone", ref toonTone, 1f, 40f);
            MMD4MecanimEditorCommon.UI.PopEnabled();
            if (EditorGUI.EndChangeCheck())
            {
                materialHelper.toonTone = toonTone;
                materialHelper.UpdateMaterial_ToonTone();
                flag = true;
            }

            MMD4MecanimEditorCommon.UI.EndLine();
            MMD4MecanimEditorCommon.UI.BeginLineWithLeadSpacing();
            EditorGUI.BeginChangeCheck();
            MMD4MecanimEditorCommon.UI.ToggleLeft((object) "", ref materialProperty.overrideShadowLum,
                GUILayout.ExpandWidth(false));
            MMD4MecanimEditorCommon.UI.PushEnabled(materialProperty.overrideShadowLum);
            float shadowLum = materialHelper.shadowLum;
            double num5 =
                (double) MMD4MecanimEditorCommon.UI.FloatField_Min((object) "Override Shadow Lum", ref shadowLum, 0.0f);
            MMD4MecanimEditorCommon.UI.PopEnabled();
            if (EditorGUI.EndChangeCheck())
            {
                materialHelper.shadowLum = shadowLum;
                materialHelper.UpdateMaterial_Shadow();
                flag = true;
            }

            MMD4MecanimEditorCommon.UI.EndLine();
            MMD4MecanimEditorCommon.UI.BeginLineWithLeadSpacing();
            EditorGUI.BeginChangeCheck();
            MMD4MecanimEditorCommon.UI.ToggleLeft((object) "", ref materialProperty.overrideTess);
            MMD4MecanimEditorCommon.UI.PushEnabled(materialProperty.overrideTess);
            MMD4MecanimEditorCommon.UI.LabelField("Override Tess");
            MMD4MecanimEditorCommon.UI.PopEnabled();
            GUILayout.FlexibleSpace();
            if (EditorGUI.EndChangeCheck())
            {
                materialHelper.UpdateMaterial_Shader();
                materialHelper.UpdateMaterial_Tess();
                flag = true;
            }

            MMD4MecanimEditorCommon.UI.EndLine();
            MMD4MecanimEditorCommon.UI.PushEnabled(materialProperty.overrideTess);
            MMD4MecanimEditorCommon.UI.PushLead();
            EditorGUI.BeginChangeCheck();
            MMD4MecanimEditorCommon.UI.Toggle((object) "Tess Enabled", ref materialProperty.tessEnabled);
            if (EditorGUI.EndChangeCheck())
            {
                materialHelper.UpdateMaterial_Shader();
                flag = true;
            }

            MMD4MecanimEditorCommon.UI.PushEnabled(materialProperty.tessEnabled);
            EditorGUI.BeginChangeCheck();
            float tessEdgeLength = materialHelper.tessEdgeLength;
            float tessPhongStrength = materialHelper.tessPhongStrength;
            float tessExtrusionAmount = materialHelper.tessExtrusionAmount;
            double num6 =
                (double) MMD4MecanimEditorCommon.UI.Slider((object) "Tess Edge Length", ref tessEdgeLength, 2f, 50f);
            double num7 =
                (double) MMD4MecanimEditorCommon.UI.Slider((object) "Tess Phong Strength", ref tessPhongStrength, 0.0f,
                    1f);
            double num8 =
                (double) MMD4MecanimEditorCommon.UI.FloatField((object) "Tess Extrusion Amount",
                    ref tessExtrusionAmount);
            if (EditorGUI.EndChangeCheck())
            {
                materialHelper.tessEdgeLength = tessEdgeLength;
                materialHelper.tessPhongStrength = tessPhongStrength;
                materialHelper.tessExtrusionAmount = tessExtrusionAmount;
                materialHelper.UpdateMaterial_Tess();
                flag = true;
            }

            MMD4MecanimEditorCommon.UI.PopEnabled();
            MMD4MecanimEditorCommon.UI.PopLead();
            MMD4MecanimEditorCommon.UI.PopEnabled();
            MMD4MecanimEditorCommon.UI.PopEnabled();
            MMD4MecanimEditorCommon.UI.PopLead();
            MMD4MecanimEditorCommon.UI.PopLead();
        }

        return flag;
    }

    public bool DrawMaterialBlockProperty(
        PMX2FBXConfig.MaterialBlockProperty materialBlockProperty,
        int materialBlockIndex)
    {
        bool flag1 = false;
        materialBlockProperty.Repair();
        PMX2FBXConfig.MaterialGlobalProperty materialGlobalProperty =
            this.pmx2fbxConfig.mmd4MecanimProperty.materialGlobalProperty;
        List<MaterialHelper> materialHelpers =
            this.GetMaterialHelpers(materialBlockProperty.materialNames);
        string unityVersion = Application.unityVersion;
        MMD4MecanimEditorCommon.UI.LabelField("Base", EditorStyles.boldLabel);
        MMD4MecanimEditorCommon.UI.PushLead();
        if (unityVersion.StartsWith("4.") || unityVersion.StartsWith("5.0."))
        {
            EditorGUI.BeginChangeCheck();
            MMD4MecanimEditorCommon.UI.ColorField((object) "Diffuse", ref materialBlockProperty.baseColor);
            if (EditorGUI.EndChangeCheck())
            {
                materialHelpers.ForEach(
                    (Action<MaterialHelper>) (m => m.UpdateMaterial_Color()));
                flag1 = true;
            }

            EditorGUI.BeginChangeCheck();
            MMD4MecanimEditorCommon.UI.ColorField((object) "Emissive", ref materialBlockProperty.emissionColor);
            MMD4MecanimEditorCommon.UI.PushLead();
            double num =
                (double) MMD4MecanimEditorCommon.UI.FloatField_Min((object) "", ref materialBlockProperty.emissionPower,
                    0.0f);
            MMD4MecanimEditorCommon.UI.PopLead();
            if (EditorGUI.EndChangeCheck())
            {
                materialHelpers.ForEach(
                    (Action<MaterialHelper>) (m => m.UpdateMaterial_Emission()));
                flag1 = true;
            }
        }
        else
        {
            MMD4MecanimEditorCommon.UI.BeginLine();
            MMD4MecanimEditorCommon.UI.Label("Diffuse / Emissive");
            float width1 = EditorStyles.label.CalcHeight(new GUIContent(), 0.0f) * 3f;
            float width2 = EditorStyles.label.CalcHeight(new GUIContent(), 0.0f) * 2f;
            EditorGUI.BeginChangeCheck();
            MMD4MecanimEditorCommon.UI.ColorField_RGB((object) "", ref materialBlockProperty.baseColor,
                GUILayout.ExpandWidth(false), GUILayout.Width(width1));
            if (EditorGUI.EndChangeCheck())
            {
                materialHelpers.ForEach(
                    (Action<MaterialHelper>) (m => m.UpdateMaterial_Color()));
                flag1 = true;
            }

            EditorGUI.BeginChangeCheck();
            MMD4MecanimEditorCommon.UI.ColorField_RGB((object) "", ref materialBlockProperty.emissionColor,
                GUILayout.ExpandWidth(false), GUILayout.Width(width1));
            double num = (double) MMD4MecanimEditorCommon.UI.FloatField_Min((object) "",
                ref materialBlockProperty.emissionPower, 0.0f, GUILayout.ExpandWidth(false), GUILayout.Width(width2));
            if (EditorGUI.EndChangeCheck())
            {
                materialHelpers.ForEach(
                    (Action<MaterialHelper>) (m => m.UpdateMaterial_Emission()));
                flag1 = true;
            }

            MMD4MecanimEditorCommon.UI.EndLine();
        }

        MMD4MecanimEditorCommon.UI.PopLead();
        if (materialGlobalProperty.shaderType == PMX2FBXConfig.ShaderType.Standard)
        {
            MMD4MecanimEditorCommon.UI.LabelField("Standard", EditorStyles.boldLabel);
            MMD4MecanimEditorCommon.UI.PushLead();
            EditorGUI.BeginChangeCheck();
            int num1 = (int) MMD4MecanimEditorCommon.UI.EnumPopup<PMX2FBXConfig.StandardBRDF>(
                (object) "BRDF", ref materialBlockProperty.standardBRDF);
            if (EditorGUI.EndChangeCheck())
            {
                materialHelpers.ForEach(
                    (Action<MaterialHelper>) (m => m.UpdateMaterial_StandardBRDF()));
                flag1 = true;
            }

            if (materialBlockProperty.standardBRDF == PMX2FBXConfig.StandardBRDF.Specular)
            {
                EditorGUI.BeginChangeCheck();
                if (unityVersion.StartsWith("4.") || unityVersion.StartsWith("5.0."))
                    MMD4MecanimEditorCommon.UI.ColorField((object) "Specular",
                        ref materialBlockProperty.standardSpecularColor);
                else
                    MMD4MecanimEditorCommon.UI.ColorField_RGB((object) "Specular",
                        ref materialBlockProperty.standardSpecularColor);
                if (EditorGUI.EndChangeCheck())
                {
                    materialHelpers.ForEach(
                        (Action<MaterialHelper>) (m => m.UpdateMaterial_Specular()));
                    flag1 = true;
                }
            }

            if (materialBlockProperty.standardBRDF == PMX2FBXConfig.StandardBRDF.Metallic)
            {
                EditorGUI.BeginChangeCheck();
                double num2 = (double) MMD4MecanimEditorCommon.UI.Slider((object) "Metalic",
                    ref materialBlockProperty.standardMetallic, 0.0f, 1f);
                if (EditorGUI.EndChangeCheck())
                {
                    materialHelpers.ForEach(
                        (Action<MaterialHelper>) (m => m.UpdateMaterial_StandardMetallic()));
                    flag1 = true;
                }
            }

            EditorGUI.BeginChangeCheck();
            if (materialBlockProperty.standardBRDF == PMX2FBXConfig.StandardBRDF.Metallic)
            {
                double num3 = (double) MMD4MecanimEditorCommon.UI.Slider((object) "Glossiness",
                    ref materialBlockProperty.standardMetallicGlossiness, 0.0f, 1f);
            }

            if (materialBlockProperty.standardBRDF == PMX2FBXConfig.StandardBRDF.Specular)
            {
                double num4 = (double) MMD4MecanimEditorCommon.UI.Slider((object) "Glossiness",
                    ref materialBlockProperty.standardSpecularGlossiness, 0.0f, 1f);
            }

            if (EditorGUI.EndChangeCheck())
            {
                materialHelpers.ForEach(
                    (Action<MaterialHelper>) (m => m.UpdateMaterial_StandardGlossiness()));
                flag1 = true;
            }

            MMD4MecanimEditorCommon.UI.PopLead();
        }

        if (materialBlockProperty.materialNames.Count == 0)
        {
            MMD4MecanimEditorCommon.UI.PushEnabled(false);
            MMD4MecanimEditorCommon.UI.Label("No materials");
            MMD4MecanimEditorCommon.UI.PopEnabled();
            return flag1;
        }

        bool flag2 = Application.systemLanguage == SystemLanguage.Japanese;
        int count = this.pmx2fbxConfig.mmd4MecanimProperty.materialBlockPropertyList.Count;
        string[] displayedOptions = new string[count + 1];
        int num5 = 0;
        int index1 = 1;
        for (; num5 < count; ++num5)
        {
            if (num5 != materialBlockIndex)
            {
                displayedOptions[index1] = " -> Materials " + num5;
                ++index1;
            }
        }

        displayedOptions[count] = " -> New Materials";
        float pixels = EditorStyles.label.CalcHeight(new GUIContent(), 0.0f);
        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(pixels);
        for (int index2 = 0; index2 < materialHelpers.Count; ++index2)
        {
            MaterialHelper materialHelper = materialHelpers[index2];
            MMDModel.Material xmlMaterial = materialHelper.xmlMaterial;
            string str1 = flag2 ? xmlMaterial.nameJp : xmlMaterial.nameEn;
            string str2;
            if (!string.IsNullOrEmpty(str1))
            {
                str2 = materialHelper.materialIndex.ToString() + "." + str1;
            }
            else
            {
                str2 = materialHelper.materialName;
                if (string.IsNullOrEmpty(str2))
                    str2 = string.Concat((object) materialHelper.materialIndex);
            }

            displayedOptions[0] = str2;
            int num6 = EditorGUILayout.Popup(0, displayedOptions);
            if (num6 > 0)
            {
                materialBlockProperty.materialNames.Remove(materialHelper.materialName);
                int num7 = num6 - 1;
                int index3 = num7 >= materialBlockIndex ? num7 + 1 : num7;
                if (index3 >= this.pmx2fbxConfig.mmd4MecanimProperty.materialBlockPropertyList.Count)
                    this.pmx2fbxConfig.mmd4MecanimProperty.materialBlockPropertyList.Add(
                        new PMX2FBXConfig.MaterialBlockProperty());
                this.pmx2fbxConfig.mmd4MecanimProperty.materialBlockPropertyList[index3].materialNames
                    .Add(materialHelper.materialName);
                flag1 = true;
            }

            EditorGUI.BeginChangeCheck();
            materialHelper.materialProperty.isVisible = GUILayout.Toggle(materialHelper.materialProperty.isVisible, "");
            if (EditorGUI.EndChangeCheck())
            {
                materialHelper.UpdateMaterial_Color();
                materialHelper.UpdateMaterial_Edge();
                flag1 = true;
            }

            if (index2 + 1 != materialHelpers.Count && index2 % 3 == 2)
            {
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal();
                GUILayout.Space(pixels);
            }
        }

        if (materialHelpers.Count % 3 != 0)
        {
            MMD4MecanimEditorCommon.UI.PushEnabled(false);
            for (int index4 = 0; index4 < 3 - materialHelpers.Count % 3; ++index4)
            {
                displayedOptions[0] = "";
                EditorGUILayout.Popup(0, displayedOptions);
                GUILayout.Toggle(false, "", GUILayout.ExpandWidth(false));
            }

            MMD4MecanimEditorCommon.UI.PopEnabled();
        }

        EditorGUILayout.EndHorizontal();
        return flag1;
    }

    public void Process()
    {
        _ProcessMaterial(fbxAsset, mmdModel, pmx2fbxConfig, true);
        Debug.Log("Processed.");
    }

    private static bool _Contains(string[] keywords, string keyword)
    {
        if (keywords != null)
        {
            for (int index = 0; index != keywords.Length; ++index)
            {
                if (keywords[index] == keyword)
                    return true;
            }
        }

        return false;
    }

    private static MaterialState _CheckMaterialState(
        Material material)
    {
        MaterialState materialState = MaterialState.Nothing;
        if (material != null)
        {
            if (material.shader != null && material.shader.name != null &&
                material.shader.name.StartsWith("MMD4Mecanim"))
            {
                if (material.HasProperty("_Revision"))
                {
                    if (material.GetFloat("_Revision") < 1.5f)
                        materialState |= MaterialState.Previous;
                }
                else
                    materialState |= MaterialState.Previous;
            }
            else if ((Object) material.mainTexture != (Object) null)
                materialState |= MaterialState.Normally;
            else
                materialState |= MaterialState.Uninitialized;
        }

        return materialState;
    }

    private static MaterialState _CheckMaterialStates(
        Material[] materials)
    {
        MaterialState materialState = MaterialState.Nothing;
        if (materials != null)
        {
            foreach (Material material in materials)
                materialState |= _CheckMaterialState(material);
        }

        return materialState;
    }

    private bool _CheckFBXMaterialMaterial(
        MeshRenderer[] meshRenderers,
        SkinnedMeshRenderer[] skinnedMeshRenderers)
    {
        if (fbxAsset == null || fbxAssetPath == null ||
            mmdModel == null || pmx2fbxConfig == null)
            return false;

        bool flag = false;

        RichLog.Log($"_initializeMaterialAtLeastOnce: {_initializeMaterialAtLeastOnce}", Color.yellow);
        RichLog.Log($"_initializeMaterialAfterPMX2FBX: {_initializeMaterialAfterPMX2FBX}", Color.yellow);

        if (!_initializeMaterialAtLeastOnce || _initializeMaterialAfterPMX2FBX)
        {

            if (_initializeMaterialAfterPMX2FBX)
            {
                _initializeMaterialAfterPMX2FBX = false;
                if (!File.Exists(GetIndexDataPath(fbxAssetPath)))
                    flag = true;
            }

            if (!flag)
            {
                MaterialState materialState = MaterialState.Nothing;
                if (meshRenderers != null)
                {
                    foreach (MeshRenderer meshRenderer in meshRenderers)
                        materialState |= _CheckMaterialStates(meshRenderer.sharedMaterials);
                }

                if ((materialState == MaterialState.Nothing ||
                     materialState == MaterialState.Normally) && skinnedMeshRenderers != null)
                {
                    foreach (SkinnedMeshRenderer skinnedMeshRenderer in skinnedMeshRenderers)
                        materialState |=
                            _CheckMaterialStates(skinnedMeshRenderer.sharedMaterials);
                }

                flag = materialState != MaterialState.Nothing &&
                       materialState != MaterialState.Normally;
            }
        }

        RichLog.Log($"Flag {flag} {_forceProcessConvertMaterial}", Color.green);

        //強制執行
        if (flag | _forceProcessConvertMaterial)
        {
            Debug.Log("MMD4Mecanim:Initialize FBX Material:" + fbxAsset.name);
            _initializeMaterialAtLeastOnce = true;
            _ProcessMaterial(fbxAsset, mmdModel, pmx2fbxConfig, false);
        }

        return flag;
    }

    private bool _CheckFBXMaterialIndex(SkinnedMeshRenderer[] skinnedMeshRenderers)
    {
        if ((Object) this.fbxAsset == (Object) null || this.mmdModel == null ||
            this.pmx2fbxConfig == null || skinnedMeshRenderers == null || skinnedMeshRenderers.Length == 0)
            return false;
        bool flag = false;
        if (this.indexData != null)
        {
            if (!AuxData.ValidateIndexData(this.indexData, skinnedMeshRenderers))
            {
                this.indexData = (AuxData.IndexData) null;
                flag = true;
            }
        }
        else
            flag = true;

        if (flag)
        {
            Debug.Log((object) ("MMD4Mecanim:Initialize FBX Index:" + this.fbxAsset.name));
            _ProcessIndex(this.fbxAsset, this.mmdModel);
            this.indexAsset =
                AssetDatabase.LoadAssetAtPath(GetIndexDataPath(this.fbxAssetPath),
                    typeof(TextAsset)) as TextAsset;
            if ((Object) this.indexAsset != (Object) null)
                this.indexData = AuxData.BuildIndexData(this.indexAsset);
        }

        return flag;
    }

    private bool _CheckFBXMaterialVertex(SkinnedMeshRenderer[] skinnedMeshRenderers)
    {
        if (fbxAsset == null || mmdModel == null ||
            this.pmx2fbxConfig == null || skinnedMeshRenderers == null || skinnedMeshRenderers.Length == 0)
            return false;
        bool flag = false;
        if (this.vertexData != null)
        {
            if (!AuxData.ValidateVertexData(this.vertexData, skinnedMeshRenderers))
            {
                this.vertexData = null;
                flag = true;
            }
        }
        else
            flag = true;

        if (!flag && this.mmdModel != null && this.mmdModel.globalSettings != null)
        {
            if (this.vertexData != null)
            {
                float vertexScale = this.mmdModel.globalSettings.vertexScale;
                float num = this.fbxAssertImportScale;
                if ((double) num == 0.0)
                    num = this.mmdModel.globalSettings.importScale;
                float f1 = num - this.vertexData.importScale;
                float f2 = vertexScale - this.vertexData.vertexScale;
                if ((double) Mathf.Abs(f1) > (double) Mathf.Epsilon || (double) Mathf.Abs(f2) > (double) Mathf.Epsilon)
                {
                    MMD4MecanimData.ExtraData extraData = MMD4MecanimData.BuildExtraData(
                        AssetDatabase.LoadAssetAtPath(
                            GetExtraDataPath(
                                AssetDatabase.GetAssetPath((Object) this.fbxAsset)),
                            typeof(TextAsset)) as TextAsset);
                    if (extraData != null)
                    {
                        float f3 = extraData.vertexScale - this.vertexData.vertexScale;
                        if ((double) Mathf.Abs(f1) > (double) Mathf.Epsilon ||
                            (double) Mathf.Abs(f3) > (double) Mathf.Epsilon)
                            flag = true;
                    }
                }
            }
            else
                flag = true;
        }

        if (flag)
        {
            string extraDataPath =
                GetExtraDataPath(
                    AssetDatabase.GetAssetPath(fbxAsset));
            if (!File.Exists(extraDataPath))
                flag = false;
            else if (AssetDatabase.LoadAssetAtPath(extraDataPath, typeof(TextAsset)) == (Object) null)
                AssetDatabase.ImportAsset(extraDataPath,
                    ImportAssetOptions.ForceUpdate | ImportAssetOptions.ForceSynchronousImport);
        }

        if (flag)
        {
            Debug.Log("MMD4Mecanim:Initialize FBX Vertex:" + this.fbxAsset.name);
            _ProcessVertex(this.fbxAsset, this.fbxAssertImportScale, this.mmdModel);
            this.vertexAsset =
                AssetDatabase.LoadAssetAtPath(GetVertexDataPath(this.fbxAssetPath),
                    typeof(TextAsset)) as TextAsset;
            if (this.vertexAsset != null)
                this.vertexData = AuxData.BuildVertexData(this.vertexAsset);
        }

        return flag;
    }

    public void _CheckFBXMaterial()
    {
        Debug.Log($"fbxAsset {fbxAsset}");
        Debug.Log($"mmdModel {mmdModel}");
        Debug.Log($"pmx2fbxConfig {pmx2fbxConfig}");

        if (fbxAsset == null || this.mmdModel == null ||
            this.pmx2fbxConfig == null)
            return;

        Debug.Log($"-----CheckMaterial-----");

        MeshRenderer[] meshRenderers = MMD4MecanimCommon.GetMeshRenderers(this.fbxAsset.gameObject);
        SkinnedMeshRenderer[] skinnedMeshRenderers =
            MMD4MecanimCommon.GetSkinnedMeshRenderers(fbxAsset.gameObject);
        _CheckFBXMaterialMaterial(meshRenderers, skinnedMeshRenderers);
        if ((0 | (_CheckFBXMaterialIndex(skinnedMeshRenderers) ? 1 : 0) |
             (_CheckFBXMaterialVertex(skinnedMeshRenderers) ? 1 : 0)) == 0)
            return;
        AssetDatabase.Refresh();
    }

    public void _CheckFBXMaterialVertex()
    {
        if (fbxAsset == null || mmdModel == null ||
            pmx2fbxConfig == null)
            return;
        _CheckFBXMaterialVertex(MMD4MecanimCommon.GetSkinnedMeshRenderers(fbxAsset.gameObject));
    }

    private static void _ProcessMaterial(
        GameObject fbxAsset,
        MMDModel mmdModel,
        PMX2FBXConfig pmx2fbxConfig,
        bool overwriteMaterials)
    {
        RichLog.Log("*****_ProcessMaterial*****", Color.cyan);

        if (fbxAsset == null)
            Debug.LogError("FBXAsset is null.");
        else if (mmdModel == null)
            Debug.LogError("MMDModel is null.");
        else if (pmx2fbxConfig == null)
            Debug.LogError("pmx2fbxConfig is null.");
        else
            new MaterialBuilder()
            {
                fbxAsset = fbxAsset,
                mmdModel = mmdModel,
                mmd4MecanimProperty = pmx2fbxConfig.mmd4MecanimProperty
            }.Build(overwriteMaterials);
    }

    private static void _ProcessIndex(GameObject fbxAsset, MMDModel mmdModel)
    {
        if (fbxAsset == null)
            Debug.LogError("FBXAsset is null.");
        else if (mmdModel == null)
            Debug.LogError("MMDModel is null.");
        else
            new IndexBuilder()
            {
                fbxAsset = fbxAsset,
                mmdModel = mmdModel
            }.Build();
    }

    private static void _ProcessVertex(
        GameObject fbxAsset,
        float importScale,
        MMDModel mmdModel)
    {
        if (fbxAsset == null)
            Debug.LogError("FBXAsset is null.");
        else if (mmdModel == null)
            Debug.LogError("MMDModel is null.");
        else
            new VertexBuilder()
            {
                fbxAsset = fbxAsset,
                importScale = importScale,
                mmdModel = mmdModel
            }.Build();
    }

    public static PMX2FBXConfig GetPMX2FBXConfig(
        string xmlAssetPath)
    {
        if (string.IsNullOrEmpty(xmlAssetPath))
            return null;
        if (!File.Exists(xmlAssetPath))
            return null;
        try
        {
            using (FileStream fileStream =
                   new FileStream(xmlAssetPath, FileMode.Open, FileAccess.Read, FileShare.Read))
                return (PMX2FBXConfig) new XmlSerializer(
                    typeof(PMX2FBXConfig)).Deserialize((Stream) fileStream);
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static bool WritePMX2FBXConfig(
        string xmlAssetPath,
        PMX2FBXConfig pmx2fbxConfig)
    {
        if (string.IsNullOrEmpty(xmlAssetPath))
        {
            Debug.LogWarning((object) "xmlAssetPath is null.");
            return false;
        }

        XmlWriterSettings settings = new XmlWriterSettings()
        {
            Indent = true,
            OmitXmlDeclaration = false,
            Encoding = Encoding.UTF8
        };
        XmlSerializer xmlSerializer = new XmlSerializer(typeof(PMX2FBXConfig));
        try
        {
            using (FileStream output =
                   new FileStream(xmlAssetPath, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                using (XmlWriter xmlWriter = XmlWriter.Create((Stream) output, settings))
                    xmlSerializer.Serialize(xmlWriter, pmx2fbxConfig);
            }

            return true;
        }
        catch (Exception ex)
        {
            Debug.LogError((object) "");
            return false;
        }
    }

    public static string GetImporterAssetPath(string mmdAssetPath)
    {
        if (mmdAssetPath == null)
            return null;
        else
            return (MMD4MecanimEditorCommon.GetPathWithoutExtension(mmdAssetPath) + ".MMD4Mecanim.asset").Normalize(
            NormalizationForm.FormC);
    }

    public static string GetImporterPropertyAssetPath(string mmdAssetPath) => mmdAssetPath == null
        ? (string) null
        : (MMD4MecanimEditorCommon.GetPathWithoutExtension(mmdAssetPath) + ".MMD4Mecanim.xml").Normalize(
            NormalizationForm.FormC);

    public static string GetMMDModelPath(GameObject fbxAsset) =>
        (Object) fbxAsset == (Object) null
            ? (string) null
            : (MMD4MecanimEditorCommon.GetPathWithoutExtension(
                AssetDatabase.GetAssetPath((Object) fbxAsset)) + ".xml").Normalize(NormalizationForm.FormC);

    public static string GetMMDModelPath(string fbxAssetPath) => fbxAssetPath == null
        ? null
        : (MMD4MecanimEditorCommon.GetPathWithoutExtension(fbxAssetPath) + ".xml").Normalize(NormalizationForm.FormC);

    public static string GetIndexDataPath(string fbxAssetPath) => fbxAssetPath == null
        ? (string) null
        : (MMD4MecanimEditorCommon.GetPathWithoutExtension(fbxAssetPath) + ".index.bytes").Normalize(NormalizationForm
            .FormC);

    public static string GetVertexDataPath(string fbxAssetPath) => fbxAssetPath == null
        ? (string) null
        : (MMD4MecanimEditorCommon.GetPathWithoutExtension(fbxAssetPath) + ".vertex.bytes").Normalize(NormalizationForm
            .FormC);

    public static string GetModelDataPath(string fbxAssetPath) => fbxAssetPath == null
        ? (string) null
        : (MMD4MecanimEditorCommon.GetPathWithoutExtension(fbxAssetPath) + ".model.bytes").Normalize(NormalizationForm
            .FormC);

    public static string GetExtraDataPath(string fbxAssetPath) => fbxAssetPath == null
        ? (string) null
        : (MMD4MecanimEditorCommon.GetPathWithoutExtension(fbxAssetPath) + ".extra.bytes").Normalize(NormalizationForm
            .FormC);

    public static string GetAnimDataPath(string vmdAssetPath) => vmdAssetPath == null
        ? (string) null
        : (MMD4MecanimEditorCommon.GetPathWithoutExtension(vmdAssetPath) + ".anim.bytes").Normalize(NormalizationForm
            .FormC);

    public static MMDModel GetMMDModel(
        string xmlAssetPath)
    {
        if (string.IsNullOrEmpty(xmlAssetPath) || !File.Exists(xmlAssetPath))
            return (MMDModel) null;
        XmlSerializer xmlSerializer = new XmlSerializer(typeof(MMDModel));
        try
        {
            using (FileStream fileStream =
                   new FileStream(xmlAssetPath, System.IO.FileMode.Open, FileAccess.Read, FileShare.Read))
                return (MMDModel) xmlSerializer.Deserialize((Stream) fileStream);
        }
        catch (Exception ex)
        {
            Debug.LogError((object) ("GetMMDModel:" + ex.ToString()));
            return (MMDModel) null;
        }
    }

    private void _OnInspectorGUI_PMX2FBX()
    {
        GUI.enabled = !this.isProcessing;
        if (this.pmx2fbxProperty == null || this.pmx2fbxConfig == null)
            return;
        PMX2FBXConfig.MMD4MecanimProperty mmd4MecanimProperty =
            this.pmx2fbxConfig.mmd4MecanimProperty;
        PMX2FBXConfig.GlobalSettings globalSettings = this.pmx2fbxConfig.globalSettings;
        PMX2FBXConfig.BulletPhysics bulletPhysics = this.pmx2fbxConfig.bulletPhysics;
        if (this._editorAdvancedMode)
        {
            GUILayout.Label("Global Settings", EditorStyles.boldLabel);
            if (globalSettings != null && mmd4MecanimProperty != null)
            {
                EditorGUILayout.BeginHorizontal();
                GUILayout.Space(20f);
                EditorGUILayout.BeginVertical();
                if (mmd4MecanimProperty.vertexScaleByHeightFlag)
                {
                    mmd4MecanimProperty.vertexScaleByHeight =
                        EditorGUILayout.FloatField("Height", mmd4MecanimProperty.vertexScaleByHeight);
                    globalSettings.vertexScaleByHeight = mmd4MecanimProperty.vertexScaleByHeight;
                }
                else
                {
                    globalSettings.vertexScale = EditorGUILayout.FloatField("Vertex Scale", globalSettings.vertexScale);
                    globalSettings.vertexScaleByHeight = 0.0f;
                }

                mmd4MecanimProperty.vertexScaleByHeightFlag =
                    GUILayout.Toggle(mmd4MecanimProperty.vertexScaleByHeightFlag, "Vertex Scale by Height");
                globalSettings.importScale = EditorGUILayout.FloatField("Import Scale", globalSettings.importScale);
                globalSettings.blendShapesFlag =
                    EditorGUILayout.Toggle("BlendShapes", (uint) globalSettings.blendShapesFlag > 0U) ? 1 : 0;
                EditorGUILayout.EndVertical();
                EditorGUILayout.EndHorizontal();
                this.pmx2fbxProperty.viewAdvancedGlobalSettings =
                    EditorGUILayout.Foldout(this.pmx2fbxProperty.viewAdvancedGlobalSettings, "Advanced");
                if (this.pmx2fbxProperty.viewAdvancedGlobalSettings)
                {
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Space(20f);
                    EditorGUILayout.BeginVertical();
                    this.pmx2fbxProperty.viewAdvancedGlobalSettingsIK =
                        EditorGUILayout.Foldout(this.pmx2fbxProperty.viewAdvancedGlobalSettingsIK, "IK");
                    if (this.pmx2fbxProperty.viewAdvancedGlobalSettingsIK)
                    {
                        bool enabled = GUI.enabled;
                        globalSettings.enableIK =
                            EditorGUILayout.Toggle("EnableIK", (uint) globalSettings.enableIK > 0U) ? 1 : 0;
                        globalSettings.ikLoopMinIterations = EditorGUILayout.IntField("IKLoopMinIterations",
                            globalSettings.ikLoopMinIterations);
                        globalSettings.keepIKTargetBoneFlag = EditorGUILayout.Toggle("KeepIKTargetBone",
                            (uint) globalSettings.keepIKTargetBoneFlag > 0U)
                            ? 1
                            : 0;
                        globalSettings.forceIKResetBoneFlag = EditorGUILayout.Toggle("ForceIKResetBone",
                            (uint) globalSettings.forceIKResetBoneFlag > 0U)
                            ? 1
                            : 0;
                        globalSettings.enableIKFixedAxisFlag =
                            EditorGUILayout.IntField("IKFixedAxis", globalSettings.enableIKFixedAxisFlag);
                        globalSettings.enableIKSecondPassFlag = EditorGUILayout.BeginToggleGroup("IKSecondPass",
                            (uint) globalSettings.enableIKSecondPassFlag > 0U)
                            ? 1
                            : 0;
                        globalSettings.secondPassLimitAngle = EditorGUILayout.FloatField("SecondPassLimitAngle",
                            globalSettings.secondPassLimitAngle);
                        EditorGUILayout.EndToggleGroup();
                        globalSettings.enableIKInnerLockFlag = EditorGUILayout.BeginToggleGroup("IKInnerLock",
                            (uint) globalSettings.enableIKInnerLockFlag > 0U)
                            ? 1
                            : 0;
                        globalSettings.enableIKInnerLockKneeFlag = EditorGUILayout.IntField("EnableIKInnerLockKnee",
                            globalSettings.enableIKInnerLockKneeFlag);
                        globalSettings.innerLockKneeClamp =
                            EditorGUILayout.FloatField("InnerLockKneeClamp", globalSettings.innerLockKneeClamp);
                        globalSettings.innerLockKneeRatioU = EditorGUILayout.FloatField("InnerLockKneeRatioU",
                            globalSettings.innerLockKneeRatioU);
                        globalSettings.innerLockKneeRatioL = EditorGUILayout.FloatField("InnerLockKneeRatioL",
                            globalSettings.innerLockKneeRatioL);
                        globalSettings.innerLockKneeScale =
                            EditorGUILayout.FloatField("InnerLockKneeScale", globalSettings.innerLockKneeScale);
                        EditorGUILayout.EndToggleGroup();
                        globalSettings.enableIKMuscleFlag =
                            EditorGUILayout.BeginToggleGroup("IKMuscle", (uint) globalSettings.enableIKMuscleFlag > 0U)
                                ? 1
                                : 0;
                        GUI.enabled = enabled && (uint) globalSettings.enableIKMuscleFlag > 0U;
                        globalSettings.enableIKMuscleHipFlag = EditorGUILayout.Toggle("EnableIKMuscleHip",
                            (uint) globalSettings.enableIKMuscleHipFlag > 0U)
                            ? 1
                            : 0;
                        GUI.enabled = GUI.enabled && (uint) globalSettings.enableIKMuscleHipFlag > 0U;
                        globalSettings.muscleHipUpperXAngle = EditorGUILayout.FloatField("MuscleHipUpperXAngle",
                            globalSettings.muscleHipUpperXAngle);
                        globalSettings.muscleHipLowerXAngle = EditorGUILayout.FloatField("MuscleHipLowerXAngle",
                            globalSettings.muscleHipLowerXAngle);
                        globalSettings.muscleHipInnerYAngle = EditorGUILayout.FloatField("MuscleHipInnerYAngle",
                            globalSettings.muscleHipInnerYAngle);
                        globalSettings.muscleHipOuterYAngle = EditorGUILayout.FloatField("MuscleHipOuterYAngle",
                            globalSettings.muscleHipOuterYAngle);
                        globalSettings.muscleHipInnerZAngle = EditorGUILayout.FloatField("MuscleHipInnerZAngle",
                            globalSettings.muscleHipInnerZAngle);
                        globalSettings.muscleHipOuterZAngle = EditorGUILayout.FloatField("MuscleHipOuterZAngle",
                            globalSettings.muscleHipOuterZAngle);
                        GUI.enabled = enabled && (uint) globalSettings.enableIKMuscleFlag > 0U;
                        globalSettings.enableIKMuscleFootFlag = EditorGUILayout.Toggle("EnableIKMuscleFoot",
                            (uint) globalSettings.enableIKMuscleFootFlag > 0U)
                            ? 1
                            : 0;
                        GUI.enabled = GUI.enabled && (uint) globalSettings.enableIKMuscleFootFlag > 0U;
                        globalSettings.muscleFootUpperXAngle = EditorGUILayout.FloatField("MuscleFootUpperXAngle",
                            globalSettings.muscleFootUpperXAngle);
                        globalSettings.muscleFootLowerXAngle = EditorGUILayout.FloatField("MuscleFootLowerXAngle",
                            globalSettings.muscleFootLowerXAngle);
                        globalSettings.muscleFootInnerYAngle = EditorGUILayout.FloatField("MuscleFootInnerYAngle",
                            globalSettings.muscleFootInnerYAngle);
                        globalSettings.muscleFootOuterYAngle = EditorGUILayout.FloatField("MuscleFootOuterYAngle",
                            globalSettings.muscleFootOuterYAngle);
                        globalSettings.muscleFootInnerZAngle = EditorGUILayout.FloatField("MuscleFootInnerZAngle",
                            globalSettings.muscleFootInnerZAngle);
                        globalSettings.muscleFootOuterZAngle = EditorGUILayout.FloatField("MuscleFootOuterZAngle",
                            globalSettings.muscleFootOuterZAngle);
                        GUI.enabled = enabled;
                        EditorGUILayout.EndToggleGroup();
                    }

                    EditorGUILayout.Separator();
                    globalSettings.bone4MecanimFlag =
                        EditorGUILayout.BeginToggleGroup("Bone4Mecanim", (uint) globalSettings.bone4MecanimFlag > 0U)
                            ? 1
                            : 0;
                    globalSettings.bodyBone4Mecanim =
                        EditorGUILayout.Toggle("BodyBone4Mecanim", (uint) globalSettings.bodyBone4Mecanim > 0U) ? 1 : 0;
                    globalSettings.armBone4Mecanim =
                        EditorGUILayout.Toggle("ArmBone4Mecanim", (uint) globalSettings.armBone4Mecanim > 0U) ? 1 : 0;
                    globalSettings.handBone4Mecanim =
                        EditorGUILayout.Toggle("HandBone4Mecanim", (uint) globalSettings.handBone4Mecanim > 0U) ? 1 : 0;
                    globalSettings.handBone4MecanimModThumb0 = EditorGUILayout.Toggle("HandBone4MecanimModThumb0",
                        (uint) globalSettings.handBone4MecanimModThumb0 > 0U)
                        ? 1
                        : 0;
                    globalSettings.legBone4Mecanim =
                        EditorGUILayout.Toggle("LegBone4Mecanim", (uint) globalSettings.legBone4Mecanim > 0U) ? 1 : 0;
                    globalSettings.headBone4Mecanim =
                        EditorGUILayout.Toggle("HeadBone4Mecanim", (uint) globalSettings.headBone4Mecanim > 0U) ? 1 : 0;
                    EditorGUILayout.EndToggleGroup();
                    EditorGUILayout.Separator();
                    EditorGUILayout.LabelField("Etcetera", EditorStyles.boldLabel);
                    globalSettings.boneRenameFlag =
                        EditorGUILayout.Toggle("BoneRename", (uint) globalSettings.boneRenameFlag > 0U) ? 1 : 0;
                    globalSettings.prefixBoneRenameFlag = EditorGUILayout.Toggle("PrefixBoneRename",
                        (uint) globalSettings.prefixBoneRenameFlag > 0U)
                        ? 1
                        : 0;
                    globalSettings.prefixBoneNoNameFlag = EditorGUILayout.Toggle("PrefixBoneNoName",
                        (uint) globalSettings.prefixBoneNoNameFlag > 0U)
                        ? 1
                        : 0;
                    globalSettings.prefixNullBoneNameFlag = EditorGUILayout.Toggle("PrefixNullBoneName",
                        (uint) globalSettings.prefixNullBoneNameFlag > 0U)
                        ? 1
                        : 0;
                    globalSettings.morphRenameFlag =
                        EditorGUILayout.Toggle("MorphRename", (uint) globalSettings.morphRenameFlag > 0U) ? 1 : 0;
                    globalSettings.prefixMorphNoNameFlag = EditorGUILayout.Toggle("PrefixMorphNoName",
                        (uint) globalSettings.prefixMorphNoNameFlag > 0U)
                        ? 1
                        : 0;
                    globalSettings.materialRenameFlag =
                        EditorGUILayout.Toggle("MaterialRename", (uint) globalSettings.materialRenameFlag > 0U) ? 1 : 0;
                    globalSettings.prefixMaterialNoNameFlag = EditorGUILayout.Toggle("PrefixMaterialNoName",
                        (uint) globalSettings.prefixMaterialNoNameFlag > 0U)
                        ? 1
                        : 0;
                    globalSettings.escapeMaterialNameFlag = EditorGUILayout.Toggle("EscapeMaterialName",
                        (uint) globalSettings.escapeMaterialNameFlag > 0U)
                        ? 1
                        : 0;
                    EditorGUILayout.Separator();
                    globalSettings.boneMorphFlag =
                        EditorGUILayout.Toggle("BoneMorph", (uint) globalSettings.boneMorphFlag > 0U) ? 1 : 0;
                    globalSettings.createSkeletonFlag =
                        EditorGUILayout.Toggle("CreateSkeleton", (uint) globalSettings.createSkeletonFlag > 0U) ? 1 : 0;
                    globalSettings.addMotionNameExtFlag = EditorGUILayout.Toggle("AddMotionNameExt(_vmd)",
                        (uint) globalSettings.addMotionNameExtFlag > 0U)
                        ? 1
                        : 0;
                    globalSettings.addMotionNameExtAsLegacyFlag = EditorGUILayout.Toggle(" - As Legacy(.vmd)",
                        (uint) globalSettings.addMotionNameExtAsLegacyFlag > 0U)
                        ? 1
                        : 0;
                    globalSettings.nullBoneFlag =
                        EditorGUILayout.Toggle("NullBone", (uint) globalSettings.nullBoneFlag > 0U) ? 1 : 0;
                    globalSettings.nullBoneAnimationFlag = EditorGUILayout.Toggle("NullBoneAnimation",
                        (uint) globalSettings.nullBoneAnimationFlag > 0U)
                        ? 1
                        : 0;
                    globalSettings.fixedAxisFlag = EditorGUILayout.IntField("FixedAxis", globalSettings.fixedAxisFlag);
                    globalSettings.localAxisFlag = EditorGUILayout.IntField("LocalAxis", globalSettings.localAxisFlag);
                    bool enabled1 = GUI.enabled;
                    globalSettings.splitMeshFlag =
                        EditorGUILayout.Toggle("SplitMesh", (uint) globalSettings.splitMeshFlag > 0U) ? 1 : 0;
                    GUI.enabled = enabled1 && (uint) globalSettings.splitMeshFlag > 0U;
                    globalSettings.splitMeshVertexMorphFlag = EditorGUILayout.Toggle("SplitMesh VertexMorph",
                        (uint) globalSettings.splitMeshVertexMorphFlag > 0U)
                        ? 1
                        : 0;
                    globalSettings.splitMeshVertexMorphExtraFlag = EditorGUILayout.Toggle("SplitMesh VertexMorphExtra",
                        (uint) globalSettings.splitMeshVertexMorphExtraFlag > 0U)
                        ? 1
                        : 0;
                    globalSettings.splitMeshVertexMorphCombineFlag = EditorGUILayout.Toggle(
                        "SplitMesh VertexMorphCombine", (uint) globalSettings.splitMeshVertexMorphCombineFlag > 0U)
                        ? 1
                        : 0;
                    globalSettings.splitMeshXDEFFlag =
                        EditorGUILayout.Toggle("SplitMesh XDEF", (uint) globalSettings.splitMeshXDEFFlag > 0U) ? 1 : 0;
                    globalSettings.splitMeshXDEFExtraFlag = EditorGUILayout.Toggle("SplitMesh XDEF Extra",
                        (uint) globalSettings.splitMeshXDEFExtraFlag > 0U)
                        ? 1
                        : 0;
                    globalSettings.splitMeshXDEFExtraAlphaFlag = EditorGUILayout.Toggle("SplitMesh XDEF ExtraAlpha",
                        (uint) globalSettings.splitMeshXDEFExtraAlphaFlag > 0U)
                        ? 1
                        : 0;
                    globalSettings.splitMeshNoShadowCastingFlag = EditorGUILayout.Toggle("SplitMesh NoShadowCasting",
                        (uint) globalSettings.splitMeshNoShadowCastingFlag > 0U)
                        ? 1
                        : 0;
                    GUI.enabled = enabled1;
                    EditorGUILayout.Separator();
                    globalSettings.sameVertexLength = (double) EditorGUILayout.FloatField("SameVertexLength",
                        (float) globalSettings.sameVertexLength);
                    EditorGUILayout.Separator();
                    globalSettings.rotationOrderToZXY = EditorGUILayout.Toggle("RotationOrderToZXY",
                        (uint) globalSettings.rotationOrderToZXY > 0U)
                        ? 1
                        : 0;
                    globalSettings.enableFBXTexture =
                        EditorGUILayout.Toggle("EnableFBXTexture", (uint) globalSettings.enableFBXTexture > 0U) ? 1 : 0;
                    globalSettings.exportFBXFormat =
                        EditorGUILayout.TextField("ExportFBXFormat", globalSettings.exportFBXFormat);
                    EditorGUILayout.Separator();
                    EditorGUILayout.Separator();
                    globalSettings.supportVMDIKDisabled = EditorGUILayout.Toggle("SupportVMDIKDisabled",
                        (uint) globalSettings.supportVMDIKDisabled > 0U)
                        ? 1
                        : 0;
                    globalSettings.supportVMDPhysicsDisabled = EditorGUILayout.Toggle("SupportVMDPhysicsDisabled",
                        (uint) globalSettings.supportVMDPhysicsDisabled > 0U)
                        ? 1
                        : 0;
                    EditorGUILayout.Separator();
                    globalSettings.animKeyReductionFlag = EditorGUILayout.Toggle("AnimKeyReduction",
                        (uint) globalSettings.animKeyReductionFlag > 0U)
                        ? 1
                        : 0;
                    globalSettings.animNullAnimationFlag = EditorGUILayout.Toggle("AnimNullAnimation",
                        (uint) globalSettings.animNullAnimationFlag > 0U)
                        ? 1
                        : 0;
                    globalSettings.animRootTransformFlag = EditorGUILayout.Toggle("AnimRootTransform",
                        (uint) globalSettings.animRootTransformFlag > 0U)
                        ? 1
                        : 0;
                    globalSettings.animKeyRotationEpsilon1 = EditorGUILayout.FloatField("AnimKeyRotationEpsilon1",
                        globalSettings.animKeyRotationEpsilon1);
                    globalSettings.animKeyRotationEpsilon2 = EditorGUILayout.FloatField("AnimKeyRotationEpsilon2",
                        globalSettings.animKeyRotationEpsilon2);
                    globalSettings.animKeyTranslationEpsilon1 = EditorGUILayout.FloatField("AnimKeyTranslationEpsilon1",
                        globalSettings.animKeyTranslationEpsilon1);
                    globalSettings.animKeyTranslationEpsilon2 = EditorGUILayout.FloatField("AnimKeyTranslationEpsilon2",
                        globalSettings.animKeyTranslationEpsilon2);
                    globalSettings.animAwakeWaitingTime = EditorGUILayout.IntField("AnimAwakeWaitingTime",
                        globalSettings.animAwakeWaitingTime);
                    EditorGUILayout.Separator();
                    globalSettings.morphOppaiFlag =
                        EditorGUILayout.BeginToggleGroup("MorphOppai", (uint) globalSettings.morphOppaiFlag > 0U)
                            ? 1
                            : 0;
                    globalSettings.morphOppaiTanimaMethod = EditorGUILayout.IntField("MorphOppaiTanimaMethod",
                        globalSettings.morphOppaiTanimaMethod);
                    globalSettings.morphOppaiPower =
                        (double) EditorGUILayout.FloatField("MorphOppaiPower", (float) globalSettings.morphOppaiPower);
                    globalSettings.morphOppaiCenter = (double) EditorGUILayout.FloatField("MorphOppaiCenter",
                        (float) globalSettings.morphOppaiCenter);
                    globalSettings.morphOppaiRaidus = (double) EditorGUILayout.FloatField("MorphOppaiRaidus",
                        (float) globalSettings.morphOppaiRaidus);
                    globalSettings.morphOppaiBoneName1 =
                        EditorGUILayout.TextField("MorphOppaiBoneName1", globalSettings.morphOppaiBoneName1);
                    globalSettings.morphOppaiBoneName2 =
                        EditorGUILayout.TextField("MorphOppaiBoneName2", globalSettings.morphOppaiBoneName2);
                    EditorGUILayout.EndToggleGroup();
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.EndHorizontal();
                }
            }

            EditorGUILayout.Separator();
            GUILayout.Label("Bullet Physics", EditorStyles.boldLabel);
            if (bulletPhysics != null && this.pmx2fbxProperty != null)
            {
                EditorGUILayout.BeginHorizontal();
                GUILayout.Space(20f);
                EditorGUILayout.BeginVertical();
                bulletPhysics.enabled = EditorGUILayout.Toggle("Enabled", (uint) bulletPhysics.enabled > 0U) ? 1 : 0;
                mmd4MecanimProperty.bulletPhysicsVersion =
                    (PMX2FBXConfig.BulletPhysicsVersion) EditorGUILayout.Popup("Version",
                        (int) mmd4MecanimProperty.bulletPhysicsVersion,
                        PMX2FBXConfig.BulletPhysicsVersionString);
                if (!this.isProcessing)
                    GUI.enabled = (uint) bulletPhysics.enabled > 0U;
                bulletPhysics.framePerSecond =
                    EditorGUILayout.IntField("Frame Per Second", bulletPhysics.framePerSecond);
                bulletPhysics.gravityScale = EditorGUILayout.FloatField("Gravity Scale", bulletPhysics.gravityScale);
                EditorGUILayout.EndVertical();
                EditorGUILayout.EndHorizontal();
                this.pmx2fbxProperty.viewAdvancedBulletPhysics =
                    EditorGUILayout.Foldout(this.pmx2fbxProperty.viewAdvancedBulletPhysics, "Advanced");
                if (this.pmx2fbxProperty.viewAdvancedBulletPhysics)
                {
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Space(20f);
                    EditorGUILayout.BeginVertical();
                    bulletPhysics.worldSolverInfoNumIterations =
                        EditorGUILayout.IntField("WorldSolverInfoNumIterations",
                            bulletPhysics.worldSolverInfoNumIterations);
                    bulletPhysics.worldSolverInfoSplitImpulse = EditorGUILayout.Toggle("WorldSolverInfoSplitImpulse",
                        (uint) bulletPhysics.worldSolverInfoSplitImpulse > 0U)
                        ? 1
                        : 0;
                    bulletPhysics.worldAddFloorPlane = EditorGUILayout.Toggle("WorldAddFloorPlane",
                        (uint) bulletPhysics.worldAddFloorPlane > 0U)
                        ? 1
                        : 0;
                    bulletPhysics.rigidBodyIsAdditionalDamping = EditorGUILayout.Toggle("RigidBodyIsAdditionalDamping",
                        (uint) bulletPhysics.rigidBodyIsAdditionalDamping > 0U)
                        ? 1
                        : 0;
                    bulletPhysics.rigidBodyIsForceBoneAlignment = EditorGUILayout.Toggle(
                        "RigidBodyIsForceBoneAlignment", (uint) bulletPhysics.rigidBodyIsForceBoneAlignment > 0U)
                        ? 1
                        : 0;
                    bulletPhysics.rigidBodyIsEnableSleeping = EditorGUILayout.Toggle("RigidBodyIsEnableSleeping",
                        (uint) bulletPhysics.rigidBodyIsEnableSleeping > 0U)
                        ? 1
                        : 0;
                    bulletPhysics.rigidBodyIsUseCcd =
                        EditorGUILayout.Toggle("RigidBodyIsUseCcd", (uint) bulletPhysics.rigidBodyIsUseCcd > 0U)
                            ? 1
                            : 0;
                    bulletPhysics.rigidBodyCcdMotionThreshold =
                        EditorGUILayout.FloatField("RigidBodyCcdMotionThreshold",
                            bulletPhysics.rigidBodyCcdMotionThreshold);
                    bulletPhysics.rigidBodyShapeScale =
                        EditorGUILayout.FloatField("RigidBodyShapeScale", bulletPhysics.rigidBodyShapeScale);
                    bulletPhysics.rigidBodyMassRate =
                        EditorGUILayout.FloatField("RigidBodyMassRate", bulletPhysics.rigidBodyMassRate);
                    bulletPhysics.rigidBodyLinearDampingRate = EditorGUILayout.FloatField("RigidBodyLinearDampingRate",
                        bulletPhysics.rigidBodyLinearDampingRate);
                    bulletPhysics.rigidBodyAngularDampingRate =
                        EditorGUILayout.FloatField("RigidBodyAngularDampingRate",
                            bulletPhysics.rigidBodyAngularDampingRate);
                    bulletPhysics.rigidBodyRestitutionRate = EditorGUILayout.FloatField("RigidBodyRestitutionRate",
                        bulletPhysics.rigidBodyRestitutionRate);
                    bulletPhysics.rigidBodyFrictionRate = EditorGUILayout.FloatField("RigidBodyFrictionRate",
                        bulletPhysics.rigidBodyFrictionRate);
                    bulletPhysics.rigidBodyAntiJitterRate = EditorGUILayout.Slider("RigidBodyAntiJitterRate",
                        bulletPhysics.rigidBodyAntiJitterRate, 0.0f, 1f);
                    bulletPhysics.rigidBodyAntiJitterRateOnKinematic = EditorGUILayout.Slider(
                        "RigidBodyAntiJitterRateOnKinematic", bulletPhysics.rigidBodyAntiJitterRateOnKinematic, 0.0f,
                        1f);
                    bulletPhysics.rigidBodyPreBoneAlignmentLimitLength = EditorGUILayout.FloatField(
                        "RigidBodyPreBoneAlignmentLimitLength", bulletPhysics.rigidBodyPreBoneAlignmentLimitLength);
                    bulletPhysics.rigidBodyPreBoneAlignmentLossRate = EditorGUILayout.FloatField(
                        "RigidBodyPreBoneAlignmentLossRate", bulletPhysics.rigidBodyPreBoneAlignmentLossRate);
                    bulletPhysics.rigidBodyPostBoneAlignmentLimitLength = EditorGUILayout.FloatField(
                        "RigidBodyPostBoneAlignmentLimitLength", bulletPhysics.rigidBodyPostBoneAlignmentLimitLength);
                    bulletPhysics.rigidBodyPostBoneAlignmentLossRate = EditorGUILayout.FloatField(
                        "RigidBodyPostBoneAlignmentLossRate", bulletPhysics.rigidBodyPostBoneAlignmentLossRate);
                    bulletPhysics.rigidBodyLinearDampingLossRate =
                        EditorGUILayout.FloatField("RigidBodyLinearDampingLossRate",
                            bulletPhysics.rigidBodyLinearDampingLossRate);
                    bulletPhysics.rigidBodyLinearDampingLimit =
                        EditorGUILayout.FloatField("RigidBodyLinearDampingLimit",
                            bulletPhysics.rigidBodyLinearDampingLimit);
                    bulletPhysics.rigidBodyAngularDampingLossRate =
                        EditorGUILayout.FloatField("RigidBodyAngularDampingLossRate",
                            bulletPhysics.rigidBodyAngularDampingLossRate);
                    bulletPhysics.rigidBodyAngularDampingLimit =
                        EditorGUILayout.FloatField("RigidBodyAngularDampingLimit",
                            bulletPhysics.rigidBodyAngularDampingLimit);
                    bulletPhysics.rigidBodyLinearVelocityLimit =
                        EditorGUILayout.FloatField("RigidBodyLinearVelocityLimit",
                            bulletPhysics.rigidBodyLinearVelocityLimit);
                    bulletPhysics.rigidBodyAngularVelocityLimit =
                        EditorGUILayout.FloatField("RigidBodyAngularVelocityLimit",
                            bulletPhysics.rigidBodyAngularVelocityLimit);
                    bulletPhysics.rigidBodyIsUseForceAngularVelocityLimit =
                        EditorGUILayout.Toggle("RigidBodyIsUseForceAngularVelocityLimit",
                            (uint) bulletPhysics.rigidBodyIsUseForceAngularVelocityLimit > 0U)
                            ? 1
                            : 0;
                    bulletPhysics.rigidBodyIsUseForceAngularAccelerationLimit =
                        EditorGUILayout.Toggle("RigidBodyIsUseForceAngularAccelerationLimit",
                            (uint) bulletPhysics.rigidBodyIsUseForceAngularAccelerationLimit > 0U)
                            ? 1
                            : 0;
                    bulletPhysics.rigidBodyForceAngularVelocityLimit = EditorGUILayout.FloatField(
                        "RigidBodyForceAngularVelocityLimit", bulletPhysics.rigidBodyForceAngularVelocityLimit);
                    bulletPhysics.rigidBodyIsAdditionalCollider = EditorGUILayout.Toggle(
                        "RigidBodyIsAdditionalCollider", (uint) bulletPhysics.rigidBodyIsAdditionalCollider > 0U)
                        ? 1
                        : 0;
                    bulletPhysics.rigidBodyAdditionalColliderBias =
                        EditorGUILayout.FloatField("RigidBodyAdditionalColliderBias",
                            bulletPhysics.rigidBodyAdditionalColliderBias);
                    bulletPhysics.rigidBodyIsForceTranslate = EditorGUILayout.Toggle("RigidBodyIsForceTranslate",
                        (uint) bulletPhysics.rigidBodyIsForceTranslate > 0U)
                        ? 1
                        : 0;
                    bulletPhysics.jointRootAdditionalLimitAngle =
                        EditorGUILayout.FloatField("JointRootAdditionalLimitAngle",
                            bulletPhysics.jointRootAdditionalLimitAngle);
                    bulletPhysics.jointAdditionalLimitAngle = EditorGUILayout.FloatField("JointAdditionalLimitAngle",
                        bulletPhysics.jointAdditionalLimitAngle);
                    bulletPhysics.jointAdditionalLimitForcibly = EditorGUILayout.Toggle("JointAdditionalLimitForcibly",
                        (uint) bulletPhysics.jointAdditionalLimitForcibly > 0U)
                        ? 1
                        : 0;
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.EndHorizontal();
                }

                if (!this.isProcessing)
                    GUI.enabled = true;
            }

            EditorGUILayout.Separator();
            GUILayout.Label("Rename List", EditorStyles.boldLabel);
            if (this.pmx2fbxConfig.renameList != null && this.pmx2fbxConfig.renameList.Count > 0)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Space(40f);
                GUILayout.Label("From", GUILayout.ExpandWidth(true));
                GUILayout.Label("To", GUILayout.ExpandWidth(true));
                GUILayout.EndHorizontal();
                int index = 0;
                while (index < this.pmx2fbxConfig.renameList.Count)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(20f);
                    bool flag = GUILayout.Button("-", EditorStyles.miniButton, GUILayout.ExpandWidth(false));
                    if (this.pmx2fbxConfig.renameList[index].from == null)
                        this.pmx2fbxConfig.renameList[index].from = "";
                    if (this.pmx2fbxConfig.renameList[index].to == null)
                        this.pmx2fbxConfig.renameList[index].to = "";
                    this.pmx2fbxConfig.renameList[index].from =
                        EditorGUILayout.TextField(this.pmx2fbxConfig.renameList[index].from);
                    this.pmx2fbxConfig.renameList[index].to =
                        EditorGUILayout.TextField(this.pmx2fbxConfig.renameList[index].to);
                    GUILayout.EndHorizontal();
                    if (flag)
                        this.pmx2fbxConfig.renameList.RemoveAt(index);
                    else
                        ++index;
                }
            }

            GUILayout.BeginHorizontal();
            GUILayout.Space(20f);
            bool flag1 = GUILayout.Button("+", EditorStyles.miniButton, GUILayout.ExpandWidth(false));
            GUILayout.Label("Add");
            GUILayout.EndHorizontal();
            if (flag1)
            {
                PMX2FBXConfig.Rename
                    rename = new PMX2FBXConfig.Rename();
                rename.from = "";
                rename.to = "";
                if (this.pmx2fbxConfig.renameList == null)
                    this.pmx2fbxConfig.renameList = new List<PMX2FBXConfig.Rename>();
                this.pmx2fbxConfig.renameList.Add(rename);
            }

            EditorGUILayout.Separator();
            GUILayout.Label("SplitMeshBone List", EditorStyles.boldLabel);
            if (this.pmx2fbxConfig.splitMeshBoneList != null && this.pmx2fbxConfig.splitMeshBoneList.Count > 0)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Space(40f);
                GUILayout.Label("Name", GUILayout.ExpandWidth(true));
                GUILayout.Label("Extend", GUILayout.ExpandWidth(true));
                GUILayout.EndHorizontal();
                int index = 0;
                while (index < this.pmx2fbxConfig.splitMeshBoneList.Count)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(20f);
                    bool flag2 = GUILayout.Button("-", EditorStyles.miniButton, GUILayout.ExpandWidth(false));
                    if (this.pmx2fbxConfig.splitMeshBoneList[index].name == null)
                        this.pmx2fbxConfig.splitMeshBoneList[index].name = "";
                    this.pmx2fbxConfig.splitMeshBoneList[index].name =
                        EditorGUILayout.TextField(this.pmx2fbxConfig.splitMeshBoneList[index].name);
                    this.pmx2fbxConfig.splitMeshBoneList[index].extend =
                        (int) (PMX2FBXConfig.SplitMeshBoneExtend) EditorGUILayout.EnumPopup("",
                            (Enum) (PMX2FBXConfig.SplitMeshBoneExtend) this.pmx2fbxConfig
                                .splitMeshBoneList[index].extend);
                    GUILayout.EndHorizontal();
                    if (flag2)
                        this.pmx2fbxConfig.splitMeshBoneList.RemoveAt(index);
                    else
                        ++index;
                }
            }

            GUILayout.BeginHorizontal();
            GUILayout.Space(20f);
            bool flag3 = GUILayout.Button("+", EditorStyles.miniButton, GUILayout.ExpandWidth(false));
            GUILayout.Label("Add");
            GUILayout.EndHorizontal();
            if (flag3)
            {
                PMX2FBXConfig.SplitMeshBone splitMeshBone =
                    new PMX2FBXConfig.SplitMeshBone();
                if (this.pmx2fbxConfig.splitMeshBoneList == null)
                    this.pmx2fbxConfig.splitMeshBoneList =
                        new List<PMX2FBXConfig.SplitMeshBone>();
                this.pmx2fbxConfig.splitMeshBoneList.Add(splitMeshBone);
            }

            EditorGUILayout.Separator();
            GUILayout.Label("EdgeStretch List", EditorStyles.boldLabel);
            globalSettings.edgeStretchEnabled =
                EditorGUILayout.Toggle("    EdgeStretchEnabled", (uint) globalSettings.edgeStretchEnabled > 0U) ? 1 : 0;
            globalSettings.sameVertexLengthEdgeStretch =
                (double) EditorGUILayout.FloatField("    SameVertexLengthEdgeStretch",
                    (float) globalSettings.sameVertexLengthEdgeStretch);
            if (this.pmx2fbxConfig.edgeStretchList != null && this.pmx2fbxConfig.edgeStretchList.Count > 0)
            {
                int index = 0;
                while (index < this.pmx2fbxConfig.edgeStretchList.Count)
                {
                    if (this.pmx2fbxConfig.edgeStretchList[index] == null)
                        this.pmx2fbxConfig.edgeStretchList[index] =
                            new PMX2FBXConfig.EdgeStretch();
                    PMX2FBXConfig.EdgeStretch edgeStretch =
                        this.pmx2fbxConfig.edgeStretchList[index];
                    if (edgeStretch.targetArea == null)
                        edgeStretch.targetArea = new PMX2FBXConfig.EdgeStretchArea();
                    if (edgeStretch.neighborArea == null)
                        edgeStretch.neighborArea = new PMX2FBXConfig.EdgeStretchArea();
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(20f);
                    bool flag4 = GUILayout.Button("-", EditorStyles.miniButton, GUILayout.ExpandWidth(false));
                    GUILayout.Label("EdgeStretch");
                    GUILayout.FlexibleSpace();
                    GUILayout.EndHorizontal();
                    EditorGUILayout.Separator();
                    edgeStretch.targetArea.targetBoneName = EditorGUILayout.TextField("      TargetBoneName",
                        edgeStretch.targetArea.targetBoneName);
                    edgeStretch.targetArea.ignoreBoneName = EditorGUILayout.TextField("      IgnoreBoneName",
                        edgeStretch.targetArea.ignoreBoneName);
                    edgeStretch.targetArea.materialName =
                        EditorGUILayout.TextField("      MaterialName", edgeStretch.targetArea.materialName);
                    edgeStretch.targetArea.stretchLength = (double) EditorGUILayout.FloatField("      StretchLength",
                        (float) edgeStretch.targetArea.stretchLength);
                    edgeStretch.targetArea.extrudeAmount = (double) EditorGUILayout.FloatField("      ExtrudeAmount",
                        (float) edgeStretch.targetArea.extrudeAmount);
                    edgeStretch.neighborEnabled =
                        EditorGUILayout.Toggle("      NeighborEnabled", (uint) edgeStretch.neighborEnabled > 0U)
                            ? 1
                            : 0;
                    if (edgeStretch.neighborEnabled != 0)
                    {
                        edgeStretch.neighborLength = (double) EditorGUILayout.FloatField("      NeighborLength",
                            (float) edgeStretch.neighborLength);
                        edgeStretch.neighborArea.targetBoneName = EditorGUILayout.TextField("      TargetBoneName",
                            edgeStretch.neighborArea.targetBoneName);
                        edgeStretch.neighborArea.ignoreBoneName = EditorGUILayout.TextField("      IgnoreBoneName",
                            edgeStretch.neighborArea.ignoreBoneName);
                        edgeStretch.neighborArea.materialName = EditorGUILayout.TextField("      MaterialName",
                            edgeStretch.neighborArea.materialName);
                        edgeStretch.neighborArea.stretchLength =
                            (double) EditorGUILayout.FloatField("      StretchLength",
                                (float) edgeStretch.neighborArea.stretchLength);
                        edgeStretch.neighborArea.extrudeAmount =
                            (double) EditorGUILayout.FloatField("      ExtrudeAmount",
                                (float) edgeStretch.neighborArea.extrudeAmount);
                    }

                    EditorGUILayout.Separator();
                    if (flag4)
                        this.pmx2fbxConfig.edgeStretchList.RemoveAt(index);
                    else
                        ++index;
                }
            }

            GUILayout.BeginHorizontal();
            GUILayout.Space(20f);
            bool flag5 = GUILayout.Button("+", EditorStyles.miniButton, GUILayout.ExpandWidth(false));
            GUILayout.Label("Add");
            GUILayout.EndHorizontal();
            if (flag5)
            {
                PMX2FBXConfig.EdgeStretch edgeStretch =
                    new PMX2FBXConfig.EdgeStretch();
                if (this.pmx2fbxConfig.edgeStretchList == null)
                    this.pmx2fbxConfig.edgeStretchList = new List<PMX2FBXConfig.EdgeStretch>();
                this.pmx2fbxConfig.edgeStretchList.Add(edgeStretch);
            }

            EditorGUILayout.Separator();
            GUILayout.Label("Freeze Rigid Body List", EditorStyles.boldLabel);
            if (this.pmx2fbxConfig.freezeRigidBodyList != null)
            {
                int index = 0;
                while (index < this.pmx2fbxConfig.freezeRigidBodyList.Count)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(20f);
                    bool flag6 = GUILayout.Button("-", EditorStyles.miniButton, GUILayout.ExpandWidth(false));
                    if (this.pmx2fbxConfig.freezeRigidBodyList[index].boneName == null)
                        this.pmx2fbxConfig.freezeRigidBodyList[index].boneName = "";
                    GUILayout.Label("BoneName", GUILayout.ExpandWidth(false));
                    GUILayout.Space(4f);
                    this.pmx2fbxConfig.freezeRigidBodyList[index].boneName =
                        EditorGUILayout.TextField(this.pmx2fbxConfig.freezeRigidBodyList[index].boneName);
                    this.pmx2fbxConfig.freezeRigidBodyList[index].recursively =
                        GUILayout.Toggle((uint) this.pmx2fbxConfig.freezeRigidBodyList[index].recursively > 0U,
                            "Recursively")
                            ? 1
                            : 0;
                    GUILayout.EndHorizontal();
                    if (flag6)
                        this.pmx2fbxConfig.freezeRigidBodyList.RemoveAt(index);
                    else
                        ++index;
                }
            }

            GUILayout.BeginHorizontal();
            GUILayout.Space(20f);
            bool flag7 = GUILayout.Button("+", EditorStyles.miniButton, GUILayout.ExpandWidth(false));
            GUILayout.Label("Add");
            GUILayout.EndHorizontal();
            if (flag7)
            {
                PMX2FBXConfig.FreezeRigidBody freezeRigidBody =
                    new PMX2FBXConfig.FreezeRigidBody();
                freezeRigidBody.boneName = "";
                freezeRigidBody.recursively = 0;
                if (this.pmx2fbxConfig.freezeRigidBodyList == null)
                    this.pmx2fbxConfig.freezeRigidBodyList =
                        new List<PMX2FBXConfig.FreezeRigidBody>();
                this.pmx2fbxConfig.freezeRigidBodyList.Add(freezeRigidBody);
            }

            EditorGUILayout.Separator();
            GUILayout.Label("Freeze Motion List", EditorStyles.boldLabel);
            if (this.pmx2fbxConfig.freezeMotionList != null)
            {
                int index = 0;
                while (index < this.pmx2fbxConfig.freezeMotionList.Count)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(20f);
                    bool flag8 = GUILayout.Button("-", EditorStyles.miniButton, GUILayout.ExpandWidth(false));
                    if (this.pmx2fbxConfig.freezeMotionList[index].boneName == null)
                        this.pmx2fbxConfig.freezeMotionList[index].boneName = "";
                    GUILayout.Label("BoneName", GUILayout.ExpandWidth(false));
                    GUILayout.Space(4f);
                    this.pmx2fbxConfig.freezeMotionList[index].boneName =
                        EditorGUILayout.TextField(this.pmx2fbxConfig.freezeMotionList[index].boneName);
                    this.pmx2fbxConfig.freezeMotionList[index].recursively =
                        GUILayout.Toggle((uint) this.pmx2fbxConfig.freezeMotionList[index].recursively > 0U,
                            "Recursively")
                            ? 1
                            : 0;
                    GUILayout.EndHorizontal();
                    if (flag8)
                        this.pmx2fbxConfig.freezeMotionList.RemoveAt(index);
                    else
                        ++index;
                }
            }

            GUILayout.BeginHorizontal();
            GUILayout.Space(20f);
            bool flag9 = GUILayout.Button("+", EditorStyles.miniButton, GUILayout.ExpandWidth(false));
            GUILayout.Label("Add");
            GUILayout.EndHorizontal();
            if (flag9)
            {
                PMX2FBXConfig.FreezeMotion freezeMotion =
                    new PMX2FBXConfig.FreezeMotion();
                freezeMotion.boneName = "";
                freezeMotion.recursively = 0;
                if (this.pmx2fbxConfig.freezeMotionList == null)
                    this.pmx2fbxConfig.freezeMotionList =
                        new List<PMX2FBXConfig.FreezeMotion>();
                this.pmx2fbxConfig.freezeMotionList.Add(freezeMotion);
            }

            EditorGUILayout.Separator();
        }

        GUILayout.Label("PMX/PMD", EditorStyles.boldLabel);
        GUILayout.BeginHorizontal();
        GUILayout.Space(26f);
        Object assetObject1 = EditorGUILayout.ObjectField(
            this.pmx2fbxProperty != null ? this.pmx2fbxProperty.pmxAsset : (Object) null,
            typeof(Object), false);
        if (assetObject1 != (Object) null)
        {
            string lower = Path.GetExtension(AssetDatabase.GetAssetPath(assetObject1)).ToLower();
            if (lower == ".pmx" || lower == ".pmd")
                this.pmx2fbxProperty.pmxAsset = assetObject1;
        }

        GUILayout.EndHorizontal();
        EditorGUILayout.Separator();
        GUILayout.Label("VMD", EditorStyles.boldLabel);
        if (this.pmx2fbxProperty != null && this.pmx2fbxProperty.vmdAssetList != null)
        {
            int index = 0;
            while (index < this.pmx2fbxProperty.vmdAssetList.Count)
            {
                GUILayout.BeginHorizontal();
                bool flag = GUILayout.Button("-", EditorStyles.miniButton, GUILayout.ExpandWidth(false));
                Object assetObject2 = EditorGUILayout.ObjectField(this.pmx2fbxProperty.vmdAssetList[index],
                    typeof(Object), false);
                GUILayout.EndHorizontal();
                if (assetObject2 != (Object) null)
                {
                    if (Path.GetExtension(AssetDatabase.GetAssetPath(assetObject2)).ToLower() == ".vmd")
                        this.pmx2fbxProperty.vmdAssetList[index] = assetObject2;
                }
                else
                    flag = true;

                if (flag)
                    this.pmx2fbxProperty.vmdAssetList.RemoveAt(index);
                else
                    ++index;
            }
        }

        GUILayout.BeginHorizontal();
        GUILayout.Space(26f);
        Object assetObject3 =
            EditorGUILayout.ObjectField(null, typeof(Object), false);
        if (assetObject3 != null &&
            Path.GetExtension(AssetDatabase.GetAssetPath(assetObject3)).ToLower() == ".vmd")
        {
            if (this.pmx2fbxProperty.vmdAssetList == null)
                this.pmx2fbxProperty.vmdAssetList = new List<Object>();
            this.pmx2fbxProperty.vmdAssetList.Add(assetObject3);
        }

        GUILayout.EndHorizontal();
        EditorGUILayout.Separator();
        GUILayout.Label("FBX Path", EditorStyles.boldLabel);
        GUILayout.BeginHorizontal();
        GUILayout.Space(26f);
        mmd4MecanimProperty.fbxOutputPath = EditorGUILayout.TextField(mmd4MecanimProperty.fbxOutputPath);
        GUILayout.EndHorizontal();
        if (Application.platform != RuntimePlatform.WindowsEditor)
        {
            GUILayout.Label("Wine", EditorStyles.boldLabel);
            GUILayout.BeginHorizontal();
            GUILayout.Space(26f);
            mmd4MecanimProperty.useWineFlag = EditorGUILayout.Toggle("Enabled", mmd4MecanimProperty.useWineFlag);
            GUILayout.EndHorizontal();
            if (!this.isProcessing && !mmd4MecanimProperty.useWineFlag)
                GUI.enabled = false;
            GUILayout.BeginHorizontal();
            GUILayout.Space(26f);
            mmd4MecanimProperty.wine =
                (PMX2FBXConfig.Wine) EditorGUILayout.EnumPopup("Type",
                    (Enum) mmd4MecanimProperty.wine);
            GUILayout.EndHorizontal();
            string winePath = WinePaths[(int) mmd4MecanimProperty.wine];
            if (mmd4MecanimProperty.wine == PMX2FBXConfig.Wine.Manual)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Space(26f);
                mmd4MecanimProperty.winePath = EditorGUILayout.TextField(mmd4MecanimProperty.winePath);
                winePath = mmd4MecanimProperty.winePath;
                GUILayout.EndHorizontal();
            }

            if (!File.Exists(winePath))
            {
                GUILayout.BeginHorizontal();
                GUILayout.Space(26f);
                EditorGUILayout.LabelField("! Not found Wine path.");
                GUILayout.EndHorizontal();
            }

            if (!this.isProcessing && !mmd4MecanimProperty.useWineFlag)
                GUI.enabled = true;
        }

        EditorGUILayout.Separator();
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        bool flag10 = false;
        bool flag11 = false;
        if (this._editorAdvancedMode)
        {
            flag10 = GUILayout.Button("Revert", GUILayout.ExpandWidth(false));
            flag11 = GUILayout.Button("Apply", GUILayout.ExpandWidth(false));
        }

        bool flag12 = GUILayout.Button("Process", GUILayout.ExpandWidth(false));
        if (flag10)
        {
            this.pmx2fbxConfig = null;
            this.SetupWithReload();
        }
        else if (flag11)
            this.SavePMX2FBXConfig();
        else if (flag12)
        {
            ResetParam();
            SavePMX2FBXConfig();
            ProcessPMX2FBX();
        }

        GUILayout.EndHorizontal();
    }

    public void ResetParam()
    {
        fbxAsset = null;
        fbxAssetPath = null;
        materialList = null;
        mmdModel = null;
        mmdModelLastWriteTime = new DateTime();
        pmx2fbxConfig.mmd4MecanimProperty.fbxAssetPath = null;
        _initializeMaterialAtLeastOnce = false;
        _initializeMaterialAfterPMX2FBX = true;
    }

    private void _OnInspectorGUI_Rig()
    {
        if (this.pmx2fbxConfig == null || this.pmx2fbxConfig.mmd4MecanimProperty == null)
            return;
        GUILayout.Label("FBX", EditorStyles.boldLabel);
        GUILayout.BeginHorizontal();
        GUILayout.Space(26f);
        this._OnInspectorGUI_ShowFBXField();
        GUILayout.EndHorizontal();
        EditorGUILayout.Separator();
        GUILayout.Label("Reset Humanoid Mapping", EditorStyles.boldLabel);
        GUILayout.Label("Humanoidのスケルトンマッピングを自動補正します。");
        GUILayout.Label("一旦Rig>HumanoidのConfigureを行ってから実行してください。");
        GUILayout.Label(".metaを直接書き換えるため、Unityバージョンによっては不整合を起こします。");
        this._rigAccept1 = GUILayout.Toggle(this._rigAccept1, "非推奨機能です。必ずバックアップを取ってから実行してください。");
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUI.enabled = this._rigAccept1;
        if (GUILayout.Button("Process"))
            this._ProcessAvatarHumanoid();
        GUI.enabled = true;
        GUILayout.EndHorizontal();
    }

    public static bool ParseLine(string lineString, string name, out string valueString)
    {
        valueString = (string) null;
        if (lineString.StartsWith(name + ": "))
        {
            valueString = lineString.Substring(name.Length + 2);
            return true;
        }

        return lineString.StartsWith(name + ":");
    }

    public static string ParseText(string valueString) => valueString;

    public static int ParseInt(string valueString)
    {
        try
        {
            return int.Parse(valueString);
        }
        catch (Exception ex)
        {
        }

        return 0;
    }

    public static float ParseFloat(string valueString)
    {
        try
        {
            return float.Parse(valueString);
        }
        catch (Exception ex)
        {
        }

        return 0.0f;
    }

    public static Range GetRange(
        string text,
        string str)
    {
        int s = text.IndexOf(str);
        return s >= 0 ? new Range(s, str.Length) : new Range(-1, 0);
    }

    public static Range GetRange(
        string text,
        string str,
        int pos)
    {
        if (pos < 0)
            return new Range(-1, 0);
        int s = text.IndexOf(str, pos);
        return s >= 0 ? new Range(s, str.Length) : new Range(-1, 0);
    }

    public static string GetMiddleString(
        string str,
        Range r0,
        Range r1)
    {
        return r0.startIndex >= 0 && r1.startIndex >= 0
            ? str.Substring(r0.endIndex, r1.startIndex - r0.endIndex)
            : (string) null;
    }

    public static Vector3 ParseVector3(string valueString)
    {
        if (valueString == "{x: 0, y: 0, z: 0}")
            return Vector3.zero;
        if (valueString == "{x: 1, y: 1, z: 1}")
            return Vector3.one;
        Range range1 = GetRange(valueString, "{x: ");
        Range range2 = GetRange(valueString, ", y: ", range1.endIndex);
        Range range3 = GetRange(valueString, ", z: ", range2.endIndex);
        Range range4 = GetRange(valueString, "}", range3.endIndex);
        if (range1.startIndex >= 0 && range2.startIndex >= 0 && range3.startIndex >= 0 && range4.startIndex >= 0)
        {
            double x = (double) ParseFloat(
                GetMiddleString(valueString, range1, range2));
            float num1 =
                ParseFloat(
                    GetMiddleString(valueString, range2, range3));
            float num2 =
                ParseFloat(
                    GetMiddleString(valueString, range3, range4));
            double y = (double) num1;
            double z = (double) num2;
            return new Vector3((float) x, (float) y, (float) z);
        }

        Debug.LogError((object) ("ParseVector3: Error." + valueString));
        return Vector3.zero;
    }

    public static Quaternion ParseQuaternion(string valueString)
    {
        if (valueString == "{x: 0, y: 0, z: 0, w: 1}")
            return Quaternion.identity;
        Range range1 = GetRange(valueString, "{x: ");
        Range range2 = GetRange(valueString, ", y: ", range1.endIndex);
        Range range3 = GetRange(valueString, ", z: ", range2.endIndex);
        Range range4 = GetRange(valueString, ", w: ", range3.endIndex);
        Range range5 = GetRange(valueString, "}", range4.endIndex);
        if (range1.startIndex >= 0 && range2.startIndex >= 0 && range3.startIndex >= 0 && range4.startIndex >= 0 &&
            range5.startIndex >= 0)
        {
            double x = (double) ParseFloat(
                GetMiddleString(valueString, range1, range2));
            float num1 =
                ParseFloat(
                    GetMiddleString(valueString, range2, range3));
            float num2 =
                ParseFloat(
                    GetMiddleString(valueString, range3, range4));
            float num3 =
                ParseFloat(
                    GetMiddleString(valueString, range4, range5));
            double y = (double) num1;
            double z = (double) num2;
            double w = (double) num3;
            return new Quaternion((float) x, (float) y, (float) z, (float) w);
        }

        Debug.LogError((object) ("ParseQuaternion: Error." + valueString));
        return Quaternion.identity;
    }

    public static FBXMetaData Deserialize(string text)
    {
        FBXMetaData fbxMetaData = new FBXMetaData();
        bool flag1 = false;
        bool flag2 = false;
        bool flag3 = false;
        int length = 0;
        int startIndex = -1;
        int num1 = -1;
        int num2 = 0;
        List<HumanBone> humanBoneList = new List<HumanBone>();
        List<SkeletonBone> skeletonBoneList = new List<SkeletonBone>();
        bool flag4 = false;
        bool flag5 = false;
        bool flag6 = false;
        int num3 = 1;
        while (num2 < text.Length)
        {
            int num4 = num2;
            int num5 = text.IndexOf('\n', num2);
            int num6 = num5 + 1;
            if (num5 < 0)
            {
                num5 = text.Length;
                num6 = text.Length;
            }

            int num7 = 0;
            while (text[num2] == ' ' && num2 < num5)
            {
                ++num2;
                ++num7;
            }

            string lineString = text.Substring(num2, num5 - num2);
            if (!flag1)
            {
                if (lineString == "humanDescription:")
                {
                    flag1 = true;
                    length = num6;
                    fbxMetaData.humanDescriptionTab = num7;
                }
            }
            else
            {
                if (!flag2 && !flag3)
                    length = num4;
                int num8 = fbxMetaData.humanDescriptionTab + 2;
                if (num7 > num8 || num7 == num8 && lineString.StartsWith("- "))
                {
                    if (lineString.StartsWith("- "))
                    {
                        lineString = lineString.Substring(2);
                        num7 += 2;
                        if (flag4)
                            humanBoneList.Add(new HumanBone());
                        else if (flag6)
                            skeletonBoneList.Add(new SkeletonBone());
                    }

                    int num9 = num8 + 2;
                    string valueString = (string) null;
                    if (flag4)
                    {
                        HumanBone humanBone = humanBoneList[humanBoneList.Count - 1];
                        if (num7 == num9 && ParseLine(lineString, "boneName", out valueString))
                        {
                            flag5 = false;
                            humanBone.boneName = ParseText(valueString);
                        }
                        else if (num7 == num9 &&
                                 ParseLine(lineString, "humanName", out valueString))
                        {
                            flag5 = false;
                            humanBone.humanName = ParseText(valueString);
                        }
                        else if (num7 == num9 &&
                                 ParseLine(lineString, "limit", out valueString))
                        {
                            flag5 = true;
                            humanBone.limit = new HumanLimit();
                        }
                        else if (flag5 && num7 == num9 + 2 &&
                                 ParseLine(lineString, "min", out valueString))
                            humanBone.limit.min = ParseVector3(valueString);
                        else if (flag5 && num7 == num9 + 2 &&
                                 ParseLine(lineString, "max", out valueString))
                            humanBone.limit.max = ParseVector3(valueString);
                        else if (flag5 && num7 == num9 + 2 &&
                                 ParseLine(lineString, "value", out valueString))
                            humanBone.limit.center = ParseVector3(valueString);
                        else if (flag5 && num7 == num9 + 2 &&
                                 ParseLine(lineString, "length", out valueString))
                            humanBone.limit.axisLength = ParseFloat(valueString);
                        else if (flag5 && num7 == num9 + 2 &&
                                 ParseLine(lineString, "modified", out valueString))
                            humanBone.limit.useDefaultValues = ParseInt(valueString) == 0;
                        else if (num7 == num9)
                            flag5 = false;

                        humanBoneList[humanBoneList.Count - 1] = humanBone;
                    }
                    else if (flag6)
                    {
                        SkeletonBone skeletonBone = skeletonBoneList[skeletonBoneList.Count - 1];
                        if (num7 == num9 && ParseLine(lineString, "name", out valueString))
                            skeletonBone.name = ParseText(valueString);
                        else if (num7 == num9 &&
                                 ParseLine(lineString, "position", out valueString))
                            skeletonBone.position = ParseVector3(valueString);
                        else if (num7 == num9 &&
                                 ParseLine(lineString, "rotation", out valueString))
                            skeletonBone.rotation = ParseQuaternion(valueString);
                        else if (num7 == num9 &&
                                 ParseLine(lineString, "scale", out valueString))
                            skeletonBone.scale = ParseVector3(valueString);
                        else if (num7 == num9)
                            ParseLine(lineString, "transformModified", out valueString);
                        skeletonBoneList[skeletonBoneList.Count - 1] = skeletonBone;
                    }
                }
                else if (num7 == num8)
                {
                    flag4 = false;
                    flag5 = false;
                    flag6 = false;
                    if (lineString == "human:" || lineString == "human: []")
                    {
                        if (flag2)
                        {
                            Debug.LogError((object) ("Already found human. Line:" + (object) num3));
                            return (FBXMetaData) null;
                        }

                        if (startIndex != -1 && num1 == -1)
                            num1 = num4;
                        flag2 = true;
                        if (lineString == "human:")
                            flag4 = true;
                    }
                    else if (lineString == "skeleton:" || lineString == "skeleton: []")
                    {
                        if (flag3)
                        {
                            Debug.LogError((object) ("Already found skeleton. Line:" + (object) num3));
                            return (FBXMetaData) null;
                        }

                        if (startIndex != -1 && num1 == -1)
                            num1 = num4;
                        flag3 = true;
                        if (lineString == "skeleton:")
                            flag6 = true;
                    }
                    else if (flag2 != flag3)
                    {
                        if (startIndex == -1)
                        {
                            startIndex = num4;
                            num1 = num6;
                        }
                    }
                    else if (flag2 & flag3)
                    {
                        num2 = num4;
                        break;
                    }
                }
                else
                {
                    num2 = num4;
                    break;
                }
            }

            num2 = num6;
            ++num3;
        }

        if (!flag1 || !flag2 || !flag3)
        {
            if (!flag1)
                Debug.LogError((object) "Not found humanDescription line.");
            if (!flag2)
                Debug.LogError((object) "Not found human line.");
            if (!flag3)
                Debug.LogError((object) "Not found skeleton line.");
            return (FBXMetaData) null;
        }

        fbxMetaData.humanDescription.human = humanBoneList.ToArray();
        fbxMetaData.humanDescription.skeleton = skeletonBoneList.ToArray();
        fbxMetaData.humanBones = humanBoneList;
        fbxMetaData.skeletonBones = skeletonBoneList;
        fbxMetaData.headerString = text.Substring(0, length);
        if (num2 < text.Length)
            fbxMetaData.footerString = text.Substring(num2);
        if (num1 != -1)
            fbxMetaData.middleString = text.Substring(startIndex, num1 - startIndex);
        return fbxMetaData;
    }

    public static string ToString(int v) => v.ToString();

    public static string ToString(float v)
    {
        string str = v.ToString("g10");
        if (str.StartsWith("0."))
            return str.Substring(1);
        return str.StartsWith("-0.") ? "-" + str.Substring(2) : str;
    }

    public static string ToString(Vector3 v)
    {
        if (v == Vector3.zero)
            return "{x: 0, y: 0, z: 0}";
        if (v == Vector3.one)
            return "{x: 1, y: 1, z: 1}";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append("{x: ");
        stringBuilder.Append(ToString(v.x));
        stringBuilder.Append(", y: ");
        stringBuilder.Append(ToString(v.y));
        stringBuilder.Append(", z: ");
        stringBuilder.Append(ToString(v.z));
        stringBuilder.Append("}");
        return stringBuilder.ToString();
    }

    public static string ToString(Quaternion v)
    {
        if (v == Quaternion.identity)
            return "{x: 0, y: 0, z: 0, w: 1}";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append("{x: ");
        stringBuilder.Append(ToString(v.x));
        stringBuilder.Append(", y: ");
        stringBuilder.Append(ToString(v.y));
        stringBuilder.Append(", z: ");
        stringBuilder.Append(ToString(v.z));
        stringBuilder.Append(", w: ");
        stringBuilder.Append(ToString(v.w));
        stringBuilder.Append("}");
        return stringBuilder.ToString();
    }

    public static void SerializeLine(StringBuilder str, int tab, string name, string value)
    {
        str.Append(' ', tab);
        str.Append(name);
        if (value != null)
        {
            str.Append(": ");
            str.Append(value);
            str.Append('\n');
        }
        else
        {
            str.Append(":");
            str.Append('\n');
        }
    }

    public static string Serialize(FBXMetaData metaData)
    {
        if (metaData == null || metaData.humanDescription.human == null || metaData.humanDescription.skeleton == null)
        {
            Debug.LogError((object) "");
            return (string) null;
        }

        StringBuilder str = new StringBuilder();
        int num = metaData.humanDescriptionTab + 2;
        int tab = num + 2;
        if (!string.IsNullOrEmpty(metaData.headerString))
            str.Append(metaData.headerString);
        if (metaData.humanDescription.human.Length != 0)
        {
            str.Append(' ', num);
            str.Append("human:");
            str.Append('\n');
            for (int index = 0; index < metaData.humanDescription.human.Length; ++index)
            {
                HumanBone humanBone = metaData.humanDescription.human[index];
                SerializeLine(str, num, "- boneName", humanBone.boneName);
                SerializeLine(str, tab, "humanName", humanBone.humanName);
                SerializeLine(str, tab, "limit", (string) null);
                SerializeLine(str, tab + 2, "min",
                    ToString(humanBone.limit.min));
                SerializeLine(str, tab + 2, "max",
                    ToString(humanBone.limit.max));
                SerializeLine(str, tab + 2, "value",
                    ToString(humanBone.limit.center));
                SerializeLine(str, tab + 2, "length",
                    ToString(humanBone.limit.axisLength));
                SerializeLine(str, tab + 2, "modified",
                    ToString(humanBone.limit.useDefaultValues ? 0 : 1));
            }
        }
        else
        {
            str.Append(' ', num);
            str.Append("human: []");
            str.Append('\n');
        }

        if (!string.IsNullOrEmpty(metaData.middleString))
            str.Append(metaData.middleString);
        if (metaData.humanDescription.skeleton.Length != 0)
        {
            str.Append(' ', num);
            str.Append("skeleton:");
            str.Append('\n');
            for (int index = 0; index < metaData.humanDescription.skeleton.Length; ++index)
            {
                SkeletonBone skeletonBone = metaData.humanDescription.skeleton[index];
                SerializeLine(str, num, "- name", skeletonBone.name);
                SerializeLine(str, tab, "position",
                    ToString(skeletonBone.position));
                SerializeLine(str, tab, "rotation",
                    ToString(skeletonBone.rotation));
                SerializeLine(str, tab, "scale",
                    ToString(skeletonBone.scale));
            }
        }
        else
        {
            str.Append(' ', num);
            str.Append("skeleton: []");
            str.Append('\n');
        }

        if (!string.IsNullOrEmpty(metaData.footerString))
            str.Append(metaData.footerString);
        return str.ToString();
    }

    private void _ProcessAvatarHumanoid()
    {
        if (!MMD4MecanimEditorCommon.ValidateUnityVersion() || !this.Setup() ||
            (Object) this.fbxAsset == (Object) null || this.fbxAssetPath == null ||
            this.mmdModel == null)
            return;
        string pathFromAssetPath = AssetDatabase.GetTextMetaFilePathFromAssetPath(this.fbxAssetPath);
        if (string.IsNullOrEmpty(pathFromAssetPath))
        {
            Debug.LogError((object) (".meta data is not found. " + this.fbxAssetPath));
        }
        else
        {
            string text = File.ReadAllText(pathFromAssetPath);
            if (string.IsNullOrEmpty(text))
            {
                Debug.LogError((object) (".meta data is empty. " + this.fbxAssetPath));
            }
            else
            {
                FBXMetaData metaData = Deserialize(text);
                if (metaData == null)
                {
                    Debug.LogError((object) (".meta Deserialize failed. " + this.fbxAssetPath));
                }
                else
                {
                    GameObject root = (GameObject) null;
                    foreach (Transform transform in this.fbxAsset.transform)
                    {
                        if (transform.name.Contains("Root"))
                        {
                            root = transform.gameObject;
                            break;
                        }
                    }

                    if ((Object) root == (Object) null)
                        Debug.LogError((object) "");
                    else if ((Object) this.fbxAsset.GetComponent<Animator>() == (Object) null)
                    {
                        Debug.LogError((object) "");
                    }
                    else
                    {
                        List<HumanBone> humanBones = metaData.humanBones;
                        List<SkeletonBone> skeletonBones = metaData.skeletonBones;
                        _RemoveHuman(humanBones, "Jaw");
                        if (this.mmdModel.boneList != null)
                        {
                            for (int index = 0; index < this.mmdModel.boneList.Length; ++index)
                            {
                                MMDModel.Bone bone = this.mmdModel.boneList[index];
                                if (!string.IsNullOrEmpty(bone.humanType))
                                    _AddHuman(humanBones, root, bone.skeletonName,
                                        bone.humanType);
                            }
                        }

                        _AddSkeletonRoot(skeletonBones, this.fbxAsset);
                        metaData.humanDescription.human = humanBones.ToArray();
                        metaData.humanDescription.skeleton = skeletonBones.ToArray();
                        string contents = Serialize(metaData);
                        if (string.IsNullOrEmpty(contents))
                        {
                            Debug.LogError((object) (".meta Serialize failed. " + this.fbxAssetPath));
                        }
                        else
                        {
                            try
                            {
                                File.Delete(pathFromAssetPath);
                                File.WriteAllText(pathFromAssetPath, contents);
                                Debug.Log((object) (".meta file processed. " + pathFromAssetPath));
                                AssetDatabase.ImportAsset(this.fbxAssetPath,
                                    ImportAssetOptions.ForceUpdate | ImportAssetOptions.ForceSynchronousImport);
                            }
                            catch (Exception ex)
                            {
                                Debug.LogError((object) ex.ToString());
                            }
                        }
                    }
                }
            }
        }
    }

    public static GameObject _FindContains(GameObject go, string name)
    {
        if (go.name.Contains(name))
            return go;
        foreach (Component component in go.transform)
        {
            GameObject contains = _FindContains(component.gameObject, name);
            if ((Object) contains != (Object) null)
                return contains;
        }

        return (GameObject) null;
    }

    public static void _AddHuman(List<HumanBone> humanBones, HumanBone humanBone)
    {
        for (int index = 0; index < humanBones.Count; ++index)
        {
            if (humanBones[index].humanName == humanBone.humanName)
            {
                HumanBone humanBone1 = humanBones[index] = new HumanBone()
                {
                    boneName = humanBone.boneName
                };
                humanBones[index] = humanBone1;
                return;
            }
        }

        humanBones.Add(humanBone);
    }

    public static void _AddSkeleton(List<SkeletonBone> skeletonBones, SkeletonBone skeletonBone)
    {
        for (int index = 0; index < skeletonBones.Count; ++index)
        {
            if (skeletonBones[index].name == skeletonBone.name)
            {
                skeletonBones.RemoveAt(index);
                break;
            }
        }

        skeletonBones.Add(skeletonBone);
    }

    public static void _AddHuman(
        List<HumanBone> humanBones,
        GameObject root,
        string boneName,
        string humanName)
    {
        GameObject contains = _FindContains(root, boneName);
        if (!((Object) contains != (Object) null))
            return;
        _AddHuman(humanBones, new HumanBone()
        {
            boneName = contains.name,
            humanName = humanName
        });
    }

    public static void _RemoveHuman(List<HumanBone> humanBones, string humanName)
    {
        for (int index = 0; index < humanBones.Count; ++index)
        {
            if (humanBones[index].humanName == humanName)
            {
                humanBones.RemoveAt(index);
                break;
            }
        }
    }

    public static void _AddSkeleton(List<SkeletonBone> skeletonBones, GameObject go)
    {
        _AddSkeleton(skeletonBones, new SkeletonBone()
        {
            name = go.name,
            position = go.transform.localPosition,
            rotation = go.transform.localRotation
        });
        foreach (Transform transform in go.transform)
            _AddSkeleton(skeletonBones, transform.gameObject);
    }

    public static void _AddSkeletonRoot(List<SkeletonBone> skeletonBones, GameObject go)
    {
        foreach (Transform transform in go.transform)
            _AddSkeleton(skeletonBones, transform.gameObject);
    }

    public enum EditorViewPage
    {
        PMX2FBX,
        Material,
        Rig,
        Animations,
    }

    public class PMX2FBXProperty
    {
        public bool viewAdvancedGlobalSettings;
        public bool viewAdvancedGlobalSettingsIK;
        public bool viewAdvancedBulletPhysics;
        public Object pmxAsset;
        public List<Object> vmdAssetList = new List<Object>();
    }

    public enum LicenseAgree
    {
        NotProcess,
        Error,
        Unknown,
        Denied,
        Warning,
    }

    public enum PMXEncoding
    {
        UTF16,
        UTF8,
    }

    public enum Comment
    {
        ModelNameJp,
        ModelNameEn,
        CommentJp,
        CommentEn,
    }

    public struct StandardParameters
    {
        public float metallicGlossiness;
        public float metallic;
        public float specularGlossiness;
        public Color specularColor;
    }

    public class TextureListBuilder
    {
        public GameObject fbxAsset;
        public MMDModel mmdModel;
        public string modelDirectoryName;
        public string texturesDirectoryName;
        public string shaderDirectoryName;
        public string systemDirectoryName;
        public List<Texture> textureList = new List<Texture>();
        public List<Texture> systemToonTextureList = new List<Texture>();

        private static readonly string[] textureExtentions = new string[4]
        {
            ".bmp",
            ".png",
            ".jpg",
            ".tga"
        };

        private static readonly string[] systemToonTextureFileName = new string[11]
        {
            "toon0.bmp",
            "toon01.bmp",
            "toon02.bmp",
            "toon03.bmp",
            "toon04.bmp",
            "toon05.bmp",
            "toon06.bmp",
            "toon07.bmp",
            "toon08.bmp",
            "toon09.bmp",
            "toon10.bmp"
        };

        private static string _PathCombine(string pathA, string pathB)
        {
            if (string.IsNullOrEmpty(pathA))
                return pathB;
            if (string.IsNullOrEmpty(pathB))
                return pathA;
            StringBuilder stringBuilder = new StringBuilder();
            for (int index = 0; index < pathA.Length; ++index)
            {
                if (pathA[index] == '\\')
                    stringBuilder.Append('/');
                else if (pathA[index] == '.')
                {
                    if (index + 1 < pathA.Length && (pathA[index + 1] == '/' || pathA[index + 1] == '\\'))
                        ++index;
                    else
                        stringBuilder.Append(pathA[index]);
                }
                else
                    stringBuilder.Append(pathA[index]);
            }

            if (pathA[pathA.Length - 1] != '/' && pathA[pathA.Length - 1] != '\\')
                stringBuilder.Append('/');
            for (int index = 0; index < pathB.Length; ++index)
            {
                if (pathB[index] == '\\')
                    stringBuilder.Append('/');
                else if (pathB[index] == '.')
                {
                    if (index + 1 < pathB.Length && (pathB[index + 1] == '/' || pathB[index + 1] == '\\'))
                        ++index;
                    else
                        stringBuilder.Append(pathB[index]);
                }
                else
                    stringBuilder.Append(pathB[index]);
            }

            return stringBuilder.ToString();
        }

        private Texture _LoadTexture(string directoryName, string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                return (Texture) null;
            string str1 = TextureListBuilder._PathCombine(directoryName, fileName);
            Texture texture1 =
                AssetDatabase.LoadAssetAtPath(str1, typeof(Texture)) as Texture;
            if ((Object) texture1 != (Object) null)
                return texture1;
            string lower = Path.GetExtension(fileName).ToLower();
            if (lower == ".spa" || lower == ".sph")
            {
                for (int index = 0;
                     index < TextureListBuilder.textureExtentions.Length;
                     ++index)
                {
                    Texture texture2 =
                        AssetDatabase.LoadAssetAtPath(
                            str1 + TextureListBuilder.textureExtentions[index],
                            typeof(Texture)) as Texture;
                    if ((Object) texture2 != (Object) null)
                        return texture2;
                }

                MMD4MecanimEditorCommon.TextureFileSign textureFileSign =
                    MMD4MecanimEditorCommon.GetTextureFileSign(str1);
                if (textureFileSign != MMD4MecanimEditorCommon.TextureFileSign.None)
                {
                    string str2 = str1;
                    string str3;
                    switch (textureFileSign - 1)
                    {
                        case MMD4MecanimEditorCommon.TextureFileSign.None:
                        case MMD4MecanimEditorCommon.TextureFileSign.Bmp:
                            str3 = str2 + ".bmp";
                            break;
                        case MMD4MecanimEditorCommon.TextureFileSign.BmpWithAlpha:
                        case MMD4MecanimEditorCommon.TextureFileSign.Png:
                            str3 = str2 + ".png";
                            break;
                        case MMD4MecanimEditorCommon.TextureFileSign.PngWithAlpha:
                            str3 = str2 + ".jpg";
                            break;
                        case MMD4MecanimEditorCommon.TextureFileSign.Jpeg:
                        case MMD4MecanimEditorCommon.TextureFileSign.Targa:
                            str3 = str2 + ".tga";
                            break;
                        default:
                            Debug.LogWarning((object) ("_LoadTexture: Unknown .sph/.spa file format. " +
                                                                   str1));
                            return (Texture) null;
                    }

                    if (!File.Exists(str3))
                    {
                        if (directoryName == this.texturesDirectoryName)
                            File.Move(str1, str3);
                        else
                            File.Copy(str1, str3);
                        AssetDatabase.ImportAsset(str3,
                            ImportAssetOptions.ForceUpdate | ImportAssetOptions.ForceSynchronousImport);
                    }
                    else
                        Debug.LogWarning((object) "_LoadTexture: Unknown flow.");

                    Texture texture3 =
                        AssetDatabase.LoadAssetAtPath(str3, typeof(Texture)) as Texture;
                    if ((Object) texture3 != (Object) null)
                        return texture3;
                }
            }

            return (Texture) null;
        }

        private static string _PrefixFileName2(string fileName)
        {
            if (fileName == null)
                return fileName;
            fileName = fileName.Replace("//", "/");
            return fileName.Replace("\\\\", "\\");
        }

        private static string _PrefixFileName(string fileName)
        {
            if (fileName == null)
                return fileName;
            int index;
            for (index = fileName.Length - 1; index >= 0; --index)
            {
                switch (fileName[index])
                {
                    case '\t':
                    case '\n':
                    case '\r':
                    case ' ':
                        continue;
                    default:
                        goto label_5;
                }
            }

            label_5:
            return TextureListBuilder._PrefixFileName2(fileName.Substring(0, index + 1));
        }

        private Texture _LoadTexture(string fileName)
        {
            fileName = TextureListBuilder._PrefixFileName(fileName);
            Texture texture1 = this._LoadTexture(this.texturesDirectoryName, fileName);
            if ((Object) texture1 != (Object) null)
                return texture1;
            Texture texture2 = this._LoadTexture(this.modelDirectoryName, fileName);
            if ((Object) texture2 != (Object) null)
                return texture2;
            Texture texture3 = this._LoadTexture(this.systemDirectoryName, fileName);
            if ((Object) texture3 != (Object) null)
                return texture3;
            Debug.Log((object) ("_LoadTexture: File not found. " + fileName));
            return (Texture) null;
        }

        public void Build()
        {
            if ((Object) this.fbxAsset == (Object) null)
                Debug.LogError((object) "Not found FBX.");
            else if (this.mmdModel == null)
            {
                Debug.LogError((object) "Not found model.xml");
            }
            else
            {
                this.modelDirectoryName =
                    Path.GetDirectoryName(AssetDatabase.GetAssetPath((Object) this.fbxAsset)) + "/";
                this.texturesDirectoryName = this.modelDirectoryName + "Textures/";
                this.systemDirectoryName =
                    Path.Combine(
                        Path.GetDirectoryName(Path.GetDirectoryName(
                            AssetDatabase.GetAssetPath((Object) Shader.Find("MMD4Mecanim/MMDLit")))),
                        "Textures") + "/";
                if (this.mmdModel.textureList != null)
                {
                    int length = this.mmdModel.textureList.Length;
                    bool[] flagArray = new bool[length];
                    if (this.mmdModel.materialList != null)
                    {
                        for (int index = 0; index < this.mmdModel.materialList.Length; ++index)
                        {
                            int textureId = this.mmdModel.materialList[index].textureID;
                            int toonTextureId = this.mmdModel.materialList[index].toonTextureID;
                            int additionalTextureId = this.mmdModel.materialList[index].additionalTextureID;
                            if ((uint) textureId < (uint) length)
                                flagArray[textureId] = true;
                            if ((uint) toonTextureId < (uint) length)
                                flagArray[toonTextureId] = true;
                            if ((uint) additionalTextureId < (uint) length)
                                flagArray[additionalTextureId] = true;
                        }
                    }

                    for (int index = 0; index < this.mmdModel.textureList.Length; ++index)
                    {
                        if (flagArray[index])
                        {
                            string fileName = this.mmdModel.textureList[index].fileName;
                            Texture texture = this._LoadTexture(fileName);
                            if ((Object) texture != (Object) null)
                            {
                                this.textureList.Add(texture);
                            }
                            else
                            {
                                Debug.LogWarning((object) ("Not found texture. " + (object) index + ":" +
                                                                       fileName));
                                this.textureList.Add((Texture) null);
                            }
                        }
                        else
                            this.textureList.Add((Texture) null);
                    }
                }

                for (int index = 0;
                     index < TextureListBuilder.systemToonTextureFileName.Length;
                     ++index)
                {
                    Texture texture = this._LoadTexture(this.systemDirectoryName,
                        TextureListBuilder.systemToonTextureFileName[index]);
                    if ((Object) texture != (Object) null)
                    {
                        this.systemToonTextureList.Add(texture);
                    }
                    else
                    {
                        Debug.LogWarning((object) ("Not found system toon texture. " + (object) index +
                                                               ":" + TextureListBuilder
                                                                   .systemToonTextureFileName[index]));
                        this.systemToonTextureList.Add((Texture) null);
                    }
                }
            }
        }
    }

    public class MaterialBlocksConfigurator
    {
        public GameObject fbxAsset;
        public MMDModel mmdModel;
        public PMX2FBXConfig.MMD4MecanimProperty mmd4MecanimProperty;

        public void Configure(
            PMX2FBXConfig.StandardSetupType standardSetupType)
        {
            if ((Object) this.fbxAsset == (Object) null)
                Debug.LogError((object) "Not found FBX.");
            else if (this.mmdModel == null)
                Debug.LogError((object) "Not found model.xml");
            else if (this.mmd4MecanimProperty == null)
            {
                Debug.LogError((object) "Not found property.xml");
            }
            else
            {
                List<int[]> materialNoLists;
                List<Material> fbxMaterialList =
                    _GetFBXMaterialList(this.fbxAsset, this.mmdModel, out materialNoLists);
                if (fbxMaterialList == null || fbxMaterialList.Count == 0)
                    return;
                this.mmd4MecanimProperty.PrepareMaterialPropertyList(fbxMaterialList);
                this.mmd4MecanimProperty.PrepareMaterialBlockPropertyList(fbxMaterialList);
                if (this.mmd4MecanimProperty.materialBlockPropertyList == null)
                    this.mmd4MecanimProperty.materialBlockPropertyList =
                        new List<PMX2FBXConfig.MaterialBlockProperty>();
                else
                    this.mmd4MecanimProperty.materialBlockPropertyList.Clear();
                PMX2FBXConfig.MaterialBlockProperty rootMaterialBlockProperty =
                    new PMX2FBXConfig.MaterialBlockProperty();
                if (this.mmd4MecanimProperty.materialProperyList == null)
                    return;
                this.mmd4MecanimProperty.materialProperyList.ForEach(
                    (Action<PMX2FBXConfig.MaterialProperty>) (m =>
                        rootMaterialBlockProperty.materialNames.Add(m.materialName)));
                this.mmd4MecanimProperty.materialBlockPropertyList.Add(rootMaterialBlockProperty);
                if (standardSetupType == PMX2FBXConfig.StandardSetupType.None)
                    return;
                List<MaterialHelper> materialHelpers =
                    _CreateMaterialHelpers(this.mmd4MecanimProperty, this.mmdModel,
                        fbxMaterialList, materialNoLists);
                if (materialHelpers == null)
                {
                    Debug.LogError((object) "Unknown flow.");
                }
                else
                {
                    Dictionary<int, PMX2FBXConfig.MaterialBlockProperty> dictionary =
                        new Dictionary<int, PMX2FBXConfig.MaterialBlockProperty>();
                    foreach (MaterialHelper materialHelper in materialHelpers)
                    {
                        if (materialHelper != null && materialHelper.xmlMaterial != null &&
                            materialHelper.xmlMaterial.sphereMode != MMDModel.SphereMode.None)
                        {
                            int additionalTextureId = materialHelper.xmlMaterial.additionalTextureID;
                            if (additionalTextureId >= 0)
                            {
                                PMX2FBXConfig.MaterialBlockProperty materialBlockProperty;
                                if (!dictionary.TryGetValue(additionalTextureId, out materialBlockProperty))
                                {
                                    materialBlockProperty =
                                        new PMX2FBXConfig.MaterialBlockProperty();
                                    dictionary.Add(additionalTextureId, materialBlockProperty);
                                    this.mmd4MecanimProperty.materialBlockPropertyList.Add(materialBlockProperty);
                                }

                                materialBlockProperty.materialNames.Add(materialHelper.materialName);
                                rootMaterialBlockProperty.materialNames.Remove(materialHelper.materialName);
                            }
                        }
                    }

                    if (!CheckStandard())
                        return;
                    PMX2FBXConfig.MaterialGlobalProperty materialGlobalProperty =
                        this.mmd4MecanimProperty.materialGlobalProperty ??
                        new PMX2FBXConfig.MaterialGlobalProperty();
                    TextureListBuilder textureListBuilder =
                        new TextureListBuilder();
                    textureListBuilder.fbxAsset = this.fbxAsset;
                    textureListBuilder.mmdModel = this.mmdModel;
                    textureListBuilder.Build();
                    List<Texture> textureList = textureListBuilder.textureList;
                    if (textureList == null)
                        return;
                    foreach (KeyValuePair<int, PMX2FBXConfig.MaterialBlockProperty> keyValuePair
                             in dictionary)
                    {
                        int key = keyValuePair.Key;
                        if (key < textureList.Count)
                        {
                            Texture texture = textureList[key];
                            if ((Object) texture != (Object) null)
                            {
                                StandardParameters standardParameters;
                                ComputeSphereCubeToStandardParameters(texture,
                                    out standardParameters);
                                PMX2FBXConfig.StandardBRDF standardBrdf =
                                    standardSetupType == PMX2FBXConfig.StandardSetupType.Metalic
                                        ? PMX2FBXConfig.StandardBRDF.Metallic
                                        : PMX2FBXConfig.StandardBRDF.Specular;
                                keyValuePair.Value.standardBRDF = standardBrdf;
                                keyValuePair.Value.standardSpecularColor =
                                    _Clamp01(standardParameters.specularColor *
                                                                     materialGlobalProperty
                                                                         .sphereCubeToSpecularColorRate);
                                keyValuePair.Value.standardSpecularGlossiness =
                                    _Clamp01(standardParameters.specularGlossiness *
                                                                     materialGlobalProperty
                                                                         .sphereCubeToSpecularGlossiness);
                                keyValuePair.Value.standardMetallic = standardParameters.metallic;
                                keyValuePair.Value.standardMetallicGlossiness =
                                    _Clamp01(standardParameters.metallicGlossiness *
                                                                     materialGlobalProperty
                                                                         .sphereCubeToMetallicGlossinessRate);
                            }
                        }
                    }
                }
            }
        }
    }

    public class MaterialBuilder
    {
        public GameObject fbxAsset;
        public MMDModel mmdModel;
        public PMX2FBXConfig.MMD4MecanimProperty mmd4MecanimProperty;
        public List<Texture> textureList = new List<Texture>();
        public List<Texture> systemToonTextureList = new List<Texture>();
        public List<Material> materialList = new List<Material>();
        private List<MaterialHelper> _materialHelpers;

        private static TextureImporter _GetTextureImporter(Texture texture)
        {
            if ((Object) texture == (Object) null)
                return (TextureImporter) null;
            string assetPath = AssetDatabase.GetAssetPath((Object) texture);
            if (string.IsNullOrEmpty(assetPath))
                return (TextureImporter) null;
            if (Path.GetExtension(assetPath).ToLower() == ".dds")
                return (TextureImporter) null;
            TextureImporter atPath = AssetImporter.GetAtPath(assetPath) as TextureImporter;
            return (Object) atPath == (Object) null ? (TextureImporter) null : atPath;
        }

        public static void SetTextureWrapMode(Texture texture, TextureWrapMode textureWrapMode)
        {
            if ((Object) texture == (Object) null)
                return;
            TextureImporter textureImporter = MaterialBuilder._GetTextureImporter(texture);
            if ((Object) textureImporter == (Object) null)
                return;
            if (textureImporter.textureType == TextureImporterType.Sprite ||
                textureImporter.textureType == TextureImporterType.GUI)
            {
                MMD4MecanimEditorCommon.SetTextureTypeToDefault(textureImporter);
                MMD4MecanimEditorCommon.UpdateImportSettings(AssetDatabase.GetAssetPath((Object) texture));
            }

            if (textureImporter.wrapMode == textureWrapMode)
                return;
            textureImporter.wrapMode = textureWrapMode;
            MMD4MecanimEditorCommon.UpdateImportSettings(AssetDatabase.GetAssetPath((Object) texture));
        }

        public static Texture ReloadTexture(Texture texture)
        {
            if (!((Object) texture != (Object) null))
                return (Texture) null;
            string assetPath = AssetDatabase.GetAssetPath((Object) texture);
            texture = AssetDatabase.LoadAssetAtPath(assetPath, typeof(Texture)) as Texture;
            if ((Object) texture == (Object) null)
                Debug.LogWarning((object) ("ReloadTexture() Failed. " + assetPath));
            return texture;
        }

        public static Texture SetTextureCubemapToSpheremap(Texture texture)
        {
            if ((Object) texture == (Object) null)
                return (Texture) null;
            TextureImporter textureImporter = MaterialBuilder._GetTextureImporter(texture);
            if ((Object) textureImporter == (Object) null)
                return texture;
            bool flag = false;
            string assetPath = AssetDatabase.GetAssetPath((Object) texture);
            if (MMD4MecanimEditorCommon.IsNewTextureImporter())
            {
                MMD4MecanimEditorCommon._TextureImporterShape textureShape =
                    MMD4MecanimEditorCommon.GetTextureShape(textureImporter);
                if (!MMD4MecanimEditorCommon.IsTextureTypeDefault(textureImporter) ||
                    textureShape != MMD4MecanimEditorCommon._TextureImporterShape.TextureCube)
                {
                    MMD4MecanimEditorCommon.SetTextureTypeToDefault(textureImporter);
                    MMD4MecanimEditorCommon.SetTextureShape(textureImporter,
                        MMD4MecanimEditorCommon._TextureImporterShape.TextureCube);
                    flag = true;
                }
            }
            else if (!MMD4MecanimEditorCommon._Legacy_IsTextureTypeAdvancedOrCubemap(textureImporter))
            {
                MMD4MecanimEditorCommon._Legacy_SetTextureTypeToCubemap(textureImporter);
                flag = true;
            }

            if (textureImporter.generateCubemap != TextureImporterGenerateCubemap.Spheremap)
            {
                textureImporter.generateCubemap = TextureImporterGenerateCubemap.Spheremap;
                flag = true;
            }

            if (textureImporter.wrapMode != TextureWrapMode.Clamp)
            {
                textureImporter.wrapMode = TextureWrapMode.Clamp;
                flag = true;
            }

            if (!flag)
                return texture;
            MMD4MecanimEditorCommon.UpdateImportSettings(assetPath);
            Texture spheremap =
                AssetDatabase.LoadAssetAtPath(assetPath, typeof(Texture)) as Texture;
            if (!((Object) spheremap == (Object) null))
                return spheremap;
            Debug.LogWarning((object) ("Updated Texture is null. " + assetPath));
            return spheremap;
        }

        public static void SetTextureAlphaIsTransparency(Texture texture, bool alphaIsTransparency)
        {
            TextureImporter textureImporter = MaterialBuilder._GetTextureImporter(texture);
            if ((Object) textureImporter == (Object) null)
                return;
            if (textureImporter.textureType == TextureImporterType.Sprite ||
                textureImporter.textureType == TextureImporterType.GUI)
            {
                MMD4MecanimEditorCommon.SetTextureTypeToDefault(textureImporter);
                MMD4MecanimEditorCommon.UpdateImportSettings(AssetDatabase.GetAssetPath((Object) texture));
            }

            if (textureImporter.alphaIsTransparency == alphaIsTransparency)
                return;
            textureImporter.alphaIsTransparency = alphaIsTransparency;
            MMD4MecanimEditorCommon.UpdateImportSettings(AssetDatabase.GetAssetPath((Object) texture));
        }

        public static void SetTextureMipmapEnabled(Texture texture, bool mipmapEnabled)
        {
            TextureImporter textureImporter = MaterialBuilder._GetTextureImporter(texture);
            if ((Object) textureImporter == (Object) null)
                return;
            if (textureImporter.textureType == TextureImporterType.Sprite ||
                textureImporter.textureType == TextureImporterType.GUI)
            {
                MMD4MecanimEditorCommon.SetTextureTypeToDefault(textureImporter);
                MMD4MecanimEditorCommon.UpdateImportSettings(AssetDatabase.GetAssetPath((Object) texture));
            }

            if (textureImporter.mipmapEnabled == mipmapEnabled)
                return;
            textureImporter.mipmapEnabled = mipmapEnabled;
            MMD4MecanimEditorCommon.UpdateImportSettings(AssetDatabase.GetAssetPath((Object) texture));
        }

        public static void SetTextureFilterMode(Texture texture, FilterMode filterMode)
        {
            TextureImporter textureImporter = MaterialBuilder._GetTextureImporter(texture);
            if ((Object) textureImporter == (Object) null)
                return;
            if (textureImporter.textureType == TextureImporterType.Sprite ||
                textureImporter.textureType == TextureImporterType.GUI)
            {
                MMD4MecanimEditorCommon.SetTextureTypeToDefault(textureImporter);
                MMD4MecanimEditorCommon.UpdateImportSettings(AssetDatabase.GetAssetPath((Object) texture));
            }

            if (textureImporter.filterMode == filterMode)
                return;
            textureImporter.filterMode = filterMode;
            MMD4MecanimEditorCommon.UpdateImportSettings(AssetDatabase.GetAssetPath((Object) texture));
        }

        public static void SetTextureType(Texture texture, TextureImporterType textureType)
        {
            TextureImporter textureImporter = MaterialBuilder._GetTextureImporter(texture);
            if ((Object) textureImporter == (Object) null ||
                textureImporter.textureType == textureType)
                return;
            textureImporter.textureType = textureType;
            MMD4MecanimEditorCommon.UpdateImportSettings(AssetDatabase.GetAssetPath((Object) texture));
        }

        private MMD4MecanimEditorCommon.TextureAccessor _GetTextureAccessor(
            int textureID,
            PMX2FBXConfig.MaterialGlobalProperty materialGlobalProperty)
        {
            if ((uint) textureID < (uint) this.textureList.Count &&
                (Object) this.textureList[textureID] != (Object) null)
            {
                switch (MMD4MecanimEditorCommon.GetTextureFileSign(
                            AssetDatabase.GetAssetPath((Object) this.textureList[textureID])))
                {
                    case MMD4MecanimEditorCommon.TextureFileSign.None:
                    case MMD4MecanimEditorCommon.TextureFileSign.BmpWithAlpha:
                    case MMD4MecanimEditorCommon.TextureFileSign.PngWithAlpha:
                    case MMD4MecanimEditorCommon.TextureFileSign.TargaWithAlpha:
                        return new MMD4MecanimEditorCommon.TextureAccessor(this.textureList[textureID]);
                }
            }

            return (MMD4MecanimEditorCommon.TextureAccessor) null;
        }

        private static Shader _GetShader(
            MaterialHelper materialHelper)
        {
            return Shader.Find(materialHelper.GetShaderName());
        }

        private PMX2FBXConfig.MaterialProperty _FindMaterialProperty(
            Material material)
        {
            return this.mmd4MecanimProperty != null && (Object) material != (Object) null
                ? this.mmd4MecanimProperty.FindMaterialProperty(material.name)
                : (PMX2FBXConfig.MaterialProperty) null;
        }

        private bool _IsMaterialPropertyProcessing(Material material)
        {
            this._FindMaterialProperty(material);
            return true;
        }

        private MMD4MecanimEditorCommon.TextureAccessor[] _CreateTextureAccessorList(
            PMX2FBXConfig.MaterialGlobalProperty materialGlobalProperty)
        {
            List<MMD4MecanimEditorCommon.TextureAccessor> textureAccessorList =
                new List<MMD4MecanimEditorCommon.TextureAccessor>();
            if (this.textureList != null)
            {
                for (int textureID = 0; textureID < this.textureList.Count; ++textureID)
                    textureAccessorList.Add(this._GetTextureAccessor(textureID, materialGlobalProperty));
            }

            return textureAccessorList.ToArray();
        }

        private bool _MorphIsTransparency(int materialID)
        {
            if (this.mmdModel == null || this.mmdModel.morphList == null)
                return false;
            for (int index1 = 0; index1 < this.mmdModel.morphList.Length; ++index1)
            {
                MMDModel.Morph morph = this.mmdModel.morphList[index1];
                if (morph != null && morph.morphType == MMDModel.MorphType.Material)
                {
                    int[] indexList = morph.indexList;
                    if (indexList != null)
                    {
                        for (int index2 = 0; index2 < indexList.Length; ++index2)
                        {
                            if (indexList[index2] == materialID)
                                return true;
                        }
                    }
                }
            }

            return false;
        }

        private Texture _GetTexture(int textureID) =>
            this.textureList != null && textureID >= 0 && textureID < this.textureList.Count
                ? this.textureList[textureID]
                : (Texture) null;

        private void _SetTexture(int textureID, Texture texture)
        {
            if (this.textureList == null || textureID < 0 || textureID >= this.textureList.Count)
                return;
            this.textureList[textureID] = texture;
        }

        public void Build(bool overwriteMaterials)
        {
            CheckStandard();
            if ((Object) this.fbxAsset == (Object) null)
                Debug.LogError((object) "Not found FBX.");
            else if (this.mmdModel == null)
                Debug.LogError((object) "Not found model.xml");
            else if (this.mmd4MecanimProperty == null)
            {
                Debug.LogError((object) "Not found property.xml");
            }
            else
            {
                List<int[]> materialNoLists;
                this.materialList =
                    _GetFBXMaterialList(this.fbxAsset, this.mmdModel, out materialNoLists);
                if (this.materialList == null || this.materialList.Count == 0)
                    return;
                this.mmd4MecanimProperty.PrepareMaterialPropertyList(this.materialList);
                this.mmd4MecanimProperty.PrepareMaterialBlockPropertyList(this.materialList);
                this._materialHelpers = _CreateMaterialHelpers(this.mmd4MecanimProperty,
                    this.mmdModel, this.materialList, materialNoLists);
                if (this._materialHelpers == null)
                {
                    Debug.LogError((object) "Unknown flow.");
                }
                else
                {
                    TextureListBuilder textureListBuilder =
                        new TextureListBuilder();
                    textureListBuilder.fbxAsset = this.fbxAsset;
                    textureListBuilder.mmdModel = this.mmdModel;
                    textureListBuilder.Build();
                    this.textureList = textureListBuilder.textureList;
                    this.systemToonTextureList = textureListBuilder.systemToonTextureList;
                    PMX2FBXConfig.MaterialGlobalProperty materialGlobalProperty =
                        this.mmd4MecanimProperty.materialGlobalProperty;
                    if (materialGlobalProperty == null)
                    {
                        materialGlobalProperty = new PMX2FBXConfig.MaterialGlobalProperty();
                        this.mmd4MecanimProperty.materialGlobalProperty = materialGlobalProperty;
                    }

                    MMD4MecanimEditorCommon.TextureAccessor[] textureAccessorArray =
                        (MMD4MecanimEditorCommon.TextureAccessor[]) null;
                    MMD4MecanimEditorCommon.MMDMesh mmdMesh = (MMD4MecanimEditorCommon.MMDMesh) null;
                    if (overwriteMaterials)
                        textureAccessorArray = this._CreateTextureAccessorList(materialGlobalProperty);
                    for (int index = 0; index < this._materialHelpers.Count; ++index)
                    {
                        MaterialHelper materialHelper = this._materialHelpers[index];
                        if (materialHelper == null)
                            Debug.LogError((object) ("Not found materialHelper: " + (object) index));
                        else if (materialHelper.materialProperty == null)
                            Debug.LogError((object) ("Not found materialProperty: " + (object) index));
                        else if (!materialHelper.materialProperty.isLocked)
                        {
                            MMDModel.Material xmlMaterial = materialHelper.xmlMaterial;
                            if (xmlMaterial == null)
                            {
                                Debug.LogError((object) ("Not found xmlMaterial: " + (object) index));
                            }
                            else
                            {
                                Material material = materialHelper.material;
                                if ((Object) material == (Object) null)
                                {
                                    Debug.LogError((object) ("Not found material: " + (object) index));
                                }
                                else
                                {
                                    if (materialHelper.materialProperty.transparencyCache ==
                                        PMX2FBXConfig.TransparencyCache.Unknown)
                                    {
                                        materialHelper.materialProperty.transparencyCache = MMD4MecanimImporterImpl
                                            .PMX2FBXConfig.TransparencyCache.Disabled;
                                        if (this._MorphIsTransparency(index))
                                            materialHelper.materialProperty.transparencyCache = MMD4MecanimImporterImpl
                                                .PMX2FBXConfig.TransparencyCache.Enabled;
                                        if (materialHelper.materialProperty.transparencyCache == MMD4MecanimImporterImpl
                                                .PMX2FBXConfig.TransparencyCache.Disabled)
                                        {
                                            int textureId = xmlMaterial.textureID;
                                            if (textureAccessorArray == null)
                                                textureAccessorArray =
                                                    this._CreateTextureAccessorList(materialGlobalProperty);
                                            if (textureAccessorArray == null)
                                                Debug.LogError((object) ("textureAccessorList is null: " +
                                                    (object) index));
                                            if (textureAccessorArray != null &&
                                                (uint) textureId < (uint) this.textureList.Count &&
                                                textureAccessorArray[textureId] != null &&
                                                textureAccessorArray[textureId].isTransparency)
                                            {
                                                if (mmdMesh == null)
                                                    mmdMesh = new MMD4MecanimEditorCommon.MMDMesh(this.fbxAsset);
                                                if (mmdMesh.CheckTransparency(materialHelper.materialName,
                                                        textureAccessorArray[textureId]))
                                                    materialHelper.materialProperty.transparencyCache =
                                                        PMX2FBXConfig.TransparencyCache.Enabled;
                                            }
                                        }
                                    }

                                    float num1 = -1f;
                                    if (material.HasProperty("_Revision"))
                                        num1 = material.GetFloat("_Revision");
                                    bool flag = (double) num1 < 1.5;
                                    if (overwriteMaterials ||
                                        (Object) material.shader == (Object) null ||
                                        !material.shader.name.Contains("MMD4Mecanim"))
                                    {
                                        flag = true;
                                        material.shader =
                                            MaterialBuilder._GetShader(materialHelper);
                                    }

                                    if ((Object) material.shader == (Object) null)
                                        Debug.LogError((object) ("Shader is null: " + (object) index));
                                    if ((Object) material.shader != (Object) null &&
                                        material.shader.name.Contains("MMD4Mecanim"))
                                    {
                                        if (flag || (double) num1 < 1.5)
                                        {
                                            if (_standardEnabled &&
                                                materialGlobalProperty.shaderType == MMD4MecanimImporterImpl
                                                    .PMX2FBXConfig.ShaderType.Standard)
                                                materialHelper.PrefixStandard();
                                            else
                                                materialHelper.PrefixLegacy();
                                            materialHelper.UpdateMaterial();
                                        }

                                        if (flag)
                                        {
                                            Texture texture = this._GetTexture(xmlMaterial.textureID);
                                            MaterialBuilder.SetTextureWrapMode(texture,
                                                TextureWrapMode.Repeat);
                                            material.mainTexture = texture;
                                        }

                                        if (flag)
                                        {
                                            Texture texture = this._GetTexture(xmlMaterial.toonTextureID);
                                            if ((Object) texture == (Object) null &&
                                                xmlMaterial.toonID > 0)
                                                texture = this.systemToonTextureList[xmlMaterial.toonID];
                                            if ((Object) texture != (Object) null)
                                                MaterialBuilder.SetTextureWrapMode(texture,
                                                    TextureWrapMode.Clamp);
                                            material.SetTexture("_ToonTex", texture);
                                        }

                                        if (flag)
                                        {
                                            if (xmlMaterial.sphereMode !=
                                                MMDModel.SphereMode.None)
                                            {
                                                Texture spheremap =
                                                    MaterialBuilder
                                                        .SetTextureCubemapToSpheremap(
                                                            this._GetTexture(xmlMaterial.additionalTextureID));
                                                this._SetTexture(xmlMaterial.additionalTextureID, spheremap);
                                                materialHelper.UpdateMaterial_SphereCube(spheremap);
                                            }
                                            else
                                                materialHelper.UpdateMaterial_SphereCube((Texture) null);
                                        }

                                        material.SetFloat("_Revision", 2f);
                                    }

                                    if (materialGlobalProperty.prefixRenderQueue)
                                    {
                                        int num2 = materialGlobalProperty.renderQueueAfterSkybox ? 2502 : 2002;
                                        material.renderQueue = num2 + index;
                                    }
                                    else
                                        material.renderQueue = -1;

                                    EditorUtility.SetDirty((Object) material);
                                }
                            }
                        }
                    }

                    AssetDatabase.SaveAssets();
                }
            }
        }
    }

    public class IndexBuilder
    {
        public GameObject fbxAsset;
        public MMDModel mmdModel;
        public const int MeshCountBitShift = 24;
        private SkinnedMeshRenderer[] _cache_skinnedMeshRenderers;

        public void _Write(MemoryStream memoryStream, uint value)
        {
            byte[] buffer = new byte[4]
            {
                (byte) (value & (uint) byte.MaxValue),
                (byte) (value >> 8 & (uint) byte.MaxValue),
                (byte) (value >> 16 & (uint) byte.MaxValue),
                (byte) (value >> 24 & (uint) byte.MaxValue)
            };
            memoryStream.Write(buffer, 0, 4);
        }

        public void Build()
        {
            if ((Object) this.fbxAsset == (Object) null)
                Debug.LogError((object) "Not found FBX.");
            else if (this.mmdModel == null)
            {
                Debug.LogError((object) "Not found model.xml");
            }
            else
            {
                string indexDataPath =
                    GetIndexDataPath(
                        AssetDatabase.GetAssetPath((Object) this.fbxAsset));
                uint numVertex = this.mmdModel.globalSettings.numVertex;
                List<uint>[] uintListArray = new List<uint>[(int) numVertex];
                for (uint index = 0; index < numVertex; ++index)
                    uintListArray[(int) index] = new List<uint>();
                uint num1 = 0;
                uint num2 = 0;
                this._cache_skinnedMeshRenderers = MMD4MecanimCommon.GetSkinnedMeshRenderers(this.fbxAsset);
                if (this._cache_skinnedMeshRenderers != null)
                {
                    num1 = (uint) this._cache_skinnedMeshRenderers.Length;
                    for (int index1 = 0; (long) index1 < (long) num1; ++index1)
                    {
                        Color32[] colors32 = this._cache_skinnedMeshRenderers[index1].sharedMesh.colors32;
                        if (colors32 != null)
                        {
                            num2 += (uint) colors32.Length;
                            for (uint index2 = 0; index2 < (uint) colors32.Length; ++index2)
                            {
                                uint index3 = (uint) ((int) colors32[(int) index2].r |
                                                      (int) colors32[(int) index2].g << 8 |
                                                      (int) colors32[(int) index2].b << 16);
                                if (index3 < numVertex)
                                    uintListArray[(int) index3].Add((uint) (index1 << 24) | index2);
                            }
                        }
                    }
                }

                MemoryStream memoryStream = new MemoryStream();
                this._Write(memoryStream, numVertex);
                this._Write(memoryStream, num1 << 24 | num2);
                uint num3 = (uint) (2 + (int) numVertex + 1);
                for (uint index = 0; index < numVertex; ++index)
                {
                    this._Write(memoryStream, num3);
                    num3 += (uint) uintListArray[(int) index].Count;
                }

                this._Write(memoryStream, num3);
                for (uint index4 = 0; index4 < numVertex; ++index4)
                {
                    for (int index5 = 0; index5 < uintListArray[(int) index4].Count; ++index5)
                        this._Write(memoryStream, uintListArray[(int) index4][index5]);
                }

                memoryStream.Flush();
                FileStream fileStream = File.Open(indexDataPath, System.IO.FileMode.Create, FileAccess.Write,
                    FileShare.None);
                fileStream.Write(memoryStream.ToArray(), 0, (int) memoryStream.Length);
                fileStream.Close();
                AssetDatabase.ImportAsset(indexDataPath,
                    ImportAssetOptions.ForceUpdate | ImportAssetOptions.ForceSynchronousImport);
            }
        }
    }

    public class VertexBuilder
    {
        public GameObject fbxAsset;
        public float importScale;
        public MMDModel mmdModel;
        public const int MeshCountBitShift = 24;
        private SkinnedMeshRenderer[] _cache_skinnedMeshRenderers;

        private static int ToOriginalBoneIndex(Transform[] bones, int boneIndex)
        {
            if (bones != null && (uint) boneIndex < (uint) bones.Length)
            {
                string name = bones[boneIndex].name;
                if (!string.IsNullOrEmpty(name))
                    return MMD4MecanimCommon.ToInt(name);
            }

            Debug.LogWarning((object) "");
            return -1;
        }

        private static Vector3 _ToUnityPos(Vector3 pos, float vertexScale)
        {
            pos[0] = -pos[0] * vertexScale;
            pos[1] *= vertexScale;
            pos[2] = -pos[2] * vertexScale;
            return pos;
        }

        public void Build()
        {
            if ((Object) this.fbxAsset == (Object) null)
                Debug.LogError((object) "Not found FBX.");
            else if (this.mmdModel == null)
            {
                Debug.LogError((object) "Not found model.xml");
            }
            else
            {
                this._cache_skinnedMeshRenderers = MMD4MecanimCommon.GetSkinnedMeshRenderers(this.fbxAsset);
                if (this._cache_skinnedMeshRenderers == null)
                {
                    Debug.LogError((object) "Not found SkinnedMeshRenderers");
                }
                else
                {
                    string assetPath = AssetDatabase.GetAssetPath((Object) this.fbxAsset);
                    string vertexDataPath = GetVertexDataPath(assetPath);
                    MMD4MecanimData.ExtraData extraData = MMD4MecanimData.BuildExtraData(
                        AssetDatabase.LoadAssetAtPath(GetExtraDataPath(assetPath),
                            typeof(TextAsset)) as TextAsset);
                    if (extraData == null || extraData.vertexDataList == null)
                    {
                        Debug.LogError((object) "Not found extra.bytes");
                    }
                    else
                    {
                        if ((double) this.importScale == 0.0)
                        {
                            Debug.LogWarning((object) "FBX not found.");
                            this.importScale = extraData.importScale;
                        }

                        int vertexCount = extraData.vertexCount;
                        int length = this._cache_skinnedMeshRenderers.Length;
                        int num1 = 0;
                        List<int> intList1 = new List<int>();
                        List<int> intList2 = new List<int>();
                        List<byte> byteList1 = new List<byte>();
                        List<int> intList3 = new List<int>();
                        List<int> intList4 = new List<int>();
                        List<byte> byteList2 = new List<byte>();
                        List<Vector3> vector3List = new List<Vector3>();
                        float vertexScale = extraData.vertexScale * this.importScale;
                        for (int index1 = 0; index1 < this._cache_skinnedMeshRenderers.Length; ++index1)
                        {
                            Transform[] bones = this._cache_skinnedMeshRenderers[index1].bones;
                            Matrix4x4[] bindposes = this._cache_skinnedMeshRenderers[index1].sharedMesh.bindposes;
                            Color32[] colors32 = this._cache_skinnedMeshRenderers[index1].sharedMesh.colors32;
                            BoneWeight[] boneWeights = this._cache_skinnedMeshRenderers[index1].sharedMesh.boneWeights;
                            if (bones == null || bones.Length == 0 || bindposes == null || bindposes.Length == 0 ||
                                colors32 == null || colors32.Length == 0 || boneWeights == null ||
                                boneWeights.Length == 0)
                            {
                                Debug.LogWarning((object) "Null SkinnedMeshRenderer. Skip processing.");
                                intList1.Add(0);
                                intList2.Add(0);
                                intList4.Add(0);
                            }
                            else
                            {
                                if (boneWeights.Length != colors32.Length)
                                {
                                    Debug.LogError((object) "Unknown.");
                                    return;
                                }

                                int count1 = byteList1.Count;
                                int count2 = byteList2.Count;
                                int num2 = Mathf.Min(bones.Length, bindposes.Length);
                                for (int boneIndex = 0; boneIndex < num2; ++boneIndex)
                                {
                                    byte num3 = 0;
                                    Matrix4x4 matrix4x4 = bindposes[boneIndex];
                                    if (matrix4x4.GetColumn(0) == new Vector4(1f, 0.0f, 0.0f, 0.0f) &&
                                        matrix4x4.GetColumn(1) == new Vector4(0.0f, 1f, 0.0f, 0.0f) &&
                                        matrix4x4.GetColumn(2) == new Vector4(0.0f, 0.0f, 1f, 0.0f) &&
                                        (double) matrix4x4.GetColumn(3).w == 1.0)
                                        num3 |= (byte) 4;
                                    byteList1.Add(num3);
                                    intList3.Add(
                                        VertexBuilder.ToOriginalBoneIndex(bones, boneIndex));
                                }

                                bool flag1 = false;
                                bool flag2 = false;
                                for (int index2 = 0; index2 < colors32.Length; ++index2)
                                {
                                    uint index3 = (uint) ((int) colors32[index2].r | (int) colors32[index2].g << 8 |
                                                          (int) colors32[index2].b << 16);
                                    if ((long) index3 < (long) vertexCount)
                                    {
                                        MMD4MecanimData.VertexData vertexData = extraData.vertexDataList[(int) index3];
                                        bool flag3 = vertexData.boneTransform == MMD4MecanimData.BoneTransform.SDEF;
                                        bool flag4 = vertexData.boneTransform == MMD4MecanimData.BoneTransform.QDEF;
                                        flag1 |= flag3;
                                        flag2 |= flag4;
                                        byte num4 = (byte) ((int) (byte) (0 | (flag3 ? 1 : 0)) | (flag4 ? 2 : 0));
                                        if (flag3)
                                        {
                                            int originalBoneIndex1 =
                                                VertexBuilder.ToOriginalBoneIndex(bones,
                                                    boneWeights[index2].boneIndex0);
                                            int originalBoneIndex2 =
                                                VertexBuilder.ToOriginalBoneIndex(bones,
                                                    boneWeights[index2].boneIndex1);
                                            if (originalBoneIndex1 != vertexData.boneWeight.boneIndex0 ||
                                                originalBoneIndex2 != vertexData.boneWeight.boneIndex1)
                                            {
                                                if (originalBoneIndex1 == vertexData.boneWeight.boneIndex1 &&
                                                    originalBoneIndex2 == vertexData.boneWeight.boneIndex0)
                                                {
                                                    num4 |= (byte) 128;
                                                }
                                                else
                                                {
                                                    float weight0 = boneWeights[(int) index3].weight0;
                                                    float weight1 = boneWeights[(int) index3].weight1;
                                                    if ((double) Mathf.Abs(weight0 - vertexData.boneWeight.weight0) >
                                                        (double) Mathf.Epsilon ||
                                                        (double) Mathf.Abs(weight1 - vertexData.boneWeight.weight1) >
                                                        (double) Mathf.Epsilon)
                                                    {
                                                        if ((double) Mathf.Abs(weight1 -
                                                                               vertexData.boneWeight.weight0) <=
                                                            (double) Mathf.Epsilon &&
                                                            (double) Mathf.Abs(weight0 -
                                                                               vertexData.boneWeight.weight1) <=
                                                            (double) Mathf.Epsilon)
                                                            num4 |= (byte) 128;
                                                        else
                                                            Debug.LogWarning((object) "");
                                                    }
                                                }
                                            }

                                            byte num5 = 1;
                                            if (boneWeights[index2].boneIndex0 >= 0)
                                                byteList1[count1 + boneWeights[index2].boneIndex0] |= num5;
                                            if (boneWeights[index2].boneIndex1 >= 0)
                                                byteList1[count1 + boneWeights[index2].boneIndex1] |= num5;
                                        }

                                        if (flag4)
                                        {
                                            byte num6 = 2;
                                            if (boneWeights[index2].boneIndex0 >= 0)
                                                byteList1[count1 + boneWeights[index2].boneIndex0] |= num6;
                                            if (boneWeights[index2].boneIndex1 >= 0)
                                                byteList1[count1 + boneWeights[index2].boneIndex1] |= num6;
                                            if (boneWeights[index2].boneIndex2 >= 0)
                                                byteList1[count1 + boneWeights[index2].boneIndex2] |= num6;
                                            if (boneWeights[index2].boneIndex3 >= 0)
                                                byteList1[count1 + boneWeights[index2].boneIndex3] |= num6;
                                        }

                                        byteList2.Add(num4);
                                        if (flag3)
                                        {
                                            vector3List.Add(
                                                VertexBuilder._ToUnityPos(vertexData.sdefC,
                                                    vertexScale));
                                            float num7 = boneWeights[index2].weight0;
                                            float num8 = boneWeights[index2].weight1;
                                            if (((int) num4 & 128) != 0)
                                            {
                                                num7 = boneWeights[index2].weight1;
                                                num8 = boneWeights[index2].weight0;
                                            }

                                            Vector3 vector3 = vertexData.sdefR0 * num7 + vertexData.sdefR1 * num8;
                                            Vector3 pos1 = vertexData.sdefR0 - vector3 + vertexData.sdefC;
                                            Vector3 pos2 = vertexData.sdefR1 - vector3 + vertexData.sdefC;
                                            vector3List.Add(
                                                VertexBuilder._ToUnityPos(pos1, vertexScale));
                                            vector3List.Add(
                                                VertexBuilder._ToUnityPos(pos2, vertexScale));
                                        }
                                    }
                                    else
                                    {
                                        Debug.LogError((object) "vertexIndex is overflow.");
                                        return;
                                    }
                                }

                                int num9 = 0 | (flag1 ? int.MinValue : 0) | (flag2 ? 1073741824 : 0);
                                num1 += colors32.Length;
                                intList1.Add(num9);
                                intList2.Add(num2);
                                intList4.Add(colors32.Length);
                                if (num9 == 0)
                                {
                                    byteList1.RemoveRange(count1, byteList1.Count - count1);
                                    intList3.RemoveRange(count1, intList3.Count - count1);
                                    byteList2.RemoveRange(count2, byteList2.Count - count2);
                                }
                            }
                        }

                        MMD4MecanimEditorCommon.StreamBuilder streamBuilder =
                            new MMD4MecanimEditorCommon.StreamBuilder();
                        streamBuilder.AddInt(vertexCount);
                        streamBuilder.AddInt(length << 24 | num1);
                        streamBuilder.AddInt((intList1.Count + 1) * 2 + intList3.Count);
                        streamBuilder.AddInt(2 + vector3List.Count * 3);
                        streamBuilder.AddInt(byteList1.Count + byteList2.Count);
                        int v = 0;
                        for (int index = 0; index < intList1.Count; ++index)
                        {
                            int num10 = intList1[index];
                            streamBuilder.AddInt(v | num10);
                            if (num10 != 0)
                                v += intList2[index];
                        }

                        streamBuilder.AddInt(v);
                        for (int index = 0; index < intList1.Count; ++index)
                        {
                            int num11 = intList1[index];
                            streamBuilder.AddInt(v);
                            if (num11 != 0)
                                v += intList4[index];
                        }

                        streamBuilder.AddInt(v);
                        for (int index = 0; index < intList3.Count; ++index)
                            streamBuilder.AddInt(intList3[index]);
                        streamBuilder.AddFloat(extraData.vertexScale);
                        streamBuilder.AddFloat(this.importScale);
                        for (int index = 0; index < vector3List.Count; ++index)
                            streamBuilder.AddVector3(vector3List[index]);
                        for (int index = 0; index < byteList1.Count; ++index)
                            streamBuilder.AddByte(byteList1[index]);
                        for (int index = 0; index < byteList2.Count; ++index)
                            streamBuilder.AddByte(byteList2[index]);
                        streamBuilder.WriteToFile(vertexDataPath);
                        AssetDatabase.ImportAsset(vertexDataPath,
                            ImportAssetOptions.ForceUpdate | ImportAssetOptions.ForceSynchronousImport);
                    }
                }
            }
        }
    }

    public class MaterialHelper
    {
        public List<MMDModel.Material> xmlMaterials;
        public List<int[]> materialNoLists;
        public int materialIndex;
        public Material material;
        public MMDModel.Material xmlMaterial;
        public PMX2FBXConfig.MMD4MecanimProperty mmd4MecanimProperty;
        public PMX2FBXConfig.MaterialGlobalProperty materialGlobalProperty;
        public PMX2FBXConfig.MaterialProperty materialProperty;
        public string materialName;

        public MaterialHelper(
            List<MMDModel.Material> xmlMaterials,
            List<int[]> materialNoLists,
            int materialIndex,
            Material material,
            PMX2FBXConfig.MMD4MecanimProperty mmd4MecanimProperty)
        {
            this.xmlMaterials = xmlMaterials;
            this.materialNoLists = materialNoLists;
            this.materialName = (Object) material != (Object) null ? material.name : "";
            this.materialIndex = materialIndex;
            this.xmlMaterial = xmlMaterials[materialIndex];
            this.materialGlobalProperty = mmd4MecanimProperty.materialGlobalProperty;
            this.materialProperty = mmd4MecanimProperty.FindMaterialProperty(this.materialName);
            this.material = material;
            this.mmd4MecanimProperty = mmd4MecanimProperty;
        }

        public PMX2FBXConfig.MaterialBlockProperty materialBlockProperty =>
            this.mmd4MecanimProperty.FindMaterialBlockProperty(this.materialName);

        public Color color => (this.xmlMaterial != null ? this.xmlMaterial.diffuse : new Color(0.6f, 0.6f, 0.6f, 1f)) *
                              this.materialBlockProperty.baseColor;

        public Color ambient => this.xmlMaterial == null ? new Color(0.5f, 0.5f, 0.5f, 1f) : this.xmlMaterial.ambient;

        public float toonTone
        {
            get
            {
                if (this.ValidateReadable())
                {
                    if (!this.materialProperty.overrideToonTone)
                        return this.materialGlobalProperty.toonTone;
                    if (this.material.HasProperty("_ToonTone"))
                        return this.material.GetVector("_ToonTone").x;
                }

                return 0.0f;
            }
            set
            {
                if (!this.ValidateWritable() || !this.materialProperty.overrideToonTone)
                    return;
                float x = value;
                float z = 0.5f;
                this.material.SetVector("_ToonTone", new Vector4(x, x * 0.5f, z, 0.0f));
            }
        }

        public float shadowLum
        {
            get
            {
                if (this.ValidateReadable())
                {
                    if (!this.materialProperty.overrideShadowLum)
                        return this.materialGlobalProperty.shadowLum;
                    if (this.material.HasProperty("_ShadowLum"))
                        return this.material.GetFloat("_ShadowLum");
                }

                return 0.0f;
            }
            set
            {
                if (!this.ValidateWritable() || !this.materialProperty.overrideShadowLum)
                    return;
                this.material.SetFloat("_ShadowLum", value);
            }
        }

        public float tessEdgeLength
        {
            get
            {
                if (this.ValidateReadable())
                {
                    if (!this.materialProperty.overrideTess)
                        return this.materialGlobalProperty.tessEdgeLength;
                    if (this.material.HasProperty("_TessEdgeLength"))
                        return this.material.GetFloat("_TessEdgeLength");
                }

                return 0.0f;
            }
            set
            {
                if (!this.ValidateWritable() || !this.materialProperty.overrideTess)
                    return;
                this.material.SetFloat("_TessEdgeLength", value);
            }
        }

        public float tessPhongStrength
        {
            get
            {
                if (this.ValidateReadable())
                {
                    if (!this.materialProperty.overrideTess)
                        return this.materialGlobalProperty.tessPhongStrength;
                    if (this.material.HasProperty("_TessPhongStrength"))
                        return this.material.GetFloat("_TessPhongStrength");
                }

                return 0.0f;
            }
            set
            {
                if (!this.ValidateWritable() || !this.materialProperty.overrideTess)
                    return;
                this.material.SetFloat("_TessPhongStrength", value);
            }
        }

        public float tessExtrusionAmount
        {
            get => this.ValidateReadable() && this.material.HasProperty("_TessExtrusionAmount")
                ? this.material.GetFloat("_TessExtrusionAmount")
                : 0.0f;
            set
            {
                if (!this.ValidateWritable())
                    return;
                this.material.SetFloat("_TessExtrusionAmount", value);
            }
        }

        public bool isTransparency
        {
            get
            {
                switch (this.materialGlobalProperty.transparencyMode)
                {
                    case PMX2FBXConfig.TransparencyMode.Disabled:
                        return false;
                    case PMX2FBXConfig.TransparencyMode.Optimized:
                        return this.materialProperty.transparencyCache !=
                               PMX2FBXConfig.TransparencyCache.Disabled ||
                               (double) this.color.a < 1.0;
                    case PMX2FBXConfig.TransparencyMode.AlwaysOn:
                        return true;
                    default:
                        return true;
                }
            }
        }

        public bool isTransparencyPhysical =>
            this.materialProperty.transparencyCache !=
            PMX2FBXConfig.TransparencyCache.Disabled || (double) this.color.a < 1.0;

        public Color emissionColor =>
            this.materialBlockProperty.emissionColor * this.materialBlockProperty.emissionPower;

        public PMX2FBXConfig.StandardBRDF standardBRDF =>
            this.materialBlockProperty.standardBRDF;

        public Color standardSpecularColor => this.materialBlockProperty.standardSpecularColor;

        public float standardMetallic => this.materialBlockProperty.standardMetallic;

        public float standardGlossiness
        {
            get
            {
                if (this.materialBlockProperty.standardBRDF ==
                    PMX2FBXConfig.StandardBRDF.Metallic)
                    return this.materialBlockProperty.standardMetallicGlossiness;
                return this.materialBlockProperty.standardBRDF ==
                       PMX2FBXConfig.StandardBRDF.Specular
                    ? this.materialBlockProperty.standardSpecularGlossiness
                    : 0.0f;
            }
        }

        public bool ValidateReadable() => (Object) this.material != (Object) null &&
                                          (Object) this.material.shader != (Object) null;

        public bool ValidateWritable() => (Object) this.material != (Object) null &&
                                          (Object) this.material.shader != (Object) null &&
                                          !this.materialProperty.isLocked;

        public void PrefixLegacy()
        {
            if (!this.ValidateWritable())
                return;
            this.material.SetColor("_EdgeColor", this.xmlMaterial.edgeColor);
            this.material.SetColor("_Ambient", this.xmlMaterial.ambient);
            this.material.SetFloat("_Shininess", this.xmlMaterial.shiness);
            this.material.mainTextureOffset = new Vector2(0.0f, 0.0f);
            this.material.mainTextureScale = new Vector2(1f, 1f);
            this.material.SetTextureOffset("_ToonTex", new Vector2(0.0f, 0.0f));
            this.material.SetTextureScale("_ToonTex", new Vector2(1f, 1f));
            this.material.SetTextureOffset("_SphereCube", new Vector2(0.0f, 0.0f));
            this.material.SetTextureScale("_SphereCube", new Vector2(1f, 1f));
        }

        public void PrefixStandard()
        {
            this.PrefixLegacy();
            if (!this.ValidateWritable())
                return;
            MMD4MecanimEditorCommon.SetOverrideTag(this.material, "RenderType", "");
            this.material.SetInt("_ZWrite", 1);
            this.material.DisableKeyword("_ALPHATEST_ON");
            this.material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            this.material.SetFloat("_Cutoff", 0.003921569f);
            if (this.isTransparency)
            {
                this.material.SetInt("_SrcBlend", 5);
                this.material.SetInt("_DstBlend", 10);
                this.material.EnableKeyword("_ALPHABLEND_ON");
                this.material.SetFloat("_Mode", 2f);
                this.material.SetFloat("_OffsetFactor", this.materialGlobalProperty.transparentOffsetFactor);
                this.material.SetFloat("_OffsetUnits", this.materialGlobalProperty.transparentOffsetUnits);
            }
            else
            {
                this.material.SetInt("_SrcBlend", 1);
                this.material.SetInt("_DstBlend", 0);
                this.material.DisableKeyword("_ALPHABLEND_ON");
                this.material.SetFloat("_Mode", 0.0f);
                this.material.SetFloat("_OffsetFactor", 0.0f);
                this.material.SetFloat("_OffsetUnits", 0.0f);
            }

            if (this.xmlMaterial.isDrawBothFaces)
                this.material.SetInt("_Cull", 0);
            else
                this.material.SetInt("_Cull", 2);
        }

        public void UpdateMaterial()
        {
            this.UpdateMaterial_Shader();
            this.UpdateMaterial_Tess();
            this.UpdateMaterial_Toon();
            this.UpdateMaterial_ToonTone();
            this.UpdateMaterial_Color();
            this.UpdateMaterial_Edge();
            this.UpdateMaterial_StandardBRDF();
            this.UpdateMaterial_StandardMetallic();
            this.UpdateMaterial_Specular();
            this.UpdateMaterial_Ambient();
            this.UpdateMaterial_Shadow();
            this.UpdateMaterial_AddLight();
            this.UpdateMaterial_Emission();
            this.UpdateMaterial_SphereMap();
            this.UpdateMaterial_NoShadowCasting();
            this.UpdateMaterial_SelfShadow();
            this.UpdateMaterial_TransparentOffset();
        }

        public void SetDirty()
        {
            if (!((Object) this.material != (Object) null))
                return;
            EditorUtility.SetDirty((Object) this.material);
        }

        #region UpdateMaterial

        public void UpdateMaterial_Shader()
        {
            if (!this.ValidateWritable())
                return;
            string shaderName = this.GetShaderName();
            if (shaderName == null || !(this.material.shader.name != shaderName))
                return;
            Shader shader = Shader.Find(shaderName);
            if (!((Object) shader != (Object) null))
                return;
            int renderQueue = this.material.renderQueue;
            this.material.shader = shader;
            this.material.renderQueue = renderQueue;
        }

        public void UpdateMaterial_Tess()
        {
            if (!this.ValidateWritable())
                return;
            float tessEdgeLength = this.materialGlobalProperty.tessEdgeLength;
            float tessPhongStrength = this.materialGlobalProperty.tessPhongStrength;
            if (this.materialProperty.overrideTess)
                return;
            this.material.SetFloat("_TessEdgeLength", tessEdgeLength);
            this.material.SetFloat("_TessPhongStrength", tessPhongStrength);
        }

        public void UpdateMaterial_Color()
        {
            if (!this.ValidateWritable())
                return;
            Color color1 = this.color;
            Color color2 = this.ambient;
            if (!this.materialProperty.isVisible)
            {
                color1 = new Color(0.0f, 0.0f, 0.0f, 0.0f);
                color2 = new Color(0.0f, 0.0f, 0.0f, 0.0f);
            }

            this.material.SetColor("_Color", color1);
            this.material.SetColor("_Ambient", color2);
        }

        public void UpdateMaterial_Edge()
        {
            if (!this.ValidateWritable())
                return;
            float edgeScale = this.materialGlobalProperty.edgeScale;
            this.material.SetFloat("_EdgeScale", edgeScale * MMD4MecanimCommon.MMDLit_edgeScale);
            this.material.SetFloat("_EdgeSize",
                this.xmlMaterial.edgeSize * edgeScale * MMD4MecanimCommon.MMDLit_edgeScale);
            if (this.materialProperty.isVisible && this.materialGlobalProperty.edgeEnabled)
                this.material.SetColor("_EdgeColor", this.xmlMaterial.edgeColor);
            else
            {
                Color _color = this.xmlMaterial.edgeColor;
                _color.a = 0;
                this.material.SetColor("_EdgeColor", _color);
            }
        }

        public void UpdateMaterial_NoShadowCasting()
        {
            if (!this.ValidateWritable() || !this.material.HasProperty("_NoShadowCasting") ||
                ((double) Mathf.Abs(this.material.GetFloat("_NoShadowCasting")) <= (double) Mathf.Epsilon ? 0 : 1) ==
                (!this.xmlMaterial.isDrawSelfShadowMap ? 1 : 0))
                return;
            this.material.SetFloat("_NoShadowCasting", !this.xmlMaterial.isDrawSelfShadowMap ? 1f : 0.0f);
        }

        public void UpdateMaterial_SelfShadow()
        {
            if (!this.ValidateWritable())
                return;
            if (this.xmlMaterial.isDrawSelfShadow)
            {
                if (this.materialGlobalProperty.selfShadowEnabled)
                    this.material.EnableKeyword("SELFSHADOW_ON");
                else
                    this.material.DisableKeyword("SELFSHADOW_ON");
            }
            else
                this.material.DisableKeyword("SELFSHADOW_ON");
        }

        public void UpdateMaterial_StandardBRDF()
        {
            if (!this.ValidateWritable())
                return;
            if (this.material.shader.name.Contains("Standard"))
            {
                if (this.standardBRDF == PMX2FBXConfig.StandardBRDF.Metallic)
                    this.material.DisableKeyword("_BRDF_SPECULAR");
                else if (this.standardBRDF == PMX2FBXConfig.StandardBRDF.Specular)
                    this.material.EnableKeyword("_BRDF_SPECULAR");
            }

            this.UpdateMaterial_StandardGlossiness();
        }

        public void UpdateMaterial_StandardMetallic()
        {
            if (!this.ValidateWritable() || !this.material.shader.name.Contains("Standard"))
                return;
            this.material.SetFloat("_Metallic", this.standardMetallic);
        }

        public void UpdateMaterial_StandardGlossiness()
        {
            if (!this.ValidateWritable() || !this.material.shader.name.Contains("Standard"))
                return;
            this.material.SetFloat("_Glossiness", this.standardGlossiness);
        }

        public void UpdateMaterial_Specular()
        {
            if (!this.ValidateWritable())
                return;
            if (this.material.shader.name.Contains("Standard"))
                this.material.SetColor("_SpecColor", this.standardSpecularColor);
            this.material.SetColor("_Specular", this.xmlMaterial.specular * MMD4MecanimCommon.MMDLit_globalLighting);
            if (this.materialGlobalProperty.specularEnabled &&
                IsNotZeroRGB(this.xmlMaterial.specular) &&
                (double) this.xmlMaterial.shiness > 0.0 && (double) this.xmlMaterial.shiness < 100.0)
                this.material.EnableKeyword("SPECULAR_ON");
            else
                this.material.DisableKeyword("SPECULAR_ON");
        }

        public void UpdateMaterial_Toon()
        {
            if (!this.ValidateWritable())
                return;
            if (this.materialGlobalProperty.toonEnabled)
                this.material.EnableKeyword("_TOON");
            else
                this.material.DisableKeyword("_TOON");
        }

        public void UpdateMaterial_ToonTone()
        {
            if (!this.ValidateWritable())
                return;
            float toonTone = this.materialGlobalProperty.toonTone;
            float z = 0.5f;
            if (this.materialProperty.overrideToonTone)
                return;
            this.material.SetVector("_ToonTone", new Vector4(toonTone, toonTone * 0.5f, z, 0.0f));
        }

        public void UpdateMaterial_Ambient()
        {
            if (!this.ValidateWritable())
                return;
            float num = 0.05f;
            float ambientToDiffuse = this.materialGlobalProperty.ambientToDiffuse;
            this.material.SetFloat("_AmbientToDiffuse",
                Mathf.Min((double) ambientToDiffuse > (double) num ? 1f / ambientToDiffuse : 1f / num, 1f / num));
            if ((double) ambientToDiffuse <= (double) num || !this.materialGlobalProperty.ambientToDiffuseEnabled)
                this.material.DisableKeyword("AMB2DIFF_ON");
            else
                this.material.EnableKeyword("AMB2DIFF_ON");
        }

        public void UpdateMaterial_Shadow()
        {
            if (!this.ValidateWritable())
                return;
            float shadowLum = this.materialGlobalProperty.shadowLum;
            if (this.materialProperty.overrideShadowLum)
                return;
            this.material.SetFloat("_ShadowLum", shadowLum);
        }

        public void UpdateMaterial_AddLight()
        {
            if (!this.ValidateWritable())
                return;
            this.material.SetFloat("_AddLightToonCen", this.materialGlobalProperty.addLightToonCen);
            this.material.SetFloat("_AddLightToonMin", this.materialGlobalProperty.addLightToonMin);
        }

        public void UpdateMaterial_Emission()
        {
            if (!this.ValidateWritable())
                return;
            Color color = this.emissionColor;
            bool flag = IsNotZeroRGB(color);
            if ((double) this.xmlMaterial.shiness > 100.0 && this.materialGlobalProperty.autoLuminous ==
                PMX2FBXConfig.AutoLuminous.Emissive)
            {
                color = MMD4MecanimCommon.ComputeAutoLuminousEmissiveColor(this.xmlMaterial.diffuse,
                    this.xmlMaterial.ambient, this.xmlMaterial.shiness, this.materialGlobalProperty.autoLuminousPower);
                flag = true;
                this.material.SetFloat("_ALPower", this.materialGlobalProperty.autoLuminousPower);
            }
            else
                this.material.SetFloat("_ALPower", 0.0f);

            if (this.material.shader.name.Contains("Standard"))
            {
                this.material.SetColor("_EmissionColor", color);
                if (flag)
                {
                    this.material.EnableKeyword("_EMISSION");
                    if (this.material.globalIlluminationFlags == MaterialGlobalIlluminationFlags.None)
                        this.material.globalIlluminationFlags = MaterialGlobalIlluminationFlags.RealtimeEmissive;
                }
                else
                    this.material.DisableKeyword("_EMISSION");

                this.UpdateMaterial_GlobalIllumination();
            }
            else
            {
                this.material.SetColor("_Emissive", color);
                if (flag)
                    this.material.EnableKeyword("EMISSIVE_ON");
                else
                    this.material.DisableKeyword("EMISSIVE_ON");
            }
        }

        public void UpdateMaterial_TransparentOffset()
        {
            if (!this.ValidateWritable())
                return;
            if (this.isTransparencyPhysical)
            {
                this.material.SetFloat("_OffsetFactor", this.materialGlobalProperty.transparentOffsetFactor);
                this.material.SetFloat("_OffsetUnits", this.materialGlobalProperty.transparentOffsetUnits);
            }
            else
            {
                this.material.SetFloat("_OffsetFactor", 0.0f);
                this.material.SetFloat("_OffsetUnits", 0.0f);
            }
        }

        public void UpdateMaterial_GlobalIllumination()
        {
            if (!this.ValidateWritable() || !this.material.shader.name.Contains("Standard"))
                return;
            if (this.materialGlobalProperty.globalIlluminationEmissiveType !=
                PMX2FBXConfig.GlobalIlluminationEmissiveType.None)
            {
                this.material.globalIlluminationFlags =
                    this.materialGlobalProperty.globalIlluminationEmissiveType != PMX2FBXConfig
                        .GlobalIlluminationEmissiveType.Realtime
                        ? MaterialGlobalIlluminationFlags.BakedEmissive
                        : MaterialGlobalIlluminationFlags.RealtimeEmissive;
                if (this.material.IsKeywordEnabled("_EMISSION"))
                    return;
                this.material.globalIlluminationFlags |= MaterialGlobalIlluminationFlags.EmissiveIsBlack;
            }
            else
                this.material.globalIlluminationFlags = MaterialGlobalIlluminationFlags.None;
        }

        public void UpdateMaterial_SphereMap()
        {
            if (!this.ValidateWritable())
                return;
            this.material.DisableKeyword("SPHEREMAP_MUL");
            this.material.DisableKeyword("SPHEREMAP_ADD");
            if (this.material.GetTexture("_SphereCube") == null || !this.materialGlobalProperty.sphereMapEnabled ||
                this.xmlMaterial.sphereMode == MMDModel.SphereMode.None)
                return;
            if (this.xmlMaterial.sphereMode == MMDModel.SphereMode.Multiply)
                this.material.EnableKeyword("SPHEREMAP_MUL");
            else if (this.xmlMaterial.sphereMode == MMDModel.SphereMode.Adding)
            {
                this.material.EnableKeyword("SPHEREMAP_ADD");
            }
            else
            {
                if (this.xmlMaterial.sphereMode != MMDModel.SphereMode.SubTexture)
                    return;
                Debug.LogWarning((object) ("SphereMap [" + this.material.name +
                                                       "] Not supported SubTexture."));
            }
        }

        public void UpdateMaterial_SphereCube(Texture additionalTexture)
        {
            if (!this.ValidateWritable())
                return;
            if (this.xmlMaterial.sphereMode != MMDModel.SphereMode.None)
            {
                this.material.SetTexture("_SphereCube", additionalTexture);
                if (this.xmlMaterial.sphereMode != MMDModel.SphereMode.None &&
                    (Object) additionalTexture == (Object) null)
                    Debug.LogWarning((object) ("SphereCube [" + this.material.name +
                                                           "] additionalTexture is null. " + this.material.name));
            }
            else
                this.material.SetTexture("_SphereCube", (Texture) null);

            this.UpdateMaterial_SphereMap();
        }

        #endregion

        private bool _IsNoShadowCastingInRenderers()
        {
            if (this.materialNoLists == null || this.xmlMaterials == null)
                return true;
            bool flag1 = false;
            foreach (int[] materialNoList in this.materialNoLists)
            {
                if (materialNoList != null)
                {
                    bool flag2 = false;
                    foreach (int num in materialNoList)
                    {
                        if (num == this.materialIndex)
                        {
                            flag2 = true;
                            flag1 = true;
                            break;
                        }
                    }

                    if (flag2)
                    {
                        foreach (int index in materialNoList)
                        {
                            if (index != this.materialIndex && index >= 0 && index < this.xmlMaterials.Count &&
                                this.xmlMaterials[index].isDrawSelfShadowMap)
                                return true;
                        }
                    }
                }
            }

            return !flag1;
        }

        public string GetShaderName()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("MMD4Mecanim/");
            bool flag = !this.xmlMaterial.isDrawSelfShadowMap;
            if (flag)
                flag = this._IsNoShadowCastingInRenderers();
            bool tessEnabled = this.materialGlobalProperty.tessEnabled;
            if (this.materialProperty.overrideTess)
                tessEnabled = this.materialProperty.tessEnabled;
            if (_standardEnabled && this.materialGlobalProperty.shaderType ==
                PMX2FBXConfig.ShaderType.Standard)
            {
                stringBuilder.Append("Standard/MMDLit");
                if (tessEnabled)
                    stringBuilder.Append("-Tess");
                if (flag)
                    stringBuilder.Append("-NoShadowCasting");
                if (this.xmlMaterial.isDrawBothFaces && this.isTransparency)
                {
                    stringBuilder.Append("-BothFaces");
                    stringBuilder.Append("-Transparent");
                }
            }
            else
            {
                stringBuilder.Append("MMDLit");
                if (tessEnabled)
                    stringBuilder.Append("-Tess");
                if (flag)
                    stringBuilder.Append("-NoShadowCasting");
                if (this.xmlMaterial.isDrawBothFaces)
                    stringBuilder.Append("-BothFaces");
                if (this.isTransparency)
                    stringBuilder.Append("-Transparent");
            }

            if (this.xmlMaterial.isDrawEdge && this.materialGlobalProperty.edgeEnabled)
                stringBuilder.Append("-Edge");
            return stringBuilder.ToString();
        }
    }

    [Flags]
    public enum MaterialState
    {
        Nothing = 0,
        Uninitialized = 1,
        Previous = 2,
        Normally = 4,
    }

    public class MMD4MecanimGlobalSettings
    {
        public const string XmlPath = "Library/MMD4MecanimGlobalSettings.xml";
        public int importerInspectorViewPage;
        public bool importerInspectorViewAdvanced;
        public int materialShakeHandsEnabled;
        private static MMD4MecanimGlobalSettings _instance;

        public static MMD4MecanimGlobalSettings instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = _Deserialize("Library/MMD4MecanimGlobalSettings.xml");
                    if (_instance == null)
                    {
                        _instance = new MMD4MecanimGlobalSettings();
                        _Serialize("Library/MMD4MecanimGlobalSettings.xml",
                            MMD4MecanimGlobalSettings._instance);
                    }
                }

                return _instance;
            }
        }

        public static void Sync()
        {
            if (MMD4MecanimGlobalSettings._instance == null)
                return;
            MMD4MecanimGlobalSettings._Serialize("Library/MMD4MecanimGlobalSettings.xml",
                MMD4MecanimGlobalSettings._instance);
        }

        private static MMD4MecanimGlobalSettings _Deserialize(
            string xmlAssetPath)
        {
            if (string.IsNullOrEmpty(xmlAssetPath))
                return (MMD4MecanimGlobalSettings) null;
            if (!File.Exists(xmlAssetPath))
                return (MMD4MecanimGlobalSettings) null;
            try
            {
                using (FileStream fileStream = new FileStream(xmlAssetPath, System.IO.FileMode.Open, FileAccess.Read,
                           FileShare.Read))
                    return (MMD4MecanimGlobalSettings) new XmlSerializer(
                        typeof(MMD4MecanimGlobalSettings)).Deserialize((Stream) fileStream);
            }
            catch (Exception ex)
            {
                return (MMD4MecanimGlobalSettings) null;
            }
        }

        private static bool _Serialize(
            string xmlAssetPath,
            MMD4MecanimGlobalSettings globalSettings)
        {
            if (string.IsNullOrEmpty(xmlAssetPath))
            {
                Debug.LogWarning((object) "xmlAssetPath is null.");
                return false;
            }

            try
            {
                XmlWriterSettings settings = new XmlWriterSettings()
                {
                    Indent = true,
                    OmitXmlDeclaration = false,
                    Encoding = Encoding.UTF8
                };
                using (FileStream output = new FileStream(xmlAssetPath, System.IO.FileMode.Create, FileAccess.Write,
                           FileShare.None))
                {
                    using (XmlWriter xmlWriter = XmlWriter.Create((Stream) output, settings))
                        new XmlSerializer(typeof(MMD4MecanimGlobalSettings)).Serialize(xmlWriter,
                            (object) globalSettings);
                }

                return true;
            }
            catch (Exception ex)
            {
                Debug.LogError((object) "");
                return false;
            }
        }
    }

    public class PMX2FBXConfig
    {
        public static readonly string[] BulletPhysicsVersionString = new string[2]
        {
            "2.75",
            "Latest (2.83)"
        };

        public GlobalSettings globalSettings;
        public BulletPhysics bulletPhysics;
        public List<Rename> renameList;
        public List<SplitMeshBone> splitMeshBoneList;
        public List<EdgeStretch> edgeStretchList;
        public List<FreezeRigidBody> freezeRigidBodyList;
        public List<FreezeMotion> freezeMotionList;
        public MMD4MecanimProperty mmd4MecanimProperty;

        public MaterialProperty FindMaterialProperty(
            string materialName)
        {
            return mmd4MecanimProperty != null
                ? mmd4MecanimProperty.FindMaterialProperty(materialName)
                : (PMX2FBXConfig.MaterialProperty) null;
        }

        public MaterialBlockProperty FindMaterialBlockProperty(
            string materialName)
        {
            return this.mmd4MecanimProperty != null
                ? this.mmd4MecanimProperty.FindMaterialBlockProperty(materialName)
                : (PMX2FBXConfig.MaterialBlockProperty) null;
        }

        public void PrepareMaterialPropertyList(List<Material> materials)
        {
            if (this.mmd4MecanimProperty == null)
                return;
            this.mmd4MecanimProperty.PrepareMaterialPropertyList(materials);
        }

        public void PrepareMaterialBlockPropertyList(List<Material> materials)
        {
            if (this.mmd4MecanimProperty == null)
                return;
            this.mmd4MecanimProperty.PrepareMaterialBlockPropertyList(materials);
        }

        public enum ShaderType
        {
            Compatible,
            Standard,
        }

        public enum AutoLuminous
        {
            Disable,
            Emissive,
        }

        public enum Wine
        {
            NXWine,
            WineBottler,
            Wine,
            Manual,
        }

        public enum BulletPhysicsVersion
        {
            _275,
            Latest,
        }

        public enum StandardBRDF
        {
            Metallic,
            Specular,
        }

        public enum TransparencyMode
        {
            Disabled,
            Optimized,
            AlwaysOn,
        }

        public enum TransparencyCache
        {
            Unknown,
            Disabled,
            Enabled,
        }

        public enum SplitMeshBoneExtend
        {
            SameMaterial,
            SharedVertex1,
            SharedVertex2,
            NoExtend,
        }

        public class GlobalSettings
        {
            public bool editorAdvancedMode;
            public float vertexScale = 8f;
            public float vertexScaleByHeight;
            public float importScale = 0.01f;
            public int enableIK = 1;
            public int ikLoopMinIterations = -1;
            public int keepIKTargetBoneFlag;
            public int forceIKResetBoneFlag;
            public int enableIKFixedAxisFlag = -1;
            public int enableIKSecondPassFlag;
            public float secondPassLimitAngle = -1f;
            public int enableIKInnerLockFlag;
            public int enableIKInnerLockKneeFlag = -1;
            public float innerLockKneeClamp = -1f;
            public float innerLockKneeRatioU = -1f;
            public float innerLockKneeRatioL = -1f;
            public float innerLockKneeScale = -1f;
            public int enableIKMuscleFlag;
            public int enableIKMuscleHipFlag = 1;
            public int enableIKMuscleFootFlag = 1;
            public float muscleHipUpperXAngle = -1f;
            public float muscleHipLowerXAngle = -1f;
            public float muscleHipInnerYAngle = -1f;
            public float muscleHipOuterYAngle = -1f;
            public float muscleHipInnerZAngle = -1f;
            public float muscleHipOuterZAngle = -1f;
            public float muscleFootUpperXAngle = -1f;
            public float muscleFootLowerXAngle = -1f;
            public float muscleFootInnerYAngle = -1f;
            public float muscleFootOuterYAngle = -1f;
            public float muscleFootInnerZAngle = -1f;
            public float muscleFootOuterZAngle = -1f;
            public int createSkeletonFlag = 1;
            public int boneRenameFlag = 1;
            public int prefixBoneRenameFlag = 1;
            public int prefixBoneNoNameFlag = 1;
            public int prefixNullBoneNameFlag = 1;
            public int morphRenameFlag;
            public int prefixMorphNoNameFlag = 1;
            public int materialRenameFlag = 1;
            public int prefixMaterialNoNameFlag = 1;
            public int escapeMaterialNameFlag = 1;
            public int addMotionNameExtFlag = 1;
            public int addMotionNameExtAsLegacyFlag;
            public int nullBoneFlag = 1;
            public int nullBoneAnimationFlag = 1;
            public int dummyCharBoneFlag;
            public int fixedAxisFlag = -1;
            public int localAxisFlag = -1;
            public int boneMorphFlag = 1;
            public int bone4MecanimFlag = 1;
            public int bodyBone4Mecanim = 1;
            public int armBone4Mecanim = 1;
            public int handBone4Mecanim = 1;
            public int handBone4MecanimModThumb0 = 1;
            public int legBone4Mecanim = 1;
            public int headBone4Mecanim = 1;
            public int blendShapesFlag = 1;
            public int splitMeshFlag = 1;
            public int splitMeshVertexMorphFlag = 1;
            public int splitMeshVertexMorphExtraFlag = 1;
            public int splitMeshVertexMorphCombineFlag = 1;
            public int splitMeshXDEFFlag;
            public int splitMeshXDEFExtraFlag;
            public int splitMeshXDEFExtraAlphaFlag;
            public int splitMeshNoShadowCastingFlag = 1;
            public double sameVertexLength;
            public int edgeStretchEnabled;
            public double sameVertexLengthEdgeStretch = 0.001;
            public int rotationOrderToZXY = 1;
            public int enableFBXTexture;
            public string exportFBXFormat = "";
            public int supportVMDIKDisabled = 1;
            public int supportVMDPhysicsDisabled = 1;
            public int animKeyReductionFlag = 1;
            public int animNullAnimationFlag;
            public int animRootTransformFlag;
            public float animKeyRotationEpsilon1 = 0.02f;
            public float animKeyRotationEpsilon2 = 0.03f;
            public float animKeyTranslationEpsilon1 = 1f / 500f;
            public float animKeyTranslationEpsilon2 = 3f / 1000f;
            public int animAwakeWaitingTime = 3;
            public int morphOppaiFlag;
            public int morphOppaiTanimaMethod;
            public double morphOppaiPower = 0.5;
            public double morphOppaiCenter = 0.6;
            public double morphOppaiRaidus = 1.0;
            public string morphOppaiBoneName1 = "上半身";
            public string morphOppaiBoneName2 = "首";
        }

        public class BulletPhysics
        {
            public int enabled = 1;
            public int framePerSecond;
            public float gravityScale = 10f;
            public int worldSolverInfoNumIterations;
            public int worldSolverInfoSplitImpulse = 1;
            public int worldAddFloorPlane = 1;
            public int rigidBodyIsAdditionalDamping = 1;
            public int rigidBodyIsForceBoneAlignment = 1;
            public int rigidBodyIsEnableSleeping;
            public int rigidBodyIsUseCcd = 1;
            public float rigidBodyCcdMotionThreshold = 0.01f;
            public float rigidBodyShapeScale = 1f;
            public float rigidBodyMassRate = 1f;
            public float rigidBodyLinearDampingRate = 1f;
            public float rigidBodyAngularDampingRate = 1f;
            public float rigidBodyRestitutionRate = 1f;
            public float rigidBodyFrictionRate = 1f;
            public float rigidBodyAntiJitterRate = 1f;
            public float rigidBodyAntiJitterRateOnKinematic = 1f;
            public float rigidBodyPreBoneAlignmentLimitLength = 0.01f;
            public float rigidBodyPreBoneAlignmentLossRate = 0.01f;
            public float rigidBodyPostBoneAlignmentLimitLength = 0.01f;
            public float rigidBodyPostBoneAlignmentLossRate = 0.01f;
            public float rigidBodyLinearDampingLossRate = -1f;
            public float rigidBodyLinearDampingLimit = -1f;
            public float rigidBodyAngularDampingLossRate = -1f;
            public float rigidBodyAngularDampingLimit = -1f;
            public float rigidBodyLinearVelocityLimit = -1f;
            public float rigidBodyAngularVelocityLimit = -1f;
            public int rigidBodyIsUseForceAngularVelocityLimit = 1;
            public int rigidBodyIsUseForceAngularAccelerationLimit = 1;
            public float rigidBodyForceAngularVelocityLimit = -1f;
            public int rigidBodyIsAdditionalCollider = 1;
            public float rigidBodyAdditionalColliderBias = 0.99f;
            public int rigidBodyIsForceTranslate = 1;
            public float jointRootAdditionalLimitAngle = -1f;
            public float jointAdditionalLimitAngle = -1f;
            public int jointAdditionalLimitForcibly;
        }

        public class Rename
        {
            public string from;
            public string to;
        }

        public class SplitMeshBone
        {
            public string name;
            public int extend = 2;
        }

        public class EdgeStretchArea
        {
            public string targetBoneName = "";
            public string ignoreBoneName = "";
            public string materialName = "";
            public double stretchLength = 0.004;
            public double extrudeAmount;
        }

        public class EdgeStretch
        {
            public EdgeStretchArea targetArea = new EdgeStretchArea();
            public EdgeStretchArea neighborArea = new EdgeStretchArea();
            public int neighborEnabled;
            public double neighborLength = 0.01;
        }

        public class FreezeRigidBody
        {
            public string boneName;
            public int recursively;
        }

        public class FreezeMotion
        {
            public string boneName;
            public int recursively;
        }

        public enum GlobalIlluminationEmissiveType
        {
            None,
            Realtime,
            Baked,
        }

        public enum StandardSetupType
        {
            None,
            Metalic,
            Specular,
        }

        public class MaterialGlobalProperty
        {
            public ShaderType shaderType;
            public bool toonEnabled = true;
            public float toonTone = 1f;
            public bool prefixRenderQueue = true;
            public bool renderQueueAfterSkybox;
            public bool ambientToDiffuseEnabled;
            public float ambientToDiffuse = 0.2f;
            public bool specularEnabled = true;
            public bool edgeEnabled = true;
            public float edgeScale = 1f;
            public float shadowLum = 1.5f;
            public float specularToGlosiness = 0.05f;
            public float addLightToonCen = -0.1f;
            public float addLightToonMin = 0.5f;
            public AutoLuminous autoLuminous;
            public float autoLuminousPower = 5f;
            public bool tessEnabled;
            public float tessEdgeLength = 5f;
            public float tessPhongStrength = 0.5f;
            public StandardSetupType standardSetupType = StandardSetupType.Specular;
            public float sphereCubeToMetallicGlossinessRate = 5f;
            public float sphereCubeToSpecularGlossiness = 0.75f;
            public float sphereCubeToSpecularColorRate = 1.2f;

            public GlobalIlluminationEmissiveType globalIlluminationEmissiveType =
                GlobalIlluminationEmissiveType.Realtime;

            public TransparencyMode transparencyMode = TransparencyMode.Optimized;
            public float transparentOffsetFactor = -0.1f;
            public float transparentOffsetUnits = -1f;
            public bool selfShadowEnabled = true;
            public bool sphereMapEnabled = true;
        }

        public class MaterialProperty
        {
            public string materialName;
            public bool isVisible = true;
            public bool isLocked;
            public bool isUIFoldout;
            public bool overrideToonTone;
            public bool overrideShadowLum;
            public bool overrideTess;
            public bool tessEnabled;
            public TransparencyCache transparencyCache;
        }

        public class MaterialBlockProperty
        {
            public List<string> materialNames = new List<string>();
            public bool isUIFoldout = true;
            public Color baseColor = new Color(1f, 1f, 1f, 1f);
            public Color emissionColor;
            public float emissionPower = 1f;
            public StandardBRDF standardBRDF = StandardBRDF.Specular;
            public Color standardSpecularColor;
            public float standardMetallicGlossiness;
            public float standardSpecularGlossiness;
            public float standardMetallic;

            public void Repair()
            {
                if (this.materialNames == null)
                    this.materialNames = new List<string>();
                this.materialNames.RemoveAll((m => m == null));
            }
        }

        public class MMD4MecanimProperty
        {
            public bool vertexScaleByHeightFlag;
            public float vertexScaleByHeight = 158f;
            public bool waitProcessExitting;
            public string pmxAssetPath;
            public List<string> vmdAssetPathList;
            public string fbxOutputPath;
            public string fbxAssetPath;
            public BulletPhysicsVersion bulletPhysicsVersion;
            public bool useWineFlag;
            public Wine wine;
            public string winePath;
            public bool materialShakeHanded;
            public int materialViewPage;
            public MaterialGlobalProperty materialGlobalProperty = new MaterialGlobalProperty();
            public List<MaterialProperty> materialProperyList;
            public List<MaterialBlockProperty> materialBlockPropertyList;

            public MaterialProperty FindMaterialProperty(
                string targetMaterialName)
            {
                if (targetMaterialName != null && this.materialProperyList != null)
                {
                    foreach (MaterialProperty materialPropery in this.materialProperyList)
                    {
                        if (materialPropery != null && materialPropery.materialName != null &&
                            materialPropery.materialName == targetMaterialName)
                            return materialPropery;
                    }
                }

                return null;
            }

            public MaterialBlockProperty FindMaterialBlockProperty(
                string targetMaterialName)
            {
                if (targetMaterialName != null && this.materialBlockPropertyList != null)
                {
                    foreach (MaterialBlockProperty materialBlockProperty in this.materialBlockPropertyList)
                    {
                        if (materialBlockProperty != null && materialBlockProperty.materialNames != null)
                        {
                            foreach (string materialName in materialBlockProperty.materialNames)
                            {
                                if (materialName != null && materialName == targetMaterialName)
                                    return materialBlockProperty;
                            }
                        }
                    }
                }

                return null;
            }

            public void PrepareMaterialPropertyList(List<Material> materials)
            {
                if (this.materialProperyList == null)
                    this.materialProperyList = new List<MaterialProperty>();
                if (materials == null)
                    return;
                foreach (Material material in materials)
                {
                    if (material != null && material.name != null && FindMaterialProperty(material.name) == null)
                        this.materialProperyList.Add(new MaterialProperty()
                        {
                            materialName = material.name
                        });
                }
            }

            public void PrepareMaterialBlockPropertyList(List<Material> materials)
            {
                if (materialBlockPropertyList == null)
                    materialBlockPropertyList = new List<MaterialBlockProperty>();
                MaterialBlockProperty materialBlockProperty;
                if (materialBlockPropertyList.Count == 0)
                {
                    materialBlockProperty = new MaterialBlockProperty();
                    materialBlockPropertyList.Add(materialBlockProperty);
                }
                else
                {
                    materialBlockProperty = materialBlockPropertyList[0];
                    if (materialBlockProperty == null)
                    {
                        materialBlockProperty = new MaterialBlockProperty();
                        materialBlockPropertyList[0] = materialBlockProperty;
                    }
                }

                if (materials == null)
                    return;
                foreach (Material material in materials)
                {
                    if (material != null && material.name != null && FindMaterialBlockProperty(material.name) == null)
                        materialBlockProperty.materialNames.Add(material.name);
                }
            }
        }
    }

    #region DataModel

    public class MMDModel
    {
        public GlobalSettings globalSettings;
        public Texture[] textureList;
        public Material[] materialList;
        public Bone[] boneList;
        public IK[] ikList;
        public Morph[] morphList;
        public DisplayFrame[] displayFrameList;
        public RigidBody[] rigidBodyList;
        public Joint[] jointList;

        public enum FileType
        {
            None,
            PMD,
            PMX,
        }

        public enum BoneType
        {
            Rotate,
            RotateAndMove,
            IKDestination,
            Unknown,
            UnderIK,
            UnderRotate,
            IKTarget,
            NoDisp,
            Twist,
            FollowRotate,
        }

        public enum SphereMode
        {
            None,
            Multiply,
            Adding,
            SubTexture,
        }

        public enum ExpType
        {
            Base,
            EyeBrow,
            Eye,
            Lip,
            Other,
        }

        public enum MorphCategory
        {
            Base,
            EyeBrow,
            Eye,
            Lip,
            Other,
        }

        public enum MorphType
        {
            Group,
            Vertex,
            Bone,
            UV,
            UVA1,
            UVA2,
            UVA3,
            UVA4,
            Material,
        }

        public enum DisplayFrameItemType
        {
            Bone,
            Morph,
        }

        public enum ShapeType
        {
            Sphere,
            Box,
            Capsule,
        }

        public enum RigidBodyType
        {
            Static,
            Dynamic,
            StaticDynamic,
        }

        public enum JointType
        {
            Spring6DOF,
        }

        public class GlobalSettings
        {
            public FileType fileType;
            public float fileVersion;
            public string modelNameJp;
            public string modelNameEn;
            public string commentJp;
            public string commentEn;
            public uint numVertex;
            public float vertexScale;
            public float importScale;
        }

        public class Texture
        {
            public string fileName;
        }

        public class Material
        {
            public string nameJp;
            public string nameEn;
            public string materialName;
            public string translatedName;
            public string surfaceName;
            public Color diffuse;
            public Color specular;
            public float shiness;
            public Color ambient;
            public SphereMode sphereMode;
            public int toonID;
            public Color edgeColor;
            public float edgeSize;
            public int textureID;
            public int additionalTextureID;
            public int toonTextureID;
            public uint flags;
            public bool isDrawBothFaces;
            public bool isDrawGroundShadow;
            public bool isDrawSelfShadowMap;
            public bool isDrawSelfShadow;
            public bool isDrawEdge;
            public uint numIndex;
        }

        public class Bone
        {
            public string nameJp;
            public string nameEn;
            public string skeletonName;
            public int parentBoneID;
            public int sortedBoneID;
            public Vector3 origin;
            public uint additionalFlags;
            public bool isLimitAngleX;
            public bool isRigidBody;
            public bool isKinematic;
            public BoneType boneType;
            public int targetBoneID;
            public int childBoneID;
            public float followCoef;
            public int transformLayerID;
            public uint flags;
            public int destination;
            public bool isRotate;
            public bool isTranslate;
            public bool isVisible;
            public bool isControllable;
            public bool isIK;
            public int inherenceLocal;
            public bool isInherenceRotate;
            public bool isInherenceTranslate;
            public bool isFixedAxis;
            public bool isLocalAxis;
            public bool isTransformAfterPhysics;
            public bool isTransformExternalParent;
            public string humanType;
        }

        public class IKLink
        {
            public bool hasAngularLimit;
            public Vector3 lowerLimit;
            public Vector3 upperLimit;
        }

        public class IK
        {
            public int destBoneID;
            public int targetBoneID;
            public int iteration;
            public float constraintAngle;
            public IKLink[] ikLinkList;
        }

        public class Morph
        {
            public string nameJp;
            public string nameEn;
            public string translatedName;
            public string blendShapeName;
            public uint additionalFlags;
            public bool isMorphBaseVertex;
            public ExpType expType;
            public MorphCategory morphCategory;
            public MorphType morphType;
            public int[] indexList;
        }

        public class DisplayFrame
        {
            public string nameJp;
            public string nameEn;
            public uint additionalFlags;
            public bool isSpecial;
            public MMDModel.DisplayFrame.Item[] itemList;

            public class Item
            {
                public MMDModel.DisplayFrameItemType type;
                public int index;
            }
        }

        public class RigidBody
        {
            public string nameJp;
            public string nameEn;
            public int boneID;
            public uint collisionGroupID;
            public uint collisionMask;
            public MMDModel.ShapeType shapeType;
            public float shapeWidth;
            public float shapeHeight;
            public float shapeDepth;
            public Vector3 position;
            public Quaternion rotation;
            public float mass;
            public float linearDamping;
            public float angularDamping;
            public float restitution;
            public float friction;
            public float rigidBodyType;
        }

        public class Joint
        {
            public string nameJp;
            public string nameEn;
            public MMDModel.JointType jointType;
            public int targetRigidBodyIDA;
            public int targetRigidBodyIDB;
            public Vector3 position;
            public Quaternion rotation;
            public Vector3 limitPosFrom;
            public Vector3 limitPosTo;
            public Vector3 limitRotFrom;
            public Vector3 limitRotTo;
        }
    }

    public class FBXMetaData
    {
        public HumanDescription humanDescription;
        public List<HumanBone> humanBones = new List<HumanBone>();
        public List<SkeletonBone> skeletonBones = new List<SkeletonBone>();
        public int humanDescriptionTab;
        public string headerString = "";
        public string middleString = "";
        public string footerString = "";
    }

    public struct Range
    {
        public int startIndex;
        public int count;

        public int endIndex => this.startIndex + this.count;

        public Range(int s, int c)
        {
            this.startIndex = s;
            this.count = c;
        }
    }

    #endregion
}