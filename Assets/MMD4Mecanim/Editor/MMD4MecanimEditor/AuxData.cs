﻿// Decompiled with JetBrains decompiler
// Type: MMD4MecanimInternal.AuxData
// Assembly: MMD4Mecanim, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: ADBCBBA1-CEE8-40F4-A775-E57AB2D2693F
// Assembly location: C:\Github\RuntimeMMD\Assets\MMD4Mecanim\Scripts\MMD4Mecanim.dll

using System;
using UnityEngine;

namespace MMD4MecanimInternal
{
  public static class AuxData
  {
    public static int ReadInt(byte[] bytes, int index) => bytes != null && index * 4 + 3 < bytes.Length ? (int) bytes[index * 4] | (int) bytes[index * 4 + 1] << 8 | (int) bytes[index * 4 + 2] << 16 | (int) bytes[index * 4 + 3] << 24 : 0;

    public static AuxData.IndexData BuildIndexData(TextAsset indexFile)
    {
      if (!((UnityEngine.Object) indexFile == (UnityEngine.Object) null))
        return AuxData.BuildIndexData(indexFile.bytes);
      Debug.LogError((object) "BuildIndexData: indexFile is norhing.");
      return (AuxData.IndexData) null;
    }

    public static AuxData.IndexData BuildIndexData(byte[] indexBytes)
    {
      if (indexBytes == null || indexBytes.Length == 0)
        return (AuxData.IndexData) null;
      int length = indexBytes.Length / 4;
      if (length < 2)
        return (AuxData.IndexData) null;
      int[] dst = new int[length];
      Buffer.BlockCopy((Array) indexBytes, 0, (Array) dst, 0, length * 4);
      return new AuxData.IndexData() { indexValues = dst };
    }

    public static bool ValidateIndexData(
      AuxData.IndexData indexData,
      SkinnedMeshRenderer[] skinnedMeshRenderers)
    {
      if (indexData == null || skinnedMeshRenderers == null)
        return false;
      if (indexData.meshCount != skinnedMeshRenderers.Length)
      {
        Debug.LogError((object) "ValidateIndexData: FBX reimported. Disabled morph, please recreate index file.");
        return false;
      }
      int num = 0;
      foreach (SkinnedMeshRenderer skinnedMeshRenderer in skinnedMeshRenderers)
      {
        if ((UnityEngine.Object) skinnedMeshRenderer.sharedMesh != (UnityEngine.Object) null)
          num += skinnedMeshRenderer.sharedMesh.vertexCount;
      }
      if (indexData.meshVertexCount == num)
        return true;
      Debug.LogError((object) "ValidateIndexData: FBX reimported. Disabled morph, please recreate index file.");
      return false;
    }

    public static AuxData.VertexData BuildVertexData(TextAsset vertexFile)
    {
      if (!((UnityEngine.Object) vertexFile == (UnityEngine.Object) null))
        return AuxData.BuildVertexData(vertexFile.bytes);
      Debug.LogError((object) "BuildVertexData: xdefFile is norhing.");
      return (AuxData.VertexData) null;
    }

    public static AuxData.VertexData BuildVertexData(byte[] vertexBytes)
    {
      if (vertexBytes == null)
        return (AuxData.VertexData) null;
      if (vertexBytes.Length == 0)
        return (AuxData.VertexData) null;
      int intValueCount = AuxData.VertexData.GetIntValueCount(vertexBytes);
      int floatValueCount = AuxData.VertexData.GetFloatValueCount(vertexBytes);
      int byteValueCount = AuxData.VertexData.GetByteValueCount(vertexBytes);
      if (intValueCount <= 0 || floatValueCount < 0 || byteValueCount < 0)
        return (AuxData.VertexData) null;
      if (vertexBytes.Length != intValueCount * 4 + floatValueCount * 4 + byteValueCount)
        return (AuxData.VertexData) null;
      AuxData.VertexData vertexData = new AuxData.VertexData();
      int srcOffset1 = 0;
      vertexData.intValues = new int[intValueCount];
      Buffer.BlockCopy((Array) vertexBytes, srcOffset1, (Array) vertexData.intValues, 0, intValueCount * 4);
      int srcOffset2 = srcOffset1 + intValueCount * 4;
      if (floatValueCount > 0)
      {
        vertexData.floatValues = new float[floatValueCount];
        Buffer.BlockCopy((Array) vertexBytes, srcOffset2, (Array) vertexData.floatValues, 0, floatValueCount * 4);
        srcOffset2 += floatValueCount * 4;
      }
      if (byteValueCount > 0)
      {
        vertexData.byteValues = new byte[byteValueCount];
        Buffer.BlockCopy((Array) vertexBytes, srcOffset2, (Array) vertexData.byteValues, 0, byteValueCount);
      }
      vertexData.PostfixBuild();
      return vertexData;
    }

    public static bool ValidateVertexData(
      AuxData.VertexData vertexData,
      SkinnedMeshRenderer[] skinnedMeshRenderers)
    {
      if (vertexData == null || skinnedMeshRenderers == null || vertexData.meshCount != skinnedMeshRenderers.Length)
        return false;
      int num = 0;
      foreach (SkinnedMeshRenderer skinnedMeshRenderer in skinnedMeshRenderers)
      {
        if ((UnityEngine.Object) skinnedMeshRenderer.sharedMesh != (UnityEngine.Object) null)
          num += skinnedMeshRenderer.sharedMesh.vertexCount;
      }
      return vertexData.meshVertexCount == num;
    }

    public class IndexData
    {
      public const int VertexIndexBitMask = 16777215;
      public const int MeshCountBitShift = 24;
      public const int HeaderSize = 2;
      public int[] indexValues;

      public int vertexCount => this.indexValues != null && this.indexValues.Length != 0 ? this.indexValues[0] & 16777215 : 0;

      public int meshCount => this.indexValues != null && 1 < this.indexValues.Length ? (int) ((uint) this.indexValues[1] >> 24) : 0;

      public int meshVertexCount => this.indexValues != null && 1 < this.indexValues.Length ? this.indexValues[1] & 16777215 : 0;
    }

    public class VertexData
    {
      public const int VertexIndexBitMask = 16777215;
      public const int MeshCountBitShift = 24;
      public const int MeshBoneOffsetBitMask = 16777215;
      public const int MeshVertexOffsetBitMask = 16777215;
      public const int HeaderSize = 5;
      public const int FloatHeaderSize = 2;
      public int[] intValues;
      public float[] floatValues;
      public byte[] byteValues;
      private int _meshCount;
      private int _meshBoneIDOffset;

      public static int GetIntValueCount(byte[] bytes) => 5 + AuxData.ReadInt(bytes, 2);

      public static int GetFloatValueCount(byte[] bytes) => AuxData.ReadInt(bytes, 3);

      public static int GetByteValueCount(byte[] bytes) => AuxData.ReadInt(bytes, 4);

      public void PostfixBuild()
      {
        if (this.intValues != null && 1 < this.intValues.Length)
          this._meshCount = (int) ((uint) this.intValues[1] >> 24);
        this._meshBoneIDOffset = 5 + (this._meshCount + 1 << 1);
      }

      public int vertexCount => this.intValues != null && this.intValues.Length != 0 ? this.intValues[0] & 16777215 : 0;

      public int meshCount => this._meshCount;

      public int meshVertexCount => this.intValues != null && 1 < this.intValues.Length ? this.intValues[1] & 16777215 : 0;

      public void GetMeshBoneInfo(ref AuxData.VertexData.MeshBoneInfo r, int meshIndex)
      {
        int meshCount = this.meshCount;
        if ((uint) meshIndex >= (uint) meshCount)
          return;
        int index = 5 + meshIndex;
        if (this.intValues == null || index + 1 >= this.intValues.Length)
          return;
        r.isXDEF = (uint) (this.intValues[index] & -1073741824) > 0U;
        r.isSDEF = (uint) (this.intValues[index] & int.MinValue) > 0U;
        r.isQDEF = (uint) (this.intValues[index] & 1073741824) > 0U;
        r.index = this.intValues[index] & 16777215;
        r.count = (this.intValues[index + 1] & 16777215) - r.index;
      }

      public void GetMeshVertexInfo(ref AuxData.VertexData.MeshVertexInfo r, int meshIndex)
      {
        int meshCount = this.meshCount;
        if ((uint) meshIndex >= (uint) meshCount)
          return;
        int index = 5 + meshCount + 1 + meshIndex;
        if (this.intValues == null || index + 1 >= this.intValues.Length)
          return;
        r.index = this.intValues[index] & 16777215;
        r.count = (this.intValues[index + 1] & 16777215) - r.index;
      }

      public bool PrecheckMeshBoneInfo(ref AuxData.VertexData.MeshBoneInfo meshBoneInfo)
      {
        int meshBoneIdOffset = this._meshBoneIDOffset;
        return this.byteValues != null && (long) (uint) (meshBoneInfo.index + meshBoneInfo.count) <= (long) this.byteValues.Length && this.intValues != null && (long) (uint) (meshBoneIdOffset + meshBoneInfo.index + meshBoneInfo.count) <= (long) this.intValues.Length;
      }

      public AuxData.VertexData.BoneFlags GetBoneFlags(
        ref AuxData.VertexData.MeshBoneInfo meshBoneInfo,
        int boneIndex)
      {
        return (AuxData.VertexData.BoneFlags) this.byteValues[meshBoneInfo.index + boneIndex];
      }

      public int GetBoneID(ref AuxData.VertexData.MeshBoneInfo meshBoneInfo, int boneIndex) => this.intValues[this._meshBoneIDOffset + meshBoneInfo.index + boneIndex];

      public bool PrecheckMeshVertexInfo(
        ref AuxData.VertexData.MeshVertexInfo meshVertexInfo)
      {
        return this.byteValues != null && (long) (uint) (meshVertexInfo.index + meshVertexInfo.count) <= (long) this.byteValues.Length;
      }

      public AuxData.VertexData.VertexFlags GetVertexFlags(
        ref AuxData.VertexData.MeshVertexInfo meshVertexInfo,
        int vertexIndex)
      {
        return (AuxData.VertexData.VertexFlags) this.byteValues[meshVertexInfo.index + vertexIndex];
      }

      public float vertexScale => this.floatValues != null && this.floatValues.Length != 0 ? this.floatValues[0] : 0.0f;

      public float importScale => this.floatValues != null && 1 < this.floatValues.Length ? this.floatValues[1] : 0.0f;

      public bool PrecheckGetSDEFParams(int sdefIndex, int sdefCount) => (uint) (2 + (sdefIndex + sdefCount) * 9) <= (uint) this.floatValues.Length;

      public int SDEFIndexToSDEFOffset(int sdefIndex) => 2 + sdefIndex * 9;

      public struct MeshBoneInfo
      {
        public bool isSDEF;
        public bool isQDEF;
        public bool isXDEF;
        public int index;
        public int count;
      }

      public struct MeshVertexInfo
      {
        public int index;
        public int count;
      }

      [Flags]
      public enum MeshFlags
      {
        None = 0,
        SDEF = -2147483648, // 0x80000000
        QDEF = 1073741824, // 0x40000000
        XDEF = QDEF | SDEF, // 0xC0000000
      }

      [Flags]
      public enum BoneFlags
      {
        None = 0,
        SDEF = 1,
        QDEF = 2,
        XDEF = QDEF | SDEF, // 0x00000003
        OptimizeBindPoses = 4,
      }

      [Flags]
      public enum VertexFlags
      {
        None = 0,
        SDEF = 1,
        QDEF = 2,
        XDEF = QDEF | SDEF, // 0x00000003
        SDEFSwapIndex = 128, // 0x00000080
      }
    }
  }
}
