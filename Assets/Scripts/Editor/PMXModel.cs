﻿using MMD4MecanimInternal;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using UnityEditor;
using UnityEngine;

using static MMD4MecanimImporterImpl;
using Object = UnityEngine.Object;

public class PMXModel
{
    [NonSerialized] public PMX2FBXProperty pmx2fbxProperty;
    [NonSerialized] public PMX2FBXConfig pmx2fbxConfig;
    [NonSerialized] public MMDModel mmdModel;
    [NonSerialized] public DateTime mmdModelLastWriteTime;
    [NonSerialized] public TextAsset indexAsset;
    [NonSerialized] public AuxData.IndexData indexData;
    [NonSerialized] public TextAsset vertexAsset;
    [NonSerialized] public AuxData.VertexData vertexData;

    public GameObject fbxAsset;
    public string fbxAssetPath;

    public bool _initializeMaterialAtLeastOnce;
    public bool _initializeMaterialAfterPMX2FBX;

    public static readonly string ScriptExtension = ".MMD4Mecanim.asset";
    public const string filePath = "Assets/PMXModel/莫娜1.0";

    public static volatile int _pmx2fbxProcessingCount;
    private volatile System.Diagnostics.Process _pmx2fbxProcess;

    public static string GetEditorPath() => Path.Combine(GetBasePath(), "Editor");

    public static string GetBasePath()
    {
        Shader shader = Shader.Find("MMD4Mecanim/MMDLit");
        return shader != null
            ? Path.GetDirectoryName(Path.GetDirectoryName(AssetDatabase.GetAssetPath(shader)))
            : Path.Combine("Assets", "MMD4Mecanim");
    }

    public float fbxAssertImportScale
    {
        get
        {
            if (!string.IsNullOrEmpty(this.fbxAssetPath))
            {
                ModelImporter atPath = (ModelImporter)AssetImporter.GetAtPath(this.fbxAssetPath);
                if (atPath != null)
                    return MMD4MecanimEditorCommon.GetModelImportScale(atPath);
                Debug.LogWarning((object)("ModelImporter is not found. " + this.fbxAssetPath));
            }

            return 0.0f;
        }
    }

    #region Func
    public bool Setup()
    {
        string withoutExtension = this._GetScriptAssetPathWithoutExtension();
        if (string.IsNullOrEmpty(withoutExtension))
            return false;
        if (this.pmx2fbxProperty == null)
            this.pmx2fbxProperty = new PMX2FBXProperty();
        if (this.pmx2fbxConfig == null)
        {
            this.pmx2fbxConfig =
                GetPMX2FBXConfig(
                    (withoutExtension + ".MMD4Mecanim.xml").Normalize(NormalizationForm.FormC));
            if (this.pmx2fbxConfig == null)
            {
                this.pmx2fbxConfig =
                    GetPMX2FBXConfig(GetPMX2FBXRootConfigPath());
                if (this.pmx2fbxConfig == null)
                    this.pmx2fbxConfig = new PMX2FBXConfig();
                else
                    this.pmx2fbxConfig.renameList = null;
            }

            if (this.pmx2fbxConfig != null && this.pmx2fbxConfig.mmd4MecanimProperty != null)
            {
                PMX2FBXConfig.MMD4MecanimProperty mmd4MecanimProperty =
                    this.pmx2fbxConfig.mmd4MecanimProperty;
                if (this.pmx2fbxProperty.pmxAsset == null)
                {
                    string pmxAssetPath = mmd4MecanimProperty.pmxAssetPath;
                    if (File.Exists(pmxAssetPath))
                        this.pmx2fbxProperty.pmxAsset =
                            AssetDatabase.LoadAssetAtPath(pmxAssetPath, typeof(UnityEngine.Object));
                }

                this.pmx2fbxProperty.vmdAssetList = new List<UnityEngine.Object>();
                if (mmd4MecanimProperty.vmdAssetPathList != null)
                {
                    for (int index = 0; index < mmd4MecanimProperty.vmdAssetPathList.Count; ++index)
                    {
                        Object @object =
                            AssetDatabase.LoadAssetAtPath(mmd4MecanimProperty.vmdAssetPathList[index],
                                typeof(Object));
                        if (@object != null)
                            this.pmx2fbxProperty.vmdAssetList.Add(@object);
                    }
                }

                if (this.fbxAsset == null)
                {
                    string fbxAssetPath = mmd4MecanimProperty.fbxAssetPath;
                    if (!string.IsNullOrEmpty(fbxAssetPath) && File.Exists(fbxAssetPath))
                    {
                        this.fbxAsset = AssetDatabase.LoadAssetAtPath(fbxAssetPath, typeof(GameObject)) as GameObject;
                        this.fbxAssetPath = fbxAssetPath;
                    }

                    if (fbxAsset == null)
                    {
                        string fbxOutputPath = mmd4MecanimProperty.fbxOutputPath;
                        if (!string.IsNullOrEmpty(fbxOutputPath) && File.Exists(fbxOutputPath))
                        {
                            this.fbxAsset =
                                AssetDatabase.LoadAssetAtPath(fbxOutputPath, typeof(GameObject)) as GameObject;
                            this.fbxAssetPath = fbxOutputPath;
                        }
                    }
                }
            }
        }

        if (this.pmx2fbxConfig == null)
            this.pmx2fbxConfig = new PMX2FBXConfig();
        if (this.pmx2fbxConfig.globalSettings == null)
            this.pmx2fbxConfig.globalSettings = new PMX2FBXConfig.GlobalSettings();
        if (this.pmx2fbxConfig.bulletPhysics == null)
            this.pmx2fbxConfig.bulletPhysics = new PMX2FBXConfig.BulletPhysics();
        if (this.pmx2fbxConfig.renameList == null)
            this.pmx2fbxConfig.renameList = new List<PMX2FBXConfig.Rename>();
        if (this.pmx2fbxConfig.freezeRigidBodyList == null)
            this.pmx2fbxConfig.freezeRigidBodyList = new List<PMX2FBXConfig.FreezeRigidBody>();
        if (this.pmx2fbxConfig.freezeMotionList == null)
            this.pmx2fbxConfig.freezeMotionList = new List<PMX2FBXConfig.FreezeMotion>();
        if (this.pmx2fbxConfig.mmd4MecanimProperty == null)
            this.pmx2fbxConfig.mmd4MecanimProperty = new PMX2FBXConfig.MMD4MecanimProperty();
        if (string.IsNullOrEmpty(this.pmx2fbxConfig.mmd4MecanimProperty.fbxOutputPath))
            this.pmx2fbxConfig.mmd4MecanimProperty.fbxOutputPath =
                (withoutExtension + ".fbx").Normalize(NormalizationForm.FormC);
        if (this.pmx2fbxProperty.pmxAsset == null)
        {
            string str = withoutExtension + ".pmx";
            if (File.Exists(str))
                this.pmx2fbxProperty.pmxAsset = AssetDatabase.LoadAssetAtPath(str, typeof(Object));
        }

        if (this.pmx2fbxProperty.pmxAsset == null)
        {
            string str = withoutExtension + ".pmd";
            if (File.Exists(str))
                this.pmx2fbxProperty.pmxAsset = AssetDatabase.LoadAssetAtPath(str, typeof(Object));
        }

        if (fbxAsset == null)
        {
            string str = withoutExtension + ".fbx";
            if (!string.IsNullOrEmpty(str) && File.Exists(str))
            {
                this.fbxAsset = AssetDatabase.LoadAssetAtPath(str, typeof(GameObject)) as GameObject;
                this.fbxAssetPath = str;
            }
        }

        if (this.mmdModel == null && fbxAsset != null)
        {
            string mmdModelPath = GetMMDModelPath(this.fbxAssetPath);
            if (!string.IsNullOrEmpty(mmdModelPath) && File.Exists(mmdModelPath))
            {
                this.mmdModel = GetMMDModel(mmdModelPath);
                this.mmdModelLastWriteTime = MMD4MecanimEditorCommon.GetLastWriteTime(mmdModelPath);
            }
        }

        return true;
    }

    public void ProcessPMX2FBX()
    {
        if (pmx2fbxConfig == null || pmx2fbxConfig.mmd4MecanimProperty == null ||
            pmx2fbxProperty == null)
        {
            Debug.LogError("");
        }
        else
        {
            bool useWineFlag = false;/*this.pmx2fbxConfig.mmd4MecanimProperty.useWineFlag;*/
            string pmX2FbxPath = GetPMX2FBXPath(useWineFlag,
                this.pmx2fbxConfig.mmd4MecanimProperty.bulletPhysicsVersion);
            if (pmX2FbxPath == null)
                Debug.LogError((object)"");
            else if (this._pmx2fbxProcess != null || _pmx2fbxProcessingCount > 0)
            {
                Debug.LogWarning((object)"Already processing pmx2fbx. Please wait.");
            }
            else
            {
                string withoutExtension = this._GetScriptAssetPathWithoutExtension();
                if (string.IsNullOrEmpty(withoutExtension))
                {
                    Debug.LogWarning((object)"Not found script.");
                }
                else
                {
                    string str = (withoutExtension + ".MMD4Mecanim.xml").Normalize(NormalizationForm.FormC);
                    if (string.IsNullOrEmpty(this.pmx2fbxConfig.mmd4MecanimProperty.pmxAssetPath))
                    {
                        Debug.LogError((object)"PMX/PMD Path is null.");
                    }
                    else
                    {
                        string path = Application.dataPath;
                        if (path.EndsWith("Assets"))
                            path = Path.GetDirectoryName(path);
                        StringBuilder stringBuilder = new StringBuilder();
                        if (Application.platform != RuntimePlatform.WindowsEditor && useWineFlag)
                        {
                            stringBuilder.Append("\"");
                            stringBuilder.Append(path + "/" + pmX2FbxPath);
                            stringBuilder.Append("\" ");
                        }

                        stringBuilder.Append("-o \"");
                        stringBuilder.Append(path + "/" + this.pmx2fbxConfig.mmd4MecanimProperty.fbxOutputPath);
                        stringBuilder.Append("\" -conf \"");
                        stringBuilder.Append(path + "/" + str);
                        stringBuilder.Append("\" \"");
                        stringBuilder.Append(path + "/" + this.pmx2fbxConfig.mmd4MecanimProperty.pmxAssetPath);
                        stringBuilder.Append("\"");
                        if (this.pmx2fbxConfig.mmd4MecanimProperty.vmdAssetPathList != null)
                        {
                            foreach (string vmdAssetPath in this.pmx2fbxConfig.mmd4MecanimProperty.vmdAssetPathList)
                            {
                                stringBuilder.Append(" \"");
                                stringBuilder.Append(path + "/" + vmdAssetPath);
                                stringBuilder.Append("\"");
                            }
                        }

                        this._pmx2fbxProcess = new System.Diagnostics.Process();
                        ++_pmx2fbxProcessingCount;
                        if (Application.platform == RuntimePlatform.WindowsEditor || !useWineFlag)
                        {
                            this._pmx2fbxProcess.StartInfo.FileName = path + "/" + pmX2FbxPath;
                        }
                        //else
                        //{
                        //    string winePath =
                        //        WinePaths[(int)this.pmx2fbxConfig.mmd4MecanimProperty.wine];
                        //    if (this.pmx2fbxConfig.mmd4MecanimProperty.wine ==
                        //        MMD4MecanimImporterImpl.PMX2FBXConfig.Wine.Manual)
                        //        winePath = this.pmx2fbxConfig.mmd4MecanimProperty.winePath;
                        //    this._pmx2fbxProcess.StartInfo.FileName = winePath;
                        //}

                        this._pmx2fbxProcess.StartInfo.Arguments = stringBuilder.ToString();
                        Debug.Log("-----Arg-----");
                        Debug.Log(stringBuilder.ToString());
                        this._pmx2fbxProcess.EnableRaisingEvents = true;
                        this._pmx2fbxProcess.Exited += new EventHandler(this._pmx2fbx_OnExited);
                        if (this._pmx2fbxProcess.Start())
                            return;
                        this._pmx2fbxProcess.Dispose();
                        this._pmx2fbxProcess = null;
                    }
                }
            }
        }
    }

    private void _pmx2fbx_OnExited(object sender, EventArgs e)
    {
        Debug.Log((object)"Processed pmx2fbx.");
        this._pmx2fbxProcess.Dispose();
        this._pmx2fbxProcess = null;
        if (--_pmx2fbxProcessingCount < 0)
            _pmx2fbxProcessingCount = 0;
        MMD4MecanimImporterImplEditor._isProcessedPMX2FBX = true;


        //Change Material
        _initializeMaterialAtLeastOnce = true;
    }

    public static PMX2FBXConfig GetPMX2FBXConfig(
    string xmlAssetPath)
    {
        if (string.IsNullOrEmpty(xmlAssetPath))
            return null;
        if (!File.Exists(xmlAssetPath))
            return null;
        try
        {
            using (FileStream fileStream =
                   new FileStream(xmlAssetPath, FileMode.Open, FileAccess.Read, FileShare.Read))
                return (PMX2FBXConfig)new XmlSerializer(
                    typeof(PMX2FBXConfig)).Deserialize(fileStream);
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static string GetPMX2FBXPath(
    bool useWineFlag,
    PMX2FBXConfig.BulletPhysicsVersion bulletPhysicsVersion)
    {
        string path = Path.Combine(Path.Combine(GetEditorPath(), "PMX2FBX"), "pmx2fbx");
        if (bulletPhysicsVersion != PMX2FBXConfig.BulletPhysicsVersion.Latest)
            path += bulletPhysicsVersion.ToString();
        if (Application.platform == RuntimePlatform.WindowsEditor | useWineFlag)
            path += ".exe";
        return File.Exists(path) ? path : null;
    }

    public void SavePMX2FBXConfig()
    {
        if (this.pmx2fbxConfig == null || this.pmx2fbxProperty == null)
        {
            Debug.LogError("");
        }
        else
        {
            //string withoutExtension = this._GetScriptAssetPathWithoutExtension();
            //if (string.IsNullOrEmpty(withoutExtension))
            //{
            //    Debug.LogWarning("Not found script.");
            //}
            //else
            {
                string str = filePath + ".MMD4Mecanim.xml";
                //string str = (withoutExtension + ".MMD4Mecanim.xml").Normalize(NormalizationForm.FormC);
                if (this.pmx2fbxConfig.mmd4MecanimProperty != null)
                {
                    this.pmx2fbxConfig.mmd4MecanimProperty.pmxAssetPath =
                        !(this.pmx2fbxProperty.pmxAsset != null)
                            ? (string)null
                            : AssetDatabase.GetAssetPath(this.pmx2fbxProperty.pmxAsset);
                    if (this.pmx2fbxProperty.vmdAssetList != null)
                    {
                        this.pmx2fbxConfig.mmd4MecanimProperty.vmdAssetPathList = new List<string>();
                        foreach (UnityEngine.Object vmdAsset in this.pmx2fbxProperty.vmdAssetList)
                            this.pmx2fbxConfig.mmd4MecanimProperty.vmdAssetPathList.Add(
                                AssetDatabase.GetAssetPath(vmdAsset));
                    }
                    else
                        this.pmx2fbxConfig.mmd4MecanimProperty.vmdAssetPathList = null;

                    this.pmx2fbxConfig.mmd4MecanimProperty.fbxAssetPath =
                        !(fbxAsset != null)
                            ? null
                            : AssetDatabase.GetAssetPath(fbxAsset);
                }

                WritePMX2FBXConfig(str, this.pmx2fbxConfig);
                AssetDatabase.ImportAsset(str,
                    ImportAssetOptions.ForceUpdate | ImportAssetOptions.ForceSynchronousImport);
                AssetDatabase.Refresh();
            }
        }
    }

    private string _GetScriptAssetPath() => "Assets/PMXModel/莫娜1.0.MMD4Mecanim.asset";

    private string _GetScriptAssetPathWithoutExtension() =>
    MMD4MecanimEditorCommon.GetPathWithoutExtension(_GetScriptAssetPath(),
        ScriptExtension.Length);

    public static bool WritePMX2FBXConfig(string xmlAssetPath, PMX2FBXConfig pmx2fbxConfig)
    {
        if (string.IsNullOrEmpty(xmlAssetPath))
        {
            Debug.LogWarning("xmlAssetPath is null.");
            return false;
        }

        XmlWriterSettings settings = new XmlWriterSettings()
        {
            Indent = true,
            OmitXmlDeclaration = false,
            Encoding = Encoding.UTF8
        };

        XmlSerializer xmlSerializer = new XmlSerializer(typeof(PMX2FBXConfig));

        try
        {
            using (FileStream output = new FileStream(xmlAssetPath, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(output, settings))
                    xmlSerializer.Serialize(xmlWriter, pmx2fbxConfig);
            }

            return true;
        }
        catch (Exception ex)
        {
            Debug.LogError("");
            return false;
        }
    }
    #endregion

    #region Material Func
    public void _CheckFBXMaterial()
    {
        Debug.Log($"fbxAsset {fbxAsset}");
        Debug.Log($"mmdModel {mmdModel}");
        Debug.Log($"pmx2fbxConfig {pmx2fbxConfig}");

        if (fbxAsset == null || this.mmdModel == null ||
            this.pmx2fbxConfig == null)
            return;
        MeshRenderer[] meshRenderers = MMD4MecanimCommon.GetMeshRenderers(this.fbxAsset.gameObject);
        SkinnedMeshRenderer[] skinnedMeshRenderers =
            MMD4MecanimCommon.GetSkinnedMeshRenderers(this.fbxAsset.gameObject);
        this._CheckFBXMaterialMaterial(meshRenderers, skinnedMeshRenderers);
        if ((0 | (this._CheckFBXMaterialIndex(skinnedMeshRenderers) ? 1 : 0) |
             (this._CheckFBXMaterialVertex(skinnedMeshRenderers) ? 1 : 0)) == 0)
            return;
        AssetDatabase.Refresh();
    }

    public void _CheckFBXMaterialVertex()
    {
        if (fbxAsset == null || mmdModel == null ||
            pmx2fbxConfig == null)
            return;
        _CheckFBXMaterialVertex(MMD4MecanimCommon.GetSkinnedMeshRenderers(fbxAsset.gameObject));
    }

    private bool _CheckFBXMaterialVertex(SkinnedMeshRenderer[] skinnedMeshRenderers)
    {
        if (this.fbxAsset == null || this.mmdModel == null ||
            this.pmx2fbxConfig == null || skinnedMeshRenderers == null || skinnedMeshRenderers.Length == 0)
            return false;
        bool flag = false;
        if (this.vertexData != null)
        {
            if (!AuxData.ValidateVertexData(this.vertexData, skinnedMeshRenderers))
            {
                this.vertexData = (AuxData.VertexData)null;
                flag = true;
            }
        }
        else
            flag = true;

        if (!flag && this.mmdModel != null && this.mmdModel.globalSettings != null)
        {
            if (this.vertexData != null)
            {
                float vertexScale = this.mmdModel.globalSettings.vertexScale;
                float num = this.fbxAssertImportScale;
                if ((double)num == 0.0)
                    num = this.mmdModel.globalSettings.importScale;
                float f1 = num - this.vertexData.importScale;
                float f2 = vertexScale - this.vertexData.vertexScale;
                if ((double)Mathf.Abs(f1) > (double)Mathf.Epsilon || (double)Mathf.Abs(f2) > (double)Mathf.Epsilon)
                {
                    MMD4MecanimData.ExtraData extraData = MMD4MecanimData.BuildExtraData(
                        AssetDatabase.LoadAssetAtPath(
                            GetExtraDataPath(
                                AssetDatabase.GetAssetPath((Object)this.fbxAsset)),
                            typeof(TextAsset)) as TextAsset);
                    if (extraData != null)
                    {
                        float f3 = extraData.vertexScale - this.vertexData.vertexScale;
                        if ((double)Mathf.Abs(f1) > (double)Mathf.Epsilon ||
                            (double)Mathf.Abs(f3) > (double)Mathf.Epsilon)
                            flag = true;
                    }
                }
            }
            else
                flag = true;
        }

        if (flag)
        {
            string extraDataPath =
                GetExtraDataPath(
                    AssetDatabase.GetAssetPath((Object)this.fbxAsset));
            if (!File.Exists(extraDataPath))
                flag = false;
            else if (AssetDatabase.LoadAssetAtPath(extraDataPath, typeof(TextAsset)) == (Object)null)
                AssetDatabase.ImportAsset(extraDataPath,
                    ImportAssetOptions.ForceUpdate | ImportAssetOptions.ForceSynchronousImport);
        }

        if (flag)
        {
            Debug.Log((object)("MMD4Mecanim:Initialize FBX Vertex:" + this.fbxAsset.name));
            _ProcessVertex(this.fbxAsset, this.fbxAssertImportScale, this.mmdModel);
            this.vertexAsset =
                AssetDatabase.LoadAssetAtPath(GetVertexDataPath(this.fbxAssetPath),
                    typeof(TextAsset)) as TextAsset;
            if ((Object)this.vertexAsset != (Object)null)
                this.vertexData = AuxData.BuildVertexData(this.vertexAsset);
        }

        return flag;
    }

    private bool _CheckFBXMaterialMaterial(
    MeshRenderer[] meshRenderers,
    SkinnedMeshRenderer[] skinnedMeshRenderers)
    {
        if (fbxAsset == null || fbxAssetPath == null ||
            mmdModel == null || pmx2fbxConfig == null)
            return false;
        bool flag = false;
        if (!this._initializeMaterialAtLeastOnce || this._initializeMaterialAfterPMX2FBX)
        {
            if (this._initializeMaterialAfterPMX2FBX)
            {
                this._initializeMaterialAfterPMX2FBX = false;
                if (!File.Exists(GetIndexDataPath(this.fbxAssetPath)))
                    flag = true;
            }

            if (!flag)
            {
                MaterialState materialState = MaterialState.Nothing;
                if (meshRenderers != null)
                {
                    foreach (MeshRenderer meshRenderer in meshRenderers)
                        materialState |= _CheckMaterialStates(meshRenderer.sharedMaterials);
                }

                if ((materialState == MaterialState.Nothing ||
                     materialState == MaterialState.Normally) && skinnedMeshRenderers != null)
                {
                    foreach (SkinnedMeshRenderer skinnedMeshRenderer in skinnedMeshRenderers)
                        materialState |=
                            _CheckMaterialStates(skinnedMeshRenderer.sharedMaterials);
                }

                flag = materialState != MaterialState.Nothing &&
                       materialState != MaterialState.Normally;
            }
        }

        if (flag)
        {
            Debug.Log((object)("MMD4Mecanim:Initialize FBX Material:" + this.fbxAsset.name));
            this._initializeMaterialAtLeastOnce = true;
            _ProcessMaterial(this.fbxAsset, this.mmdModel, this.pmx2fbxConfig, false);
        }

        return flag;
    }

    private static MaterialState _CheckMaterialState(
    Material material)
    {
        MaterialState materialState = MaterialState.Nothing;
        if (material != null)
        {
            if (material.shader != null && material.shader.name != null &&
                material.shader.name.StartsWith("MMD4Mecanim"))
            {
                if (material.HasProperty("_Revision"))
                {
                    if (material.GetFloat("_Revision") < 1.5f)
                        materialState |= MaterialState.Previous;
                }
                else
                    materialState |= MaterialState.Previous;
            }
            else if (material.mainTexture != null)
                materialState |= MaterialState.Normally;
            else
                materialState |= MaterialState.Uninitialized;
        }

        return materialState;
    }

    private static MaterialState _CheckMaterialStates(
        Material[] materials)
    {
        MaterialState materialState = MaterialState.Nothing;
        if (materials != null)
        {
            foreach (Material material in materials)
                materialState |= _CheckMaterialState(material);
        }

        return materialState;
    }

    private bool _CheckFBXMaterialIndex(SkinnedMeshRenderer[] skinnedMeshRenderers)
    {
        if ((Object)this.fbxAsset == (Object)null || this.mmdModel == null ||
            this.pmx2fbxConfig == null || skinnedMeshRenderers == null || skinnedMeshRenderers.Length == 0)
            return false;
        bool flag = false;
        if (this.indexData != null)
        {
            if (!AuxData.ValidateIndexData(this.indexData, skinnedMeshRenderers))
            {
                this.indexData = (AuxData.IndexData)null;
                flag = true;
            }
        }
        else
            flag = true;

        if (flag)
        {
            Debug.Log((object)("MMD4Mecanim:Initialize FBX Index:" + this.fbxAsset.name));
            _ProcessIndex(this.fbxAsset, this.mmdModel);
            this.indexAsset =
                AssetDatabase.LoadAssetAtPath(GetIndexDataPath(this.fbxAssetPath),
                    typeof(TextAsset)) as TextAsset;
            if (indexAsset != null)
                this.indexData = AuxData.BuildIndexData(this.indexAsset);
        }

        return flag;
    }

    private static void _ProcessMaterial(
        GameObject fbxAsset,
        MMDModel mmdModel,
        PMX2FBXConfig pmx2fbxConfig,
        bool overwriteMaterials)
    {
        if (fbxAsset == null)
            Debug.LogError("FBXAsset is null.");
        else if (mmdModel == null)
            Debug.LogError("MMDModel is null.");
        else if (pmx2fbxConfig == null)
            Debug.LogError("pmx2fbxConfig is null.");
        else
            new MaterialBuilder()
            {
                fbxAsset = fbxAsset,
                mmdModel = mmdModel,
                mmd4MecanimProperty = pmx2fbxConfig.mmd4MecanimProperty
            }.Build(overwriteMaterials);
    }

    private static void _ProcessIndex(GameObject fbxAsset, MMDModel mmdModel)
    {
        if (fbxAsset == null)
            Debug.LogError("FBXAsset is null.");
        else if (mmdModel == null)
            Debug.LogError("MMDModel is null.");
        else
            new IndexBuilder()
            {
                fbxAsset = fbxAsset,
                mmdModel = mmdModel
            }.Build();
    }

    private static void _ProcessVertex(
    GameObject fbxAsset,
    float importScale,
    MMDModel mmdModel)
    {
        if (fbxAsset == null)
            Debug.LogError("FBXAsset is null.");
        else if (mmdModel == null)
            Debug.LogError("MMDModel is null.");
        else
            new VertexBuilder()
            {
                fbxAsset = fbxAsset,
                importScale = importScale,
                mmdModel = mmdModel
            }.Build();
    }
    #endregion
}
