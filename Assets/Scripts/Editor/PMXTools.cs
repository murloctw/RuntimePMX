﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

#region Tools
public class RichLog
{
    public static void Log(object msg, Color color)
    {
        string hex = ColorUtility.ToHtmlStringRGB(color);
        Debug.Log($"<color=#{hex}>{msg}</color>");
    }
}

public class PathTools
{
    public static string[] SearchPathsWithExtension(string path, params string[] extensions)
    {
        return Directory.GetFiles(path, "*.*")
           .Where(f => extensions.Contains(f.Split('.').Last().ToLower())).ToArray();
    }
}
#endregion

public class PMXTools : MonoBehaviour
{
    private const string filePath = "Assets/PMXModel/莫娜1.0";

    [MenuItem("Tools/StartConvert")]
    public static void StartConvertPMX()
    {
        SourceConvert(filePath);
        //string path = filePath + ".";
        //MMD4MecanimImporter importer = new MMD4MecanimImporter();
        //importer.ScriptAssetPath = path;
        //importer._forceProcessConvertMaterial = true;
        //importer.Setup();
        //importer.SavePMX2FBXConfig();
        //importer.ProcessPMX2FBX();
    }

    [MenuItem("Tools/RemoveFiles")]
    public static void RemoveExportFiles()
    {
        //string rootPath = Path.Combine(Application.dataPath, "PMXModel");
        string rootPath = Directory.GetParent(filePath).FullName;
        string[] results = PathTools.SearchPathsWithExtension(rootPath, new string[]
        {
            "bytes",
            "fbx",
            "xml",
        });

        string matDir = Path.Combine(rootPath, "Materials");

        foreach (string path in results)
            File.Delete(path);

        if (Directory.Exists(matDir))
            Directory.Delete(matDir, true);

        AssetDatabase.Refresh();
        Debug.Log("Remove Complete.");
    }

    /// <summary>
    /// 自訂義轉換
    /// </summary>
    private void CustomConvert()
    {
        PMXModel model = new PMXModel();
        model._initializeMaterialAtLeastOnce = false;
        model._initializeMaterialAfterPMX2FBX = true;
        model.Setup();
        model.SavePMX2FBXConfig();
        model.ProcessPMX2FBX();
    }

    /// <summary>
    /// 原生轉換
    /// </summary>
    private static void SourceConvert(string assetPath)
    {
        assetPath += ".MMD4Mecanim.asset";
        MMD4MecanimImporter importer = new MMD4MecanimImporter();
        importer.ScriptAssetPath = assetPath;
        importer.Setup();
        //importer._forceProcessConvertMaterial = true;
        importer.SavePMX2FBXConfig();
        importer.ProcessPMX2FBX();
    }
}
